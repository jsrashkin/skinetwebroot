jQuery(function( $ ){

	/*$("header .genesis-nav-menu, .nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu").addClass("responsive-menu").before('<div class="responsive-menu-icon"></div>');

	$(".responsive-menu-icon").click(function(){
		$(this).next("header .genesis-nav-menu, .nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu").slideToggle();
	});

	$(window).resize(function(){
		if(window.innerWidth > 767) {
			$("header .genesis-nav-menu, .nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu, nav .sub-menu").removeAttr("style");
			$(".responsive-menu > .menu-item").removeClass("menu-open");
		}
	});*/

	$(".search-header .search-button").click(function(event){
		
		$(".search-header .search-field").toggleClass("open");
	});

	jQuery(window).bind("load", function() {
	   jQuery(".loader").addClass("load-end");
	});


	$(".toogle-btnmore").click(function(){
		$(".hide-block").slideToggle();
		$(this).toggleClass("opn-more");
		if ($('.toogle-btnmore').hasClass('opn-more')){
	        $('.t-mero').html('Minimize');
	        $('.sign').html('-');
	    } else {
	        $('.t-mero').html('Maximize');
	        $('.sign').html('+');
	    }
	});

	var $container = $('#gear-items'),
	filters = {};

	$container.isotope({
		itemSelector : '.gear-element',
		masonry: {
		  columnWidth: 180,
		  gutter: 20
		}
	});

	// filter buttons
	$('.dropdown-filter select').change(function(){
		var $this = $(this);

		// store filter value in object
		// i.e. filters.color = 'red'
		var group = $this.attr('data-filter-group');

		filters[ group ] = $this.find(':selected').attr('data-filter-value');
		// console.log( $this.find(':selected') )
		// convert object into array
		var isoFilters = [];
		for ( var prop in filters ) {
		  	isoFilters.push( 
		  		filters[ prop ]
		  	)
		}
		console.log(filters);
		var selector = isoFilters.join('');
		$container.isotope({ 
			filter: selector 
		});
		return false;
	});



});