<?php
$page = new stdClass;
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'skinet_gear__brands';
$page->task = 'page';
$page->admin_title = 'All Brands';
$page->admin_description = '';
$page->path = 'gear/brands';
$page->access = array(
    'logic' => 'and',
    );
$page->menu = array();
$page->arguments = array();
$page->conf = array();
$page->default_handlers = array();
$handler = new stdClass;
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_skinet_gear__brands_panel_context';
$handler->task = 'page';
$handler->subtask = 'skinet_gear__brands';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'css_id' => 'three-col-page',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'css_cache' => NULL,
    );
$display = new panels_display;
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = 'Gear - All Brands';
$display->content = array();
$display->panels = array();
$pane = new stdClass;
$pane->pid = 'new-1';
$pane->panel = 'middle';
$pane->type = 'views';
$pane->subtype = 'skinet_gear__brands';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array(
    'nodes_per_page' => '0',
    'pager_id' => '1',
    'use_pager' => 0,
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'override_title' => 0,
    'override_title_text' => '',
    );
$pane->cache = array();
$pane->style = array();
$pane->css = array();
$pane->extras = array();
$pane->position = 0;
$display->content['new-1'] = $pane;
$display->panels['middle'][0] = 'new-1';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-1';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;

