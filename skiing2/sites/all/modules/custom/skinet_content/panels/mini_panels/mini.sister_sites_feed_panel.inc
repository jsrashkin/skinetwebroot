<?php
$mini = new stdClass;
$mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
$mini->api_version = 1;
$mini->name = 'sister_site_feeds_panel';
$mini->category = '';
$mini->title = 'From Our Sister Sites';
$mini->requiredcontexts = FALSE;
$mini->contexts = FALSE;
$mini->relationships = FALSE;
$display = new panels_display;
$display->layout = 'onecol';
$display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 1,
          1 => 'main',
          2 => 4,
          3 => 6,
          ),
        'parent' => NULL,
        ),
      'main' => array(
        'type' => 'column',
        'width' => '20.265960782567117',
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
          ),
        'parent' => 'canvas',
        ),
      'main-row' => array(
          'type' => 'row',
          'contains' => 'region',
          'children' => array(
            0 => 'center',
            ),
          'parent' => 'main',
          ),
      'center' => array(
          'type' => 'region',
          'title' => 'left1',
          'width' => 100,
          'width_type' => '%',
          'parent' => 'main-row',
          'class' => '',
          ),
      1 => array(
          'type' => 'column',
          'width' => '19.58038065909087',
          'width_type' => '%',
          'parent' => 'canvas',
          'children' => array(
            0 => 3,
            ),
          ),
      3 => array(
          'type' => 'row',
          'contains' => 'region',
          'children' => array(
            0 => 'left0',
            ),
          'parent' => '1',
          ),
      'left0' => array(
          'type' => 'region',
          'title' => 'left0',
          'width' => 100,
          'width_type' => '%',
          'parent' => '3',
          ),
      4 => array(
          'type' => 'column',
          'width' => '19.32782374783392',
          'width_type' => '%',
          'parent' => 'canvas',
          'children' => array(
            0 => 5,
            ),
          ),
      5 => array(
          'type' => 'row',
          'contains' => 'region',
          'children' => array(
            0 => 'left2',
            ),
          'parent' => '4',
          ),
      'left2' => array(
          'type' => 'region',
          'title' => 'left2',
          'width' => 100,
          'width_type' => '%',
          'parent' => '5',
          'class' => '',
          ),
      6 => array(
          'type' => 'column',
          'width' => '16.69187586226564',
          'width_type' => '%',
          'parent' => 'canvas',
          'children' => array(
            0 => 7,
            ),
          ),
      7 => array(
          'type' => 'row',
          'contains' => 'region',
          'children' => array(
            0 => 'left3',
            ),
          'parent' => '6',
          ),
      'left3' => array(
          'type' => 'region',
          'title' => 'left3',
          'width' => 100,
          'width_type' => '%',
          'parent' => '7',
          'class' => '',
          ),
      ),
      );
$display->panel_settings = array();
$display->cache = array();
$display->title = '';
$display->content = array();
$display->panels = array();
$pane = new stdClass;
$pane->pid = 'new-1';
$pane->panel = 'middle';
$pane->type = 'sister_site_feeds';
$pane->subtype = 'sister_site_feeds';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array();
$pane->cache = array();
$pane->style = array();
$pane->css = array();
$pane->extras = array();
$pane->position = 0;
$display->content['new-1'] = $pane;
$display->panels['middle'][0] = 'new-1';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$mini->display = $display;

