<?php
$presets['medium_thumbnail'] = array (
  'presetname' => 'medium_thumbnail',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_aspect',
      'data' => array (
        'portrait' => 'medium_portrait_thumbnail',
        'landscape' => 'medium_landscape_thumbnail',
      ),
    ),
  ),
);
