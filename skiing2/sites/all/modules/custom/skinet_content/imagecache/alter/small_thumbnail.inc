<?php
$presets['small_thumbnail'] = array (
  'presetname' => 'small_thumbnail',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' => array (
        'width' => '125',
        'height' => '125',
      ),
    ),
  ),
);

