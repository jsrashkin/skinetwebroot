<?php
$presets['article_image'] = array (
  'presetname' => 'article_image',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => array (
        'width' => '250',
        'height' => '',
        'upscale' => 1,
      ),
    ),
  ),
);
