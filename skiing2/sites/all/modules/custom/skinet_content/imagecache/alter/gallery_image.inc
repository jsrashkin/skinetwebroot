<?php
$presets['gallery_image'] = array (
  'presetname' => 'gallery_image',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => array (
        'width' => '594',
        'height' => '600',
        'upscale' => 0,
      ),
    ),
  ),
);
