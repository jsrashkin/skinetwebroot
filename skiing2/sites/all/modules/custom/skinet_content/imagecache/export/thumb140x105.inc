<?php
$presets['thumb140x105'] = array(
  'presetname' => 'thumb140x105',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '140',
        'height' => '105',
        'upscale' => 0,
      ),
    ),
  ),
);