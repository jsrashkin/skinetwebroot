<?php
$presets['medium_portrait_thumbnail'] = array (
  'presetname' => 'medium_portrait_thumbnail',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => array (
        'width' => '150',
        'height' => '',
        'upscale' => 0,
      ),
    ),
    1 => array (
      'weight' => '0',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_definecanvas',
      'data' => array (
        'RGB' => array (
          'HEX' => 'ffffff',
        ),
        'under' => 1,
        'exact' => array (
          'width' => '150',
          'height' => '150',
          'xpos' => 'center',
          'ypos' => 'center',
        ),
        'relative' => array (
          'leftdiff' => '',
          'rightdiff' => '',
          'topdiff' => '',
          'bottomdiff' => '',
        ),
      ),
    ),
    2 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_crop',
      'data' => array (
        'width' => '150',
        'height' => '150',
        'xoffset' => 'center',
        'yoffset' => 'top',
      ),
    ),
  ),
);

