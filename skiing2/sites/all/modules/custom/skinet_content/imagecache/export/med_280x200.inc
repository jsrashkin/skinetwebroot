<?php
$presets['med_280x200'] = array (
  'presetname' => 'med_280x200',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' => 
      array (
        'width' => '280',
        'height' => '200',
      ),
    ),
  ),
);

