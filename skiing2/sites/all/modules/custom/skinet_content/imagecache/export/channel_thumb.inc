<?php
$presets['channel_thumb'] = array(
  'presetname' => 'channel_thumb',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' => array (
        'width' => '210',
        'height' => '210',
      ),
    ),
  ),
);
