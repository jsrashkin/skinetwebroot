<?php
$presets['medium_landscape_thumbnail'] = array (
  'presetname' => 'medium_landscape_thumbnail',
  'actions' => array (
    0 => array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => array (
        'width' => '',
        'height' => '150',
        'upscale' => 0,
      ),
    ),
    1 => array (
      'weight' => '-9',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_definecanvas',
      'data' => array (
        'RGB' => array (
          'HEX' => 'ffffff',
        ),
        'under' => 1,
        'exact' => array (
          'width' => '150',
          'height' => '150',
          'xpos' => 'center',
          'ypos' => 'center',
        ),
        'relative' => array (
          'leftdiff' => '',
          'rightdiff' => '',
          'topdiff' => '',
          'bottomdiff' => '',
        ),
      ),
    ),
    2 => array (
      'weight' => '-8',
      'module' => 'imagecache',
      'action' => 'imagecache_crop',
      'data' => array (
        'width' => '150',
        'height' => '150',
        'xoffset' => 'center',
        'yoffset' => 'top',
      ),
    ),
  ),
);

