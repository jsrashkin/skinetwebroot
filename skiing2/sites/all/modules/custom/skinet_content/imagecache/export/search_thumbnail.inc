<?php
$presets['search_thumbnail'] = array (
  'presetname' => 'search_thumbnail',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_aspect',
      'data' => array (
        'portrait' => '21',
        'landscape' => '22',
      ),
    ),
  ),
);

