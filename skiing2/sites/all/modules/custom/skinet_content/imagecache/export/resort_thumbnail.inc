<?php
$presets['resort_thumbnail'] = array (
  'presetname' => 'resort_thumbnail',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' => array (
        'width' => '250',
        'height' => '175',
      ),
    ),
  ),
);

