<?php
$presets['large_square_landscape_thumbnail'] = array (
  'presetname' => 'large_square_landscape_thumbnail',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => array (
        'width' => '',
        'height' => '175',
        'upscale' => 0,
      ),
    ),
    1 => array (
      'weight' => '0',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_definecanvas',
      'data' => array (
        'RGB' => array (
          'HEX' => 'ffffff',
        ),
        'under' => 1,
        'exact' => array (
          'width' => '175',
          'height' => '175',
          'xpos' => 'center',
          'ypos' => 'center',
        ),
        'relative' => array (
          'leftdiff' => '',
          'rightdiff' => '',
          'topdiff' => '',
          'bottomdiff' => '',
        ),
      ),
    ),
    2 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_crop',
      'data' => array (
        'width' => '175',
        'height' => '175',
        'xoffset' => 'center',
        'yoffset' => 'top',
      ),
    ),
  ),
);

