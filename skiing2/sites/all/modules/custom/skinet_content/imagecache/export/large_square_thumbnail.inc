<?php
$presets['large_square_thumbnail'] = array (
  'presetname' => 'large_square_thumbnail',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_aspect',
      'data' => array (
        'portrait' => 'large_square_portrait_thumbnail',
        'landscape' => 'large_square_landscape_thumbnail',
      ),
    ),
  ),
);
