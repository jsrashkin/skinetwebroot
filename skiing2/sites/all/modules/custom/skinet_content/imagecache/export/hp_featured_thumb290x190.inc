<?php
$presets['hp_featured_thumb290x190'] = array (
  'presetname' => 'hp_featured_thumb290x190',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => array (
        'width' => '290',
        'height' => '190',
        'upscale' => 0,
      ),
    ),
  ),
);
