<?php
/**
 * Move shared Brightcove Playlists functionality to it's own include file
 */
/**
 * Preprocess videos page
 */
function skinet_content_preprocess_brightcove_playlists_page(&$variables) {
  //add playlists block back in
  $playlists_block = module_invoke('brightcove_playlists','block','view','playlists');
  $variables['playlists_block'] = $playlists_block['content'];
  $token = variable_get('brightcove_playlists_token_read', '');
  //check for tags playlist
  $variables['player'] = theme('brightcove_playlists_player', $variables['video'], $variables['options']);
  $variables['title'] = $variables['video']->name;
  if (module_exists('bonnier_leadgen') || module_exists('boatingmag_leadgen')) {
    module_load_include('inc', 'bonnier_leadgen', 'includes/bonnier_leadgen');
    $boat = bonnier_leadgen_get_boat_by_video_id($variables['video']->id);
 /*   if(!count($boat)) {
       $boat = boatingmag_leadgen_get_boat_by_video_id($variables['video']->id);
    }*/
    if (!empty($boat)) {
      $leadgen = theme('bonnier_leadgen_links', $boat['Boat']['attributes']['BoatId'], BONNIER_LEADGEN_LEAD_VIDEO);
      if (empty($leadgen)) {
        drupal_alter('leadgen_links', $leadgen, $boat);
      }
      $variables['leadgen'] = $leadgen;
    }
  }
  if($variables['video']->longDescription != null){
    $variables['body'] = $variables['video']->longDescription;
  } else if($variables['video']->shortDescription != null){
    $variables['body'] = $variables['video']->shortDescription;
  }

  $variables['link'] = l($variables['video']->linkText, $variables['video']->linkURL);
  if (variable_get('brightcove_playlists_player_fivestar', FALSE)) {
    $variables['fivestar'] = brightcove_playlists_fivestar_widget('brightcove_playlists_video', $variables['video']);
  }
  $arguments = array('page_size' => 12, 'video_fields' => array('id', 'name', 'thumbnailURL'));
  drupal_alter('brightcove_playlists_arguments',$arguments);
  $playlist = $variables['playlist'];
  //add the stupid pager
  global $pager_page_array, $pager_total, $pager_total_items;
  $pager_total_items[0] = count($variables['playlist']->videos);
  $pager_total[0] = ceil($pager_total_items[0] / 12);
  $page = isset($_GET['page']) ? $_GET['page'] : 0;
  $pager_page_array = explode(',', $page);
  $pager_page_array[0] = max(0, min((int) $pager_page_array[0], ((int) $pager_total[0]) - 1));
  $videos = array_slice($variables['playlist']->videos, $pager_page_array[0] * 12, 12);
  $variables['playlist']->videos = $videos;
  $variables['pager'] = theme('pager', array(), 12, 0, array(), $pager_total_items[0]);
  $variables['top'] = theme('brightcove_playlists_recent', $token, $arguments, $variables['playlist']);
  $taboola = module_invoke('skinet_content', 'block', 'view', 'taboola_main');
  $variables['taboola'] = $taboola['content'];
}
/**
 * Preprocess video item in playlist
 */
function skinet_content_preprocess_brightcove_playlists_video(&$vars) {
  //set the thumbnail
  $thumb = '<img src="' . $vars['video']->thumbnailURL . '" alt="' . $vars['title'] . '" title="' . $vars['title'] . '" width="190" height="108">';
  $vars['thumbnail'] = l($thumb ,$vars['video']->path, array('html'=>TRUE));
  $vars['title'] = l($vars['title'], $vars['video']->path);
}
/**
 * Preprocess playlists menu
 */
function skinet_content_preprocess_brightcove_playlists_playlists(&$variables) {
  $arg = arg();
  module_load_include('inc', 'brightcove_playlists', 'includes/brightcove_playlists.playlist_read');
  $result = brightcove_playlists_find_all_playlists($variables['token'], $variables['arguments']);
  $variables['brightcove_playlists_result'] = $result;
  $pattern = trim(variable_get('pathauto_brightcove_playlists_playlist_pattern', ''));
  if (!empty($result->items) && !empty($pattern)) {
    _pathauto_include();
  }
  $playlists = array();
  if (!empty($result->items)) {
    $landing = FALSE;
    $active = array('attributes' => array('class' => 'active'));
    if ($arg[0] == 'brightcove_playlists' && !$arg[1] && !$arg[2]) {
      $landing = TRUE;
    }
    $ordered_playlists = _brightcove_playlists_sort($result->items);
    $variables['ordered_playlists'] = $ordered_playlists;
    foreach ($ordered_playlists as $playlist) {
      $playlist_id = $playlist->id;
      $path = 'brightcove_playlists/playlist/' . $playlist_id;
      if (!empty($pattern) && db_result(db_query_range("SELECT pid FROM {url_alias} WHERE src LIKE '%s'", $path, 0, 1)) === FALSE) {
        $placeholders = pathauto_get_placeholders('brightcove_playlists_playlist', $playlist);
        pathauto_create_alias('brightcove_playlists_playlist', 'insert', $placeholders, $path, $playlist_id);
        pathauto_create_alias('brightcove_playlists_playlist_views', 'insert', $placeholders, $path .'/views', $playlist_id);
      }
      if(strpos($playlist->name, 'SKG ') === 0){
        $playlist->name = substr($playlist->name, 4);
      }
      $playlists[] = array(
        'data' => l($playlist->name, $path, (($landing && $playlist->name == 'GEAR') || $playlist_id == $arg[2] ? $active : array())),
      );
    }
  }
  $variables['playlists'] = theme('item_list', $playlists, $variables['title']);
}
/**
 * Preprocess recent videos tabs, I guess
 */
function skinet_content_preprocess_brightcove_playlists_recent(&$variables) {
  module_load_include('inc', 'brightcove_playlists', 'includes/brightcove_playlists.video_read');
  $token = $variables['token'];
  $arguments = $variables['arguments'];
  $arguments['video_fields'][] = 'name';

  $arguments['sort_by'] = 'PUBLISH_DATE';
  $arguments['sort_order'] = 'DESC';
  $arguments['video_fields'][] = 'longDescription';
  $arguments['video_fields'][] = 'shortDescription';
  $result = new stdclass();
  if(isset($variables['playlist']->videos) && count($variables['playlist']->videos > 0)) {
    $result->items = $variables['playlist']->videos;
  }
  else if (!empty($variables['playlist']->filterTags) && $variables['playlist']->playlistType != 'EXPLICIT') {
    $result = brightcove_playlists_find_videos_by_tags($token, $variables['playlist']->filterTags, array(), $arguments);

  }
  else {
    $result = brightcove_playlists_find_all_videos($token, $arguments);
  }
  $variables['result'] = $result;
  if (isset($variables['playlist']->id)) {
    $playlist_pattern = trim(variable_get('pathauto_brightcove_playlists_playlist_pattern', ''));
    $video_pattern = trim(variable_get('pathauto_brightcove_playlists_playlist_video_pattern', ''));
    if (!empty($playlist_pattern) || !empty($video_pattern)) {
      _pathauto_include();
      $path = 'brightcove_playlists/playlist/'. $variables['playlist']->id;
      $placeholders = pathauto_get_placeholders('brightcove_playlists_playlist', $variables['playlist']);
      if (!empty($playlist_pattern) && db_result(db_query_range("SELECT pid FROM {url_alias} WHERE src LIKE '%s'", $path, 0, 1)) === FALSE) {
        pathauto_create_alias('brightcove_playlists_playlist', 'insert', $placeholders, $path, $variables['playlist']->id);
        pathauto_create_alias('brightcove_playlists_playlist_views', 'insert', $placeholders, $path .'/views', $variables['playlist']->id);
      }
    }
  }
  elseif (!empty($result->items)) {
    $video_pattern = trim(variable_get('pathauto_brightcove_playlists_video_pattern', ''));
    if (!empty($video_pattern)) {
      _pathauto_include();
    }
  }
  $items = array();
  //create additioanl vars to allow other hooks to change the data without have to run all the code below
  $videos_data = array();
  $paths = array();
  if (!empty($result->items)) {
    foreach ($result->items as $video) {
      //$title = theme('brightcove_playlists_video', $video);
      $videos_data[] = $video;
      if (isset($variables['playlist']->id)) {
        $path = 'brightcove_playlists/playlist/'. $variables['playlist']->id .'/video/'. $video->id;
        if (!empty($video_pattern) && db_result(db_query_range("SELECT pid FROM {url_alias} WHERE src LIKE '%s'", $path, 0, 1)) === FALSE) {
          $video_placeholders = module_invoke('brightcove_playlists', 'token_values', 'brightcove_playlists_video', $video);
          $video_placeholders = array(
            'tokens' => array_merge($placeholders['tokens'], token_prepare_tokens(array_keys($video_placeholders))),
            'values' => array_merge($placeholders['values'], pathauto_clean_token_values((object) array('values' => array_values($video_placeholders)))),
          );
          pathauto_create_alias('brightcove_playlists_playlist_video', 'insert', $video_placeholders, $path, $video->id);
          pathauto_create_alias('brightcove_playlists_playlist_video_views', 'insert', $video_placeholders, $path .'/views', $video->id);
        }
      }
      else {
        $path = 'brightcove_playlists/video/'. $video->id;
        if (!empty($video_pattern) && db_result(db_query_range("SELECT pid FROM {url_alias} WHERE src LIKE '%s'", $path, 0, 1)) === FALSE) {
          $placeholders = pathauto_get_placeholders('brightcove_playlists_video', $video);
          pathauto_create_alias('brightcove_playlists_video', 'insert', $placeholders, $path, $video->id);
          pathauto_create_alias('brightcove_playlists_video_views', 'insert', $placeholders, $path .'/views', $video->id);
        }
      }
      $paths[] = $path;
      //add the path to video to build theme
      $video->path = $path;
      //call theme to build video item
      $title = theme('brightcove_playlists_video', $video);
      //add the values of the item to the array created above and set them in as vars so other preprocesses can change the item layout
/*      $items[] = array(
        'data' => l($title, $path, array('html' => TRUE)),
      );*/
      $items[] = array(
        'data' => $title,
      );
    }
  }
  $variables['paths'] = $paths;
  $variables['videos_data'] = $videos_data;
  $variables['videos'] = theme('item_list', $items);
 // $variables['tabs'] = theme('menu_local_tasks');
 unset($variables['tabs']);
}

