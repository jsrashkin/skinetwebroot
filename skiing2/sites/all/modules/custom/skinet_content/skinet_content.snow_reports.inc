<?php
/** 
 * @file
 * Handles old SnowReports widget until Snocountry one is ready.
 */

/**
 * Builds old Snow Reports ticker.
 */
function skinet_content_get_snowreports_ticker() {
  $arg = arg();
  // First, get the data
  $cache_key = 'skinet_content:snowreports_data';
  $cached = cache_get($cache_key);
  if (!empty($cached) && $cached->expire > time()) {
    $data = $cached->data;
  }
  else {
    // No cached version, so we fetch a fresh copy
    // todo: turn the URL into a variable
    $url = 'http://www.skinet.com/snowreports/services/rest/resorts/colorado.json';
    $response = drupal_http_request($url, array(), 'GET', null, 3, 10);

    // Verify we got an "OK" from server
    if ($response->code != '200') {
      // If connection failed, log it and return an error message.
      $msg = 'Error: could not connect to @url. HTTP response was @code';
      $vars = array('@url' => $url, '@code' => $response->code);
      watchdog('skinet_content', $msg, $vars, WATCHDOG_ERROR);
      return t($msg, $vars);
    }

    // Verify we received JSON. If not, quit.
    $expected = 'application/json';
    if (empty($response->headers['Content-Type']) ||
      $response->headers['Content-Type'] != $expected) {
        $msg = 'Error: Expecting @expected, but received @received as content type. Cannot continue.';
        $vars = array('@expected' => $expected, '@received' => $response->headers['Content-Type']);
        watchdog('skinet_content', $msg, $vars, WATCHDOG_ERROR);
        return t($msg, $vars);
      }

    // Decode the JSON into an array
    $data = json_decode($response->data);

    // Ensure we have results.
    if (empty($data)) {
      return t('No resorts found');
    }

    // Cache this data so we don't do HTTP requests but once an hour.
    cache_set('skinet_content:snowreports_data', $data, 'cache', time() + 60*60);
  }

  if ('node' == $arg[0] && is_numeric($arg[1]) && 'edit' != $arg[2]) {
    $this_node = node_load(arg(1));
    if (isset($this_node->nat)) {
      foreach ($this_node->nat as $node_term) {
        $term = $node_term;      
      }
    }
  }

  $parsed = skinet_content_parse_snow_reports($data, $node_term);

  $more_opts = array(
    'absolute' => TRUE,
    'external' => TRUE,
    'attributes' => array(
      'target' => '_blank',
    ),
  );
  $more_link = l('Snow Reports:', 'http://www.skinet.com/snowreports', $more_opts);

  //$ad = dart_tag('frame1');
  $ad = theme('bonnier_ads_ad', 'frame1');

  return theme('snowreports_ticker', $parsed, $more_link, $ad);
}

/**
 * Parses returned data from Snow Reports for display
 */
function skinet_content_parse_snow_reports($data, $term) {
  if ($term) {
    foreach ($data as $key => $item) {
      if ($item->fields->title->value == $term->name) {
        $move = $item;
        unset($data[$key]);
        break;
      }
    }
    array_unshift($data, $move);
  }
  if (is_array($data)) {
    $feed_item = array();
    foreach ($data as $item) {
      $title = l(t($item->fields->title->value), $item->fields->url->value, array('absolute' => TRUE, 'external' => TRUE, 'target' => '_blank'));
      $status = t($item->fields->ss_cck_field_resort_status->value);
      $low_temp = t('L ' . $item->fields->is_resort_low_temp_value->value . '°');
      $hi_temp = t('H ' . $item->fields->is_resort_high_temp_value->value . '°');
      $snowfall_24 = t('24 hour snow ' . $item->fields->sis_cck_field_24_hour_snowfall->value . '"');
      $snowfall_48 = t('48 hours snow ' . $item->fields->sis_cck_field_48_hour_snowfall->value . '"');
      $lifts_open = t('Lifts Open ' . $item->fields->sis_cck_field_lifts_open->value . ' of ' . $item->fields->sis_cck_field_total_number_lifts->value);
      $base_depth = t('Base Lower/Upper ' . $item->fields->sis_reports_base_depth_low->value . '/' . $item->fields->sis_reports_base_depth_high->value);

      $feed_item[] = theme('snowreports_ticker_item', $title, $status, $low_temp, $hi_temp, $snowfall_24, $snowfall_48, $lifts_open, $base_depth);
    }
    return $feed_item;
  }
}

