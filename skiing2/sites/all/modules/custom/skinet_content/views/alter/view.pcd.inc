<?php
$view = new view;
$view->name = 'pcd';
$view->description = 'PCD Controller';
$view->tag = 'pcd';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = 2;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->override_option('fields', array(
      'field_pcd_sub_offer_link_url' => array(
        'label' => '',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'strip_tags' => 0,
          'html' => 0,
          ),
        'link_to_node' => 0,
        'label_type' => 'none',
        'format' => 'default',
        'multiple' => array(
            'group' => TRUE,
            'multiple_number' => '',
            'multiple_from' => '',
            'multiple_reversed' => FALSE,
            ),
        'exclude' => 0,
        'id' => 'field_pcd_sub_offer_link_url',
        'table' => 'node_data_field_pcd_sub_offer_link',
        'field' => 'field_pcd_sub_offer_link_url',
        'relationship' => 'none',
        ),
        'field_pcd_sub_offer_image_data' => array(
            'label' => 'Sub Offer Image (field_pcd_sub_offer_image) - data',
            'alter' => array(
              'alter_text' => 0,
              'text' => '',
              'make_link' => 1,
              'path' => '[field_pcd_sub_offer_link_url]',
              'link_class' => '',
              'alt' => '',
              'prefix' => '',
              'suffix' => '',
              'help' => '',
              'trim' => 0,
              'max_length' => '',
              'word_boundary' => 1,
              'ellipsis' => 1,
              'strip_tags' => 0,
              'html' => 0,
              ),
            'link_to_node' => 0,
            'data_key' => 'description',
            'exclude' => 0,
            'id' => 'field_pcd_sub_offer_image_data',
            'table' => 'node_data_field_pcd_sub_offer_image',
            'field' => 'field_pcd_sub_offer_image_data',
            'relationship' => 'none',
            'override' => array(
                'button' => 'Override',
                ),
            ),
            ));
$handler->override_option('filters', array(
      'type' => array(
        'operator' => 'in',
        'value' => array(
          'pcd' => 'pcd',
          ),
        'group' => '0',
        'exposed' => FALSE,
        'expose' => array(
          'operator' => FALSE,
          'label' => '',
          ),
        'id' => 'type',
        'table' => 'node',
        'field' => 'type',
        'relationship' => 'none',
        ),
      'status' => array(
        'operator' => '=',
        'value' => '1',
        'group' => '0',
        'exposed' => FALSE,
        'expose' => array(
          'operator' => FALSE,
          'label' => '',
          ),
        'id' => 'status',
        'table' => 'node',
        'field' => 'status',
        'relationship' => 'none',
        ),
      ));
$handler->override_option('access', array(
      'type' => 'none',
      ));
$handler->override_option('cache', array(
      'type' => 'none',
      ));
$handler->override_option('row_plugin', 'node');
$handler->override_option('row_options', array(
      'relationship' => 'none',
      'build_mode' => 'full',
      'links' => 0,
      'comments' => 0,
      ));
$handler = $view->new_display('block', 'Sub Offer', 'block_1');
$handler->override_option('block_description', '');
$handler->override_option('block_caching', -1);
$handler = $view->new_display('block', 'Sub Form', 'block_2');
$handler->override_option('block_description', '');
$handler->override_option('block_caching', -1);
$handler = $view->new_display('block', 'Sub Form Footer', 'block_3');
$handler->override_option('block_description', '');
$handler->override_option('block_caching', -1);
$handler = $view->new_display('block', 'Print Subscriptions', 'block_4');
$handler->override_option('block_description', '');
$handler->override_option('block_caching', -1);
$handler = $view->new_display('block', 'Digital Subscriptions', 'block_5');
$handler->override_option('block_description', '');
$handler->override_option('block_caching', -1);

