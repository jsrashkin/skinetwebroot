<?php
/**
 * Implements hook_imagecache_default_presets
 */
function skinet_content_imagecache_default_presets(){
  $presets = array();
  $module_path = drupal_get_path('module', 'skinet_content');
  $regex = '\.inc$';
  $path = $module_path . '/imagecache/export';
  $files = drupal_system_listing($regex, $path, 'name', 0);
  foreach($files as $file){
    include($file->filename);
  }
  return $presets;
}
/**
 * Implementation of hook_imagecache_default_presets_alter().
 */
function skinet_content_imagecache_default_presets_alter(&$presets) {
  $module_path = drupal_get_path('module', 'skinet_content');
  $regex = '\.inc$';
  $path = $module_path . '/imagecache/alter';
  $files = drupal_system_listing($regex, $path, 'name', 0);
  foreach($files as $file) {
    include_once $file->filename;
  }
}
/**
* Implementation of hook_default_page_manager_handlers().
*/
function skinet_content_default_page_manager_handlers() {
	$handlers = array();
  $module_path = drupal_get_path('module', 'skinet_content');
  $regex = '\.inc$';
	$path = $module_path . '/panels/variants';
	$files = drupal_system_listing($regex, $path, 'name', 0);
	foreach($files as $file) {
		include_once $file->filename;
		$handlers[$handler->name] = $handler;
	}
	return $handlers;
}
/**
* Implementation of hook_default_page_manager_pages().
*/
function skinet_content_default_page_manager_pages() {
	$pages = array();
  $module_path = drupal_get_path('module', 'skinet_content');
  $regex = '\.inc$';
	$path = $module_path . '/panels/pages/export';
	$files = drupal_system_listing($regex, $path, 'name', 0);
	foreach($files as $file) {
		include_once $file->filename;
		$pages[$page->name] = $page;
  }
	return $pages;
}
/**
 * Implementation of hook_default_page_manage_pages_alter.
 */
function skinet_content_default_page_manager_pages_alter() {
  $pages = array();
  $module_path = drupal_get_path('module', 'skinet_content');
  $regex = '\.inc$';
  $path = $module_path . '/panels/pages/alter';
  $files = drupal_system_listing($regex, $path, 'name', 0);
  foreach($files as $file) {
    include_once $file->filename;
    $pages[$page->name] = $page;
  }
  return $pages;
}
/**
 * Implementation of hook_views_default_views
 */
function skinet_content_views_default_views() {
  $module_path = drupal_get_path('module', 'skinet_content');
  $regex = '\.inc$';
  $path = $module_path . '/views/export';
  $files = drupal_system_listing($regex, $path, 'name', 0);
  foreach ($files as $file) {
    include_once $file->filename;
    if (!empty($view->name)) {
      $views[$view->name] = $view;
    }
  }
  return $views;
}
/**
 * Implementation of hook_views_default_views()
 */
/*function skinet_content_views_default_views_alter(&$views) {
  $module_path = drupal_get_path('module', 'skinet_content');
  $regex = '\.inc$';
  $path = $module_path . '/views/alter';
  $files = drupal_system_listing($regex, $path, 'name', 0);
  foreach ($files as $file) {
    include_once $file->filename;
    if (!empty($view->name)) {
      $views[$view->name] = $view;
    }
  }
  return $views;
}*/
/**
 * Implementation of hook_context_default_contexts().
 */
function skinet_content_context_default_contexts() {
	$contexts = array();
  $module_path = drupal_get_path('module', 'skinet_content');
  $regex = '\.inc$';
	$path = $module_path . '/contexts/export';
	$files = drupal_system_listing($regex, $path, 'name', 0);
	foreach($files as $file) {
		include_once $file->filename;
	}
	return $contexts;
}
/**
 * Implementation of hook_context_default_contexts_alter()
 */
function skinet_content_context_default_contexts_alter() {
  $contexts = array();
  $module_path = drupal_get_path('module', 'skinet_content');
  $regex = '\.inc$';
  $path = $module_path . '/comtexts/alter';
  $files = drupal_system_listing($regex, $path, 'name', 0);
  foreach($files as $file) {
    include_once $file->filename;
  }
  return $contexts;
}
/**
 * Implementation of hook_ctools_plugin_api().
 */
function skinet_content_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  $path = drupal_get_path('module', 'skinet_content');
  if ($module == 'page_manager' && $api == 'pages_default') {
    return array('version' => 1);
  }
  if ($module == 'panels_mini' && $api == 'panels_default') {
    return array('version' => 1);
  }
  if ($module == 'dart' && $api == 'dart_tag_default') {
    return array('version' => 1);
  }
}
/**
 * Implementation of hook_ctools_plugin_directory
 */
function skinet_content_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools') {
    return 'plugins/'.$plugin;
  }
}
/**
 * Implementation of hook_default_panels_mini()
 */
function skinet_content_default_panels_mini() {
  $minis = array();
  $module_path = drupal_get_path('module', 'skinet_content');
  $regex = '\.inc$';
  $path = $module_path . '/panels/mini_panels';
  $files = drupal_system_listing($regex, $path, 'name', 0);
  foreach ($files as $file) {
    //unset($mini);
    require_once($file->filename);
    if ($mini) {
      $minis[$mini->name] = $mini;
    }
  }
  return $minis;
}
/**
 * Implentation of hook_default_dart_tag_alter
 * Adding the path to include the individual position in code
 */
function skinet_content_default_dart_tag(&$tags) {
  $tags = array();
  $module_path = drupal_get_path('module', 'skinet_content');
  $regex = '\.inc$';
  $path = $module_path . '/dart';
  $files = drupal_system_listing($regex, $path, 'name', 0);
  foreach ($files as $file) {
    include_once ($file->filename);
    if (!empty($tag->name)) {
      $tags[$tag->name] = $tag;
    }
  }
  return $tags;
}
/**
 * Get vocabulary by name
 */
function skinet_content_get_vocab_by_name($name) {
  $vocabs = taxonomy_get_vocabularies(NULL);
  foreach ($vocabs as $vocab_object) {
    if ($vocab_object->name == $name) {
      return $vocab_object;
    }
  }
  return NULL; 
}
