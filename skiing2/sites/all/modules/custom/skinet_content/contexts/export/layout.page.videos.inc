<?php
$contexts[] = array(
  'namespace' => 'layout',
  'attribute' => 'page',
  'value' => 'videos',
  'description' => 'Sets up page for video landing page',
  'path' => array(
    'videos*' => 'videos*',
    'brightcove_playlists*' => 'brightcove_playlists*',
  ),
  'block' => array(
    'bonnier_ads-x90' => array(
      'module' => 'bonnier_ads',
      'delta' => 'x90',
      'weight' => 142,
      'region' => 'below_header',
      'status' => '0',
      'label' => 'DART tag: x90',
      'type' => 'context_ui',
    ),
  ),
);
