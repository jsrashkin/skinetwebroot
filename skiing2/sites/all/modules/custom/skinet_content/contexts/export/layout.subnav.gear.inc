<?php
$contexts[] = array(
  'namespace' => 'layout',
  'attribute' => 'subnav',
  'value' => 'gear',
  'description' => 'Gear channel',
  'path' => array(
    'gear' => 'gear',
    'gear/bindings' => 'gear/bindings',
    'gear/backcountry-gear' => 'gear/backcountry-gear',
    'gear/apparel' => 'gear/apparel',
    'gear/accessories' => 'gear/accessories',
    'gear/gadgets' => 'gear/gadgets',
    'gear/gear-deals' => 'gear/gear-deals',
    'gear/brands' => 'gear/brands',
  ),
);
