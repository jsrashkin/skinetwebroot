<?php
$contexts[] = array(
  'namespace' => 'layout',
  'attribute' => 'page',
  'value' => 'channel',
  'node' => array(
    '0' => 'channel',
  ),
  'block' => array(
    'skinet_content_skinet_channel_title' => array(
      'module' => 'skinet_content',
      'delta' => 'skinet_channel_title',
      'weight' => 137,
      'region' => 'below_header',
      'status' => '0',
      'label' => 'Skinet Channel Title',
      'type' => 'context_ui',
    ),
    'bonnier_ads-x90' => array(
      'module' => 'bonnier_ads',
      'delta' => 'x90',
      'weight' => 138,
      'region' => 'below_header',
      'status' => '0',
      'label' => 'DART tag: x90',
      'type' => 'context_ui',
    ),
    'skinet_content_skinet_flexslider' => array(
      'module' => 'skinet_content',
      'delta' => 'skinet_flexslider',
      'weight' => 139,
      'region' => 'below_header',
      'status' => '0',
      'label' => 'Skinet Flex Slider',
      'type' => 'context_ui',
    ),
  ),
); 
