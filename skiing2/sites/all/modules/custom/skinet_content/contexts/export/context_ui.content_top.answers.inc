<?php
$contexts[] = array(
    'namespace' => 'context_ui',
    'attribute' => 'content_top',
    'value' => 'answers',
    'views' => array(
      '0' => 'questions',
      ),
    'block' => array(
      'views_featured_question-block_1' => array(
        'module' => 'views',
        'delta' => 'featured_question-block_1',
        'weight' => 20,
        'region' => 'content_top',
        'status' => '0',
        'label' => 'featured_question: Block',
        'type' => 'context_ui',
        ),
      ),
    );
