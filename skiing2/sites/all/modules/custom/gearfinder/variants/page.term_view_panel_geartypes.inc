<?php
$handler = new stdClass;
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'term_view_panel_geartypes';
$handler->task = 'term_view';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = -27;
$handler->conf = array(
    'autogenerate_title' => FALSE,
    'title' => 'Vocabulary - Gear Types',
    'no_blocks' => FALSE,
    'css_id' => 'term-page',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'term_vocabulary',
          'settings' => array(
            'vids' => array(
              12 => 12,
              ),
            ),
          'context' => 'argument_terms_1',
          ),
        ),
      ),
    );
$display = new panels_display;
$display->layout = 'twocol';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = '%term:name';
$display->content = array();
$display->panels = array();
$pane = new stdClass;
$pane->pid = 'new-1';
$pane->panel = 'left';
$pane->type = 'block';
$pane->subtype = 'views--exp-skinet_gear__term-default';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Narrow results by:',
    );
$pane->cache = array();
$pane->style = array();
$pane->css = array(
    'css_id' => '',
    'css_class' => '',
    );
$pane->extras = array();
$pane->position = 0;
$display->content['new-1'] = $pane;
$display->panels['left'][0] = 'new-1';
$pane = new stdClass;
$pane->pid = 'new-2';
$pane->panel = 'left';
$pane->type = 'block';
$pane->subtype = 'bonnier_ads-left';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    );
$pane->cache = array();
$pane->style = array();
$pane->css = array();
$pane->extras = array();
$pane->position = 1;
$display->content['new-2'] = $pane;
$display->panels['left'][1] = 'new-2';
$pane = new stdClass;
$pane->pid = 'new-3';
$pane->panel = 'right';
$pane->type = 'views';
$pane->subtype = 'skinet_gear__term';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array(
    'nodes_per_page' => '10',
    'pager_id' => '1',
    'use_pager' => 1,
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'context' => array(
      0 => 'argument_terms_1.tids',
      ),
    'override_title' => 1,
    'override_title_text' => '',
    );
$pane->cache = array();
$pane->style = array();
$pane->css = array();
$pane->extras = array();
$pane->position = 0;
$display->content['new-3'] = $pane;
$display->panels['right'][0] = 'new-3';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = 'new-3';
$handler->conf['display'] = $display;

