<div class="header-tout-wrapper">
	
	<div class="tout-box" id="skis">
		<a href="gear/skis">
			<div class="tout-anchor">> <span class="browse-link">Browse Skis</span></div>
		</a>
	</div><!-- tout-box -->
	
	<div class="tout-box" id="boots">
		<a href="gear/boots">
			<div class="tout-anchor">> <span class="browse-link">Browse Boots</span></div>
		</a>
	</div><!-- tout-box -->
	
	<div class="tout-box" id="video">
		<a target="_blank" href="<?php print $video_link; ?>">
			<div class="tout-anchor">> <span class="browse-link"><?php print $test_text; ?></span></div>
			<div class="video-text"><?php print $video_text; ?></div>
		</a>
		
	</div><!-- tout-box -->
	
</div><!-- header-tout-wrapper -->