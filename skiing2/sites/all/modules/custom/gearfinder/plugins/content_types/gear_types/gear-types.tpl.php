<div class="gear-types-list-wrapper">
  <h3>The Latest Gear</h3>
  <div class="list" id="col1">
    <?php if ($first): ?>
      <?php print $first; ?>
    <?php endif; ?>
  </div>
	
  <div class="list" id="col2">
    <?php if ($second): ?>
      <?php print $second; ?>
    <?php endif; ?>
  </div>
	
  <div class="list" id="col3">
    <?php if ($third): ?>
      <?php print $third; ?>
    <?php endif; ?>
  </div>
	
</div><!-- gear-types-list-wrapper -->
