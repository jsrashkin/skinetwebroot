<?php
/**
* Implementation of hook_ctools_content_types
*/
function gearfinder_gear_results_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Gearfinder Search Results'),
    'description' => 'Gearfinder Search Results',
    'category' => 'Gearfinder v2',
    'hook theme' => 'gearfinder_gear_results_theme',
  );
}

/**
* Implementation of hook_content_type_render
*/
function gearfinder_gear_results_content_type_render($subtype, $conf, $panel_args, $context) {
	dpm($context, '');
	$view = _get_results_from_view('gearfinder_filters', 'page_1', gearfinder_set_content_type(), '', 18);
	dpm($view);
	// $view = views_get_view('gearfinder_filters');
  // $display = $view->execute_display('page_1');
  // print $display['content'];

// 	$content = gearfinder_apachesolr_search_view();
	// dpm($content);
  $block->content = theme('gear_results', $content);
  $block->title = '';
  return $block;
}

/**
* Implementation of hook_theme
*/
function gearfinder_gear_results_theme(&$theme){
  $path = drupal_get_path('module', 'gearfinder') . '/plugins/content_types/gear_results';
  $theme['gear_results'] = array(
    'template' => 'gear_results',
    'arguments' => array('results' => NULL),
    'path' => $path,
  );
  $theme['gear_result'] = array(
    'template' => 'gear_result',
    'arguments' => array('result' => NULL),
    'path' => $path,
  );
}

/**
* Preprocess this junk
*/
function template_preprocess_gear_results(&$vars){
  $vars['pager'] = theme('pager', NULL, 10, 0);
}

/**
* Implementation of hook_preprocess_gear_result
*/
function template_preprocess_gear_result(&$vars){
  $node = node_load($vars['result']['fields']['nid']);
  $vars['title'] = l($node->title, 'node/'.$node->nid, array('html' => true));
  $vars['description'] = truncate_utf8(strip_tags($node->body)  , 150, true, true);
  $vars['image'] = theme('imagecache', 'medium_thumbnail', node_load($node->field_thumbnail[0]['nid'])->field_image[0]['filepath']);
  $terms = array();
  if(is_array($node->taxonomy)) {
    foreach($node->taxonomy as $taxonomy){
      $terms[] = l($taxonomy->name, 'taxonomy/term/'.$taxonomy->tid);
    }
    $vars['tags'] = implode(' / ', $terms);
  }
}

