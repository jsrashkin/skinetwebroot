<?php
/**
* Implementation of hook_ctools_content_types
*/
function gearfinder_gear_header_ctools_content_types(){
  return array(
    'single' => TRUE,
    'title' => t('Skinet Gear Header'),
    'description' => 'Skinet Gear Search Header',
    'category' => 'SOLR-powered gear search',
    'hook theme' => 'gearfinder_gear_header_theme',
  );  
}

/**
* Implementation of hook_theme
*/
function gearfinder_gear_header_theme(&$theme){
  $path = drupal_get_path('module', 'gearfinder') . '/plugins/content_types/gear_header';
  $theme['gear_header'] = array(
    'template' => 'gear_header',
    'arguments' => array('min' => null, 'max' => null, 'total' => null, 'header' => null),
    'path' => $path,
  );
}

function gearfinder_gear_header_content_type_render($subtype, $conf, $panel_args, $context){
  $block->title = '';
  $block->content = theme('gear_header');
  return $block;
}

function gearfinder_preprocess_gear_header(&$vars){
	$type = gearfinder_set_content_type();
	$my_view = views_get_view('gearfinder_filters', 'page_1');
	$my_view->args[0] = $type;
	$page = $type == 'product_ski' ? 'page_1' : 'page_2';
	$my_view->preview($page);
	$total_rows = $my_view->total_rows;
	$vars['total_rows'] = number_format($total_rows);
  // make sure we have results
  if($total_rows == 0) $vars['found'] = false;
	else $vars['found'] = true;
}



