<?php

function gearfinder_detail_header_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Gearfinder Detail Header'),
    'description' => t('Gear detail page header'),
    'category' => 'Gearfinder v2',
    'hook theme' => 'gearfinder_detail_header_theme',
  );
}

function gearfinder_detail_header_theme(&$theme) {
  $theme['detail_header'] = array(
    'template' => 'detail_header',
		'arguments' => array('node' => null),
    'path' => drupal_get_path('module','gearfinder').'/plugins/content_types/detail_header',
  );
}

function gearfinder_detail_header_content_type_render($subtype, $conf, $panel_args, $context) {
	$node = node_load(arg(1));
  $content = theme('detail_header', $node);
  $block->content = $content;
  $block->title = '';
  $block->delta = 'detail_header';
  $block->css_class = 'detail-header';
  return $block;
}




function template_preprocess_detail_header(&$vars) {
	$node = $vars['node'];
	$browse_links = detail_header_get_links();
	$vars['browse_links'] = 'Browse: ' . theme('item_list', $browse_links);
	$vars['share'] = theme('laserfist_share',$node);
	$vars['print'] = theme('laserfist_print',$node);
	$vars['email'] = theme('laserfist_email',$node);
	$vars['comment'] = l('comment', "node/". $node->nid, array('fragment' => 'comments'));
	_gear_core_header($vars);
}



function detail_header_get_links() {
	$links[] = l('Skis', 'gear/skis');
	$links[] = l('Boots', 'gear/boots');
	if ($nid = variable_get('gearfinder_how_we_test_nid', '')) {
		$links[] = l('How We Test', 'node/'.$nid);
	}
	return $links;
}

function _gear_core_header(&$vars) {
	$node = $vars['node'];	
	$vars['title'] = $node->title;
	if ($node->type == 'product_ski') {
		// first get the awards we want to show:
		foreach ($node->field_product_awards as $a => $award) {
			$vars['awards'] .= _gear_return_award($award['value']);
		}
		// build other awards:
		$field_rank_hard_snow = _gear_check_awards($node, 'field_rank_hard_snow');
		$field_rank_mixed_snow = _gear_check_awards($node, 'field_rank_mixed_snow');
		$field_rank_deep_snow = _gear_check_awards($node, 'field_rank_deep_snow');
		$field_rank_hard_snow_value = _gear_check_awards($node, 'field_rank_hard_snow_value');
		$field_rank_mixed_snow_value = _gear_check_awards($node, 'field_rank_mixed_snow_value');
		if ($field_rank_hard_snow) $vars['field_rank_hard_snow'] = '<span class="rank-label">Rank:</span> #' . $field_rank_hard_snow . ' In ' . _gear_convert_field_to_text('field_rank_hard_snow');
		if ($field_rank_mixed_snow) $vars['field_rank_mixed_snow'] = '<span class="rank-label">Rank:</span> #' . $field_rank_mixed_snow . ' In ' . _gear_convert_field_to_text('field_rank_mixed_snow');
		if ($field_rank_deep_snow) $vars['field_rank_deep_snow'] = '<span class="rank-label">Rank:</span> #' . $field_rank_deep_snow . ' In ' . _gear_convert_field_to_text('field_rank_deep_snow');
		if ($field_rank_hard_snow_value) $vars['field_rank_hard_snow_value'] = '<span class="rank-label">Rank:</span> #' . $field_rank_hard_snow_value . ' In ' . _gear_convert_field_to_text('field_rank_hard_snow_value');
		if ($field_rank_mixed_snow_value) $vars['field_rank_mixed_snow_value'] = '<span class="rank-label">Rank:</span> #' . $field_rank_mixed_snow_value . ' In ' . _gear_convert_field_to_text('field_rank_mixed_snow_value');

	} 
	// only show overall rating if this is a new/tested ski & has a review
	if ($node->field_product_review[0]['value']) {
		$vars['rating_overall'] = $node->type == 'product_ski' ? _gear_check_value($node, 'field_ski_rating_overall') : _gear_check_value($node, 'field_boot_rating_overalimp');
	}	
	$fb_like = theme('facebook_like', url(gearfinder_current_url(), array('absolute' => TRUE)), 'like', 'button_count');
	$tweet = theme('twitter_share_button', gearfinder_current_url());
	$vars['social_buttons'] = $fb_like . $tweet;
}

function _gear_return_award($val) {
	// wanted: GMG, Best Value, Rocker
	$wanted = array('6', '7', '8');
	$content_field = content_allowed_values(content_fields('field_product_awards'));
	if (in_array($val, $wanted)) {
		$id = strtolower(str_replace(' ', '-', $content_field[$val]));
		$html = '<div id="'.$id.'" class="ski-labeled">'.$content_field[$val].'</div><!-- ' . $id . ' -->';
		return $html;
	}
}

function _gear_check_awards($node, $value) {
	if ($node->{$value}[0]['value']) {
		return $node->{$value}[0]['value'];
	}
	else {
		return null;
	}
}

function _gear_check_value($node, $value) {
	if ($node->{$value}[0]['value']) return $node->{$value}[0]['value'];
	else return null;
}

function _gear_convert_field_to_text($field) {
	$field = explode('_', $field);
	return ucwords($field[2] . ' ' . $field[3]);
}
