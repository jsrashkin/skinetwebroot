<div class="gear-detail-header-content-wrapper">

	<div class="gear-detail-below-header">
	
		<div class="utility-links">
		  <div class="email"><?php print $email; ?></div>
		  <div class="print"><?php print $print; ?></div>
		  <div class="share"><?php print $share; ?></div>
		  <?php if ($comment): ?>
		    <div class="comment"><?php print $comment; ?></div>
		  <?php endif; ?>
		</div>
	
	</div><!-- gear-detail-below-header -->
	
	<div class="gear-detail-core-header-wrapper">

		<div class="gear-header-details">

			<div class="gear-title"><?php print $title; ?></div><!-- gear-title -->

			<div class="gear-awards">

				<?php if ($awards): ?>
					<?php print $awards; ?>
				<?php endif; ?>

				<?php if ($field_rank_hard_snow): ?>
					<div id="rank-award">
						<?php print $field_rank_hard_snow; ?>
					</div><!-- rank-award -->
				<?php endif; ?>
				<?php if ($field_rank_mixed_snow): ?>
					<div id="rank-award">
						<?php print $field_rank_mixed_snow; ?>
					</div><!-- rank-award -->
				<?php endif; ?>
				<?php if ($field_rank_deep_snow): ?>
					<div id="rank-award">
						<?php print $field_rank_deep_snow; ?>
					</div><!-- rank-award -->
				<?php endif; ?>
				<?php if ($field_rank_hard_snow_value ): ?>
					<div id="rank-award">
						<?php print $field_rank_hard_snow_value; ?>
					</div><!-- rank-award -->
				<?php endif; ?>
				<?php if ($field_rank_mixed_snow_value ): ?>
					<div id="rank-award">
						<?php print $field_rank_mixed_snow_value; ?>
					</div><!-- rank-award -->
				<?php endif; ?>

			</div><!-- gear-awards -->
			
			<div class="social-buttons">
				<?php print $social_buttons; ?>
			</div><!-- social-buttons -->

		</div><!-- gear-header-details -->
		
		<?php if ($rating_overall): ?>
			<div class="gear-header-rating">			
				<?php print $rating_overall; ?>
			</div><!-- gear-header-rating -->
		<?php endif; ?>

	</div><!-- gear-detail-core-header-wrapper -->

</div><!-- gear-detail-header-content-wrapper -->





