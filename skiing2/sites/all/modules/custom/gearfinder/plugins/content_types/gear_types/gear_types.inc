<?php

function gearfinder_gear_types_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Gearfinder Gear Types'),
    'description' => t('Nuff said.'),
    'category' => 'Gearfinder v2',
    'hook theme' => 'gearfinder_gear_types_theme',
  );
}

function gearfinder_gear_types_theme(&$theme) {
  $theme['gear_types'] = array(
    'template' => 'gear-types',
    'path' => drupal_get_path('module','gearfinder').'/plugins/content_types/gear_types',
    'arguments' => array('first' => NULL, 'second' => NULL, 'third' => NULL),
  );
}

function gearfinder_gear_types_content_type_render($subtype, $conf, $panel_args, $context) {
  $content = theme('gear_types');
  $block->content = $content;
  $block->title = '';
  $block->delta = 'gear_types';
  $block->css_class = 'gear-types';
  return $block;
}




function template_preprocess_gear_types(&$vars) {
  $cid = 'skinet_gear_types';
  $cached = cache_get($cid);
  if (!empty($cached)) {
    $data = $cached->data;
  }
  else {
    $terms = array('Boots', 'Helmets', 'Skis', 'Apparel', 'Goggles and Glasses', 'Hats and Gloves');
    $vid = variable_get('skinet_vocab_site_channel', '');
    $links = array();
    $data = array();
    foreach ($terms as $term) {
      $data[] = db_fetch_object(db_query("SELECT tid FROM term_data WHERE name = '%s' AND vid = '%d'", $term, $vid));
    }
    // Terms don't change until this function changes and caches are flushed.
    cache_set('skinet_gear_types', $data, 'cache', CACHE_PERMANENT);
  }
  $links = array();
  foreach ($data as $tid) {
    $term = taxonomy_get_term($tid->tid);
    $links[] = l(str_replace(array('Goggles and Glasses', 'Hats and Gloves'), array('Goggles', 'Gloves'), $term->name), 'taxonomy/term/' . $term->tid);
  }
  $first = array();
  $second = array();
  $third = array();
  foreach ($links as $key => $link) {
    if ($key < 2) {
      $first[] = $link;
    }
    elseif ($key > 1 && $key < 4) {
      $second[] = $link;
    }
    else {
      $third[] = $link;
    }
  }
  $vars['first'] = theme('item_list', $first, NULL, 'ul');
  $vars['second'] = theme('item_list', $second, NULL, 'ul');
  $vars['third'] = theme('item_list', $third, NULL, 'ul');
}


function gearfinder_gear_types_content_type_edit_form(&$form, &$form_state) {}
