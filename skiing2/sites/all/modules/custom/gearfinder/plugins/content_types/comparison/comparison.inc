<?php

function gearfinder_comparison_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Gearfinder Product Comparison'),
    'description' => t('Product comparion - gearfinder'),
    'category' => 'Gearfinder v2',
    'hook theme' => 'gearfinder_comparison_theme',
  );
}

function gearfinder_comparison_theme(&$theme) {
  $theme['comparison_header'] = array(
    'template' => 'comparison_header',
    'path' => drupal_get_path('module','gearfinder').'/plugins/content_types/comparison',
  );
	$theme['comparison'] = array(
    'template' => 'comparison',
    'path' => drupal_get_path('module','gearfinder').'/plugins/content_types/comparison',
  );
}

function gearfinder_comparison_content_type_render($subtype, $conf, $panel_args, $context) {
  $content = theme('comparison_header');
	$content .= theme('comparison');
  $block->content = $content;
  $block->title = '';
  $block->delta = 'comparison';
  $block->css_class = 'product-comparison';
  return $block;
}

function template_preprocess_comparison(&$vars) {
	if (is_array($_GET['prd'])) {
		foreach ($_GET['prd'] as $nid) {
      $node = node_load($nid);
			$nodes[] = $node;
    }
	}
	// Handle undesireable number of nodes being provided.
	if (count($nodes) > 3) {
    drupal_set_message(t('Only three products can be compared at a time.'));
    $nodes = array_slice($nodes, 0, 3); // Trim our nodes to only compare three.
  }
  if (empty($nodes)) $content = t('Please return to the Gearfinder and select two or three products to compare.');
	else $content = _comparison_items($nodes);
	$vars['content'] = $content;
}



function _comparison_items(&$nodes) {
	// first load the product specs:
	$node_specs = gearfinder_product_specs($nodes[0]->type, $nodes);
	$prod_heading = array(
	  array('data' => '', 'class' => 'empty'),
	);
	// work out which stinkin image to use:
	foreach($nodes as $node) {
		if ($node->field_main_photo[0]['nid']) $nid = $node->field_main_photo[0]['nid'];
		else if ($node->field_thumbnail[0]['nid']) $nid = $node->field_thumbnail[0]['nid'];
		else {
			// load the "no photo avail" image:
			$env = $_SERVER['SERVER_NAME'];
			$env = explode('.', $env);
			$env = $env[0];
			if($env == 'dev' || $env == 'test') $nid = '116439';
			else $nid = '55563800';
		}
		$imagenode = node_load($nid);
		$imagepath = $imagenode->field_image[0]['filepath'];
		if ($imagepath) {
			$image = l(theme('imagecache', 'gf_product_compare', $imagepath), 
												theme('laserfist_enlarge_link', $imagenode), 
		                     array('html' => TRUE, 'attributes' => array('class' => 'thickbox'))
		                    );
			$enlarge_link = l('Enlarge Image', 
												theme('laserfist_enlarge_link', $imagenode), 
		                     array('html' => TRUE, 'attributes' => array('class' => 'thickbox show-mag'))
		                    );
		} // end if image path
		
		$buttons = '';
		if ($node->field_product_review[0]['value'] || $node->field_gold_medal_gear[0]['value'] == '1') $buttons .= '<div class="ski-labeled" id="gold-medal-gear">Gold Medal Gear</div><!-- gold-medal-gear -->';
		if ($node->field_best_value[0]['value'] == '1') $buttons .= '<div class="ski-labeled" id="rocker">Rocker</div><!-- rocker-award -->';
		if ($node->field_product_rocker[0]['value'] == '1') $buttons .= '<div class="ski-labeled" id="best-value">Best Value</div><!-- best-value-award -->';
		
	  $prod_heading[] = $image . $enlarge_link 
	      . '<h3>' . l(check_plain($node->title), 'node/' . $node->nid) . '<br />' . $buttons . '</h3>';
	
	}
	// unset rows that have no data
	foreach($node_specs as $spec) {	
		foreach ($spec as $c => $s) {
			$null = false;
			if ($s[1]['data'] == '' && $s[2]['data'] == '' && $s[3]['data'] == '') {
				$unset[] = $c;
			}
		}
	}
	foreach ($unset as $u) {
		unset($node_specs['fields'][$u]);
	} 
	$output .= theme('table', $prod_heading, $node_specs['fields']);
	$build_query = $_GET;
  unset($build_query['prd'], $build_query['q']);
  $query_string = http_build_query($build_query, '', '&');
	$output .= '<div class="return-to-results-button">'.l(t('Return to results'), 'gear/' . arg(2), array('query' => $query_string)).'</div>';
	return $output;
}

function template_preprocess_comparison_header(&$vars) {
	$headerleft = '<h2>Product Comparison</h2>';
	$headerleft .= '<div class="comparison-dek">'.variable_get('gearfinder_product_comparison_dek', '').'</div>';
	$vars['headerleft'] = $headerleft;
	
	$vars['share'] = theme('laserfist_share',$node);
	$vars['print'] = theme('laserfist_print',$node);
	$vars['email'] = theme('laserfist_email',$node);
	$build_query = $_GET;
  unset($build_query['prd'], $build_query['q']);
  $query_string = http_build_query($build_query, '', '&');
  $headerright .= '<div class="return-to-results-button">'.l(t('Return to results'), 'gear/' . arg(2), array('query' => $query_string)).'</div>';
	$vars['headerright'] = $headerright;
}





function gearfinder_comparison_content_type_edit_form(&$form, &$form_state) {}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
