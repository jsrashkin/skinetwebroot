<?php

function gearfinder_above_header_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Gearfinder Above Header'),
    'description' => t('Top "Nav" for gearfinder - above logo header'),
    'category' => 'Gearfinder v2',
    'hook theme' => 'gearfinder_above_header_theme',
  );
}

function gearfinder_above_header_theme(&$theme) {
  $theme['above_header'] = array(
    'template' => 'above-header',
    'path' => drupal_get_path('module','gearfinder').'/plugins/content_types/above_header',
  );
}

function gearfinder_above_header_content_type_render($subtype, $conf, $panel_args, $context) {
  $content = theme('above_header');
  $block->content = $content;
  $block->title = '';
  $block->delta = 'above_header';
  $block->css_class = 'above-header';
  return $block;
}




function template_preprocess_above_header(&$vars) {
	$browse_links = above_header_get_links();
	$vars['browse_links'] = 'Browse: ' . theme('item_list', $browse_links);
	$fb_like = theme('facebook_like', url(gearfinder_current_url(), array('absolute' => TRUE)), 'like', 'button_count');
	$tweet = theme('twitter_share_button', gearfinder_current_url());
	$vars['social_buttons'] = $fb_like . $tweet;
}

function above_header_get_links() {
	$links[] = l('Skis', 'gear/skis');
	$links[] = l('Boots', 'gear/boots');
	if ($nid = variable_get('gearfinder_how_we_test_nid', '')) {
		$links[] = l('How We Test', 'node/'.$nid);
	}
	return $links;
}

function gearfinder_above_header_content_type_edit_form(&$form, &$form_state) {}
