<?php
$items[] = array(
  'namespace' => 'layout',
  'attribute' => 'page',
  'value' => 'gearfinder',
  'path' => array(
    'gear/*' => 'gear/*',
  ),
  'block' => array(
    'panels_mini_gearfinder_header' => array(
      'module' => 'panels_mini',
      'delta' => 'gearfinder_header',
      'weight' => 143,
      'region' => 'above_main',
      'status' => '0',
      'label' => 'Mini panel: "Gearfinder Header"',
      'type' => 'context_ui',
    ),
    'gearfinder_gf_header' => array(
      'module' => 'gearfinder',
      'delta' => 'gf_header',
      'weight' => 144,
      'region' => 'above_main',
      'status' => '0',
      'label' => 'Gearfinder: Header',
      'type' => 'context_ui',
    ),
    'bonnier_ads-x90' => array(
      'module' => 'bonnier_ads',
      'delta' => 'x90',
      'weight' => 143,
      'region' => 'below_header',
      'status' => '0',
      'label' => 'DART tag: x90',
      'type' => 'context_ui',
    ),
  ),
);
