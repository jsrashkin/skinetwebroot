<?php
$items[] = array(
  'namespace' => 'layout',
  'attribute' => 'sidebars',
  'value' => '_a_gearfinder',
  'description' => 'Gearfinder sidebar layout',
  'path' => array(
    'gear' => 'gear',
    'gear/*' => 'gear/*',
  ),
  'block' => array(
    'panels_mini_right_column_gearfinder' => array(
      'module' => 'panels_mini',
      'delta' => 'right_column_gearfinder',
      'weight' => 118,
      'region' => 'right',
      'status' => '0',
      'label' => 'Mini panel: "Right Column: Gearfinder"',
      'type' => 'context_ui',
    ),
  ),
);