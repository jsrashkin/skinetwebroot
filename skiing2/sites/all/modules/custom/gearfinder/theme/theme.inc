<?php
// $Id$
/**
 * @file
 * Preprocess and theme functions for SkiNet Gearfinder
 */

/**
 * Implementation of MODULE_preprocess_views_view_field__gearfinder_types__nothing().
 */
function gearfinder_preprocess_views_view_field__gearfinder_filters__page_1__nothing(&$vars) {
  if (isset($vars['row']->nid)) {
    if (!isset($vars['output'])) {
      $vars['output'] = check_plain($vars['field']->options['alter']['text']);
    }
    $nid = check_plain($vars['row']->nid);
    $vars['output'] = '<input id="product-comparison-select-' . $nid . '" type="checkbox" value="' . $nid . '" name="prd[]" />'
      . '<label for="product-comparison-select-' . $nid . '">' . $vars['output'] . '</label>';
  }
  // If no nid then do not display any output.
  else {
    $vars['output'] = '';
  }
}

/**
 * Implementation of MODULE_preprocess_views_view_field__gearfinder_types__nothing().
 */
function gearfinder_preprocess_views_view_field__gearfinder_filters__page_2__nothing(&$vars) {
  if (isset($vars['row']->nid)) {
    if (!isset($vars['output'])) {
      $vars['output'] = check_plain($vars['field']->options['alter']['text']);
    }
    $nid = check_plain($vars['row']->nid);
    $vars['output'] = '<input id="product-comparison-select-' . $nid . '" type="checkbox" value="' . $nid . '" name="prd[]" />'
      . '<label for="product-comparison-select-' . $nid . '">' . $vars['output'] . '</label>';
  }
  // If no nid then do not display any output.
  else {
    $vars['output'] = '';
  }
}

/**
 * Custom formatter theme function to show teaser and links back to node.
 */
/*
function theme_gearfinder_formatter_teaserlinks($element) {
  if (empty($element['#item'])) {
    return '';
  }

  // Get the complete review node.
  $review = node_load($element['#item']['nid']);

  // Return if there is not review text to show.
  if (!isset($review->body)) {
    return '';
  }

  $link = l(t('Read full review'), 'node/' . $element['#item']['nid']);
  $description_field = $review->body;

  // Create a teaser with a read more link from the description field.
  $teaser_text = _filter_htmlcorrector(node_teaser($description_field, 1));
  $teaser = $teaser_text . '<span class="tested-review-read-more read-more ref-link">' . l(t('Read more'), 'node/' . $element['#item']['nid']) .'</span>';
  
  $output = '<div class="tested-review-link ref-link">' . $link . '</div>';
  $output .= '<div class="tested-review-text">' . $teaser . '</div>';
  $output .= '<div class="review-right"></div><div class="review-bottom"></div>';

  return $output;
}
*/

/**
 * Custom formatter theme function to show six thumbnails from the referenced gallery node.
 */
/*
function theme_gearfinder_formatter_gallerysixthumbs($element) {
	
  if (empty($element['#item'])) {
    return '';
  }

  // Load the full gallery node.
  $gallery = node_load($element['#item']['nid']);
  $image_count = count($gallery->field_pages);
  // Set the number of thumbnails to show.
  $limit = 6;

  // Make sure we have images in the gallery.
  if ($image_count <= 1) {
    return t('No photos currently available.');
  }

  // If the number of images in the gallery is less than our limit,
  // make that the new limit.
  if ($image_count < $limit) {
    $limit = $image_count;
  }

  $images = array();
  $link = l(t('View all @count images', array('@count' => $image_count)), 'node/' . $element['#item']['nid']);

  // Prepare a set of classes for each image.
  $class = array('one', 'two', 'three', 'four', 'five', 'six');
  
  // Gallery images are noderefs in the gallery node, so we need 
  // to look those up too, to get the actual images.
  // We only want to show up to 6 images.
  for ($i = 0; $i < $limit; $i++) {
    $image = node_load($gallery->field_pages[$i]['nid']);
    // Make the resized images popup in thickbox. Assign each <li> a unique class.
    $images[] = array(
      'data' => l(theme('imagecache', 'bg_product_photo_thumb', $image->field_image[0]['filepath']), $image->field_image[0]['filepath'], array('html' => TRUE, 'attributes' => array('class' => 'thickbox', 'rel' => 'gallery-' . $element['#item']['nid']))),
      'class' => 'image-' . $class[$i],
    );
  }

  $output = theme('item_list', $images, NULL, 'ul', array('class' => 'product-photos'));
  $output .= '<div class="product-photo-link ref-link">' . $link . '</div>';

  return $output;

}
*/
/**
 * Custom formatter theme function to show two thumbnails from the referenced gallery node.
 */
/*
function theme_gearfinder_formatter_gallerytwothumbs($element) {
  if (empty($element['#item'])) {
    return '';
  }

  // Load the full gallery node.
  $gallery = node_load($element['#item']['nid']);
  $image_count = count($gallery->field_pages);
  // Set the number of thumbnails to show.
  $limit = 2;

  // Check if there are actually photos in the gallery.
  if ($image_count <= 1) {
    return t('No photos currently available.');
  }
  // If the number of images in the gallery is less than our limit,
  // make that the new limit.
  if ($image_count < $limit) {
    $limit = $image_count;
  }

  $images = array();
  $link = l(t('View all @count images', array('@count' => $image_count)), 'node/' . $element['#item']['nid']);

  // Gallery images are noderefs in the gallery node, so we need 
  // to look those up too, to get the actual images.
  // We only want to show 2 images.
  for ($i = 0; $i < $limit; $i++) {
    $image = node_load($gallery->field_pages[$i]['nid']);
    // Make images popup in thickbox.
    $images[] = l(theme('imagecache', 'bg_sample_photo_thumb', $image->field_image[0]['filepath']), $image->field_image[0]['filepath'], array('html' => TRUE, 'attributes' => array('class' => 'thickbox', 'rel' => 'gallery-' . $element['#item']['nid'])));
  }

  $output = theme('item_list', $images, NULL, 'ul', array('class' => 'product-photos'));
  $output .= '<div class="sample-photos-link ref-link">' . $link . '</div>';

  return $output;
}'
*/

/**
 * Custom formatter theme function to show thumbnail of main image from the referenced node (camera/lens).
 */
/*
function theme_gearfinder_formatter_thumbnail($element) {
  if (empty($element['#item'])) {
    return '';
  }

  // Load the full referenced node.
  $refnode = node_load($element['#item']['nid']);
  // Check if there is a main image set.
  if (!isset($refnode->field_main_image[0]['filepath'])) {
    return '';
  }

  $image = l(theme('imagecache', 'bg_thumb_140x92', $refnode->field_main_image[0]['filepath']), 'node/' . $refnode->nid, array('html' => TRUE));
  $title = l($refnode->title, 'node/' . $refnode->nid);

  // The "+ All cameras" (or lenses) link is added in the theme layer, because the noderefs cannot be grouped here.
  $output = '<div class="more-item-thumb">' . $image . '</div>';
  $output .= '<div class="more-item-title ref-link">' . $title .'</div>';

  return $output;
}
*/
/**
 * Custom formatter theme function to show a thumbnail of Thumbnail Image from the referenced article node.
 */
/*
function theme_gearfinder_formatter_bgtipsthumb($element) {
  if (empty($element['#item'])) {
    return '';
  }

  // Load the full referenced node.
  $refnode = node_load($element['#item']['nid']);

  // Check if there is a main image set.
  if (!isset($refnode->field_image[0]['filepath'])) {
    return '';
  }

  $image = l(theme('imagecache', 'bg_thumb_90x60', $refnode->field_image[0]['filepath']), 'node/' . $element['#item']['nid'], array('html' => TRUE));
  $output = '<div class="bgtips-item-thumb">' . $image . '</div>';

  return $output;  
}
*/

/**
 * Output the product specifications as two tables.
 */
/*
function theme_gearfinder_product_specifications($specs_definition) {
  // Format results into two tables
  $split_point = ceil(count($specs_definition['fields']) / 2);

  // Theme the two tables
  $table1 = theme('table', array(), array_slice($specs_definition['fields'], 0, $split_point), array('class' => 'table-first'));
  $table2 = theme('table', array(), array_slice($specs_definition['fields'], $split_point), array('class' => 'table-second'));

  $output = '<div class="product-specifications">' .
    '<div class="field-label">' . t('Product specifications') . '</div>' . $table1 . $table2 .
  '</div>';

  return $output;
}
*/