<?php

/**
 * Admin settings form for Gearfinder import
 * menu item: admin/settings/gearfinder_import
 */
function gearfinder_import() {
    $form['#attributes'] = array('enctype' => "multipart/form-data");
    $form['gearfinder_import_skis'] = array(
        '#type' => 'file',
        '#title' => 'Upload file to import skis',
        //	'#default_value' => $filepath,
        '#description' => 'CSV file containing the skis to import.<br/>
            The first row with headers is ignored. Fields are separated by comma.',
    );
    $form['gearfinder_import_boots'] = array(
        '#type' => 'file',
        '#title' => 'Upload file to import boots',
        //	'#default_value' => $filepath,
        '#description' => 'CSV file containing the skis to import.',
    );
    $form['#submit'][] = 'gearfinder_import_form_submit';
    return system_settings_form($form);
}

function gearfinder_import_form_submit($form, &$form_state) {

    module_load_include('inc', 'parser_csv', 'parser_csv_parser');
    $destination = file_directory_path();

    //import file for skis
    if (!move_uploaded_file($_FILES['files']['tmp_name']['gearfinder_import_skis'], $destination . '/' . $_FILES['files']['name']['gearfinder_import_skis'])) {
        watchdog('file', 'Upload error. Could not move uploaded file %file to destination %destination.', array('%file' => $_FILES['files']['name']['gearfinder_import_skis'], '%destination' => $destination));
        drupal_set_message(t('File to import skis not selected'));
    } else {
        $it = new ParserCSVIterator($destination . '/' . $_FILES['files']['name']['gearfinder_import_skis']);
        $rows = parser_csv_parse($it, ',');
        if (count($rows) == 0) {
            drupal_set_message('Could not open CSV file. Please verify the file ' . $_FILES['files']['name']['gearfinder_import_skis'] . ' exists.');
        }
        //skip the first row with headings
        $dud = array_shift($rows);
        $upd = 0; $crt = 0;
        foreach ($rows as $row) {
            if ($row[1] == '' || $row[2] == '') {
              // if field brand or model empty
                continue;
            }
            /*     $body = $row[14];
              $body = gearfinder_import_fixSmartfQuotes($body);
              $body = htmlentities($body); */
            $body = '';
            //explode - in year
            $years = explode('-', $row[4]);
            if (strpos($years[1], '20') === 0) {
                $year = $years[1];
            } else {
               //if year is indicated by two digits
                $year = '20' . $years[1];
            }
            //Update if the node exist
            $sql_nid = 'select nid from node where type = "product_ski" and title = "' . trim($row[1]) . ' ' . trim($row[2]) . ' (' . trim($year) . ')' . '"';
            $query_nid = db_query($sql_nid);
            $result_nid = db_result($query_nid);
            if ($result_nid !== false) {
                $node = node_load($result_nid);
                $upd++;
            } else {
                //Create new node
                $node = new stdClass();
                $node->title = $row[2];
                $node->created = time();
                $crt++;
            }
            $node->body = $body;
            $node->type = 'product_ski';
            $node->changed = time();
            $node->uid = 1;
            $node->status = 1;
            $node->field_product_year[0]['value'] = $year;
            // not in content type:
            //$node->active = 1;

            $node->promote = 0;

            //get the brand node nid
            $sql = 'select nid from node where type = "brand" and title = "' . $row[1] . '"';
            $query = db_query($sql);
            $result = db_result($query);
            // If brand is already exist
            if ($result !== false) {
                $node->field_product_brand[0]['nid'] = $result;
            } else {
                //If brand do not yet exist we create a new "brand" node
                $brand_node = new stdClass();
                $brand_node->title = $row[1];
                $brand_node->body = '';
                $brand_node->type = 'brand';
                $brand_node->created = time();
                $brand_node->changed = time();
                $brand_node->uid = 1;
                $brand_node->status = 1;
                $brand_node->promote = 0;
                node_save($brand_node);
                $sql2 = 'select nid from node where type = "brand" and title = "' . $row[1] . '"';
                $query2 = db_query($sql2);
                $result2 = db_result($query2);
                $node->field_product_brand[0]['nid'] = $result2;
            }
            $node->field_product_model[0]['value'] = $row[2];

            if ($row[0] != '' && $row[0] != '0') {
                $node->field_main_photo[0]['nid'] = $row[0];
                $node->field_thumbnail[0]['nid'] = $row[0];
            }
            // is always product
            $node->field_is_product[0]['value'] = 1; 
            $node->field_ski_binding[0]['value'] = $row[5];

            //get the length field;
            //old length parsing:
            /*
              //explode comas
              $lengths = explode(',',$row[10]);
              if(count($lengths) < 1) {
              //try to explode by -
              $lengths = explode('-',$row[10]);
              }
              $len = array();
              foreach($lengths as $length) {
              $len[] = array('value'=>$length);
              }
              $node->field_ski_lengths = $len;
             */
            //new length parsing:

            $node->field_ski_rating_corduroyperform[0]['value'] = '';

            $node->field_ski_lengths[0]['value'] = $row[10];

            $node->field_ski_rating_overall[0]['value'] = $row[21];
            $node->field_ski_rating_flotation[0]['value'] = $row[22];
            $node->field_ski_rating_stabilityspeed[0]['value'] = $row[23];
            $node->field_ski_rating_maneuver[0]['value'] = $row[24];
            $node->field_ski_rating_energylivelines[0]['value'] = $row[25];
            $node->field_ski_rating_forgiveness[0]['value'] = $row[26];
            $node->field_ski_rating_crudperform[0]['value'] = $row[27];
            $node->field_ski_rating_hardsnowgrip[0]['value'] = $row[28];
            $node->field_ski_rating_versatility[0]['value'] = $row[29];
            switch ($row[30]) {
                case 'Hard Snow':
                    $node->field_product_category[0]['value'] = 1;
                    break;
                case 'Mixed Snow':
                    $node->field_product_category[0]['value'] = 2;
                    break;
                case 'Deep Snow':
                    $node->field_product_category[0]['value'] = 3;
                    break;
                case 'Mixed Snow Value':
                    $node->field_product_category[0]['value'] = 4;
                    break;
                case 'Hard Snow Value':
                    $node->field_product_category[0]['value'] = 5;
                    break;
            }
            //set the range
            /*
              // *** stability-speed
              '2.75'
              '3'
              '3.25'

              if($row[23] == '') {
              $stability_range = '';
              }
              else if($row[23] > 0 && $row[23] < 3) {
              $stability_range = '2.75';
              }
              else if($row[23] < 3.25) {
              $stability_range = '3';
              }
              else {
              $stability_range = '3.25';
              }
              $node->field_range_stability_speed[0]['value'] = $stability_range;

              $node->field_range_stability_speed[0]['value'] = $row[23] */
            //tip tail waist

            $node->field_ski_tiptailwaist[0]['value'] = $row[6];

            $crap = explode('-', $row[9]);
            if (count($crap) < 1) {
                //try to explode by /
                $crap = explode('/', $row[9]);
            }
            $craps = array();
            $width_range = array();
            foreach ($crap as $c) {
                $craps[] = array('value' => $c);
                //put the width into the range
                if ($c <= 80) {
                    $width_range[] = array('value' => '0-80');
                } else if ($c <= 95) {
                    $width_range[] = array('value' => '81-95');
                } else if ($c <= 110) {
                    $width_range[] = array('value' => '96-110');
                } else {
                    $width_range[] = array('value' => '110-10000');
                }
            }
            $node->field_ski_waistwidth = $craps;
            $node->field_range_waistwidth = $width_range;
            //do some more stupid logic for Y and N
            /* $gold = gearfinder_import_yesNo($row[31]);
              $node->field_gold_medal_gear[0]['value'] = $gold;
              $best = gearfinder_import_yesNo($row[32]);
              $node->field_best_value[0]['value'] = $best;
              $rocker = gearfinder_import_yesNo($row[33]);
              $node->field_product_rocker[0]['value'] = $rocker;
             */

            // not in content type:
            //$node->field_rank_hard_snow[0]['value'] = $row[43];
            //get quickness range
            /*
              // *** quickness
              '2.75'
              '3'
              '3.25'

              if($row[24] == '') {
              $quick = '';
              }
              else if($row[24] > 0 && $row[24] <  3) {
              $quick = '2.75';
              }
              else if($row[24] < 3.25) {
              $quick = '3';
              }
              else {
              $quick = '3.25';
              }
              $node->field_range_quickness[0]['value'] = $row[24]; */

            // not in content type:
            //$node->field_rank_mixed_snow[0]['value'] = $row[41];
            //get the rating range
            /*
              // *** Overall Rating
              '2.75'
              '3'
              '3.25'

              if($row[21] == '') {
              $rating = '';
              }
              else if($row[21] > 0 && $row[21] < 3) {
              $rating = '2.75';
              }
              else if($row[21] < 3.25) {
              $rating = '3';
              }
              else {
              $rating = '3.25';
              }
              $node->field_range_rating[0]['value'] = $row[21]; */

            // not in content type:
            //$node->field_rank_deep_snow[0]['value'] = $row[39];
            // not in content type:
            //$node->field_rank_hard_snow_value[0]['value'] = $row[35];
            // not in content type:
            //$node->field_rank_mixed_snow_value[0]['value'] = $row[37];
            //range for floatation
            /*
              // *** flotation-surfability
              '2.75'
              '3'
              '3.25'

              if($row[22] == '') {
              $float = '';
              }
              else if($row[22] > 0 && $row[22] < 3) {
              $float = '2.75';
              }
              else if($row[22] < 3.25) {
              $float = '3';
              }
              else {
              $float = '3.25';
              }

              $node->field_range_flotation_surf[0]['value'] = $row[22]; */
            //range for fun
            /*
              // *** forgiveness-fun
              '2.75'
              '3'
              '3.25'

              if($row[26] == '') {
              $fun = '';
              }
              else if($row[26] > 0 && $row[26] < 3) {
              $fun = '2.75';
              }
              else if($row[26] < 3.25) {
              $fun = '3';
              }
              else {
              $fun = '3.25';
              }
              $node->field_range_forgiveness_fun[0]['value'] = $row[26]; */
            //crud performance range
            /*
              // *** crud-performance
              '2.75'
              '3'
              '3.25'

              if($row[27] == '') {
              $crud = '';
              }
              else if($row[27] > 0 && $row[27] < 3) {
              $crud = '2.75';
              }
              else if($row[27] < 3.25) {
              $crud = '3';
              }
              else {
              $crud = '3.25';
              }
              $node->field_range_crud_performance[0]['value'] = $crud; */
            //range for hard snow grip
            /*
              // *** hard-snow-grip
              '2.75'
              '3'
              '3.25'

              if($row[28] == '') {
              $hard = '';
              }
              else if($row[28] > 0 && $row[28] < 3) {
              $hard = '2.75';
              }
              else if($row[28] < 3.25) {
              $hard = '3';
              }
              else {
              $hard = '3.25';
              }
              $node->field_range_hard_snow_grip[0]['value'] = $hard; */

            //get the level
            //row[16-20]
            $levels = array();
            if (gearfinder_import_yesNo($row[16])) {
                $levels[] = array('value' => 1);
            }
            if (gearfinder_import_yesNo($row[17])) {
                $levels[] = array('value' => 2);
            }
            if (gearfinder_import_yesNo($row[18])) {
                $levels[] = array('value' => 3);
            }
            if (gearfinder_import_yesNo($row[19])) {
                $levels[] = array('value' => 4);
            }
            if (gearfinder_import_yesNo($row[20])) {
                $levels[] = array('value' => 5);
            }
            $node->field_product_level = $levels;
            //strip comma from price
            $price = str_replace(',', '', $row[11]);
            $node->field_product_price[0]['value'] = $price;
            //get price range
            /*
              // *** Price
              '0-600'
              '601-750'
              '751'
             */
            if ($price > 750) {
                $price_range[] = '751';
            } else if ($price <= 750 && $price > 600) {
                $price_range[] = '601-750';
            } else {
                $price_range[] = '0-600';
            }

            $node->field_range_price[0]['value'] = $price_range;
            $node->field_product_map[0]['value'] = str_replace(',', '', $row[12]);
            //convert gender to full text
            if ($row[3] == 'M' || $row[3] == 'C') {
                $gender = 'Male';
            } else {
                $gender = 'Female';
            }
            $node->field_product_gender[0]['value'] = $gender;

            $node->field_product_tested[0]['value'] = gearfinder_import_yesNo($row[13]);

            if ($row[14] != '') {
                $review = gearfinder_import_fixSmartfQuotes($row[14]);
                $review = htmlentities($review);
                $node->field_product_review[0]['value'] = $review;
            }
            if ($row[15] != '') {
                $notes = gearfinder_import_fixSmartfQuotes($row[15]);
                $notes = htmlentities($notes);
                $node->field_product_notes[0]['value'] = $notes;
            }

            //generate awards
            $awards = array();
            if (gearfinder_import_yesNo($row[31])) {
                //31.	Gold Medal Gear
                $awards[] = array('value' => 6);
            }
            if (gearfinder_import_yesNo($row[32])) {
                //32.	Best Value
                $awards[] = array('value' => 7);
            }
            if (gearfinder_import_yesNo($row[33])) {
                //33.	Rocker
                $awards[] = array('value' => 8);
            }
            if (gearfinder_import_yesNo($row[34])) {
                //34.	Winner - Hard Snow
                $awards[] = array('value' => 1);
            }
            if (gearfinder_import_yesNo($row[36])) {
                //36.	Winner - Mixed Snow
                $awards[] = array('value' => 2);
            }
            if (gearfinder_import_yesNo($row[38])) {
                //38.	Winner - Deep Snow
                $awards[] = array('value' => 3);
            }
            /*
            if (gearfinder_import_yesNo($row[40])) {
                //40.	Winner - Value
                $awards[] = array('value' => 4);
            }*/
            $node->field_product_awards = $awards;

            //add gear tearm
            $node->taxonomy[2] = taxonomy_get_term(2);

            //add brand term
            $sql = 'select tid from term_data where vid = 5 and name = "%s"';
            $query = db_query($sql, $row[1]);
            $result = db_result($query);
            if ($result !== false) {
                $node->taxonomy[$result] = taxonomy_get_term($result);
            }
            // Extract the brand.
            $brand = $row[1];
            // Get the model name.
            $model = trim($node->field_product_model[0]['value']);
            // Get the year.
            $year = trim($node->field_product_year[0]['value']);
            // Combine the fields.
            $title = trim(($brand ? trim($brand) . ' ' : '') . ($model ? $model . ' ' : '') . ($year ? '(' . $year . ')' : ''));
            $node->title = $title;
            node_save($node);
            drupal_set_message(t('saved ski %product', array('%product' => $node->nid . ' ' . $node->title)));
        }
        drupal_set_message(t('%count1 products was created and %count2 products was updated successfully', array('%count1' => $crt, '%count2' => $upd )));
    }

    //import file for boots
    if (!move_uploaded_file($_FILES['files']['tmp_name']['gearfinder_import_boots'], $destination . '/' . $_FILES['files']['name']['gearfinder_import_boots'])) {
        watchdog('file', 'Upload error. Could not move uploaded file %file to destination %destination.', array('%file' => $_FILES['files']['name']['gearfinder_import_boots'], '%destination' => $destination));
        drupal_set_message(t('File to import boots not selected'));
    } else {

        $it = new ParserCSVIterator($destination . '/' . $_FILES['files']['name']['gearfinder_import_boots']);
        $rows = parser_csv_parse($it, ',');
        if (count($rows) == 0) {
            drupal_set_message('Could not open CSV file. Please verify the file ' . $_FILES['files']['name']['gearfinder_import_boots'] . ' exists.');
        }
        //skip the first row with headings
        $dud = array_shift($rows);
        $upd = 0; $crt = 0;
        foreach ($rows as $row) {
            if ($row[1] == '' || $row[2] == '') {
              // if field brand or model empty
                continue;
            }
            //explode - in year
            $years = explode('-', $row[4]);
            if (strpos($years[1], '20') === 0) {
                $year = $years[1];
            } else {
               //if year is indicated by two digits
                $year = '20' . $years[1];
            }
            //Update if the node exist
            $sql_nid = 'select nid from node where type = "product_boot" and title = "' . trim($row[1]) . ' ' . trim($row[2]) . ' (' . trim($year) . ')' . '"';
            $query_nid = db_query($sql_nid);
            $result_nid = db_result($query_nid);
            if ($result_nid !== false) {
                $node = node_load($result_nid);
                $upd++;
            } else {
                //Create new node
                $node = new stdClass();
                $node->title = $row[2];
                $node->created = time();
                $crt++;
            }
            $node->body = '';
            $node->type = 'product_boot';
            $node->changed = time();
            $node->uid = 1;
            $node->status = 1;
            $node->active = 1;
            $node->promote = 0;
            $node->field_product_year[0]['value'] = $year;

            $node->field_boot_sizes[0]['value'] = $row[8];
            $node->field_new[0]['value'] = gearfinder_import_yesNo($row[5]);
            $node->field_product_tested[0]['value'] = gearfinder_import_yesNo($row[16]);
            $node->field_product_reviewed[0]['value'] = gearfinder_import_yesNo($row[17]);
            if ($row[19] != '') {
                $notes = gearfinder_import_fixSmartfQuotes($row[19]);
                $notes = htmlentities($notes);
                $node->field_product_notes[0]['value'] = $notes;
            }
            if ($row[18] != '') {
                $review = gearfinder_import_fixSmartfQuotes($row[18]);
                $review = htmlentities($review);
                $node->field_product_review[0]['value'] = $review;
            }
            //get the brand node nid
            $sql = 'select nid from node where type = "brand" and title = "' . $row[1] . '"';
            $query = db_query($sql);
            $result = db_result($query);
            // If brand is already exist
            if ($result !== false) {
                $node->field_product_brand[0]['nid'] = $result;
            } else {
                //If brand do not yet exist we create a new "brand" node
                $brand_node = new stdClass();
                $brand_node->title = $row[1];
                $brand_node->body = '';
                $brand_node->type = 'brand';
                $brand_node->created = time();
                $brand_node->changed = time();
                $brand_node->uid = 1;
                $brand_node->status = 1;
                $brand_node->promote = 0;
                node_save($brand_node);
                $sql2 = 'select nid from node where type = "brand" and title = "' . $row[1] . '"';
                $query2 = db_query($sql2);
                $result2 = db_result($query2);
                $node->field_product_brand[0]['nid'] = $result2;
            }
            $node->field_product_model[0]['value'] = $row[2];

            //convert gender to full text
            if ($row[3] == 'M' || $row[3] == 'C') {
                $gender = 'Male';
            } else {
                $gender = 'Female';
            }
            $node->field_product_gender[0]['value'] = $gender;
            //get the level
            //row[16-20]
            $levels = array();
            if ($row[9] == 'Y') {
                $levels[] = array('value' => 1);
            }
            if ($row[10] == 'Y') {
                $levels[] = array('value' => 2);
            }
            if ($row[11] == 'Y') {
                $levels[] = array('value' => 3);
            }
            if ($row[12] == 'Y') {
                $levels[] = array('value' => 4);
            }
            if ($row[13] == 'Y') {
                $levels[] = array('value' => 5);
            }
            $node->field_product_level = $levels;
            //get the widths
            $width_lists = explode('-', $row[6]);
            $widths = array();
            $range_widths = array();
            $found_width1 = '';
            $found_width2 = '';
            $found_width3 = '';
            foreach ($width_lists as $w) {
                $widths[] = array('value' => $w);
                if ($w < 100 && $found_width1 == '') {
                    $range_widths[] = array('value' => '0-99');
                    $found_width1 = 1;
                } else if ($w < 103 && $found_width2 == '') {
                    $range_widths[] = array('value' => '100-102');
                    $found_width2 = 1;
                } else if ($found_width3 == '') {
                    $range_widths[] = array('value' => '102-10000');
                    $found_width3 = 1;
                }
            }
            $node->field_last_width = $widths;
            //get the flex
            $flex_list = explode('-', $row[7]);
            $flexes = array();
            $range_flex = array();
            foreach ($flex_list as $flex) {
                $flexes[] = array('value' => $flex);
                //build range values
                if ($flex < 80) {
                    $range_flex[] = array('value' => '0-79');
                } else if ($flex < 101) {
                    $range_flex[] = array('value' => '80-100');
                } else if ($flex < 121) {
                    $range_flex[] = array('value' => '101-120');
                } else if ($flex < 136) {
                    $range_flex[] = array('value' => '121-135');
                } else {
                    $range_flex[] = array('value' => '136-10000');
                }
            }
            $node->field_boot_flex = $flexes;
            $node->field_main_photo[0]['nid'] = $row[0];
            $node->field_thumbnail[0]['nid'] = $row[0];
            //get prices
            $price_list = explode('-', $row[14]);
            $prices = array();
            $range_price = array();
            foreach ($price_list as $p) {
                $prices[] = array('value' => $p);
                //build range values
                if ($p < 601) {
                    $range_price[] = array('value' => '0-600');
                } else if ($p < 751) {
                    $range_price[] = array('value' => '601-750');
                } else {
                    $range_price[] = array('value' => '751');
                }
            }
            $node->field_product_price = $prices;
            //get map
            $map_list = explode('-', $row[15]);
            $maps = array();
            foreach ($map_list as $m) {
                $maps[] = array('value' => $m);
            }
            $node->field_product_map = $maps;
     //     $node->field_is_product[0]['value'] = gearfinder_import_yesNo($row[20]);
            // is always product
            $node->field_is_product[0]['value'] = 1;
            //get flex range
            /*
              0-79|Under 80
              80-100|80-100
              101-120|101-120
              121-135|121-135
              136-10000|136+
             */
            $node->field_range_flex = $range_flex;
            //get price rance
            /*
              0-600|Under $600
              601-750|$601-750
              751|$751+
             */
            $node->field_range_price = $range_price;
            //get last width range
            /*
              0-99|Under 100 (race)
              100-102|100-102 (average)
              102-10000|Over 102 (comfort)
             */
            $node->field_range_lastwidth = $range_widths;



            //add gear tearm
            $node->taxonomy[2] = taxonomy_get_term(2);
            //boots
            $node->taxonomy[9] = taxonomy_get_term(9);
            //add brand term
            $sql = 'select tid from term_data where vid = 5 and name = "%s"';
            $query = db_query($sql, $row[1]);
            $result = db_result($query);
            $node->taxonomy[$result] = taxonomy_get_term($result);
            // Extract the brand.

            $brand = $row[1];
            // Get the model name.
            $model = trim($node->field_product_model[0]['value']);
            // Get the year.
            $year = trim($node->field_product_year[0]['value']);
            // Combine the fields.
            $title = trim(($brand ? trim($brand) . ' ' : '') . ($model ? $model . ' ' : '') . ($year ? '(' . $year . ')' : ''));
            $node->title = $title;

            node_save($node);
            drupal_set_message(t('saved boot %product', array('%product' => $node->nid . ' ' . $node->title)));
        }
        drupal_set_message(t('%count1 products was created and %count2 products was updated successfully', array('%count1' => $crt, '%count2' => $upd )));
    }
}

function gearfinder_import_yesNo($value) {
    if ($value == 'Y' || $value == 'y' || $value == 'X' || $value == 'x') {
        $flag = 1;
    } else {
        $flag = 0;
    }
    return $flag;
}

function gearfinder_import_fixSmartfQuotes($str, $replace_single_quotes = true, $replace_double_quotes = true, $replace_emdash = true, $use_entities = false) {

    $translation_table_ascii = array(
        145 => '\'',
        146 => '\'',
        147 => '"',
        148 => '"',
        151 => '-'
    );

    $translation_table_entities = array(
        145 => '&lsquo;',
        146 => '&rsquo;',
        147 => '&ldquo;',
        148 => '&rdquo;',
        151 => '&mdash;'
    );

    $translation_table = ($use_entities ? $translation_table_entities : $translation_table_ascii);

    if ($replace_single_quotes) {
        $str = preg_replace('#\x' . dechex(145) . '#', $translation_table[145], $str);
        $str = preg_replace('#\x' . dechex(146) . '#', $translation_table[146], $str);
    }

    if ($replace_double_quotes) {
        $str = preg_replace('#\x' . dechex(147) . '#', $translation_table[147], $str);
        $str = preg_replace('#\x' . dechex(148) . '#', $translation_table[148], $str);
    }

    if ($replace_emdash) {
        $str = preg_replace('#\x' . dechex(151) . '#', $translation_table[151], $str);
    }

    return $str;
}