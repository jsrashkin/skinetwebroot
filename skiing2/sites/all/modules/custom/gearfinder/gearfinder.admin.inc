<?php
// $Id$

/**
 * @file
 * Gearfinder administration items.
 */

/**
 * Admin settings form for Gearfinder
 */
function gearfinder_admin_settings(&$form_state) {
	// Get a list of vocabularies to select from.
	// $vocabs = array();
	// 	$vocabularies = taxonomy_get_vocabularies();
	// 	foreach ($vocabularies as $vid => $vocabulary) {
	// 	  $vocabs[$vid] = $vocabulary->name;
	// 	}
	
	$form = array();
	$form['gf'] = array(
	  '#type' => 'fieldset',
	  '#title' => t('Settings'),
	  '#collapsible' => TRUE,
	  '#collapsed' => FALSE,
	);
	$form['gf']['gearfinder_how_we_test_nid'] = array(
	  '#type' => 'textfield',
	  '#title' => t('How We Test Nid'),
	  '#description' => t('Which node is the "How We Test" Video?'),
	  '#default_value' => variable_get('gearfinder_how_we_test_nid', ''),
	);
	$form['gf']['gearfinder_how_we_test_link_text'] = array(
	  '#type' => 'textfield',
	  '#title' => t('How We Test Link Text'),
	  '#description' => t('What should the link say? Be short and sweet!'),
	  '#default_value' => variable_get('gearfinder_how_we_test_link_text', ''),
	);
	$form['gf']['gearfinder_how_we_test_text'] = array(
	  '#type' => 'textarea',
	  '#title' => t('How We Test Text'),
	  '#description' => t('Text for the Gearfinder landing page "How We Test" box.'),
	  '#default_value' => variable_get('gearfinder_how_we_test_text', ''),
	);
	$form['gf']['gearfinder_product_comparison_dek'] = array(
	  '#type' => 'textarea',
	  '#title' => t('Product Comparison Dek'),
	  '#description' => t('Text for the Gearfinder product comparison page. If this is blank, nothing will show.'),
	  '#default_value' => variable_get('gearfinder_product_comparison_dek', ''),
	);
	$form['gf']['gearfinder_map_explanation'] = array(
	  '#type' => 'textarea',
	  '#title' => t('MAP explanation'),
	  '#description' => t(''),
	  '#default_value' => variable_get('gearfinder_map_explanation', ''),
	);
	$form['gf']['gearfinder_gmg_text'] = array(
	  '#type' => 'textarea',
	  '#title' => t('Gold Metal Gear explanation'),
	  '#description' => t(''),
	  '#default_value' => variable_get('gearfinder_gmg_text', ''),
	);
	$form['gf']['gearfinder_bestvalue_explanation'] = array(
	  '#type' => 'textarea',
	  '#title' => t('Best Value explanation'),
	  '#description' => t(''),
	  '#default_value' => variable_get('gearfinder_bestvalue_explanation', ''),
	);
	$form['gf']['gearfinder_rocker_explanation'] = array(
	  '#type' => 'textarea',
	  '#title' => t('Rocker explanation'),
	  '#description' => t(''),
	  '#default_value' => variable_get('gearfinder_rocker_explanation', ''),
	);
	// LOGO
	$form['gf']['gearfinder_logo_nid'] = array(
	  '#type' => 'textfield',
	  '#title' => 'Gearfinder Header Image Nid',
	  '#description' => 'Image node nid for gearfinder header',
	  '#default_value' => variable_get('gearfinder_logo_nid',''),
	);
	// Show the logo if its present.
  if ($logo_nid = variable_get('gearfinder_logo_nid', FALSE)) {
		$imagenode = node_load($logo_nid);
		$logo_path = $imagenode->field_image[0]['filepath'];
		variable_set('gearfinder_logo_path', $logo_path);
    $form['gf']['gearfinder_logo_preview'] = array(
      '#type' => 'markup',
      '#value' => '<p>' . t('Header Preview:') . '</p>' . theme('image', $logo_path),
    );
  }
	return system_settings_form($form);
}