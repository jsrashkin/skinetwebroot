<?php


/**
* Implementation of hook_default_page_manager_handlers().
*/
function gearfinder_default_page_manager_handlers() {
	$handlers = array();
	$path = drupal_get_path('module', 'gearfinder') . '/variants';
	$files = drupal_system_listing('.inc$', $path, 'name', 0);
	foreach($files as $file) {
		include_once $file->filename;
		$handlers[$handler->name] = $handler;
	 }
	return $handlers;
}
/* NEED this to override variants or else we can't revert default ones that already been created.  Comment it for now
function gearfinder_default_page_manager_handlers_alter(&$handlers){
  $path = drupal_get_path('module', 'gearfinder') . '/variants';
  $files = drupal_system_listing('.inc$', $path, 'name', 0);
  foreach ($files as $file) {
    include ($file->filename);
    if (!empty($handler->name)) {
      $handlers[$handler->name] = $handler;
    }
  }
}
*/
/**
* Implementation of hook_default_page_manager_pages().
*/
function gearfinder_default_page_manager_pages() {
	$pages = array();
	$path = drupal_get_path('module', 'gearfinder') . '/pages';
	$files = drupal_system_listing('.inc$', $path, 'name', 0);
	foreach($files as $file) {
		unset($page);
		include_once $file->filename;
		$pages[$page->name] = $page;
	}
	return $pages;
}
