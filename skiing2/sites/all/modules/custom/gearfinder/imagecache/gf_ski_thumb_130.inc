<?php
$presets['gf_ski_thumb_130'] = array (
  'presetname' => 'gf_ski_thumb_130',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '0',
      'module' => 'imagecache_icon',
      'action' => 'imagecache_icon',
      'data' => 
      array (
        'width' => '130',
        'pad_width' => 1,
        'height' => '130',
        'pad_height' => 1,
        'upscale' => 0,
        'canvas_x_offset' => 'center',
        'canvas_y_offset' => 'top',
        'bg_hex' => 'ffffff',
        'border_hex' => '',
        'border_size' => '0',
        'ratio_landscape' => '',
        'ratio_portrait' => '',
      ),
    ),
  ),
);