<?php
// Main product photo
$presets['gf_product_compare'] = array (
  'presetname' => 'gf_product_compare',
  'actions' =>
  array (
    0 =>
    array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' =>
      array (
        'width' => '135',
        'height' => '200',
        'upscale' => 0,
      ),
    ),
  ),
);
