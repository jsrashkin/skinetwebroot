<?php
/**
 * Implementation of hook_ctools_content_types()
 */
function bonnier_common_related_topics_ctools_content_types(){
  return array(
    'single' => TRUE,
    'title' => t('Related Topics'),
    'description' => t('A list of topics related to a term'),
    'category' => 'Bonnier',
    'hook theme' => 'bonnier_common_related_topics_theme',
  );
}

/**
 * Implementation of hook_block
 */
function bonnier_common_related_topics_theme(&$theme) {
  $path = drupal_get_path('module', 'bonnier_common') . '/plugins/content_types/related_topics';
  $theme['related_topics'] = array(
    'template' => 'related-topics',
    'arguments' => array('title' => NULL, 'items' => NULL, 'css_class' => NULL),
    'path' => $path,
  );
}

/**
 * Template preprocess related_topics
 */
function template_preprocess_related_topics(&$vars){
  // Insert template preprocess logic here
}

/**
 * Output function for the 'related_topics' content type.
 */
function bonnier_common_related_topics_content_type_render($subtype, &$conf, $panel_args, $context){
  $tid = $panel_args[0];
  
  if($conf['topic_limit'])
    $limit = 'LIMIT ' . $conf['topic_limit'];
  else
    $limit = '';

  if(!$conf['topic_order'])
    $order = 'DESC';
  else
    $order = $conf['topic_order'];
    
  $block = new stdClass();

  if($tid){
    $results = db_query('
      SELECT
        td.tid,
        td.name,
        td.description,
        tc.node_count
      FROM
       (
        SELECT
          nid
         FROM
          {term_node} tn
         WHERE
          tn.tid = %d
        ) nids
        INNER JOIN {term_node} tn
          ON tn.nid = nids.nid
        INNER JOIN {term_data} td
          ON td.tid = tn.tid
        INNER JOIN {term_node_count} tc
          ON td.tid = tc.tid
      WHERE
        td.tid != %d
      GROUP BY
        td.tid
      ORDER BY
        tc.node_count ' . $order . '
      ' . $limit . '
    ', $tid, $tid);
    $items = array();
    while ($related = db_fetch_object($results)) {
      $items[$related->tid] = l($related->name, taxonomy_term_path($related), array('rel' => 'tag', 'title' => strip_tags($related->description)));
    }

    $block->items = theme('item_list', $items, NULL, 'ul');
  }
  else {
    $block->items = '';
  }
  
  if($conf['override_title']){
    $block->title = $conf['override_title_text'];
    $conf['override_title'] = false;
  }    
  $block->content = theme('related_topics', $block->title, $block->items);

  return $block;
}

/**
 * Returns an edit form for the related_topics.
 */
function bonnier_common_related_topics_content_type_edit_form(&$form, &$form_state) {
  $conf = $form_state['conf'];

  $form['topic_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Show how many terms?'),
    '#size' => 4,
    '#default_value' => $conf['topic_limit'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );
  
  $form['topic_order'] = array(
    '#type' => 'select',
    '#title' => t('Ordering'),
    '#options' => array('DESC' => t('Popular to unpopular'), 'ASC' => t('Unpopular to popular')),
    '#default_value' => $conf['list_type'],
  );

}

/**
 *  Content type form settings
 */
function bonnier_common_related_topics_content_type_edit_form_submit(&$form, &$form_state) {
  foreach (element_children($form) as $key) {
    if (!empty($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 *  Content type submit settings
 */
function bonnier_common_related_topics_content_type_admin_title($subtype, $conf, $context) {
  return t('Related Topics (Bonnier Common)');
}
