<?php

function laserfist_search_search_results_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Search Results'),
    'description' => t('Search Results'),
    'category' => 'Apache SOLR Search',
  );
}

function laserfist_search_search_results_content_type_render($subtype, $conf, $panel_args, $context) {

	$menu = menu_get_item();
	$search_path = $menu['path'];
	$content = laserfist_apachesolr_search_view($search_path);

  $block->content = $content;
  $block->title = '';
  $block->delta = 'laserfist_search_results';
  return $block;
}
