<?php

function laserfist_panel_node_form_panel_node_form_alter(&$form, &$form_state) {
  $form['panel_contexts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Applicable Contexts'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $node = $form['#node'];
  $panel_contexts = $node->panel_contexts;

  $contexts = laserfist_panel_node_get_contexts();
  $options = array();
  foreach ($contexts as $name => $context) {
    $options[$name] = $context['title'];
  }
  $form['panel_contexts']['contexts'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
  );
  
  // Assuming $panel_contexts is populated was creating an error in form.inc
  // on node/add/panel
  if (!empty($panel_contexts)) {
    $form['panel_contexts']['contexts']['#default_value'] = $panel_contexts;
  }
}

