<?php

function laserfist_panel_node_get_contexts() {
  $contexts = module_invoke_all('laserfist_panel_node_contexts');
  return $contexts;
}

function laserfist_panel_node_laserfist_panel_node_contexts() {
  return array(
    'page_node' => array(
      'title' => t('Node being Display / Channel Node/Term'),
      'callback' => 'laserfist_panel_node_context_handler',
    ),
  );
}

function laserfist_panel_node_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  switch ($op) {
    case 'delete':
      laserfist_panel_node_delete($node);
      break;
    case 'insert':
    case 'update':
      laserfist_panel_node_set($node);
      break;
    case 'load':
      return array(
        'panel_contexts' => laserfist_panel_node_load($node)
      );
      break;
  }
}

function laserfist_panel_node_delete(&$node) {
  return db_query('DELETE FROM {laserfist_panel_node_contexts} WHERE nid = %d', $node->nid);
}

function laserfist_panel_node_set(&$node) {
  $data = serialize($node->contexts);
  if (laserfist_panel_node_load($node) == FALSE) {
    db_query("INSERT INTO {laserfist_panel_node_contexts} (nid,data) VALUES (%d,'%s')", $node->nid, $data);   
  }
  else {
    db_query("UPDATE {laserfist_panel_node_contexts} SET data = '%s' WHERE nid = '%d'", $data, $node->nid);   
  }
}

function laserfist_panel_node_load(&$node) {
  $query = 'SELECT data from {laserfist_panel_node_contexts} WHERE nid = %d';
  $results = db_query($query, $node->nid);
  while ($row = db_fetch_object($results)) {
    $data = unserialize($row->data);
  }
  return $data;
}

/*
  Implements hook_panel_node_context
*/
function laserfist_panel_node_panel_node_context(&$node) {
  $contexts = laserfist_panel_node_get_contexts();
  $active_contexts = array();
  if (!empty($node->panel_contexts)) {
    foreach ($node->panel_contexts as $key) {
      if ($key) {
        $active_contexts[$key] = $contexts[$key];
      } 
    } 
  }
  $panel_contexts = array();
  foreach ($active_contexts as $key => $context) {
    $callback = $context['callback'];
    $panel_context = $callback($node, $key);
    if (is_array($panel_context)) {
      $panel_contexts += $panel_context;
    }
  }
  return $panel_contexts;
}

function laserfist_panel_node_context_handler(&$node, $key) {
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $nid = arg(1);
  }

  if(!$nid) {
    return;
  }

  $contexts = array();
  ctools_include('context');

  // Node Displayed Context
  if($nid == $node->nid && arg(2)=='edit' ) {
    $context = ctools_context_create_empty('node');
    $context->identifier = t('Node being Displayed');
    $context->keyword = 'page_node';
  }
  else {
    $page_node = node_load($nid);
    $context = ctools_context_create('node', $page_node);
    $context->identifier = t('Node being Displayed');
    $context->keyword = 'page_node';
  } 
  $contexts['page-node'] = $context; 

  // Process context stuff based on channel node
  if ($nid == $node->nid && arg(2) != '') {
    laserfist_process_empty_channel_context($contexts);
  }
  else {
    laserfist_process_channel_context($contexts, $page_node);
  } 

  return $contexts;
}

function laserfist_process_empty_channel_context(&$contexts) {
  // Channel Term Node Associated
  $channel = laserfist_get_channel_context(TRUE);
  $contexts['channel-node'] = $channel;

  $term_context = ctools_context_create_empty('term', NULL); 
  $term_context->identifier = t('Channel Term'); 
  $term_context->keyword = 'channel_term'; 
  $contexts['channel-term'] = $term_context;
}

function laserfist_process_channel_context(&$contexts, $node) {
  // Channel Term Node Associated
  $channel = laserfist_get_channel_context(FALSE, $node);
  if ($channel) {
    $contexts['channel-node'] = $channel;
  }  

  // Channel Term
  $term = laserfist_get_channel_term($node);
  if ($term) {
    $term_context = ctools_context_create('term', $term);
    $term_context->identifier = t('Channel Term'); 
    $term_context->keyword = 'channel_term'; 
    $contexts['channel-term'] = $term_context;
  }
}

function laserfist_get_channel_context($empty = FALSE, &$node = NULL) {
  if ($empty) {
    $channel = ctools_context_create_empty('node');
    $channel->identifier = t('Channel Node');
    $channel->keyword = 'channel_node';
    return $channel;
  }

  $channel_node = laserfist_get_channel_node($node);
  if (!$channel_node) {
    return;
  }
  $channel = ctools_context_create('node', $channel_node);
  $channel->identifier = t('Channel Node');
  $channel->keyword = 'channel_node';
  return $channel;
}
