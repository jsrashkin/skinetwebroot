function laserfist_add_hint(el,h) { 
	elem = $(el); 
	var hint = h;

	$(elem).each(function() {
		if($(this).val() == '') {
			$(this).val(hint);
      $(this).addClass('form-hint');
		}
	});

	$(elem).blur(function() {
		if($(this).val() == '') {
			$(this).val(hint);
      $(this).addClass('form-hint');
		}
	});

	$(elem).focus(function() {
		if($(this).val() == hint) {
			$(this).val('');
      $(this).removeClass('form-hint');
		}
	});
}

$(document).ready( function() {
	$('.laserfist-admin-links').each( function(i,item) {
		$(item).parent().mouseover( function() {
			$(item).addClass('show');
		});
		$(item).parent().mouseout( function() {
			$(item).removeClass('show');
		});
	});
});
