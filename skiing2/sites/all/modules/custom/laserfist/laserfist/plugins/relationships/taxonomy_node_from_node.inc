<?php
// $Id: taxonomy_node_node.inc,v 1.2.2.2 2009/10/21 21:25:39 merlinofchaos Exp $

/**
 * @file
 * Plugin to provide an relationship handler for term from node.
 */

/**
 * Implementation of specially named hook_laserfist_relationships().
 */
function laserfist_taxonomy_node_from_node_ctools_relationships() {
  return array(
    'title' => t('Taxonomy Node from node'),
    'keyword' => 'node',
    'description' => t('Grabs the taxonomy node from a term attached to node'),
    'required context' => new ctools_context_required(t('Node'), 'node'),
    'context' => 'laserfist_taxonomy_node_from_node_context',
    'settings form' => 'laserfist_taxonomy_node_from_node_settings_form',
    'settings form validate' => 'laserfist_taxonomy_node_from_node_settings_form_validate',
    'defaults' => array('vid' => ''),
  );
  return $args;
}

/**
 * Return a new context based on an existing context.
 */
function laserfist_taxonomy_node_from_node_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('node', NULL);
  }

  $taxonomy_term = null;
  if (isset($context->data->taxonomy)) {
    foreach ($context->data->taxonomy as $term) {
      if ($term->vid == $conf['vid']) {
        $taxonomy_term = $term;    
      }
    }
  }

  // Grab the node representing the term
  if ($taxonomy_term) {
    $nid = laserfist_taxonomy_node_get_nid($taxonomy_term->tid);
    $node = node_load($nid);
  }
  if ($node) {
    return ctools_context_create('node', $node);
  }
}

/**
 * Settings form for the relationship.
 */
function laserfist_taxonomy_node_from_node_settings_form($conf) {
  $options = array();
  foreach (taxonomy_get_vocabularies() as $vid => $vocabulary) {
    $options[$vid] = $vocabulary->name;
  }
  $form['vid'] = array(
    '#title' => t('Vocabulary'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $conf['vid'],
    '#prefix' => '<div class="clear-block">',
    '#suffix' => '</div>',
  );

  return $form;
}
