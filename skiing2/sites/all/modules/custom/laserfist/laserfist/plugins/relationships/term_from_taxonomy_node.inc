<?php
// $Id$

/**
 * Definition of the plugin.
 */
function laserfist_term_from_taxonomy_node_ctools_relationships() {
  return array(
    'title' => t('Term from taxonomy node'),
    'keyword' => 'term',
    'description' => t('Get the term from a taxonomy node.'),
    'required context' => new ctools_context_required(t('Node'), 'node'),
    'context' => 'laserfist_term_from_taxonomy_node_context',
    'settings form' => 'laserfist_term_from_taxonomy_node_settings_form',
  );
}

/**
 * Return a new context based on an existing context.
 */
function laserfist_term_from_taxonomy_node_context($context = NULL, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('term', NULL);
  }

  if (function_exists('_taxonomynode_get_tid_from_nid')) {
    $tid = _taxonomynode_get_tid_from_nid($context->data->nid);
    $term = taxonomy_get_term($tid);
    if ($term) {
      return ctools_context_create('term', $term);
    }
  }
}

/**
 * Settings form for the relationship.
 */
function laserfist_term_from_taxonomy_node_settings_form($conf) {
  // We won't configure it in this case.
  return array();
}
