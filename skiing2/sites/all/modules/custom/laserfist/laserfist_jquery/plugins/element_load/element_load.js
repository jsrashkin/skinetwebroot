// This will check for dom elements based on IDs until the onload function is fired. Interval set at 30 ms  
// no reliance on external scripts
element_load = new (function() {
	var jobs = [];
	var active = true;

	this.on_element_load = function(func, element_id, fast) {
	  fast = typeof(fast) != 'undefined' ? fast : false;
		jobs[jobs.length] = {'id':element_id,'func':func,'fast':fast};
	};

	this.stop = function() {	
		actve = false;
	}

	var check_jobs = function() {
		for (var i in jobs) 	
		{
			job = jobs[i];
			var obj = document.getElementById(job.id);
			/* 
			  nextSibling check is to avoid IE operation aborted errors. 
			  Make sure your trigger has a nextSibling
			*/
			if (obj && (!active || (job.fast || obj.nextSibling))) {
				jobs.splice(i,1);	
				job.func();
			}	
		}
		if (active) {
			setTimeout(check_jobs,30);
		}
	}
	window.onload = function() {
		element_load.stop();	
	}
	check_jobs();
})();
