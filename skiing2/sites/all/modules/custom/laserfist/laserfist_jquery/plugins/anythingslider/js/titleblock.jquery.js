(function($) {

    $.titleBlock = {
    
        defaults: {
            
            removeTitle: true,
            fontSize: "20px"
            
        }
            
    }
    
    $.fn.extend({
        titleBlock:function(config) {
        
            var config = $.extend({}, $.titleBlock.defaults, config);
         
            return this.each(function() {
            
                var theImage    = $(this),
                    removeTitle = config.removeTitle,
                    fontSize = config.fontSize;
                
                theImage
                    .wrap("<div class='image'>")
                    .parent()
                    .append("<h2>")
                    .find("h2")
                    .html(theImage.attr('title'))
                    .wrapInner("<span style='font-size:" + fontSize + "';>")
                    .find("br")
                    .before("<span class='spacer'>&nbsp;</span>")
                    .after("<span class='spacer'>&nbsp;</span>");
                    
                if (removeTitle) {
                
                    theImage
                        .removeAttr("title");
                
                }

            
            })
        
        
        }
    
    })


})(jQuery);