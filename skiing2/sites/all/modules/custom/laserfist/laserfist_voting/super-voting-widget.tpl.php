<?php
	/*
		This is more of a demo file. Should create your own  and use the preprocessors provided
	*/
?>
<div id="super-voting-widget laserfist-voting-cid-<?php print $cid; ?>">
	<?php if ($up_class) : ?>
		<?php print $count_up; ?>
		<span id="vote_up_<?php print $cid; ?>" class="<?php print $up_class; ?>" title="<?php print $up_title; ?>"><?php print $up_link; ?></span>
	<?php endif; ?>

	<?php if ($down_class) : ?>
		<?php print $count_down; ?>
		<span id="vote_down_<?php print $cid; ?>" class="<?php print $down_class; ?>" title="<?php print $down_title; ?>"><?php print $down_link; ?></span>
	<?php endif; ?>
</div>
