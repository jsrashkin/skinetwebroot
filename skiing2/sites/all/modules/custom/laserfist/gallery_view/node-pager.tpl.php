<?php
/**
 * Available variables:
 *  $total - the total number of items.
 *  $current - the current item.
 *  $prev - link to the previous item.
 *  $next - link to the next item.
 */
?>
<?php if ($total > 1): ?>

  <div class="prev"><?php print $prev; ?></div>
  <div class="info"><?php print $current; ?> of <?php print $total; ?></div>
  <div class="next"><?php print $next; ?></div>

<?php endif; ?>
