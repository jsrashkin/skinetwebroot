<?php
/**
 * Implementation of hook_imagecache_default_presets().
 */
function gallery_view_imagecache_default_presets() {
  $presets = array();
  $presets['gallery_image'] = array (
    'presetname' => 'gallery_image',
    'actions' => array(
      0 => array(
        'weight' => '0',
        'module' => 'imagecache',
        'action' => 'imagecache_scale',
        'data' => array(
          'width' => '600',
          'height' => '',
          'upscale' => 0,
        ),
      ),
    ),
  );
	$presets['gallery_list_image'] = array (
		'presetname' => 'gallery_list_image',
		'actions' => array(
			0 => array(
				'weight' => '0',
				'module' => 'imagecache',
				'action' => 'imagecache_scale_and_crop',
				'data' => array(
					'width' => '50',
					'height' => '50',
				),
			),
		),
	);
  return $presets;
}
