<?php
// $Id: taxonomy_node_node.inc,v 1.2.2.2 2009/10/21 21:25:39 merlinofchaos Exp $

/**
 * @file
 * Plugin to provide an relationship handler for term from node.
 */

/**
 * Implementation of specially named hook_laserfist_relationships().
 */
function gallery_view_current_gallery_page_ctools_relationships() {
  return array(
    'title' => t('Current Gallery Page'),
    'keyword' => 'node',
    'description' => t('Grabs the Current Gallery Page'),
    'required context' => new ctools_context_required(t('Node'), 'node'),
    'context' => 'gallery_view_current_gallery_page_context',
    'settings form' => 'gallery_view_current_gallery_page_settings_form',
    'settings form validate' => 'gallery_view_current_gallery_page_settings_form_validate',
  );
  return $args;
}

/**
 * Return a new context based on an existing context.
 */
function gallery_view_current_gallery_page_context($context, $conf) {
  // If unset it wants a generic, unfilled context, which is just NULL.
  if (empty($context->data)) {
    return ctools_context_create_empty('node', NULL);
  }

	$gallery = $context->data;

	// Grab if defined
	if($_GET['pnid']) {
		$nid = $_GET['pnid'];
	} else {
		$nid = $gallery->field_pages[0]['nid'];
	}

	if($nid) {
		$node = node_load($nid);
		return ctools_context_create('node',$node);
	}
}

/**
 * Settings form for the relationship.
 */
function gallery_view_current_gallery_page_settings_form($conf) {
  $options = array();
  return $form;
}
