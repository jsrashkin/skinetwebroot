<?php
?>

<div class="left">
	<div class="image">
		<?php print $image; ?>
	</div>
</div>

<div class="right">
	<h4 class="title">
		<?php print $title; ?>
	</h4>
	<div class="dek">
		<?php print $dek; ?>
	</div>
</div>

<div class="clear-block"></div>
