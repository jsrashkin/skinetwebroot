<?php

function gallery_view_preprocess_gallery_list(&$vars) {
  // Verify that the list should be accessible.  Set the variable
  // 'gallery_view_show_list' to FALSE to hide this.
  if (variable_get('gallery_view_show_list', TRUE)) {
  	$item = node_load($vars['row']->node_node_data_field_pages_nid);		

  	$gallery_nid = $vars['row']->nid;

  	$url = 'node/' . $gallery_nid;

  	$img_path = bonnier_common_teaser_image($item);
  	$image = theme('imagecache', 'gallery_list_image', $img_path);
  	$vars['image'] = l($image, $url, array('html' => true, 'query' => 'pnid=' . $item->nid));

  	$vars['title'] = l(bonnier_common_title($item), $url, array('html' => TRUE, 'query' => 'pnid=' . $item->nid));
  	// If item has no dek, dfault to cutting off the body
  	$dek = bonnier_common_dek($item);
  	if (!$dek) {
  		$dek = substr($item->body, 0, 100);
  	}
  	$vars['dek'] = $dek;
  }

  // Don't show the List page, redirect to the main gallery page.
  else {
    drupal_goto('node/' . $vars['row']->nid);
  }
}
