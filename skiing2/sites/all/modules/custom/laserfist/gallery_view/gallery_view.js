/**
 * @file
 * 
 */

// Jump to the requested destination.
slide_jump = function() {
  // If the URL is not empty, go there.
  if (slide_start == true && slide_stop == false && slide_link != '' && slide_link != null) {
    $('#slide-button').addClass('please-wait');
    $(location).attr('href', slide_link);
  };
}

// Wait for the slideshow object to be ready, then add a click event.
$('#slide-button').ready(function() {
  // Grab the URL argument.
  var slide_auto = $(document).getUrlParam('slide');
  // If the argument is present and enabled, start the slideshow.
  if (slide_auto == 'true' && parseInt(slide_time) > 2) {
    // Set the DOM element to say it is active.
    $('#slide-button').addClass('active');
    $('#node-pager .prev').hide();
    $('#node-pager .next').hide();
    // Set the site-wide variable to say the slideshow is in process.
    slide_start = true;
    // Start the slideshow.
    $.timer(parseInt(slide_time), function(timer) {
      slide_jump();
      timer.stop();
    });
  };
});

// Interact when the user clicks on the button.
$('#slide-button').click(function() {
  // The slideshow is currently active, so stop it.
  if (slide_start == true) {
    // Tell the slider to stop.
    slide_start = false;
    slide_stop = true;
    // Set the DOM element to say it is active.
    $('#slide-button').removeClass('active');
    $('#node-pager .prev').show();
    $('#node-pager .next').show();
  }
  // The slideshow is not currently active, so start it.
  else {
    // Set the DOM element to say it is active.
    $('#slide-button').addClass('active');
    $('#node-pager .prev').hide();
    $('#node-pager .next').hide();
    // Set the global variable.
    slide_start = true;
    slide_stop = false;
    // Start the slideshow after a 1/2sec pause
    $.timer(500, function(timer) {
      slide_jump();
      timer.stop();
    });
  };
});
