<?php
// $Id: node.tpl.php,v 1.4 2008/09/15 08:11:49 johnalbin Exp $

/**
 * @file node.tpl.php
 *
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: Node body or teaser depending on $teaser flag.
 * - $picture: The authors picture of the node output from
 *   theme_user_picture().
 * - $date: Formatted creation date (use $created to reformat with
 *   format_date()).
 * - $links: Themed links like "Read more", "Add new comment", etc. output
 *   from theme_links().
 * - $name: Themed username of node author output from theme_user().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $submitted: themed submission information output from
 *   theme_node_submitted().
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $teaser: Flag for the teaser state.
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */
?>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"><div class="node-inner">

<a id="gallery-content" name="gallery-content"></a>

  <?php if ($unpublished): ?>
    <div class="unpublished"><?php print t('Unpublished'); ?></div>
  <?php endif; ?>

  <div class="content">

    <?php if ($image): ?>
      <div class="photo-box">
        <div class="image">
          <?php print $image; ?>
        </div>
        <div class="bottom-box">
          <?php if ($enlarge): ?>
            <div class="enlarge"><?php print $enlarge; ?></div>
          <?php endif; ?>
  				<?php if ($photo_credit): ?>
  					<div class="credit"><?php print '&copy; ' . $photo_credit; ?></div>
  				<?php endif; ?>
  			</div>
  		</div>
    <?php endif; ?>

		<?php if ($node_pager): ?>
		  <?php if ($node_slideshow): ?><div class="left"><?php endif; ?>
  		<div id="node-pager">
  			<?php print $node_pager; ?>
  		</div>
		  <?php if ($node_slideshow): ?></div><?php endif; ?>
		<?php endif; ?>
		<?php if ($node_slideshow): ?>
		  <?php if ($node_pager): ?><div class="right"><?php endif; ?>
  		<div id="node-slideshow">
  			<?php print $node_slideshow; ?>
  		</div>
		  <?php if ($node_pager): ?></div><?php endif; ?>
		<?php endif; ?>
		<?php if ($node_pager || $node_slideshow): ?>
		  <div class="clear"></div>
	  <?php endif; ?>

    <?php if ($above_node_content): ?>
      <div class="above-node-content">
        <?php print $above_node_content; ?>
      </div>
    <?php endif; ?>

    <h2><?php print $title; ?></h2>

		<div class="image-node-content">		
			<?php print $content; ?>
		</div>
    <div class="clear-block"></div>

<?php /*
    <div class="utility-links">
      <?php print $email; ?>
      <?php print $print; ?>
      <?php print $share; ?>
    </div>
    <div class="clear-block"></div>
*/ ?>

    <?php if ($below_node_content): ?>
      <div class="below-node-content">
        <?php print $below_node_content; ?>
      </div>
      <div class="clear-block"></div>
    <?php endif; ?>

  </div>

  <div class="related-info">
    <?php print $related_info; ?>   
  </div>

  <?php print $links; ?>

</div></div> <!-- /node-inner, /node -->
