<?php
/**
 * @file
 * Display a simple copyright line, for use on the site's footer.
 */

/**
 * Implementation of hook_ctools_content_types().
 */
function laserfist_baseline_copyright_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Site Copyright'),
    'description' => '',
    'category' => 'Laserfist Baseline',
    'defaults' => array('owner' => ''),
		'hook theme' => 'laserfist_baseline_copyright_theme',
  );
}

/**
 * Implementation of hook_content_type_render().
 */
function laserfist_baseline_copyright_content_type_render($subtype, $conf) {
  $year = date("Y");
  $owner = $conf['owner'];
  $content = theme('laserfist_baseline_copyright', $year, $owner);

  $block = new StdClass();
  $block->content = $content;
  $block->title = '';
  $block->delta = 'copyright';
	$block->css_class = 'site-copyright';

  return $block;
}

/**
 * Theme the output.
 */
function laserfist_baseline_copyright_theme(&$theme) {
	$theme['laserfist_baseline_copyright'] = array(
    'template' => 'laserfist_baseline_copyright',
		'arguments' => array('year' => NULL, 'owner' => NULL),
		'path' => drupal_get_path('module', 'laserfist_baseline') .'/plugins/content_types/copyright',
	);
}

/**
 * Implementation of template_preprocess().
 */
function template_preprocess_laserfist_baseline_copyright(&$vars) {
}

/**
 * Implementation of hook_content_type_edit_form() - show the settings form
 * for this content type.
 */
function laserfist_baseline_copyright_content_type_edit_form(&$form, &$form_state) {
  // Current settings.
  $conf = $form_state['conf'];

  // Add the name field.
  $form['owner'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Owner'),
    '#default_value' => (!empty($conf['owner']) ? $conf['owner'] : 'Bonnier Corporation'),
    '#description' => t('This is the name of the organization that owns this site, usually the name of the magazine.'),
  );
}

/**
 * Implementation of hook_FORM_ID_form_submit().
 */
function laserfist_baseline_copyright_content_type_edit_form_submit(&$form, &$form_state) {
  // Save the Owner field, skip the rest.
  $form_state['conf']['owner'] = $form_state['values']['owner'];
}
