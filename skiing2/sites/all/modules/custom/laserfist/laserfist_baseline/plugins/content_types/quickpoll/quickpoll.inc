<?php
/*
	This plugs into the Channel Node and has a tabbed (Articles/Galleries/Answers) 
	listing for the channel
*/
function laserfist_baseline_quickpoll_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Quick Poll'),
    'description' => t('Quick Poll'),
    'required context' => new ctools_context_required(t('Channel'),'node'),
    'category' => 'Laserfist Baseline',
		'hook theme' => 'laserfist_baseline_quickpoll_theme',
  );
}

function laserfist_baseline_quickpoll_content_type_render($subtype, $conf, $panel_args, $context) {

	// Retrieve the latest poll.
	$sql = db_rewrite_sql("SELECT MAX(n.created) FROM {node} n INNER JOIN {poll} p ON p.nid = n.nid WHERE n.status = 1 AND p.active = 1");
	$timestamp = db_result(db_query($sql));

	if ($timestamp) {
		$poll = node_load(array('type' => 'poll', 'created' => $timestamp, 'status' => 1));

		if ($poll->nid) {
			$poll = poll_view($poll, TRUE, FALSE, TRUE);
		}
	}

	$content = drupal_render($poll->content);
	
  $block->content = $content;
  $block->title = 'Quick Poll';
  $block->delta = 'quickpoll';
	$block->css_class = 'quick-poll';
  return $block;
}

function laserfist_baseline_quickpoll_theme(&$theme) {
}

function laserfist_baseline_quickpoll_content_type_edit_form(&$form, &$form_state) {
}
