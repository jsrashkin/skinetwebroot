<?php
/*
	This plugs into the Channel Node and has a tabbed (Articles/Galleries/Answers) 
	listing for the channel
*/
function laserfist_baseline_feature_slideshow_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Feature Slideshow'),
    'description' => t('Feature Slideshow'),
    'required context' => new ctools_context_required(t('Node being Viewed'),'node'),
    'category' => 'Laserfist Baseline',
		'hook theme' => 'laserfist_baseline_feature_slideshow_theme',
  );
}

function laserfist_baseline_feature_slideshow_content_type_render($subtype, $conf, $panel_args, $context) {
	
	$node = $context->data;
	$max = 5;

	$hook = 'feature_slideshow_content_gather';
	$caller_module = 'laserfist_baseline';

	$nodes = array();
	foreach(module_implements($hook) as $module) {
		if($module == $caller_module) { continue; }
		$func = $module . '_' .$hook;
		$return = $func($node);
		if(isset($return) || count($nodes) > $max) {
			$nodes = array_merge($nodes,$return);
		}
	}

	if(count($nodes) < $max) {
		$func = $caller_module.'_'.$hook;
		$nodes = $func($node);
	}

	drupal_alter($hook,$nodes);
	$content = theme('homepage_feature_slideshow_list',$nodes,$max);

  $block->content = $content;
  $block->title = '';
  $block->delta = 'feature_slideshow';
	$block->css_class = 'feature-slideshow';
	$block->css_id = 'homepage-feature-slideshow';
  return $block;
}

function laserfist_baseline_feature_slideshow_content_gather_alter(&$nodes) {
	global $user;
	if(!$user->uid) { return; }

	foreach($nodes as $i => $node) {
		if($node->status == 0) {
			unset($nodes[$i]);	
		}
	}
}

function laserfist_baseline_feature_slideshow_content_gather(&$node) {
	$items = array();
	list($items,$nids) = laserfist_grab_nodes($node,'field_feature_slideshow'); 
	$homepage = laserfist_get_homepage_node();
	// We are on the homepage, return
	if($homepage && $homepage->nid == $node->nid) {
		return $items;
	}

	// I need the list of nids
	$count = count($items);
	list($overrides,$o_nids) = laserfist_grab_nodes($homepage,'field_feature_slideshow');
	foreach($overrides as $override) {
		if($count >= $max) { break; }
		if(in_array($override->nid,$nids)) {
			continue;
		}
		$items[] = $override; 
		$count++;
	}
	return $items;
}

function template_preprocess_homepage_feature_slideshow_list(&$vars) {
	$nodes = $vars['nodes'];
	$count = 1;
	$items = array();
	foreach($nodes as $node) {
		$items[] = theme('homepage_feature_slideshow',$node);
		$count++;
	}
	$vars['items'] = $items;
	$vars['content'] = implode($items);
}

function template_preprocess_homepage_feature_slideshow(&$vars) {
	$node = $vars['node']; 

	$link = bonnier_common_link($node);
	$imagepath = bonnier_common_teaser_image($node);
	$dek = bonnier_common_dek($node);
	$image = theme('imagecache','feature_slideshow',$imagepath);
	$vars['title'] = l(bonnier_common_title($node),$link);
	$vars['teaser_image'] = l($image,$link,array('html'=>TRUE));
	$vars['dek'] = l($dek,$link,array('html'=>true));
}

function laserfist_baseline_feature_slideshow_theme(&$theme) {
	$theme['homepage_feature_slideshow_list'] = array(
		'template' => 'homepage-feature-slideshow-list',
		'arguments' => array('nodes' => array()),
		'path' => drupal_get_path('module','laserfist_baseline').'/plugins/content_types/feature_slideshow',
	);
	$theme['homepage_feature_slideshow'] = array(
		'template' => 'homepage-feature-slideshow',
		'arguments' => array('node' => null),
		'path' => drupal_get_path('module','laserfist_baseline').'/plugins/content_types/feature_slideshow',
	);
}

function laserfist_baseline_feature_slideshow_content_type_edit_form(&$form, &$form_state) {
  $form['#submit'][] = 'laserfist_baseline_channel_promos_content_type_edit_form_submit';
	$conf = $form_state['conf'];
	$default = $conf['noderef_fieldname'];
	$default = $default ? $default : 'field_feature_slideshow'; 
	$form['noderef_fieldname'] = array(
		'#title' => t('Node Reference Fieldname'),
		'#type' => 'textarea',
  	'#default_value' => $default,
    '#value' => $default,
	);
}

function laserfist_baseline_channel_promos_content_type_edit_form_submit(&$form, &$form_state) {
	$form_state['conf']['noderef_fieldname'] = $_POST['noderef_fieldname'];
}
