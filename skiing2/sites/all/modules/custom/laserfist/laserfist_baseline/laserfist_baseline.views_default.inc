<?php
/**
 * @file
 * Load default Views.
 */

/**
 * hook_views_default_views() - load some views.
 */
function laserfist_baseline_views_default_views() {
  $views = array();
  $path = dirname(__FILE__) . '/views/';

  // QnA block.
  include($path . 'view.bonnier_fresh_content.inc');
  $views[$view->name] = $view;

  return $views;
}
