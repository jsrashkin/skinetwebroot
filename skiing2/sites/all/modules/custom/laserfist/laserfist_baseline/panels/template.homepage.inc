<?php

$template = new stdClass;
$template->disabled = FALSE; /* Edit this to true to make a default template disabled initially */
$template->api_version = 1;
$template->name = 'homepage';
$template->template_type = 'homepage';
$template->category = '';
$template->title = 'Homepage';
$template->requiredcontexts = FALSE;
$template->contexts = array(
  '0' => array(
    'name' => 'panels_template_node',
    'id' => 1,
    'identifier' => 'Node being viewed',
    'keyword' => 'node',
  ),
);
$template->relationships = FALSE;
$display = new panels_display;
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = '';
$display->hide_title = FALSE;
$display->title_pane = 0;
$display->content = array();
$display->panels = array();
$template->display = $display;
