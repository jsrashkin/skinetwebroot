<?php

function laserfist_article_core_header_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Core Header'),
    'description' => t('Core Content Header'),
    'required context' => new ctools_context_required(t('Node being Viewed'),'node'),
    'category' => 'Article',
  );
}

function laserfist_article_core_header_content_type_render($subtype, $conf, $panel_args, $context) {
  $nid = $panel_args[0];
	$node = node_load($nid);
	$content = theme('core_header',$node);

  $block->content = $content;
  $block->title = '';
  $block->delta = 'core_header';
  return $block;
}
