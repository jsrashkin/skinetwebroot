<div class="photo-box template-<?php print $class; ?>">

  <div class="image">
    <?php print $image; ?>
  </div>

  <?php if ($title): ?>
    <h4 class="title"><?php print $title; ?></h4>
  <?php endif; ?>

  <?php if ($caption): ?>
    <div class="caption">
      <?php print $caption; ?>
    </div>
  <?php endif; ?>

  <?php if ($related_box): ?>
    <div class="related-box">
      <?php print $related_box; ?>
    </div>
  <?php endif; ?>

</div>
