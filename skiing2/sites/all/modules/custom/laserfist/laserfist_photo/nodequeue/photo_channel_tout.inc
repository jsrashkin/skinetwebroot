<?php
$queue = new stdClass();
$queue->title = 'Photo Channel Tout';
$queue->subqueue_title = '';
$queue->size = 2;
$queue->reverse = 0;
$queue->link = '';
$queue->link_remove = '';
$queue->roles = array();
$queue->types = array(
  'article',
  'blog_post',
  'laserfist_gallery',
  'photo',
  );
$queue->i18n = 1;
$queue->owner = 'nodequeue';
$queue->show_in_links = NULL;
$queue->show_in_tab = 1;
$queue->show_in_ui = 1;
$queue->reference = 0;
$queue->subqueues = array();
$queue->new = 1;
$queue->add_subqueue = array('Photo Channel Tout');
