<?php
$content['type']  = array (
  'name' => 'Topic Tout',
  'type' => 'topic_tout',
  'description' => 'This allows for editorial control over the topics displayed in a tout.',
  'title_label' => 'Title',
  'body_label' => '',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => false,
    'sticky' => false,
    'revision' => false,
  ),
  'upload' => '0',
  'addthis_nodetype' => 0,
  'forward_display' => 0,
  'scheduler' => 0,
  'scheduler_touch' => 0,
  'old_type' => 'topic_tout',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'nodewords_edit_metatags' => 1,
  'nodewords_basic_use_teaser' => false,
  'content_profile_use' => 0,
  'comment' => '2',
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => '0',
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
  'comment_upload' => '0',
  'comment_upload_images' => 'none',
  'flatcomments_remove_reply_link' => 
  array (
    'reply' => false,
  ),
  'fivestar' => 0,
  'fivestar_stars' => 5,
  'fivestar_labels_enable' => 1,
  'fivestar_label_0' => 'Cancel rating',
  'fivestar_label_1' => 'Poor',
  'fivestar_label_2' => 'Okay',
  'fivestar_label_3' => 'Good',
  'fivestar_label_4' => 'Great',
  'fivestar_label_5' => 'Awesome',
  'fivestar_label_6' => 'Give it @star/@count',
  'fivestar_label_7' => 'Give it @star/@count',
  'fivestar_label_8' => 'Give it @star/@count',
  'fivestar_label_9' => 'Give it @star/@count',
  'fivestar_label_10' => 'Give it @star/@count',
  'fivestar_style' => 'average',
  'fivestar_text' => 'dual',
  'fivestar_title' => 1,
  'fivestar_feedback' => 1,
  'fivestar_unvote' => 0,
  'fivestar_position_teaser' => 'hidden',
  'fivestar_position' => 'below',
  'fivestar_comment' => 0,
  'print_display' => 1,
  'print_display_comment' => 0,
  'print_display_urllist' => 1,
  'ant' => 0,
  'ant_pattern' => '',
  'ant_php' => '',
  'better_formats_allowed' => 
  array (
    1 => false,
    2 => false,
    3 => false,
    4 => false,
  ),
  'better_formats_defaults' => 
  array (
    'node-4' => 
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'node-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'node-2' => 
    array (
      'format' => '0',
      'weight' => '-23',
    ),
    'node-1' => 
    array (
      'format' => '0',
      'weight' => '-22',
    ),
    'comment-4' => 
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'comment-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'comment-2' => 
    array (
      'format' => '4',
      'weight' => '-23',
    ),
    'comment-1' => 
    array (
      'format' => '4',
      'weight' => '-22',
    ),
  ),
);
$content['fields']  = array (
  0 => 
  array (
    'label' => 'Topic Tout Tags',
    'field_name' => 'field_topic_tout_tags',
    'type' => 'content_taxonomy',
    'widget_type' => 'content_taxonomy_options',
    'change' => 'Change basic information',
    'weight' => '-4',
    'show_depth' => 1,
    'group_parent' => '0',
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'value' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => false,
    'required' => 1,
    'multiple' => '1',
    'save_term_node' => 1,
    'vid' => '2',
    'parent' => '0',
    'parent_php_code' => '',
    'depth' => '',
    'op' => 'Save field settings',
    'module' => 'content_taxonomy',
    'widget_module' => 'content_taxonomy_options',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'int',
        'not null' => false,
        'sortable' => false,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '-4',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content['extra']  = array (
  'title' => '-5',
  'body_field' => '-3',
  'revision_information' => '1',
  'comment_settings' => '4',
  'menu' => '-2',
  'path' => '3',
  'nodewords' => '-1',
  'print' => '2',
  'scheduler_settings' => '0',
);
