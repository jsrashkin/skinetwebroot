<?php
// $Id: panels_template.module,v 1.5.2.17 2009/10/21 20:51:49 merlinofchaos Exp $

/**
 * @file panels_template.module
 *
 * This module provides template panels which are basically panels that can be
 * used within blocks or other panels.
 */

module_load_include('inc', 'panels_template', 'node_form');
module_load_include('inc', 'panels_template', 'contexts');

/**
 * Implementation of hook_perm().
 */
function panels_template_perm() {
  return array('create template panels', 'adtemplatester template panels');
}

/**
 * Implementation of hook_menu().
 */
function panels_template_menu() {
  // Safety: go away if CTools is not at an appropriate version.
  if (!defined('PANELS_REQUIRED_CTOOLS_API') || !module_invoke('ctools', 'api_version', PANELS_REQUIRED_CTOOLS_API)) {
    return array();
  }
  require_once drupal_get_path('module', 'panels_template') . '/panels_template.admin.inc';
  return _panels_template_menu();
}

/**
 * Statically store all used IDs to ensure all template panels get a unique id.
 */
function panels_template_get_id($name) {
  static $id_cache = array();

  $id = 'template-panel-' . $name;
  if (!empty($id_cache[$name])) {
    $id .= "-" . $id_cache[$name]++;
  }
  else {
    $id_cache[$name] = 1;
  }

  return $id;
}

// ---------------------------------------------------------------------------
// Database functions.

/**
 * Create a new page with defaults appropriately set from schema.
 */
function panels_template_new() {
  ctools_include('export');
  return ctools_export_new_object('panels_template');
}

/**
 * Load a single template panel.
 */
function panels_template_load($name) {
  ctools_include('export');
  $result = ctools_export_load_object('panels_template', 'names', array($name));
  if (isset($result[$name])) {
    if (empty($result[$name]->display)) {
      $result[$name]->display = panels_load_display($result[$name]->did);
    }
    // Always default frist context to the Node being viewed
    $result[$name]->contexts[0] = array(
      'name' => 'panels_template_node',
      'id' => 1,
      'identifier' => t('Node being viewed'),
      'keyword' => 'node',
    );
    return $result[$name];
  }
}

/**
 * Load all template panels.
 */
function panels_template_load_all($reset=FALSE) {
  static $templates;
  // If this page was loaded before and data was found, just return it.
  if ($templates) {
    return $templates;
  }
  // If there's a cached version of the template_panels available, use it.
  if (!$reset && $cache = cache_get('panels_template_load_all')) {
    $templates = $cache->data;
    return $templates;
  }

  ctools_include('export');
  $templates = ctools_export_load_object('panels_template');
  $dids = array();
  foreach ($templates as $template) {
    if (!empty($template->did)) {
      $dids[$template->did] = $template->name;
    }
  }

  $displays = panels_load_displays(array_keys($dids));
  foreach ($displays as $did => $display) {
    $templates[$dids[$did]]->display = $display;
  }

  // Cache the template panels for later.
  cache_set('panels_template_load_all', $templates);

  return $templates;
}

/**
 * Write a template panel to the database.
 */
function panels_template_save(&$template) {
  if (!empty($template->display)) {
    $display = panels_save_display($template->display);
    $template->did = $display->did;
  }

  $update = (isset($template->pid) && $template->pid != 'new') ? array('pid') : array();
  drupal_write_record('panels_template', $template, $update);
  cache_clear_all('panels_template_load_all', 'cache');

  return $template;
}

/**
 * Remove a template panel.
 */
function panels_template_delete($template) {
  db_query("DELETE FROM {panels_template} WHERE name = '%s'", $template->name);
  return panels_delete_display($template->did);
}

/**
 * Export a template panel.
 */
function panels_template_export($template, $indent = '') {
  ctools_include('export');
  $output = ctools_export_object('panels_template', $template, $indent);
  // Export the primary display
  $display = !empty($template->display) ? $template->display : panels_load_display($template->did);
  $output .= panels_export_display($display, $indent);
  $output .= $indent . '$template->display = $display' . ";\n";
  return $output;
}

/**
 * Menu callback to check to see if a template panel is valid as part
 * of a path, and if it is, return the template.
 */
function panels_template_admin_load($name) {
  $template = panels_template_load($name);
  if ($template && empty($template->disabled)) {
    return $template;
  }
}

/**
 * Implementation of hook_ctools_plugin_directory() to let the system know
 * we implement task and task_handler plugins.
 */
function panels_template_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools') {
    return 'plugins/' . $plugin;
  }
}

function panels_template_panels_template_list($template_type = NULL) {
  $all_panels = panels_template_load_all();
  $list = array();

  foreach ($all_panels as $name => $panel) {
    $list[$name] = check_plain($name) . ' (' . check_plain($panel->title) . ')';
  }
  return $list;
}

function panels_template_panels_template_list_type($template_type) {
  $all_panels = panels_template_load_all();
  $list = array();

  foreach ($all_panels as $name => $panel) {
    if ($template_type == $panel->template_type) {
      $list[$name] = check_plain($name) . ' (' . check_plain($panel->title) . ')';
    }
  }
  return $list;
}

function panels_template_get_template_types() {
  $types = array();

  foreach (module_implements('panels_template_template_types') as $module) {
    $func = $module . '_panels_template_template_types';
    $ret = $func();
    if (is_array($ret)) {
      $types = array_merge_recursive($types, $ret);
    }
  }

  drupal_alter('panels_template_template_types', $types);

  return $types;
}

function panels_template_panels_template_template_types() {
  $options = array(
    'article' => array(
      'label' => t('Article Templates'),
      'parent' => 'node',
    ),
    'homepage' => array(
      'label' => t('Homepage Templates'),
      'parent' => 'node',
    ),
    'right_column' => array(
      'label' => t('Right Column')
    ),
    'left_column' => array( 
      'label' => t('Left Column')
    ),
  );
  return $options;
}

/*
  Implementation of panel_task_override
  Provided by panel_template.inc panel task
*/
function panels_template_panel_task_override(&$handler,$display) {
  if ($handler->task != 'node_view') {
    return;
  }
  $node = $display->context['argument_nid_1']->data;

	// Node specific template. This is set on node edit form
	// Overtake the node_view display if the template is a node_view
	// template
  $template_types = panels_template_get_template_types();
  foreach ((array) $node->panels_template as $key=>$template) {
    if ($template_types[$key] && $template_types[$key]['parent'] == 'node') {
      $name = $node->panels_template[$key];
    }
  }

	// Default to the _default
  if (!$name) {
    $name = $node->type . '_default';
  }

  $templates = panels_template_load_all();
  $template = $templates[$name];

  if ($template) {
    $display = $template->display;
    $context = panels_template_get_contexts($template);
    $display->context = $context;
    return $display;
  }
}

function panels_template_grab_display($name) {
  $templates = panels_template_load_all();
  $template = $templates[$name];

  if ($template) {
    // By this point the display should ahve been loaded
    $display = $template->display ? $template->display : panels_load_display($template->did);
    $context = panels_template_get_contexts($template);
    $display->context = $context;
    return $display;
  }
}


function panels_template_get_contexts(&$template) {
  // include context
  ctools_include('context');  
  return ctools_context_load_contexts($template);
}
