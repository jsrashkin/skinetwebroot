<?php

function bonnier_type_article_core_header_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Core Header'),
    'description' => t('Core Content Header'),
    'required context' => new ctools_context_required(t('Node being Viewed'), 'node'),
    'category' => 'Article',
    'hook theme' => 'bonnier_type_article_core_header_theme',
  );
}

function bonnier_type_article_core_header_content_type_render($subtype, $conf, $panel_args, $context) {
  $nid = $panel_args[0];
  $node = node_load($nid);
  // If we are using context, grab from there. 
  // Core header won't always be a node_view handler
  if(!$node) {
    $node = $context->data;
  }

  $skipped = variable_get('core_header_disabled_types', array());
  if (in_array($node->type, $skipped)) {
    return;
  }

  if (variable_get('core_header_disabled_type_' . $node->type, FALSE)) {
    return;
  }

  $content = theme('core_header', $node);

  $block->content = $content;
  $block->title = '';
  $block->delta = 'core_header';
  $block->css_class = 'article-toolbar';
  return $block;
}

function bonnier_type_article_core_header_theme(&$theme) {
  $theme['core_header'] = array(
    'template' => 'core-header',
    'arguments' => array('node' => NULL),
    'path' => drupal_get_path('module', 'bonnier_type_article') . '/plugins/content_types/core_header',
    'file' => 'core_header.theme.inc',
  );
}

function bonnier_type_article_core_header_content_type_edit_form(&$form, &$form_state) {
}

function bonnier_type_article_core_header_content_type_edit_form_submit(&$form, &$form_state) {
}
