<?php
// $Id$

/**
 * @file
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Implementation of hook_ctools_access().
 */
function bonnier_type_article_main_photo_placement_ctools_access() {
  return array(
    'main_photo_placement' => array(
      'title' => t('Main photo placement'),
      'description' => t('Control access by main photo placement'),
      'callback' => 'bonnier_type_article_main_photo_placement_ctools_access_check',
      'default' => array('main_photo_placement' => array()),
      'settings form' => 'bonnier_type_article_main_photo_placement_ctools_access_settings',
      'settings form submit' => 'bonnier_type_article_main_photo_placement_ctools_access_settings_submit',
      'summary' => 'bonnier_type_article_main_photo_placement_ctools_access_summary',
      'required context' => new ctools_context_required(t('Node'), 'node'),
      'restrictions' => 'bonnier_type_article_main_photo_placement_ctools_access_restrictions',
    ),
  );
}

function bonnier_type_article_main_photo_placement_ctools_access_settings(&$form, &$form_state, $conf) {
  $form['settings']['main_photo_placement'] = array(
    '#title' => t('Main photo placement'),
    '#type' => 'checkboxes',
    '#options' => array(
      'right' => t('Right'),
      'left' => t('Left'),
      'center' => t('Center'),
    ),
    '#description' => t('Only the checked placements will be valid.'),
    '#default_value' => $conf['main_photo_placement'],
  );
}

function bonnier_type_article_main_photo_placement_ctools_access_settings_submit(&$form, &$form_state) {
  $form_state['values']['settings']['main_photo_placement'] = array_filter($form_state['values']['settings']['main_photo_placement']);
}

function bonnier_type_article_main_photo_placement_ctools_access_check($conf, $context) {
  if (!empty($context) && !empty($context->data) && !empty($context->data->main_photo_placement) && (!empty($conf['main_photo_placement'][$context->data->main_photo_placement]) || empty($conf['main_photo_placement']))) {
    return TRUE;
  }
}

function bonnier_type_article_main_photo_placement_ctools_access_restrictions($conf, &$context) {
  if (isset($context->restrictions['main_photo_placement'])) {
    $context->restrictions['main_photo_placement'] = array_unique(array_merge($context->restrictions['main_photo_placement'], array_keys($conf['main_photo_placement'])));
  }
  else {
    $context->restrictions['main_photo_placement'] = array_keys($conf['main_photo_placement']);
  }
}

function bonnier_type_article_main_photo_placement_ctools_access_summary($conf, $context) {
  if (isset($conf['main_photo_placement'])) {
    $placements = array(
      'right' => t('Right'),
      'left' => t('Left'),
      'center' => t('Center'),
    );
    foreach ($conf['main_photo_placement'] as $placement) {
      $names[] = $placements[$placement];
    }
  }
  if (empty($names)) {
    return t('@identifier main photo placement is any', array('@identifier' => $context->identifier));
  }
  else {
    $count = count($names);
    $last_name = array_pop($names);
    return format_plural($count, '@identifier main photo placement is "@main_photo_placement"', '@identifier main photo placement is one of "@main_photo_placement"', array('@identifier' => $context->identifier, '@main_photo_placement' => empty($names) ? $last_name : implode(', ', $names) .' or '. $last_name));
  }
}
