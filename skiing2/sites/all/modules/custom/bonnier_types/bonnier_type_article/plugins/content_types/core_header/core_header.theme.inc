<?php

function template_preprocess_core_header(&$vars) {
  $node = $vars['node'];

  laserfist_common_preprocess_node($vars, $node);

  $vars['title'] = bonnier_common_title($node);
  $vars['dek'] = bonnier_common_dek($node);

  $related_tags = theme('laserfist_related_tags', $node);
  $vars['related_tags'] = $related_tags;

  $vars['comment_count'] = $node->comment_count;

  $vars['date_timestamp'] = $node->created;
  $vars['date'] = date('M d, Y', $node->created);

  $vars['template_files'][] = 'core-header-' . str_replace('_', '-', $node->type);
}