<?php
$content['type']  = array (
  'name' => 'Blog Post',
  'type' => 'blog_post',
  'description' => '',
  'title_label' => 'Title',
  'body_label' => 'Body',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => true,
    'sticky' => false,
    'revision' => false,
  ),
  'addthis_nodetype' => 1,
  'forward_display' => 1,
  'scheduler' => 0,
  'scheduler_touch' => 0,
  'old_type' => 'blog_post',
  'orig_type' => '',
  'module' => 'bonnier_type_blog',
  'custom' => '0',
  'modified' => '1',
  'locked' => '1',
  'reset' => 'Reset to defaults',
  'nodewords_edit_metatags' => true,
  'content_profile_use' => 0,
  'comment' => '2',
  'comment_default_mode' => '2',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => 0,
  'comment_subject_field' => '0',
  'comment_preview' => '1',
  'comment_form_location' => '1',
  'flatcomments_remove_reply_link' => 
  array (
    'reply' => false,
  ),
  'fivestar' => 0,
  'fivestar_stars' => 5,
  'fivestar_labels_enable' => 1,
  'fivestar_label_0' => 'Cancel rating',
  'fivestar_label_1' => 'Poor',
  'fivestar_label_2' => 'Okay',
  'fivestar_label_3' => 'Good',
  'fivestar_label_4' => 'Great',
  'fivestar_label_5' => 'Awesome',
  'fivestar_label_6' => 'Give it @star/@count',
  'fivestar_label_7' => 'Give it @star/@count',
  'fivestar_label_8' => 'Give it @star/@count',
  'fivestar_label_9' => 'Give it @star/@count',
  'fivestar_label_10' => 'Give it @star/@count',
  'fivestar_style' => 'average',
  'fivestar_text' => 'dual',
  'fivestar_title' => 1,
  'fivestar_feedback' => 1,
  'fivestar_unvote' => 0,
  'fivestar_position_teaser' => 'hidden',
  'fivestar_position' => 'below',
  'fivestar_comment' => 0,
  'page_title' => 
  array (
    'show_field' => 
    array (
      'show_field' => true,
    ),
    'pattern' => '',
  ),
  'print_display' => 0,
  'print_display_comment' => 0,
  'print_display_urllist' => 0,
  'better_formats_allowed' => 
  array (
    1 => false,
    2 => false,
    3 => false,
    4 => false,
  ),
  'better_formats_defaults' => 
  array (
    'node-4' => 
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'node-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'node-2' => 
    array (
      'format' => '0',
      'weight' => '-23',
    ),
    'node-1' => 
    array (
      'format' => '0',
      'weight' => '-22',
    ),
    'comment-4' => 
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'comment-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'comment-2' => 
    array (
      'format' => '4',
      'weight' => '-23',
    ),
    'comment-1' => 
    array (
      'format' => '4',
      'weight' => '-22',
    ),
  ),
);
$content['fields']  = array (
  0 => 
  array (
    'label' => 'Blog',
    'field_name' => 'field_blog',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_select',
    'change' => 'Change basic information',
    'weight' => '-1',
    'autocomplete_match' => 'contains',
    'size' => 60,
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'nid' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => false,
    'required' => 1,
    'multiple' => '0',
    'referenceable_types' => 
    array (
      'blog' => 'blog',
      'article' => 0,
      'blog_post' => 0,
      'channel' => 0,
      'person' => 0,
      'forum' => 0,
      'homepage' => 0,
      'image' => 0,
      'laserfist_gallery' => 0,
      'menu_item' => 0,
      'pcd' => 0,
      'page' => 0,
      'panel' => 0,
      'profile' => 0,
      'webform' => 0,
      'family_member' => false,
      'directory_section' => false,
      'gallery' => false,
      'poll' => false,
      'pregnancy' => false,
      'user_answer' => false,
      'user_question' => false,
      'timeline' => false,
      'tool' => false,
      'ugc_photo' => false,
      'tag' => false,
      'link' => false,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' => 
    array (
      'nid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '-1',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  1 => 
  array (
    'label' => 'Dek',
    'field_name' => 'field_dek',
    'type' => 'text',
    'widget_type' => 'text_textarea',
    'change' => 'Change basic information',
    'weight' => 0,
    'rows' => '5',
    'size' => 60,
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'value' => '',
        '_error_element' => 'default_value_widget][field_dek][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_dek' => 
      array (
        0 => 
        array (
          'value' => '',
          '_error_element' => 'default_value_widget][field_dek][0][value',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
        'views' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '-1',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  2 => 
  array (
    'label' => 'Author',
    'field_name' => 'field_author',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_select',
    'change' => 'Change basic information',
    'weight' => '5',
    'autocomplete_match' => 'contains',
    'size' => 60,
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'nid' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_author' => 
      array (
        0 => 
        array (
          'nid' => '',
        ),
        'nid' => 
        array (
          'nid' => '',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'referenceable_types' => 
    array (
      'person' => 'person',
      'article' => 0,
      'blog' => 0,
      'blog_post' => 0,
      'channel' => 0,
      'family_member' => 0,
      'directory_section' => 0,
      'forum' => 0,
      'homepage' => 0,
      'image' => 0,
      'laserfist_gallery' => 0,
      'menu_item' => 0,
      'pcd' => 0,
      'page' => 0,
      'panel' => 0,
      'gallery' => 0,
      'poll' => 0,
      'pregnancy' => 0,
      'profile' => 0,
      'user_answer' => 0,
      'user_question' => 0,
      'timeline' => 0,
      'tool' => 0,
      'webform' => 0,
      'tag' => 0,
      'ugc_photo' => false,
      'link' => false,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' => 
    array (
      'nid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '4',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'hidden',
        'exclude' => 1,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  3 => 
  array (
    'label' => 'Main Photo',
    'field_name' => 'field_main_photo',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => '6',
    'autocomplete_match' => 'contains',
    'size' => 60,
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_main_photo][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_main_photo' => 
      array (
        0 => 
        array (
          'nid' => 
          array (
            'nid' => '',
            '_error_element' => 'default_value_widget][field_main_photo][0][nid][nid',
          ),
          '_error_element' => 'default_value_widget][field_main_photo][0][nid][nid',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'referenceable_types' => 
    array (
      'image' => 'image',
      'article' => 0,
      'blog' => 0,
      'blog_post' => 0,
      'channel' => 0,
      'family_member' => 0,
      'person' => 0,
      'directory_section' => 0,
      'forum' => 0,
      'homepage' => 0,
      'laserfist_gallery' => 0,
      'menu_item' => 0,
      'pcd' => 0,
      'page' => 0,
      'panel' => 0,
      'gallery' => 0,
      'poll' => 0,
      'pregnancy' => 0,
      'profile' => 0,
      'user_answer' => 0,
      'user_question' => 0,
      'timeline' => 0,
      'tool' => 0,
      'webform' => 0,
      'tag' => 0,
      'ugc_photo' => false,
      'link' => false,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' => 
    array (
      'nid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '5',
      'parent' => '',
      'label' => 
      array (
        'format' => 'hidden',
      ),
      'teaser' => 
      array (
        'format' => 'full',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'full',
        'exclude' => 1,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  4 => 
  array (
    'label' => 'Thumbnail Image',
    'field_name' => 'field_thumbnail',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => '7',
    'autocomplete_match' => 'contains',
    'size' => 60,
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_thumbnail][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_thumbnail' => 
      array (
        0 => 
        array (
          'nid' => 
          array (
            'nid' => '',
            '_error_element' => 'default_value_widget][field_thumbnail][0][nid][nid',
          ),
          '_error_element' => 'default_value_widget][field_thumbnail][0][nid][nid',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'referenceable_types' => 
    array (
      'image' => 'image',
      'article' => false,
      'blog' => false,
      'blog_post' => false,
      'channel' => false,
      'family_member' => false,
      'person' => false,
      'directory_section' => false,
      'forum' => false,
      'homepage' => false,
      'laserfist_gallery' => false,
      'menu_item' => false,
      'pcd' => false,
      'page' => false,
      'panel' => false,
      'gallery' => false,
      'poll' => false,
      'pregnancy' => false,
      'profile' => false,
      'user_answer' => false,
      'user_question' => false,
      'timeline' => false,
      'tool' => false,
      'ugc_photo' => false,
      'webform' => false,
      'tag' => false,
      'link' => false,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' => 
    array (
      'nid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '6',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  5 => 
  array (
    'label' => 'File Attachments',
    'field_name' => 'field_attachments',
    'type' => 'filefield',
    'widget_type' => 'filefield_widget',
    'change' => 'Change basic information',
    'weight' => '8',
    'file_extensions' => 'gif jpg png swf pdf xls xlsx doc docx ppt pptx odf osf',
    'progress_indicator' => 'bar',
    'file_path' => '_attachments_articles',
    'max_filesize_per_file' => '',
    'max_filesize_per_node' => '',
    'description' => '',
    'group' => false,
    'required' => 0,
    'multiple' => '1',
    'list_field' => '1',
    'list_default' => 0,
    'description_field' => '1',
    'op' => 'Save field settings',
    'module' => 'filefield',
    'widget_module' => 'filefield',
    'columns' => 
    array (
      'fid' => 
      array (
        'type' => 'int',
        'not null' => false,
        'views' => true,
      ),
      'list' => 
      array (
        'type' => 'int',
        'size' => 'tiny',
        'not null' => false,
        'views' => true,
      ),
      'data' => 
      array (
        'type' => 'text',
        'serialize' => true,
        'views' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '10',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  6 => 
  array (
    'label' => 'Related Content',
    'field_name' => 'field_related_content',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => '32',
    'autocomplete_match' => 'contains',
    'size' => '60',
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_related_content][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_related_content' => 
      array (
        0 => 
        array (
          'nid' => 
          array (
            'nid' => '',
            '_error_element' => 'default_value_widget][field_related_content][0][nid][nid',
          ),
          '_error_element' => 'default_value_widget][field_related_content][0][nid][nid',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '1',
    'referenceable_types' => 
    array (
      'article' => 'article',
      'blog_post' => 'blog_post',
      'gallery' => 'gallery',
      'multichoice' => 'multichoice',
      'poll' => 'poll',
      'product' => 'product',
      'product_boot' => 'product_boot',
      'product_ski' => 'product_ski',
      'user_question' => 'user_question',
      'resort' => 'resort',
      'video' => 'video',
      'webform' => 'webform',
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' => 
    array (
      'nid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '5',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'hidden',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content['extra']  = array (
  'title' => '-2',
  'body_field' => '1',
  'revision_information' => '10',
  'comment_settings' => '11',
  'menu' => '9',
  'path' => '12',
  'print' => '13',
);

