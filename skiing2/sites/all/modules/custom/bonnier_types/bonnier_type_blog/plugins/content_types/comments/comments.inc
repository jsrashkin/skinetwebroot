<?php

function bonnier_type_blog_comments_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Blogs Comments'),
    'description' => t('Blogs Comments'),
    'category' => 'Blogs',
    'required context' => new ctools_context_optional(t('Blog'),'node'),
		'hook theme' => 'bonnier_type_blog_comments_theme',
  );
}

function bonnier_type_blog_comments_theme(&$theme) {
	$path = drupal_get_path('module', 'bonnier_type_blog') . '/plugins/content_types/comments';
	$theme['blogs_comments_listing'] = array(
		'template' => 'blogs-comments-listing',
		'path' => $path,
		'arguments' => array('blog_node' => NULL, 'max' => 5),
	);

	$theme['blogs_comment'] = array(
		'template' => 'blogs-comment',
		'path' => $path,
		'arguments' => array('comment' => NULL),
	);
}


function bonnier_type_blog_comments_content_type_render($subtype, $conf, $panel_args, $context) {
	$blog_node = $context->data;
	$max = is_numeric($conf['max_comments']) ? $conf['max_comments'] : 5;
	$content = theme('blogs_comments_listing', $blog_node,$max); 

  $block->content = $content;
  $block->title = 'Recent Comments';
  $block->delta = 'comments';
	$block->css_class = 'blog-comments';
  return $block;
}

/**
 * Override or insert variables into the blogs_comment_listing template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 */
function template_preprocess_blogs_comments_listing(&$vars) {
  // If no blog_node provided show comments from all blogs.
  $blog_nid = ($nid = ($vars['blog_node']->nid)) ? $nid : 'ctbp.field_blog_nid';
   //check to make sure field_blog is not in join table
  $check_sql = 'select type_name from {content_node_field_instance} where field_name = "field_blog"';
  $check_query = db_query($check_sql);
  $count = mysql_num_rows($check_query);
  if ($count > 1) {
    $sql = 'SELECT c.cid FROM {comments} c INNER JOIN {content_field_blog} ctbp ON ctbp.nid = c.nid WHERE c.status = 0 AND ctbp.field_blog_nid = %s 
            ORDER BY cid DESC LIMIT %d';
  } 
  else {
    $sql = 'SELECT c.cid FROM {comments} c INNER JOIN {content_type_blog_post} ctbp ON ctbp.nid = c.nid WHERE c.status = 0  AND ctbp.field_blog_nid = %s
      ORDER BY cid DESC LIMIT %d';
  }

  $results = db_query($sql, $blog_nid, $vars['max']);
  while ($row = db_fetch_object($results)) {
    $comments[] = theme('blogs_comment', _comment_load($row->cid));
  }
  
  $vars['comments'] = theme('item_list', $comments);
}

function template_preprocess_blogs_comment(&$vars) {
	$comment = $vars['comment'];
	$node = node_load($comment->nid);
	$node_url = bonnier_common_link($node);

	// Comment Data
	$vars['comment_title'] = $comment->subject;

	// Author data
	$vars['comment_author'] = theme('username', $comment);

	// Node Data
	$vars['node_title'] = l(bonnier_common_title($node), $node_url);
	
  //Comment Count
  $comment_count = db_result(db_query('SELECT comment_count FROM {node_comment_statistics} WHERE nid = %d', $node->nid));
  if ($comment_count) {
    $vars['comment_count'] = $comment_count;
  } else {
    $vars['comment_count'] = 0;
  }
}

function bonnier_type_blog_comments_content_type_edit_form(&$form, &$form_state) {
	// provide a blank form so we have a place to have context setting.
	$form['max_comments'] = array(
		'#title' => t('Max # of Comments'),
		'#type' => 'textfield',
		'#default_value' => 5,
	);
}

function bonnier_type_blog_comments_content_type_edit_form_submit(&$form, &$form_state) {
	// provide a blank form so we have a place to have context setting.
	$form_state['conf']['max_comments'] = $form_state['values']['max_comments'];
}
