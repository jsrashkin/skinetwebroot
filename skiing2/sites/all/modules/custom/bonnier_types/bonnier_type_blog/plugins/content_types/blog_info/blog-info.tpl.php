<div class="blog-info <?php print $classes; ?>">
  <h2>
    <span class="blog-name"><?php print $blog_name; ?></span> 
    <?php if ($blog_author_name): ?>
      <span class="by_blog_author">by <span class="blog_author"><?php print $blog_author_name; ?></span></span> 
    <?php endif; ?>
  </h2>

  <div class="blog-info-blurb">
    <div class="image">
      <?php print $blog_info_image; ?>
    </div>
    <div class="dek">
      <?php print $blog_info_dek; ?>
      <?php if ($blog_info_read_more_href): ?>
        <span class="read-more"><a href="<?php print $blog_info_read_more_href; ?>">Read full bio</a></span>
      <?php endif; ?>
    </div>
    <div class="clear"></div>
  </div>
</div>
