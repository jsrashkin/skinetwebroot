<?php

function bonnier_type_blog_blog_share_links_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Blog Share Links'),
    'description' => t('Blog Share Links'),
    'category' => 'Blogs',
    'required context' => new ctools_context_optional(t('Blog'), 'node'),
    'hook theme' => 'bonnier_type_blog_blog_share_links_theme',
  );
}

function bonnier_type_blog_blog_share_links_theme(&$theme) {
  $path = drupal_get_path('module', 'bonnier_type_blog') . '/plugins/content_types/blog_share_links';

  $theme['blog_share_links'] = array(
    'template' => 'blog-share-links',
    'path' => $path,
    'arguments' => array('blog_node' => NULL),
  );
}

function bonnier_type_blog_blog_share_links_content_type_render($subtype, $conf, $panel_args, $context) {
  $blog_node = $context->data;
  $settings = bonnier_type_blog_get_blog_settings($blog_node);
  $block = new StdClass;
  // Only show the output if there is a feedburner variable set.
  if (!empty($settings->feedburner_name)) {
    $content = theme('blog_share_links', $blog_node); 

    $block->content = $content;
    $block->title = variable_get('bonnier_type_blog_syndicate_title', 'Syndicate');
    $block->delta = 'blog_share_links';
    $block->css_class = 'blog-share-links';
  }
  return $block;
}

function template_preprocess_blog_share_links(&$vars) {
  $node = $vars['blog_node'];
  $settings = bonnier_type_blog_get_blog_settings($node);
  // Default to jsut a nothing blog share_links 
  if ($settings->feedburner_name) {
    $vars['feedburner_name'] = $settings->feedburner_name;
  }
}


function bonnier_type_blog_blog_share_links_content_type_edit_form(&$form, &$form_state) {
}

function bonnier_type_blog_blog_share_links_content_type_edit_form_submit(&$form, &$form_state) {
}
