<?php

function bonnier_type_blog_top_posters_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Blog Top Posters'),
    'description' => t('Blog Top Posters'),
    'category' => 'Blogs',
    'required context' => new ctools_context_optional(t('Blog'),'node'),
    'hook theme' => 'bonnier_type_blog_top_posters_theme',
  );
}

function bonnier_type_blog_top_posters_theme(&$theme) {
  $path = drupal_get_path('module','bonnier_type_blog').'/plugins/content_types/top_posters';
  $theme['blogs_top_poster'] = array(
    'template' => 'blogs-top-poster',
    'arguments' => array('node' => NULL),
    'path' => $path,
  );
  $theme['blogs_top_posters_listing'] = array(
    'template' => 'blogs-top-posters-listing',
    'arguments' => array('blog_node' => NULL, 'max' => 5),
    'path' => $path,
  );
}


function bonnier_type_blog_top_posters_content_type_render($subtype, $conf, $panel_args, $context) {
  $blog_node = $context->data;
  $max = is_numeric($conf['max_top_posters']) ? $conf['max_top_posters'] : 5;
  $content = theme('blogs_top_posters_listing', $blog_node, $max); 

  $block->content = $content;
  $block->title = 'Top Posters';
  $block->delta = 'top_posters';
  $block->css_class = 'blog-top_posters';
  return $block;
}

function template_preprocess_blogs_top_posters_listing(&$vars) {
  $blog_node = $vars['blog_node'];
  $max = $vars['max'];
  
  $sql = "
    SELECT auth.field_author_nid as author_nid, mp.field_main_photo_nid as author_photo, count(auth.field_author_nid) as author_count
    FROM {content_field_author} auth 
    INNER JOIN {content_type_blog_post} bp ON auth.nid = bp.nid
    INNER JOIN {content_field_main_photo} mp ON auth.field_author_nid = mp.nid
    WHERE bp.field_blog_nid = %d
    GROUP BY auth.field_author_nid
    ORDER BY author_count DESC
    limit %d
  ";
  
  $data = db_query($sql, $blog_node->nid, $max);
  $results = array();
  while ($row = db_fetch_object($data)) {
    $results[] = $row;
  }
  
  $i = 0;
  foreach ($results as $i => $result) {
    $top_posters[$i]['obj'] = node_load($result->author_nid);
    $top_posters[$i]['photo'] = node_load($result->author_photo);
    $top_posters[$i]['count'] = $result->author_count;
  }
      
  foreach ((array)$top_posters as $poster) {
    $items[] = theme('blogs_top_poster', $poster);
  }

  if (!empty($items)) {
    $vars['top_posters'] = theme('item_list', $items);
  }
}

function template_preprocess_blogs_top_poster(&$vars) {
  $node = $vars['node']['obj'];
  
  $vars['link'] = l($node->title, 'node/' . $node->nid);
  
  $photo = $vars['node']['photo'];
  if ($photo) {
    $vars['author_photo'] = l(theme('imagecache','thumb_50',$photo->field_image[0]['filepath']),'node/'.$node->nid,array('html'=>TRUE));  
  }
  
  $vars['post_count'] = $vars['node']['count'];
}

function bonnier_type_blog_top_posters_content_type_edit_form(&$form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  $form['max_top_posters'] = array(
    '#title' => t('Max # of Top Posters'),
    '#type' => 'textfield',
    '#default_value' => 5,
  );
}

function bonnier_type_blog_top_posters_content_type_edit_form_submit(&$form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  $form_state['conf']['max_top_posters'] = $form_state['values']['max_top_posters'];
}
