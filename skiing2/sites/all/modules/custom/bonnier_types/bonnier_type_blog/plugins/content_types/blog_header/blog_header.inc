<?php

function bonnier_type_blog_blog_header_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Blog Header'),
    'description' => t('Blog Header'),
    'category' => 'Blogs',
    'required context' => new ctools_context_optional(t('Blog'), 'node'),
    'hook theme' => 'bonnier_type_blog_blog_header_theme',
  );
}

function bonnier_type_blog_blog_header_theme(&$theme) {
  $path = drupal_get_path('module', 'bonnier_type_blog') . '/plugins/content_types/blog_header';

  $theme['blog_header'] = array(
    'template' => 'blog-header',
    'path' => $path,
    'arguments' => array('blog_node' => NULL),
  );
}


function bonnier_type_blog_blog_header_content_type_render($subtype, $conf, $panel_args, $context) {
  $blog_node = $context->data;

  $content = theme('blog_header', $blog_node); 

  $block->content = $content;
  $block->title = '';
  $block->delta = 'blog_header';
  $block->css_class = 'blog-header';
  return $block;
}

function template_preprocess_blog_header(&$vars) {
  $node = $vars['blog_node'];
  // Default to jsut a nothing blog header 
  if (!$node) {
    $vars['blog_header_content'] = 'Default Blog Header';
    $vars['blog_link'] = url('blogs');
    $vars['empty_header'] = TRUE;
    $vars['classes'] = 'default-header';
  }
  if ($image_path = $node->field_header_graphic[0]['filepath']) {
    $vars['blog_header_image'] = theme('imagecache','blog_header_image',$image_path);
    $vars['blog_header_content'] = $vars['blog_header_image'];
    $vars['blog_link'] = url('blogs');
  }
}


function bonnier_type_blog_blog_header_content_type_edit_form(&$form, &$form_state) {
}

function bonnier_type_blog_blog_header_content_type_edit_form_submit(&$form, &$form_state) {
}
