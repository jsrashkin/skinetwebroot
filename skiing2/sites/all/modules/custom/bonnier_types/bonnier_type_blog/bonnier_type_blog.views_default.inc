<?php
/**
 * Implementation of hook_views_default_views().
 */
function bonnier_type_blog_views_default_views() {
  $views = array();
  $path = dirname(__FILE__) .'/views';

  // 
  include($path . '/view.blogs_home.inc');
  $views[$view->name] = $view;

  // 
  include($path . '/view.blog_posts.inc');
  $views[$view->name] = $view;

  return $views;
}
