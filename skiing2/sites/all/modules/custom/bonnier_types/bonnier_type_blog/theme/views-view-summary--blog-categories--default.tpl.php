<?php
// $Id: views-view-summary.tpl.php,v 1.6 2009/01/07 19:21:34 merlinofchaos Exp $
/**
 * @file views-view-summary.tpl.php
 * Default simple view template to display a list of summary lines
 *
 * @ingroup views_templates
 */
?>
<div class="item-list">
  <?php
    $flip = array('even' => 'odd', 'odd' => 'even');
    $class = 'even';
    $num = 0;
    $count = count($rows);
  ?>
  <ul class="views-summary">
  <?php foreach ($rows as $row): ?>
    <?php
      $class = $flip[$class];
      $num++;
      $classes = "views-row views-row-$num views-row-$class";
      $classes .= $num == 1 ? " views-row-first" : '';
      $classes .= $num == $count ? " views-row-last" : '';
    ?>
    <li class="<?php print $classes; ?>"><a href="<?php print $row->url; ?>"><?php print $row->link; ?></a>
      <?php if (!empty($options['count'])): ?>
        (<?php print $row->count?>)
      <?php endif; ?>
    </li>
  <?php endforeach; ?>
  </ul>
</div>
