<?php if(in_array('admin',$user->roles) && arg(2) == 'edit') {?>
	<?php print drupal_render($form); ?>
<?php } else { ?>
<?php if($form_description): ?>
<div class="form-description">
  <?php print $form_description; ?>
</div>
<?php endif; ?>
<div id="step-1 video" class="form-step">
  <div class="header">
    <h2>
      <span class="number">1.</span>
      Video
    </h2>
  </div>
  <div class="form-item">
    <?php print $upload; ?>
  </div>
</div>
<div id="step-2 video-info" class="form-step">
  <div class="header">
    <h2>
      <span class="number">2.</span>
      Video Info
    </h2>
  </div>
	<div class="content">
		<?php print $title; ?>
	</div>
  <div class="form-item">
	  <?php print $body; ?>
   </div>
</div>
<div id="step-3 video" class="form-step">
	<div class="header">
		<h2><span class="number">3.</span>Tag video (optional)</h2>
	</div>
	<div class="form-item">
		<?php print $categories; ?>
	</div>
</div>
<div id="step-4 buttons" class="form-step">
	<?php print $submit_button; ?>
  <p>Please be aware this may take some time based on your Internet connection speed</p>
</div>

<div class="hidden" style="display:none;">
	<?php print $other; ?>
</div>
<?php } ?>
