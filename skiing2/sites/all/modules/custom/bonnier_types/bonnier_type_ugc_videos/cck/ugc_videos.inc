<?php
$content['type']  = array (
  'name' => 'UGC Videos',
  'type' => 'ugc_videos',
  'description' => 'Controls UGC Videos',
  'title_label' => 'Title',
  'body_label' => 'Body',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => false,
    'sticky' => false,
    'revision' => false,
  ),
  'upload' => '0',
  'addthis_nodetype' => 0,
  'forward_display' => 0,
  'scheduler' => 0,
  'scheduler_touch' => 0,
  'old_type' => 'ugc_videos',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'nodewords_edit_metatags' => 1,
  'nodewords_metatags_generation_method' => '0',
  'nodewords_metatags_generation_source' => '2',
  'nodewords_use_alt_attribute' => 1,
  'nodewords_filter_modules_output' => 
  array (
    'imagebrowser' => false,
    'img_assist' => false,
  ),
  'nodewords_filter_regexp' => '',
  'content_profile_use' => 0,
  'comment' => '2',
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => '0',
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
  'comment_upload' => '0',
  'comment_upload_images' => 'none',
  'flatcomments_remove_reply_link' => 
  array (
    'reply' => false,
  ),
  'fivestar' => 0,
  'fivestar_stars' => 5,
  'fivestar_labels_enable' => 1,
  'fivestar_label_0' => 'Cancel rating',
  'fivestar_label_1' => 'Poor',
  'fivestar_label_2' => 'Okay',
  'fivestar_label_3' => 'Good',
  'fivestar_label_4' => 'Great',
  'fivestar_label_5' => 'Awesome',
  'fivestar_label_6' => 'Give it @star/@count',
  'fivestar_label_7' => 'Give it @star/@count',
  'fivestar_label_8' => 'Give it @star/@count',
  'fivestar_label_9' => 'Give it @star/@count',
  'fivestar_label_10' => 'Give it @star/@count',
  'fivestar_style' => 'average',
  'fivestar_text' => 'dual',
  'fivestar_title' => 1,
  'fivestar_feedback' => 1,
  'fivestar_unvote' => 0,
  'fivestar_position_teaser' => 'hidden',
  'fivestar_position' => 'below',
  'fivestar_comment' => 0,
  'page_title' => 
  array (
    'show_field' => 
    array (
      'show_field' => false,
    ),
    'pattern' => '',
  ),
  'print_display' => 1,
  'print_display_comment' => 0,
  'print_display_urllist' => 1,
  'ant' => '0',
  'ant_pattern' => '',
  'ant_php' => 0,
  'better_formats_allowed' => 
  array (
    1 => false,
    2 => false,
    3 => false,
    4 => false,
  ),
  'better_formats_defaults' => 
  array (
    'node-4' => 
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'node-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'node-2' => 
    array (
      'format' => '0',
      'weight' => '-23',
    ),
    'node-1' => 
    array (
      'format' => '0',
      'weight' => '-22',
    ),
    'comment-4' => 
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'comment-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'comment-2' => 
    array (
      'format' => '4',
      'weight' => '-23',
    ),
    'comment-1' => 
    array (
      'format' => '4',
      'weight' => '-22',
    ),
  ),
);
$content['fields']  = array (
  0 => 
  array (
    'label' => 'Video',
    'field_name' => 'field_ugc_video',
    'type' => 'video_upload',
    'widget_type' => 'video_upload_widget',
    'change' => 'Change basic information',
    'weight' => '-2',
    'file_extensions' => 'mov avi mp4 mpa mpe mpg mpeg qt wmv',
    'progress_indicator' => 'bar',
    'file_path' => 'videos/[uid]',
    'max_filesize_per_file' => '',
    'max_filesize_per_node' => '',
    'use_browser_upload_method' => false,
    'auto_delete_rejected_videos' => 0,
    'remove_deleted_videos' => 0,
    'display' => 
    array (
      'default_width' => '480',
      'default_height' => '295',
      'small_width' => '240',
      'small_height' => '148',
      'thumb_width' => '128',
      'thumb_height' => '72',
      'related_videos' => 0,
      'autoplay' => 0,
      'fullscreen' => 1,
    ),
    'default_title' => '',
    'default_title_sync' => '1',
    'default_description' => '',
    'default_description_sync' => '2',
    'default_keywords' => 'Gear,Instructional,Racing',
    'default_keyword_sync' => '2',
    'developer_tags' => '',
    'video_category' => 'Sports',
    'description' => '',
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'op' => 'Save field settings',
    'module' => 'video_upload',
    'widget_module' => 'video_upload',
    'columns' => 
    array (
      'fid' => 
      array (
        'type' => 'int',
        'not null' => false,
        'views' => true,
      ),
      'list' => 
      array (
        'type' => 'int',
        'size' => 'tiny',
        'not null' => false,
        'views' => true,
      ),
      'data' => 
      array (
        'type' => 'text',
        'serialize' => true,
        'views' => true,
      ),
      'video_id' => 
      array (
        'type' => 'varchar',
        'length' => 32,
        'views' => true,
      ),
      'video_status' => 
      array (
        'type' => 'varchar',
        'length' => 32,
        'default' => 'upload_pending',
        'sortable' => true,
        'views' => true,
      ),
      'video_status_ts' => 
      array (
        'type' => 'int',
        'length' => 11,
        'sortable' => true,
        'default' => '0',
        'views' => true,
      ),
    ),
    'display_settings' => 
    array (
      'label' => 
      array (
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content['extra']  = array (
  'title' => '-5',
  'body_field' => '-3',
  'revision_information' => '1',
  'comment_settings' => '4',
  'menu' => '-4',
  'path' => '3',
  'nodewords' => '-1',
  'print' => '2',
  'scheduler_settings' => '0',
);

