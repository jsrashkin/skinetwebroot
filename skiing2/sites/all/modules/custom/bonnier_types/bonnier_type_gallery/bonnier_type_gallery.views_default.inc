<?php
// $Id$

/**
 * @file
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Implementation of hook_views_default_views().
 */
function bonnier_type_gallery_views_default_views() {
  $views = array();
  $path = dirname(__FILE__) .'/views';

  // 
  include($path .'/view.gallery_page_view.inc');
  $views[$view->name] = $view;

  // List of related galleries.
  include($path .'/view.related_galleries.inc');
  $views[$view->name] = $view;

  // Carousel of galleries, depends on the jcarousellite_views module.
  include($path .'/view.bonnier_galleries_list.inc');
  $views[$view->name] = $view;

  return $views;
}
