<?php
$content['type']  = array (
  'name' => 'video',
  'type' => 'video',
  'description' => 'Video is the technology of electronically capturing, recording, processing, storing, transmitting, and reconstructing a sequence of still images representing scenes in motion.',
  'title_label' => 'Title',
  'body_label' => 'Dek',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => false,
    'sticky' => false,
    'revision' => false,
  ),
  'upload' => '0',
  'addthis_nodetype' => 1,
  'forward_display' => 1,
  'scheduler' => 1,
  'scheduler_touch' => 0,
  'old_type' => 'video',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'nodewords_edit_metatags' => 1,
  'nodewords_basic_use_teaser' => 0,
  'content_profile_use' => 0,
  'comment' => '2',
  'comment_default_mode' => '2',
  'comment_default_order' => '2',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => '2',
  'comment_subject_field' => '0',
  'comment_preview' => '1',
  'comment_form_location' => '1',
  'comment_upload' => '0',
  'comment_upload_images' => 'none',
  'flatcomments_remove_reply_link' => 
  array (
    'reply' => false,
  ),
  'fivestar' => 0,
  'fivestar_stars' => 5,
  'fivestar_labels_enable' => 1,
  'fivestar_label_0' => 'Cancel rating',
  'fivestar_label_1' => 'Poor',
  'fivestar_label_2' => 'Okay',
  'fivestar_label_3' => 'Good',
  'fivestar_label_4' => 'Great',
  'fivestar_label_5' => 'Awesome',
  'fivestar_label_6' => 'Give it @star/@count',
  'fivestar_label_7' => 'Give it @star/@count',
  'fivestar_label_8' => 'Give it @star/@count',
  'fivestar_label_9' => 'Give it @star/@count',
  'fivestar_label_10' => 'Give it @star/@count',
  'fivestar_style' => 'average',
  'fivestar_text' => 'dual',
  'fivestar_title' => 1,
  'fivestar_feedback' => 1,
  'fivestar_unvote' => 0,
  'fivestar_position_teaser' => 'hidden',
  'fivestar_position' => 'below',
  'fivestar_comment' => 0,
  'page_title' => 
  array (
    'show_field' => 
    array (
      0 => 1,
      'show_field' => false,
    ),
    'pattern' => '',
  ),
  'print_display' => 1,
  'print_display_comment' => 0,
  'print_display_urllist' => 1,
  'ant' => '0',
  'ant_pattern' => '',
  'ant_php' => 0,
  'better_formats_allowed' => 
  array (
    1 => false,
    2 => false,
    3 => false,
    4 => false,
  ),
  'better_formats_defaults' => 
  array (
    'node-4' => 
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'node-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'node-2' => 
    array (
      'format' => '0',
      'weight' => '-23',
    ),
    'node-1' => 
    array (
      'format' => '0',
      'weight' => '-22',
    ),
    'comment-4' => 
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'comment-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'comment-2' => 
    array (
      'format' => '4',
      'weight' => '-23',
    ),
    'comment-1' => 
    array (
      'format' => '4',
      'weight' => '-22',
    ),
  ),
);
$content['fields']  = array (
  0 => 
  array (
    'label' => 'Video Embed Code',
    'field_name' => 'field_video_embed_code',
    'type' => 'emvideo',
    'widget_type' => 'emvideo_textfields',
    'change' => 'Change basic information',
    'weight' => '-2',
    'providers' => 
    array (
      0 => 1,
      'archive' => false,
      'bliptv' => false,
      'brightcove3' => false,
      'dailymotion' => false,
      'google' => false,
      'guba' => false,
      'imeem' => false,
      'lastfm' => false,
      'livevideo' => false,
      'metacafe' => false,
      'myspace' => false,
      'revver' => false,
      'sevenload' => false,
      'spike' => false,
      'tudou' => false,
      'twistage' => false,
      'ustream' => false,
      'ustreamlive' => false,
      'vimeo' => false,
      'voicethread' => false,
      'yahoomusic' => false,
      'youtube' => false,
      'zzz_custom_url' => false,
    ),
    'video_width' => '425',
    'video_height' => '350',
    'video_autoplay' => '',
    'preview_width' => '300',
    'preview_height' => '250',
    'preview_autoplay' => '',
    'thumbnail_width' => '120',
    'thumbnail_height' => '90',
    'thumbnail_default_path' => '_thumbs_videos',
    'thumbnail_link_title' => 'See video',
    'description' => 'Embed code from Brightcove',
    'default_value' => 
    array (
      0 => 
      array (
        'embed' => '',
        'value' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => false,
    'required' => 1,
    'multiple' => '0',
    'op' => 'Save field settings',
    'module' => 'emvideo',
    'widget_module' => 'emvideo',
    'columns' => 
    array (
      'embed' => 
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
      ),
      'value' => 
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'provider' => 
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'data' => 
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => false,
      ),
      'version' => 
      array (
        'description' => 'The version of the provider\'s data.',
        'type' => 'int',
        'unsigned' => true,
        'not null' => true,
        'default' => 0,
      ),
      'duration' => 
      array (
        'description' => 'Store the duration of a video in seconds.',
        'type' => 'int',
        'unsigned' => true,
        'not null' => true,
        'default' => 0,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '-1',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'video_preview',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'video_video',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  1 => 
  array (
    'label' => 'Thumbnail Image',
    'field_name' => 'field_thumbnail',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => '-1',
    'autocomplete_match' => 'contains',
    'size' => '60',
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_thumbnail][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_thumbnail' => 
      array (
        0 => 
        array (
          'nid' => 
          array (
            'nid' => '',
            '_error_element' => 'default_value_widget][field_thumbnail][0][nid][nid',
          ),
          '_error_element' => 'default_value_widget][field_thumbnail][0][nid][nid',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'referenceable_types' => 
    array (
      'image' => 'image',
      'article' => 0,
      'blog_post' => 0,
      'boat_test' => 0,
      'channel' => 0,
      'person' => 0,
      'forum' => 0,
      'gallery' => 0,
      'homepage' => 0,
      'link' => 0,
      'marketplace' => 0,
      'menu_item' => 0,
      'news' => 0,
      'pcd' => 0,
      'page' => 0,
      'panel' => 0,
      'photo' => 0,
      'poll' => 0,
      'profile' => 0,
      'user_answer' => 0,
      'user_question' => 0,
      'topic_tout' => 0,
      'video' => 0,
      'vtd' => 0,
      'webform' => 0,
      'video' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' => 
    array (
      'nid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '6',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  2 => 
  array (
    'label' => 'Related Content',
    'field_name' => 'field_related_content',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => 0,
    'autocomplete_match' => 'contains',
    'size' => '60',
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_related_content][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_related_content' => 
      array (
        0 => 
        array (
          'nid' => 
          array (
            'nid' => '',
            '_error_element' => 'default_value_widget][field_related_content][0][nid][nid',
          ),
          '_error_element' => 'default_value_widget][field_related_content][0][nid][nid',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '1',
    'referenceable_types' => 
    array (
      'article' => 'article',
      'blog_post' => 'blog_post',
      'page' => 'page',
      'user_answer' => 'user_answer',
      'user_question' => 'user_question',
      'video' => 'video',
      'webform' => 'webform',
      'boat_test' => 0,
      'channel' => 0,
      'person' => 0,
      'forum' => 0,
      'gallery' => 0,
      'homepage' => 0,
      'image' => 0,
      'link' => 0,
      'marketplace' => 0,
      'menu_item' => 0,
      'news' => 0,
      'pcd' => 0,
      'panel' => 0,
      'photo' => 0,
      'poll' => 0,
      'profile' => 0,
      'topic_tout' => 0,
      'vtd' => 0,
      'video' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' => 
    array (
      'nid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '8',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content['extra']  = array (
  'title' => '-5',
  'body_field' => '-4',
  'revision_information' => '4',
  'comment_settings' => '7',
  'menu' => '1',
  'taxonomy' => '-3',
  'path' => '6',
  'nodewords' => '2',
  'print' => '5',
  'scheduler_settings' => '3',
);
