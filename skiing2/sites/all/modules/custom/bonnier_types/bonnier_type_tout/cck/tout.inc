<?php
$content['type']  = array (
  'name' => 'Tout',
  'type' => 'tout',
  'description' => 'Touts are used to replace a node being viewed in a list.  This allows editors to customize the teaser display per section.',
  'title_label' => 'Title',
  'body_label' => '',
  'min_word_count' => 0,
  'help' => '',
  'node_options' => 
  array (
    'status' => false,
    'promote' => false,
    'sticky' => false,
    'revision' => false,
  ),
  'addthis_nodetype' => 1,
  'forward_display' => true,
  'scheduler' => 0,
  'scheduler_touch' => 0,
  'old_type' => 'tout',
  'orig_type' => 'tout',
  'module' => 'bonnier_type_tout',
  'custom' => false,
  'modified' => false,
  'locked' => true,
  'reset' => 'Reset to defaults',
  'nodewords_edit_metatags' => true,
  'nodewords_basic_use_teaser' => 1,
  'xmlsitemap_node_status' => 1,
  'xmlsitemap_node_priority' => 0.5,
  'content_profile_use' => false,
  'comment' => 0,
  'comment_default_mode' => 2,
  'comment_default_order' => 2,
  'comment_default_per_page' => 50,
  'comment_controls' => 3,
  'comment_anonymous' => 2,
  'comment_subject_field' => 0,
  'comment_preview' => 1,
  'comment_form_location' => 1,
  'flatcomments_remove_reply_link' => 
  array (
    'reply' => false,
  ),
  'fivestar' => 0,
  'fivestar_stars' => 5,
  'fivestar_labels_enable' => 1,
  'fivestar_label_0' => 'Cancel rating',
  'fivestar_label_1' => 'Poor',
  'fivestar_label_2' => 'Okay',
  'fivestar_label_3' => 'Good',
  'fivestar_label_4' => 'Great',
  'fivestar_label_5' => 'Awesome',
  'fivestar_label_6' => 'Give it @star/@count',
  'fivestar_label_7' => 'Give it @star/@count',
  'fivestar_label_8' => 'Give it @star/@count',
  'fivestar_label_9' => 'Give it @star/@count',
  'fivestar_label_10' => 'Give it @star/@count',
  'fivestar_style' => 'average',
  'fivestar_text' => 'dual',
  'fivestar_title' => 1,
  'fivestar_feedback' => 1,
  'fivestar_unvote' => 0,
  'fivestar_position_teaser' => 'hidden',
  'fivestar_position' => 'below',
  'fivestar_comment' => 0,
  'page_title' => 
  array (
    'show_field' => 
    array (
      'show_field' => false,
    ),
    'pattern' => '',
  ),
  'print_display' => 1,
  'print_display_comment' => 0,
  'print_display_urllist' => 1,
  'feedapi' => 
  array (
    'enabled' => false,
    'upload_method' => 'url',
    'refresh_on_create' => 0,
    'update_existing' => 1,
    'refresh_time' => 1800,
    'items_delete' => 0,
    'parsers' => 
    array (
      'travelocity_parser_csv' => 
      array (
        'enabled' => false,
        'weight' => 0,
        'delimiter' => ',',
      ),
      'parser_csv' => 
      array (
        'enabled' => false,
        'weight' => 0,
        'csv_on' => '',
        'delimiter' => ',',
      ),
      'parser_common_syndication' => 
      array (
        'enabled' => false,
        'weight' => 0,
        'accept_invalid_cert' => false,
      ),
    ),
    'processors' => 
    array (
      'feedapi_node' => 
      array (
        'enabled' => false,
        'weight' => 0,
        'content_type' => 'webform',
        'input_format' => 0,
        'node_date' => 'feed',
        'x_dedupe' => 0,
      ),
    ),
  ),
  'better_formats_allowed' => 
  array (
    1 => false,
    2 => false,
    3 => false,
    4 => false,
  ),
  'better_formats_defaults' => 
  array (
    'node-4' => 
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'node-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'node-9' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'node-2' => 
    array (
      'format' => '0',
      'weight' => '-23',
    ),
    'node-1' => 
    array (
      'format' => '0',
      'weight' => '-22',
    ),
    'comment-4' => 
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'comment-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'comment-2' => 
    array (
      'format' => '4',
      'weight' => '-23',
    ),
    'comment-1' => 
    array (
      'format' => '4',
      'weight' => '-22',
    ),
  ),
);
$content['groups']  = array (
  0 => 
  array (
    'label' => 'Target',
    'group_type' => 'standard',
    'settings' => 
    array (
      'form' => 
      array (
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => 
      array (
        'description' => '',
        'teaser' => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        2 => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        3 => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'label' => 'above',
      ),
    ),
    'weight' => '36',
    'group_name' => 'group_target',
  ),
  1 => 
  array (
    'label' => 'Teaser Photo',
    'group_type' => 'standard',
    'settings' => 
    array (
      'form' => 
      array (
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => 
      array (
        'description' => '',
        'teaser' => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        4 => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        2 => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        3 => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => 
        array (
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'label' => 'above',
      ),
    ),
    'weight' => '37',
    'group_name' => 'group_teaser_photo',
  ),
);
$content['fields']  = array (
  0 => 
  array (
    'label' => 'Internal Destination',
    'field_name' => 'field_target',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => '50',
    'autocomplete_match' => 'contains',
    'size' => '60',
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_target][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => 'group_target',
    'required' => 0,
    'multiple' => '0',
    'referenceable_types' => 
    array (
      'accommodations' => 'accommodations',
      'article' => 'article',
      'blog' => 'blog',
      'blog_post' => 'blog_post',
      'editorial_ads' => 'editorial_ads',
      'laserfist_gallery' => 'laserfist_gallery',
      'location' => 'location',
      'magazine' => 'magazine',
      'page' => 'page',
      'travel_specials' => 'travel_specials',
      'video' => 'video',
      'villas' => 'villas',
      'wallpaper' => 'wallpaper',
      'webform' => 'webform',
      'channel' => 0,
      'person' => 0,
      'feed' => 0,
      'forum' => 0,
      'homepage' => 0,
      'hotel_feed' => 0,
      'image' => 0,
      'link' => 0,
      'location_feed' => 0,
      'menu_item' => 0,
      'pcd' => 0,
      'panel' => 0,
      'profile' => 0,
      'topic_tout_videos' => 0,
      'topic_tout_photos' => 0,
      'tout' => 0,
      'twitter_feed' => 0,
      'ugc_photo' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' => 
    array (
      'nid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' => 
    array (
      'label' => 
      array (
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  1 => 
  array (
    'label' => 'External Destination',
    'field_name' => 'field_redirect',
    'type' => 'cck_redirection',
    'widget_type' => 'cck_redirection',
    'change' => 'Change basic information',
    'weight' => '51',
    'redirect_type' => '0',
    'description' => 'Place relative path url here to send Editorial Ad to another page. IE content/test-article-1',
    'default_value' => 
    array (
      0 => 
      array (
        'value' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_redirect' => 
      array (
        0 => 
        array (
          'value' => '',
        ),
      ),
    ),
    'group' => 'group_target',
    'required' => 0,
    'multiple' => '0',
    'op' => 'Save field settings',
    'module' => 'cck_redirection',
    'widget_module' => 'cck_redirection',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
        'default value' => NULL,
      ),
    ),
    'display_settings' => 
    array (
      'label' => 
      array (
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  2 => 
  array (
    'label' => 'Main Photo',
    'field_name' => 'field_main_photo',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => '56',
    'autocomplete_match' => 'contains',
    'size' => '60',
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_main_photo][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_main_photo' => 
      array (
        0 => 
        array (
          'nid' => 
          array (
            'nid' => '',
            '_error_element' => 'default_value_widget][field_main_photo][0][nid][nid',
          ),
          '_error_element' => 'default_value_widget][field_main_photo][0][nid][nid',
        ),
      ),
    ),
    'group' => 'group_teaser_photo',
    'required' => 0,
    'multiple' => '0',
    'referenceable_types' => 
    array (
      'image' => 'image',
      'accommodations' => 0,
      'article' => 0,
      'blog' => 0,
      'blog_post' => 0,
      'channel' => 0,
      'person' => 0,
      'editorial_ads' => 0,
      'feed' => 0,
      'forum' => 0,
      'homepage' => 0,
      'hotel_feed' => 0,
      'laserfist_gallery' => 0,
      'link' => 0,
      'location' => 0,
      'location_feed' => 0,
      'magazine' => 0,
      'menu_item' => 0,
      'pcd' => 0,
      'page' => 0,
      'panel' => 0,
      'profile' => 0,
      'topic_tout_videos' => 0,
      'topic_tout_photos' => 0,
      'tout' => 0,
      'travel_specials' => 0,
      'twitter_feed' => 0,
      'ugc_photo' => 0,
      'video' => 0,
      'villas' => 0,
      'wallpaper' => 0,
      'webform' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' => 
    array (
      'nid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '5',
      'parent' => '',
      'label' => 
      array (
        'format' => 'hidden',
      ),
      'teaser' => 
      array (
        'format' => 'full',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'full',
        'exclude' => 1,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  3 => 
  array (
    'label' => 'Image',
    'field_name' => 'field_image',
    'type' => 'filefield',
    'widget_type' => 'imagefield_widget',
    'change' => 'Change basic information',
    'weight' => '57',
    'file_extensions' => 'jpg jpeg png gif',
    'progress_indicator' => 'bar',
    'file_path' => '_images/tout/[site-date-yyyy][site-date-mm]',
    'max_filesize_per_file' => '',
    'max_filesize_per_node' => '',
    'max_resolution' => 0,
    'min_resolution' => 0,
    'custom_alt' => 1,
    'alt' => '',
    'custom_title' => 0,
    'title_type' => 'textfield',
    'title' => '',
    'use_default_image' => 0,
    'default_image_upload' => '',
    'default_image' => NULL,
    'description' => '',
    'group' => 'group_teaser_photo',
    'required' => 0,
    'multiple' => '0',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'op' => 'Save field settings',
    'module' => 'filefield',
    'widget_module' => 'imagefield',
    'columns' => 
    array (
      'fid' => 
      array (
        'type' => 'int',
        'not null' => false,
        'views' => true,
      ),
      'list' => 
      array (
        'type' => 'int',
        'size' => 'tiny',
        'not null' => false,
        'views' => true,
      ),
      'data' => 
      array (
        'type' => 'text',
        'serialize' => true,
        'views' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '-4',
      'parent' => '',
      'label' => 
      array (
        'format' => 'hidden',
      ),
      'teaser' => 
      array (
        'format' => 'image_plain',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'image_plain',
        'exclude' => 1,
      ),
      2 => 
      array (
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'image_plain',
        'exclude' => 0,
      ),
    ),
  ),
  4 => 
  array (
    'label' => 'Dek',
    'field_name' => 'field_dek',
    'type' => 'text',
    'widget_type' => 'text_textarea',
    'change' => 'Change basic information',
    'weight' => '38',
    'rows' => '5',
    'size' => 60,
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'value' => '',
        'format' => '2',
        '_error_element' => 'default_value_widget][field_dek][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_dek' => 
      array (
        0 => 
        array (
          'value' => '',
          'format' => '2',
          '_error_element' => 'default_value_widget][field_dek][0][value',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'text_processing' => '1',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
        'views' => true,
      ),
      'format' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'views' => false,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '-3',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  5 => 
  array (
    'label' => 'Open In New Window',
    'field_name' => 'field_link_target_blank',
    'type' => 'text',
    'widget_type' => 'optionwidgets_onoff',
    'change' => 'Change basic information',
    'weight' => '52',
    'description' => 'Create this link so that it opens in a new windows. This applies to both the "Tout Target" and "Menu Target" fields.',
    'default_value' => 
    array (
      0 => 
      array (
        'value' => 'on',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_link_target_blank' => 
      array (
        'value' => true,
      ),
    ),
    'group' => 'group_target',
    'required' => 0,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => 'off|Open In New Window
on|Open In New Window',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'optionwidgets',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
        'views' => true,
      ),
    ),
    'display_settings' => 
    array (
      'label' => 
      array (
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      5 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content['extra']  = array (
  'title' => '35',
  'body_field' => '40',
  'revision_information' => '43',
  'comment_settings' => '47',
  'menu' => '39',
  'taxonomy' => '-3',
  'path' => '46',
  'nodewords' => '41',
  'print' => '44',
  'scheduler_settings' => '42',
  'xmlsitemap' => '45',
);

