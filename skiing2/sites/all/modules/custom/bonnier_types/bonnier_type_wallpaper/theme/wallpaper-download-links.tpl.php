<?php if($links): ?>
  <div class="wallpaper-download">
    <h3>Resolution Options</h3>
    <p>Use one of the buttons below to download the wallpaper in the resolution of your choice.</p>
    <div class="wallpaper-links">
      <?php echo $links; ?>
    </div>
  </div>
<?php endif; ?>

<?php if($instructions): ?>
  <div class="instructions">
    <?php print $instructions; ?>
  </div>
<?php endif; ?>