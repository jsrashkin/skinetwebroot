<?php
$presets['image_555x335'] = array (
  'presetname' => 'image_555x335',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '555',
        'height' => '335',
        'upscale' => 0,
      ),
    ),
    1 => 
    array (
      'weight' => '-9',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_definecanvas',
      'data' => 
      array (
        'RGB' => 
        array (
          'HEX' => 'FFFFFF',
        ),
        'under' => 1,
        'exact' => 
        array (
          'width' => '555',
          'height' => '335',
          'xpos' => 'center',
          'ypos' => 'center',
        ),
        'relative' => 
        array (
          'leftdiff' => '',
          'rightdiff' => '',
          'topdiff' => '',
          'bottomdiff' => '',
        ),
      ),
    ),
  ),
);