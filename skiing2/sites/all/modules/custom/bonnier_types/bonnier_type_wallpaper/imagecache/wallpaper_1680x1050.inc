<?php
$presets['wallpaper_1680x1050'] = array (
  'presetname' => 'wallpaper_1680x1050',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '1680',
        'height' => '1050',
        'upscale' => 0,
      ),
    ),
 ),
);
