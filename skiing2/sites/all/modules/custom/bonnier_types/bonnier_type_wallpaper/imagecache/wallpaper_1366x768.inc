<?php
$presets['wallpaper_1366x768'] = array (
  'presetname' => 'wallpaper_1366x768',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '1366',
        'height' => '768',
        'upscale' => 0,
      ),
    ),
 ),
);
