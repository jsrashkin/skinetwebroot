<?php
$presets['wallpaper_1024x768'] = array (
  'presetname' => 'wallpaper_1024x768',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '1024',
        'height' => '768',
        'upscale' => 0,
      ),
    ),
 ),
);
