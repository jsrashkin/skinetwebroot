<?php
$presets['wallpaper_640x480'] = array (
  'presetname' => 'wallpaper_640x480',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '640',
        'height' => '480',
        'upscale' => 0,
      ),
    ),
 ),
);
