<?php
$presets['wallpaper_1440x900'] = array (
  'presetname' => 'wallpaper_1440x900',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '1440',
        'height' => '900',
        'upscale' => 0,
      ),
    ),
 ),
);
