<?php
$presets['wallpaper_1280x1024'] = array (
  'presetname' => 'wallpaper_1280x1024',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '1280',
        'height' => '1024',
        'upscale' => 0,
      ),
    ),
 ),
);
