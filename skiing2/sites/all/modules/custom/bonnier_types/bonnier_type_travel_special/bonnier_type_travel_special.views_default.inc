<?php
/**
 * @file
 * 
 */

function bonnier_type_travel_special_views_default_views() {
  $views = array();
  $path = dirname(__FILE__) .'/views';

  // The main list view.
  include($path .'/view.travel_specials_list.inc');
  $views[$view->name] = $view;
  
  return $views;
}
