/**
 * @file
 * JS file for the Travel Specials List Header CTools content type
 */

Drupal.behaviors.travel_specials_jumper = function() {
  // Add the navigation items.
  $('.view-travel-specials-list a.inline-link').each(function(linky) {
    var the_name = $(this).attr('name');
    $("#region-jumper").append('<option value="#' + the_name + '">' + the_name + '</option>');
  });
  // Add the navigation action.
  $('#region-jumper').change(function() {
    var the_loc = $('#region-jumper').val();
    if (the_loc != '') {
      window.location.href = the_loc;
    }
  });
  // Show the navigation selector.
  $('#region-jumper').show();
}
