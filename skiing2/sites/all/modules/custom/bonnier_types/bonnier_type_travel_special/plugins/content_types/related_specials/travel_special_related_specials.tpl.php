<?php
/**
 * @file
 * Template file for the Travel Specials Related Specials block.
 */

/**
 * Variables available for use:
 * $title - the title for this block.
 * $all - a link to the main Travel Specials page.
 * $specials - an array of travel specials all ready for outputting.
 */

?>
<div class="travel-special-related">
  <?php if ($title || $all): ?>
    <div class="tsr-heading">
      <?php if ($title): ?>
        <div class="tsr-title"><?php print $title; ?></div>
      <?php endif; ?>
      <?php if ($all): ?>
        <div class="tsr-all"><?php print $all; ?></div>
      <?php endif; ?>
    </div>
  <?php endif; ?>
  <div class="tsr-list">
    <?php foreach ($specials as $special): ?>
      <?php print $special; ?>
    <?php endforeach; ?>
  </div>
</div>
