<?php
/**
 * @file
 * Template file for displaying one Related Travel Special.
 */

/**
 * Variables available for use:
 * $node - the full node object.
 * $thumb - node thumbnail.
 * $title - node title.
 * $dek - node dek.
 */

?>
<div class="tsr-item">
  <div class="left">
    <div class="tsr-item-thumb"><?php print $thumb; ?></div>
  </div>
  <div class="right">
    <div class="tsr-item-title"><?php print $title; ?></div>
    <div class="tsr-item-dek"><?php print $dek; ?></div>
  </div>
</div>
