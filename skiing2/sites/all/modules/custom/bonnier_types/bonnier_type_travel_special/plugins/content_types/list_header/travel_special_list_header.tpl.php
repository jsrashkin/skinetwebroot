<?php
/**
 * @file
 * Template file for the Travel Specials List Header block.
 */

/**
 * Variables available for use:
 * $title - the title for this page.
 * $rss - a link to the RSS feed.
 */

?>
<?php if ($title || $rss): ?>
  <div class="travel-special-header">
    <?php if ($title): ?>
      <div class="tsh-title"><?php print $title; ?></div>
    <?php endif; ?>
    <?php if ($rss): ?>
      <div class="tsh-rss"><?php print $rss; ?></div>
    <?php endif; ?>
    <?php if ($form): ?>
      <div class="tsh-jump">
        <?php print $form; ?>
      </div>
    <?php endif; ?>
  </div>
<?php endif; ?>
