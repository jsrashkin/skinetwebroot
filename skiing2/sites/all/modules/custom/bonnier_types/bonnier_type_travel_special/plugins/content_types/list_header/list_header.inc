<?php

function bonnier_type_travel_special_list_header_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Travel Specials - List Header'),
    'category' => 'Bonnier',
		'hook theme' => 'bonnier_type_travel_special_list_header_theme',
  );
}

function bonnier_type_travel_special_list_header_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new StdClass();
  $block->content = theme('travel_special_list_header');
  $block->title = '';
  $block->delta = 'list_header';
	$block->css_id = 'travel-special-list-header';
  return $block;
}

function template_preprocess_travel_special_list_header(&$vars) {
  $path = variable_get('travel_specials_path', 'travel-specials');
  $path_rss = variable_get('travel_specials_path_rss', $path .'.rss');
  $path_icon = variable_get('travel_specials_path_icon', base_path() .'/sites/all/modules/platform/site_map/feed-small.png');
  $title = t('Travel Specials');

  $vars['title'] = $title;
  $vars['rss'] = l("<img src=\"{$path_icon}\" />", $path_rss, array('html' => TRUE));
  $vars['form'] = drupal_get_form('bonnier_type_travel_special_list_header_form');
}

function bonnier_type_travel_special_list_header_theme(&$theme) {
	$theme['travel_special_list_header'] = array(
    'template' => 'travel_special_list_header',
		'arguments' => array(),
		'path' => drupal_get_path('module', 'bonnier_type_travel_special') .'/plugins/content_types/list_header',
	);
}

/**
 * The form to build the custom region selector navigation thing.
 */
function bonnier_type_travel_special_list_header_form() {
  // Javascript that's needed to compile the form options.
  drupal_add_js(drupal_get_path('module', 'bonnier_type_travel_special') .'/plugins/content_types/list_header/list_header.js', 'module', 'footer');

  $form = array();

  // The selector, hidden by default, will be enabled via JS.
  $form['select'] = array(
    '#type' => 'select',
    '#title' => '',
    '#options' => array('' => t('Jump to Region')),
    '#attributes' => array(
      'id' => 'region-jumper',
      'style' => 'display:none;',
    ),
  );
  
  return $form;
}
