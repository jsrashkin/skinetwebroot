<?php

function bonnier_type_logbook_logbook_user_header_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Logbook User Header'),
    'description' => t('Logbook User Header'),
    'required context' => new ctools_context_required(t('Node being Viewed'),'node'),
    'category' => 'Bonnier',
		'hook theme' => 'bonnier_type_logbook_logbook_user_header_theme',
  );
}

function bonnier_type_logbook_logbook_user_header_content_type_render($subtype, $conf, $panel_args, $context) {
	global $user;
	$node = $context->data;
	//only load profile header for user viewing the profile not for the owner
	if($node->uid != $user->uid) {
		$account = user_load($node->uid);
		$content = bonnier_profile_profile_header($account,$user);
	} else {
		//put edit and remove buttons
		$edit_button = l('Edit','logbook/'.$user->uid.'/edit/'.$node->nid, array('attributes' => array('class' => 'button-edit')));
		$remove_button = l('Remove','node/'.$node->nid.'/delete', array('attributes' => array('class' => 'button-remove')));
		$content = '<div class="logbook-buttons">'.$edit_button.' '.$remove_button.'</div>';
	}


  $block->content = $content;
  $block->title = '';
  $block->delta = 'logbook_user_header';
	$block->css_class = 'logbook-user-header';
	$block->css_id = 'logbook-user-header';
  return $block;
}

function template_preprocess_logbook_user_header(&$vars) {
	$node = $vars['node'];

	$link = bonnier_common_link($node);
	$dek = bonnier_common_dek($node);
	$title = bonnier_common_title($node);

	$vars['title'] = l($title,$link);
	$vars['dek'] = $dek;
	$vars['more'] = l('more',$link);
}

function bonnier_type_logbook_logbook_user_header_theme(&$theme) {
	$theme['logbook_user_header'] = array(
		'template' => 'logbook-user-header',
		'arguments' => array('node' => array()),
		'path' => drupal_get_path('module','bonnier_type_logbook').'/plugins/content_types/logbook_user_header',
	);
}

function bonnier_type_logbook_logbook_user_header_content_type_edit_form(&$form, &$form_state) {
}

