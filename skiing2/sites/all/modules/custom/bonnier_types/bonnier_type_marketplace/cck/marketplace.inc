<?php
$content['type']  = array (
  'name' => 'Marketplace article',
  'type' => 'marketplace',
  'description' => 'A <em>marketplace article</em> is an article about a vendor.',
  'title_label' => 'Vendor',
  'body_label' => 'Description',
  'min_word_count' => '0',
  'help' => '',
  'node_options' =>
  array (
    'status' => true,
    'promote' => false,
    'sticky' => false,
    'revision' => false,
  ),
  'addthis_nodetype' => 1,
  'forward_display' => 0,
  'scheduler' => 0,
  'scheduler_touch' => 0,
  'old_type' => 'marketplace',
  'orig_type' => 'marketplace',
  'module' => 'bonnier_type_marketplace',
  'custom' => '0',
  'modified' => '1',
  'locked' => '1',
  'reset' => 'Reset to defaults',
  'nodewords_edit_metatags' => 1,
  'nodewords_metatags_generation_method' => '0',
  'nodewords_metatags_generation_source' => '2',
  'nodewords_use_alt_attribute' => 1,
  'nodewords_filter_modules_output' =>
  array (
    'imagebrowser' => false,
    'img_assist' => false,
  ),
  'nodewords_filter_regexp' => '',
  'comment' => '0',
  'comment_default_mode' => '2',
  'comment_default_order' => '2',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => '2',
  'comment_subject_field' => '0',
  'comment_preview' => '1',
  'comment_form_location' => '1',
  'flatcomments_remove_reply_link' =>
  array (
    'reply' => false,
  ),
  'content_profile_use' => 0,
  'fivestar' => 0,
  'fivestar_stars' => 5,
  'fivestar_labels_enable' => 1,
  'fivestar_label_0' => 'Cancel rating',
  'fivestar_label_1' => 'Poor',
  'fivestar_label_2' => 'Okay',
  'fivestar_label_3' => 'Good',
  'fivestar_label_4' => 'Great',
  'fivestar_label_5' => 'Awesome',
  'fivestar_label_6' => 'Give it @star/@count',
  'fivestar_label_7' => 'Give it @star/@count',
  'fivestar_label_8' => 'Give it @star/@count',
  'fivestar_label_9' => 'Give it @star/@count',
  'fivestar_label_10' => 'Give it @star/@count',
  'fivestar_style' => 'average',
  'fivestar_text' => 'dual',
  'fivestar_title' => 1,
  'fivestar_feedback' => 1,
  'fivestar_unvote' => 0,
  'fivestar_position_teaser' => 'hidden',
  'fivestar_position' => 'below',
  'fivestar_comment' => 0,
  'better_formats_allowed' =>
  array (
    1 => false,
    2 => false,
    3 => false,
    4 => false,
  ),
  'better_formats_defaults' =>
  array (
    'node-4' =>
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'node-3' =>
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'node-2' =>
    array (
      'format' => '0',
      'weight' => '-23',
    ),
    'node-1' =>
    array (
      'format' => '0',
      'weight' => '-22',
    ),
    'comment-4' =>
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'comment-3' =>
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'comment-2' =>
    array (
      'format' => '4',
      'weight' => '-23',
    ),
    'comment-1' =>
    array (
      'format' => '4',
      'weight' => '-22',
    ),
  ),
);
$content['fields']  = array (
  0 =>
  array (
    'label' => 'Dek',
    'field_name' => 'field_dek',
    'type' => 'text',
    'widget_type' => 'text_textarea',
    'change' => 'Change basic information',
    'weight' => '-3',
    'rows' => 5,
    'size' => 60,
    'description' => '',
    'default_value' =>
    array (
      0 =>
      array (
        'value' => '',
        '_error_element' => 'default_value_widget][field_dek][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => false,
    'required' => 1,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' =>
    array (
      'value' =>
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
        'views' => true,
      ),
    ),
    'display_settings' =>
    array (
      'teaser' =>
      array (
        'format' => 'default',
        'exclude' => true,
      ),
      'full' =>
      array (
        'format' => 'default',
        'exclude' => true,
      ),
    ),
  ),
  1 =>
  array (
    'label' => 'Web site',
    'field_name' => 'field_website',
    'type' => 'link',
    'widget_type' => 'link',
    'change' => 'Change basic information',
    'weight' => '-1',
    'description' => '',
    'default_value' =>
    array (
      0 =>
      array (
        'url' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array (
      'field_website' =>
      array (
        0 =>
        array (
          'url' => '',
          'title' => '',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'url' => 0,
    'title' => 'optional',
    'title_value' => '',
    'enable_tokens' => 0,
    'display' =>
    array (
      'url_cutoff' => '80',
    ),
    'attributes' =>
    array (
      'target' => 'default',
      'rel' => '',
      'class' => '',
    ),
    'op' => 'Save field settings',
    'module' => 'link',
    'widget_module' => 'link',
    'columns' =>
    array (
      'url' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'title' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
      'attributes' =>
      array (
        'type' => 'text',
        'size' => 'medium',
        'not null' => false,
      ),
    ),
    'display_settings' =>
    array (
      'weight' => '-1',
      'parent' => '',
      'label' =>
      array (
        'format' => 'hidden',
      ),
      'teaser' =>
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  2 =>
  array (
    'label' => 'Phone',
    'field_name' => 'field_phone',
    'type' => 'int_phone',
    'widget_type' => 'phone_textfield',
    'change' => 'Change basic information',
    'weight' => 0,
    'description' => '',
    'default_value' =>
    array (
      0 =>
      array (
        'value' => '',
        '_error_element' => 'default_value_widget][field_phone][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array (
      'field_phone' =>
      array (
        0 =>
        array (
          'value' => '',
          '_error_element' => 'default_value_widget][field_phone][0][value',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'phone_country_code' => 0,
    'phone_default_country_code' => '',
    'phone_int_max_length' => '',
    'op' => 'Save field settings',
    'module' => 'phone',
    'widget_module' => 'phone',
    'columns' =>
    array (
      'value' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
      ),
    ),
    'display_settings' =>
    array (
      'weight' => 0,
      'parent' => '',
      'label' =>
      array (
        'format' => 'above',
      ),
      'teaser' =>
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  3 =>
  array (
    'label' => 'E-mail',
    'field_name' => 'field_manufacturer_email',
    'type' => 'email',
    'widget_type' => 'email_textfield',
    'change' => 'Change basic information',
    'weight' => '1',
    'size' => '60',
    'description' => '',
    'default_value' =>
    array (
      0 =>
      array (
        'value' => '',
        '_error_element' => 'default_value_widget][field_manufacturer_email][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array (
      'field_manufacturer_email' =>
      array (
        0 =>
        array (
          'value' => '',
          '_error_element' => 'default_value_widget][field_manufacturer_email][0][value',
          'email' => '',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'op' => 'Save field settings',
    'module' => 'email',
    'widget_module' => 'email',
    'columns' =>
    array (
      'email' =>
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
        'sortable' => true,
      ),
    ),
    'display_settings' =>
    array (
      'weight' => '1',
      'parent' => '',
      'label' =>
      array (
        'format' => 'above',
      ),
      'teaser' =>
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  4 =>
  array (
    'label' => 'Main Photo',
    'field_name' => 'field_main_photo',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => '3',
    'autocomplete_match' => 'contains',
    'size' => 60,
    'description' => '',
    'default_value' =>
    array (
      0 =>
      array (
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_main_photo][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array (
      'field_main_photo' =>
      array (
        0 =>
        array (
          'nid' =>
          array (
            'nid' => '',
            '_error_element' => 'default_value_widget][field_main_photo][0][nid][nid',
          ),
          '_error_element' => 'default_value_widget][field_main_photo][0][nid][nid',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'referenceable_types' =>
    array (
      'image' => 'image',
      'article' => false,
      'blog_post' => false,
      'channel' => false,
      'person' => false,
      'forum' => false,
      'gallery' => false,
      'homepage' => false,
      'link' => false,
      'marketplace' => false,
      'menu_item' => false,
      'pcd' => false,
      'page' => false,
      'panel' => false,
      'photo' => false,
      'profile' => false,
      'topic_tout' => false,
      'video' => false,
      'vtd' => false,
      'webform' => false,
      'blog' => false,
      'event' => false,
      'poll' => false,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' =>
    array (
      'nid' =>
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' =>
    array (
      'teaser' =>
      array (
        'format' => 'full',
        'exclude' => true,
      ),
      'full' =>
      array (
        'format' => 'full',
        'exclude' => true,
      ),
    ),
  ),
  5 =>
  array (
    'label' => 'Thumbnail Image',
    'field_name' => 'field_thumbnail',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_autocomplete',
    'change' => 'Change basic information',
    'weight' => '5',
    'autocomplete_match' => 'contains',
    'size' => 60,
    'description' => '',
    'default_value' =>
    array (
      0 =>
      array (
        'nid' => NULL,
        '_error_element' => 'default_value_widget][field_thumbnail][0][nid][nid',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' =>
    array (
      'field_thumbnail' =>
      array (
        0 =>
        array (
          'nid' =>
          array (
            'nid' => '',
            '_error_element' => 'default_value_widget][field_thumbnail][0][nid][nid',
          ),
          '_error_element' => 'default_value_widget][field_thumbnail][0][nid][nid',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'referenceable_types' =>
    array (
      'image' => 'image',
      'article' => 0,
      'blog_post' => 0,
      'channel' => 0,
      'person' => 0,
      'forum' => 0,
      'gallery' => 0,
      'homepage' => 0,
      'link' => 0,
      'marketplace' => 0,
      'menu_item' => 0,
      'pcd' => 0,
      'page' => 0,
      'panel' => 0,
      'photo' => 0,
      'poll' => 0,
      'profile' => 0,
      'user_answer' => 0,
      'user_question' => 0,
      'topic_tout' => 0,
      'video' => 0,
      'vtd' => 0,
      'webform' => 0,
      'blog' => false,
      'event' => false,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' =>
    array (
      'nid' =>
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' =>
    array (
      'teaser' =>
      array (
        'format' => 'default',
        'exclude' => true,
      ),
      'full' =>
      array (
        'format' => 'default',
        'exclude' => true,
      ),
    ),
  ),
);
$content['extra']  = array (
  'main_photo_placement' => '4',
  'title' => '-4',
  'body_field' => '-2',
  'revision_information' => '7',
  'comment_settings' => '8',
  'menu' => '6',
  'taxonomy' => '2',
  'path' => '9',
  'nodewords' => '10',
  'scheduler_settings' => '11',
  'xmlsitemap' => '12',
);
