<div class="location vcard"><div class="adr">
<?php if ($name): ?>
  <div class="location name"><?php print $name; ?></div>
<?php endif; ?>
<?php if ($street): ?>
  <span class="street-address"><?php print $street; ?><?php if ($additional): ?>
      <?php print $additional; ?>
    <?php endif; ?></span><?php if ($city_province_postal): ?>, <?php endif; ?>
<?php endif; ?>
<?php print $city_province_postal; ?>
<?php if ($country_name): ?>
  <?php if ($street || $city_province_postal): ?>, <?php endif; ?>
  <span class="country-name"><?php print $country_name; ?></span>
<?php endif; ?>
</div></div>
