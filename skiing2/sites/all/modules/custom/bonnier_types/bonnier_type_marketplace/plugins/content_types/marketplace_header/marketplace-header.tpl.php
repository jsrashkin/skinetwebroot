<div id="channel-sub-nav">
  <?php if (isset($node)): ?>
    <div class="left">
  <?php endif; ?>
    <?php if (isset($parent)): ?>
      <div id="parent"><?php print $parent; ?></div>
    <?php endif; ?>
    <?php if (isset($node)): ?>
      <div id="title"><?php print $title; ?></div>
    <?php else: ?>
      <div id="title"><h1><?php print $title; ?></h1></div>
    <?php endif; ?>
    <div id="description"><?php print $description; ?></div>
  <?php if (isset($node)): ?>
    </div>
    <div class="right">
      <div class="utility-links">
        <div class="email"><?php print $email; ?></div>
        <div class="print"><?php print $print; ?></div>
      </div>
    </div>
  <?php endif; ?>
</div>
