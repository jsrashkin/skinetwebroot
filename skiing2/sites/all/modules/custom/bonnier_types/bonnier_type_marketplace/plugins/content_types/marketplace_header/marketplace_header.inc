<?php
// $Id$

function bonnier_type_marketplace_marketplace_header_ctools_content_types() {
  if (module_exists('taxonomy')) {
    return array(
      'single' => TRUE,
      'title' => t('Marketplace header'),
      'description' => t('Header for marketplace channels and content type.'),
      'all contexts' => TRUE,
      'required context' => new ctools_context_required(t('Term'), 'term'),
      'category' => t('Taxonomy term'),
      'hook theme' => 'bonnier_type_marketplace_marketplace_header_theme',
    );
  }
}

function bonnier_type_marketplace_marketplace_header_content_type_edit_form(&$form, &$form_state) {
}

function bonnier_type_marketplace_marketplace_header_content_type_render($subtype, $conf, $panel_args, $contexts) {
  if (!empty($contexts[$conf['context']]) && !empty($contexts[$conf['context']]->data)) {
    $term = $contexts[$conf['context']]->data;
    $block->content = theme('marketplace_header', $term, $contexts);
    return $block;
  }
}

function bonnier_type_marketplace_marketplace_header_theme(&$theme) {
  $theme['marketplace_header'] = array(
    'template' => 'marketplace-header',
    'arguments' => array('term' => NULL, 'contexts' => NULL),
    'path' => drupal_get_path('module', 'bonnier_type_marketplace') . '/plugins/content_types/marketplace_header',
  );
}

function template_preprocess_marketplace_header(&$variables) {
  $term = $variables['term'];
  $parents = taxonomy_get_parents_all($term->tid);
  if (count($parents) > 1) {
    $marketplace = end($parents);
    $channels = taxonomy_get_tree($marketplace->vid, $marketplace->tid, -1, 1);
    if (count($channels) > 1) {
      reset($channels);
      $parent = next($parents);
    }
    else {
      $parent = end($parents);
    }
    $variables['parent'] = $parent->name;
  }
  $variables['title'] = $term->name;
  $variables['description'] = $term->description;
  $contexts = $variables['contexts'];
  if (isset($contexts['argument_nid_1'])) {
    $node = $contexts['argument_nid_1'];
    $variables['node'] = $node;
    laserfist_common_preprocess_node($variables, $node);
  }
}
