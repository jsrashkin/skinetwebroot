<?php
/**
 * @file
 * UGC Photo node template.
 */
 
/**
 * Variables:
 * $preview - Is this page the node add/edit preview page? (boolean)
 * $title - The title of this node (boolean).
 * $name - Person who uploaded the file (string).
 * $tags - Output-ready list of terms associated with this node (boolean).
 */

// On the node-edit preview page mimic the pane header.
?>
<?php if ($preview || $node_header): ?>
  <div class="panel-pane article-toolbar">
    <div class="pane-content">
      <div id="content-area-header">
        <div class="left">
          <h2 class="main-title"><?php print $title; ?></h2>
          <div class="clear"></div>
          <div class="dek">By <?php print $name; ?></div>
          <div class="clear"></div>
          <?php if ($tags): ?>
            <div class="related-tags">
              <span class="label">Related tags:</span>
              <?php print $tags; ?>
            </div>
            <div class="clear"></div>
          <?php endif; ?>
        </div>
        <?php if (!$preview): ?>
          <div class="right">
            <div class="utility-links">
              <div class="email"><a href="<?php print base_path(); ?>/forward?path=node/<?php print $node->nid; ?>">email</a></div>
              <div class="print"><a href="http://www.addthis.com/bookmark.php" onclick="window.print(); return false;">print</a></div>
              <div class="share"><a href="http://www.addthis.com/bookmark.php" onmouseover="return addthis_open(this, &quot;&quot;, &quot;[URL]&quot;, &quot;[TITLE]&quot;);" onmouseout="addthis_close();">share</a><script type="text/javascript" src="http://s7.addthis.com/js/152/addthis_widget.js"></script></div>
            </div>
          </div>  
        <?php endif; ?>
        <div class="clear"></div>
      </div>
    </div>
  </div>
<?php endif; ?>

<div class="content">
  <div class="image">
    <?php print $image; ?>
  </div>

  <?php print $body; ?>

  <?php if (!$preview): ?>
    <?php if ($super_voting_widget): ?>
      <?php print $super_voting_widget; ?>
    <?php endif; ?>

    <?php if ($flag): ?>
      <?php print $flag; ?>
    <?php endif; ?>
  <?php endif; ?>
</div>
