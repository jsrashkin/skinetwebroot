<?php if ($form_description): ?>
  <div class="form-description">
    <?php print $form_description; ?>
  </div>
<?php endif; ?>

<div id="buttons" class="form-step">
  <?php print $edit_button; ?>
  <?php print $submit_button; ?>
</div>

<div class="node-preview">
  <?php print $node_preview; ?>
</div>

<div class="hidden" style="display:none;">
  <?php print $other; ?>
</div>

<div id="buttons2" class="form-step">
  <?php print $edit_button; ?>
  <?php print $submit_button; ?>
</div>
