<div class="location vcard">
  <div class="adr">
    <?php if (isset($name)): ?>
      <div class="location-name"><?php echo $name; ?></div>
    <?php endif; ?>
    <?php if (isset($street)): ?>
      <div class="street-address">
        <?php print $street; ?><?php if ($additional): ?> <?php print $additional; ?><?php endif; ?>
      </div>
    <?php endif; ?>
    <?php if (isset($city_province_postal)): ?>
      <?php print $city_province_postal; ?>
    <?php endif; ?>
    <?php if (isset($country_name)): ?>
      <div class="country-name"><?php print $country_name; ?></div>
    <?php endif; ?>
    <?php if (!empty($latitude) || !empty($longitude)): ?>
      <span class="geo"><?php print $geo; ?></span>
    <?php endif; ?>
  </div>
</div>
<?php print $map_link; ?>
