<?php if (!empty($locations)): ?>
  <div class="field">
    <div class="field-items">
      <?php foreach ($locations as $location): ?>
        <div class="field-item">
          <?php if (!isset($label)): $label = t('Where'); ?>
            <div class="field-label-inline-first"><?php print $label; ?>: </div>
          <?php endif; ?>
          <?php print $location; ?>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
<?php endif; ?>
