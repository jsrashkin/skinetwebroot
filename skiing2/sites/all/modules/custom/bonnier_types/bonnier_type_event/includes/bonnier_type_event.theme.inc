<?php
// $Id$

function bonnier_type_event_preprocess_node_event(&$vars) {
  $node = $vars['node'];

  // Fill the photo box
  $photo_node = node_load($node->field_main_photo[0]['nid']);

  if($photo_node) {
    $vars['photo_box'] = theme('event_photo', $photo_node, $node);
  }

  // Paging
  $vars['pager'] = $node->paging;

  if (!empty($node->field_manufacturer_email[0]['email'])) {
    $vars['request'] = 'hai';
    $vars['below_node_content'] .= drupal_get_form('bonnier_type_marketplace_request_form');
  }
}

function template_preprocess_event_photo(&$vars) {
  $node = $vars['node'];
  // Fill the photo box
  $photo_node = $vars['photo_node'];
  if($photo_node) {
    $template = $node->main_photo_placement;
    $template = $template?$template:'left';
    if($photo_node->field_image[0]['data']['alt'] != '') {
    	$alt = $photo_node->field_image[0]['data']['alt'];
    } else {
    	$alt = $node->title;
    }
    if($template == 'center') {
      $image = theme('imagecache','article_image_center',$photo_node->field_image[0]['filepath'],$alt);
    } else {
      $image = theme('imagecache', 'event_image', $photo_node->field_image[0]['filepath'], $alt);
    }
    $vars['image'] = $image;
    $class = $template;
    $vars['class'] = $class;

    $caption = $photo_node->body;
    $vars['caption'] = $caption;

    $title = $photo_node->title;
    $vars['title'] = $title;

		$imagepath = bonnier_common_image($photo_node);
	  $enlarged_imagepath = imagecache_create_url('enlarged_image', $imagepath);

		$enlarge = l('Enlarge Photo', $enlarged_imagepath, array('attributes' => array('class' => 'thickbox enlarge', 'title' => $photo_node->title)));
		$vars['enlarge'] = $enlarge;
  }
}
