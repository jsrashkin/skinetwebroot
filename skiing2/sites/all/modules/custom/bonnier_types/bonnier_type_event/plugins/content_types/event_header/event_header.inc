<?php
// $Id$

function bonnier_type_event_event_header_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Event header'),
    'description' => t('Header for event channels and content type.'),
    'required context' => new ctools_context_required(t('Node'), 'node'),
    'category' => t('Node'),
    'hook theme' => 'bonnier_type_event_event_header_theme',
  );
}

function bonnier_type_event_event_header_content_type_edit_form(&$form, &$form_state) {
}

function bonnier_type_event_event_header_content_type_render($subtype, $conf, $panel_args, $context) {
  if (!empty($context) && !empty($context->data)) {
    $block->content = theme('event_header', $context->data);
    return $block;
  }
}

function bonnier_type_event_event_header_theme(&$theme) {
  $theme['event_header'] = array(
    'template' => 'event-header',
    'arguments' => array('node' => NULL),
    'path' => drupal_get_path('module', 'bonnier_type_event') . '/plugins/content_types/event_header',
  );
}

function template_preprocess_event_header(&$variables) {
  $node = $variables['node'];
  laserfist_common_preprocess_node($variables, $node);
  $variables['title'] = bonnier_common_title($node);
  $date = reset($node->field_date);
  $date = date_make_date($date['value'], $date['timezone_db']);
  $variables['date'] = date_format_date($date, 'custom', 'F Y');
}
