<?php
/**
 * @file
 * skiing_menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function skiing_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_adventure:adventure
  $menu_links['main-menu_adventure:adventure'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure',
    'router_path' => 'adventure',
    'link_title' => 'Adventure',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_adventure:adventure',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_all-gear-brands:gear/brands
  $menu_links['main-menu_all-gear-brands:gear/brands'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/brands',
    'router_path' => 'gear/brands',
    'link_title' => 'All Gear Brands',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_all-gear-brands:gear/brands',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'main-menu_:gear',
  );
  // Exported menu link: main-menu_apparel--accessories:gear/apparel-and-accessories
  $menu_links['main-menu_apparel--accessories:gear/apparel-and-accessories'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/apparel-and-accessories',
    'router_path' => 'gear/apparel-and-accessories',
    'link_title' => 'Apparel & Accessories',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_apparel--accessories:gear/apparel-and-accessories',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_gear:gear/all',
  );
  // Exported menu link: main-menu_apparel--accessories:gears/apparel-and-accessories
  $menu_links['main-menu_apparel--accessories:gears/apparel-and-accessories'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gears/apparel-and-accessories',
    'router_path' => 'gears/apparel-and-accessories',
    'link_title' => 'Apparel & Accessories',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_apparel--accessories:gears/apparel-and-accessories',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_gear:gear/all',
  );
  // Exported menu link: main-menu_athletes:culture/athletes
  $menu_links['main-menu_athletes:culture/athletes'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture/athletes',
    'router_path' => 'culture/athletes',
    'link_title' => 'Athletes',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_athletes:culture/athletes',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_the-scene:culture',
  );
  // Exported menu link: main-menu_backcountry-gear:gear/backcountry-gear
  $menu_links['main-menu_backcountry-gear:gear/backcountry-gear'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/backcountry-gear',
    'router_path' => 'gear/backcountry-gear',
    'link_title' => 'Backcountry Gear',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_backcountry-gear:gear/backcountry-gear',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'main-menu_gear:gear/all',
  );
  // Exported menu link: main-menu_backcountry:adventure/backcountry
  $menu_links['main-menu_backcountry:adventure/backcountry'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure/backcountry',
    'router_path' => 'adventure/backcountry',
    'link_title' => 'Backcountry',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_backcountry:adventure/backcountry',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_adventure:adventure',
  );
  // Exported menu link: main-menu_bindings-reviews:gear/bindings
  $menu_links['main-menu_bindings-reviews:gear/bindings'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/bindings',
    'router_path' => 'gear/bindings',
    'link_title' => 'Bindings Reviews',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_bindings-reviews:gear/bindings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_:gear',
  );
  // Exported menu link: main-menu_boots:gear/boots
  $menu_links['main-menu_boots:gear/boots'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/boots',
    'router_path' => 'gear/boots',
    'link_title' => 'Boots',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_boots:gear/boots',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_gear:gear/all',
  );
  // Exported menu link: main-menu_far-away-places:adventure/destinations
  $menu_links['main-menu_far-away-places:adventure/destinations'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure/destinations',
    'router_path' => 'adventure/destinations',
    'link_title' => 'Far Away Places',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_far-away-places:adventure/destinations',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_adventure:adventure',
  );
  // Exported menu link: main-menu_gear:gear/all
  $menu_links['main-menu_gear:gear/all'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/all',
    'router_path' => 'gear/all',
    'link_title' => 'Gear',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_gear:gear/all',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_how-to:skiing-culture/how-ski
  $menu_links['main-menu_how-to:skiing-culture/how-ski'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'skiing-culture/how-ski',
    'router_path' => 'skiing-culture/how-ski',
    'link_title' => 'How To',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_how-to:skiing-culture/how-ski',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_magazine:node/139464
  $menu_links['main-menu_magazine:node/139464'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/139464',
    'router_path' => 'node/%',
    'link_title' => 'Magazine',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_magazine:node/139464',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: main-menu_news:culture/news
  $menu_links['main-menu_news:culture/news'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture/news',
    'router_path' => 'culture/news',
    'link_title' => 'News',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_news:culture/news',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_the-scene:culture',
  );
  // Exported menu link: main-menu_newsletter:http://media.aimmedia.com/skiing/skiingmag/newslettersignup.html?loc=topnav&newsletters[1610]=1&newsletters[1620]=1&newsletters[1630]=1&lnk=skiing
  $menu_links['main-menu_newsletter:http://media.aimmedia.com/skiing/skiingmag/newslettersignup.html?loc=topnav&newsletters[1610]=1&newsletters[1620]=1&newsletters[1630]=1&lnk=skiing'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'http://media.aimmedia.com/skiing/skiingmag/newslettersignup.html?loc=topnav&newsletters[1610]=1&newsletters[1620]=1&newsletters[1630]=1&lnk=skiing',
    'router_path' => '',
    'link_title' => 'Newsletter',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_newsletter:http://media.aimmedia.com/skiing/skiingmag/newslettersignup.html?loc=topnav&newsletters[1610]=1&newsletters[1620]=1&newsletters[1630]=1&lnk=skiing',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_magazine:node/139464',
  );
  // Exported menu link: main-menu_resort-beta:skiing-culture/how-ski
  $menu_links['main-menu_resort-beta:skiing-culture/how-ski'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'skiing-culture/how-ski',
    'router_path' => 'skiing-culture/how-ski',
    'link_title' => 'Resort Beta',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_resort-beta:skiing-culture/how-ski',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_how-to:skiing-culture/how-ski',
  );
  // Exported menu link: main-menu_skiing-culture:culture/ski-culture
  $menu_links['main-menu_skiing-culture:culture/ski-culture'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture/ski-culture',
    'router_path' => 'culture/ski-culture',
    'link_title' => 'Skiing Culture',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skiing-culture:culture/ski-culture',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_the-scene:culture',
  );
  // Exported menu link: main-menu_skiing-resorts:adventure/resorts
  $menu_links['main-menu_skiing-resorts:adventure/resorts'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure/resorts',
    'router_path' => 'adventure/resorts',
    'link_title' => 'Skiing Resorts',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skiing-resorts:adventure/resorts',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_adventure:adventure',
  );
  // Exported menu link: main-menu_skills:skiing-culture/diy-maintenance-fitness-and-nutrition
  $menu_links['main-menu_skills:skiing-culture/diy-maintenance-fitness-and-nutrition'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'skiing-culture/diy-maintenance-fitness-and-nutrition',
    'router_path' => 'skiing-culture/diy-maintenance-fitness-and-nutrition',
    'link_title' => 'Skills',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skills:skiing-culture/diy-maintenance-fitness-and-nutrition',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_how-to:skiing-culture/how-ski',
  );
  // Exported menu link: main-menu_skis:gear/skis
  $menu_links['main-menu_skis:gear/skis'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/skis',
    'router_path' => 'gear/skis',
    'link_title' => 'Skis',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skis:gear/skis',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_gear:gear/all',
  );
  // Exported menu link: main-menu_stories:stoke
  $menu_links['main-menu_stories:stoke'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'stoke',
    'router_path' => 'stoke',
    'link_title' => 'STORIES',
    'options' => array(
      'attributes' => array(
        'title' => 'Stoke',
      ),
      'identifier' => 'main-menu_stories:stoke',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: main-menu_subscribe:node/139464
  $menu_links['main-menu_subscribe:node/139464'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/139464',
    'router_path' => 'node/%',
    'link_title' => 'Subscribe',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_subscribe:node/139464',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_magazine:node/139464',
  );
  // Exported menu link: main-menu_the-scene:culture
  $menu_links['main-menu_the-scene:culture'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture',
    'router_path' => 'culture',
    'link_title' => 'The Scene',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_the-scene:culture',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_video:video
  $menu_links['main-menu_video:video'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'video',
    'router_path' => 'video',
    'link_title' => 'VIDEO',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_video:video',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: main-menu_videos:node/148259
  $menu_links['main-menu_videos:node/148259'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/148259',
    'router_path' => 'node/%',
    'link_title' => 'Videos',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_videos:node/148259',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-content-by-type_subscribe:node/57018
  $menu_links['menu-content-by-type_subscribe:node/57018'] = array(
    'menu_name' => 'menu-content-by-type',
    'link_path' => 'node/57018',
    'router_path' => 'node/%',
    'link_title' => 'Subscribe!',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-content-by-type_subscribe:node/57018',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-nav_site-map:sitemap
  $menu_links['menu-footer-nav_site-map:sitemap'] = array(
    'menu_name' => 'menu-footer-nav',
    'link_path' => 'sitemap',
    'router_path' => 'sitemap',
    'link_title' => 'Site Map',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-nav_site-map:sitemap',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-gear-directory_accessories:taxonomy/term/36
  $menu_links['menu-gear-directory_accessories:taxonomy/term/36'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/36',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Accessories',
    'options' => array(
      'identifier' => 'menu-gear-directory_accessories:taxonomy/term/36',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 30,
    'customized' => 1,
  );
  // Exported menu link: menu-gear-directory_apparel:taxonomy/term/28
  $menu_links['menu-gear-directory_apparel:taxonomy/term/28'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/28',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Apparel',
    'options' => array(
      'identifier' => 'menu-gear-directory_apparel:taxonomy/term/28',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 25,
    'customized' => 0,
  );
  // Exported menu link: menu-gear-directory_backcountry-gear:taxonomy/term/24
  $menu_links['menu-gear-directory_backcountry-gear:taxonomy/term/24'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/24',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Backcountry Gear',
    'options' => array(
      'identifier' => 'menu-gear-directory_backcountry-gear:taxonomy/term/24',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 20,
    'customized' => 0,
  );
  // Exported menu link: menu-gear-directory_bindings:taxonomy/term/15
  $menu_links['menu-gear-directory_bindings:taxonomy/term/15'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/15',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Bindings',
    'options' => array(
      'identifier' => 'menu-gear-directory_bindings:taxonomy/term/15',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 10,
    'customized' => 0,
  );
  // Exported menu link: menu-gear-directory_boots:taxonomy/term/9
  $menu_links['menu-gear-directory_boots:taxonomy/term/9'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/9',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Boots',
    'options' => array(
      'identifier' => 'menu-gear-directory_boots:taxonomy/term/9',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 5,
    'customized' => 0,
  );
  // Exported menu link: menu-gear-directory_car-racks:taxonomy/term/41
  $menu_links['menu-gear-directory_car-racks:taxonomy/term/41'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/41',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Car Racks',
    'options' => array(
      'identifier' => 'menu-gear-directory_car-racks:taxonomy/term/41',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 35,
    'customized' => 0,
  );
  // Exported menu link: menu-gear-directory_gadgets:taxonomy/term/42
  $menu_links['menu-gear-directory_gadgets:taxonomy/term/42'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/42',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Gadgets',
    'options' => array(
      'identifier' => 'menu-gear-directory_gadgets:taxonomy/term/42',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 40,
    'customized' => 0,
  );
  // Exported menu link: menu-gear-directory_poles:taxonomy/term/20
  $menu_links['menu-gear-directory_poles:taxonomy/term/20'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/20',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Poles',
    'options' => array(
      'identifier' => 'menu-gear-directory_poles:taxonomy/term/20',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 15,
    'customized' => 0,
  );
  // Exported menu link: menu-gear-directory_skis:taxonomy/term/3
  $menu_links['menu-gear-directory_skis:taxonomy/term/3'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/3',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Skis',
    'options' => array(
      'identifier' => 'menu-gear-directory_skis:taxonomy/term/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: menu-most-popular_skis:taxonomy/term/3
  $menu_links['menu-most-popular_skis:taxonomy/term/3'] = array(
    'menu_name' => 'menu-most-popular',
    'link_path' => 'taxonomy/term/3',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Skis',
    'options' => array(
      'identifier' => 'menu-most-popular_skis:taxonomy/term/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_-of-lifts:<front>
  $menu_links['menu-resort-directory_-of-lifts:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => '# of lifts',
    'options' => array(
      'identifier' => 'menu-resort-directory_-of-lifts:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_activities:<front>
  $menu_links['menu-resort-directory_activities:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'activities',
    'options' => array(
      'identifier' => 'menu-resort-directory_activities:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 5,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_challenge:<front>
  $menu_links['menu-resort-directory_challenge:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'challenge',
    'options' => array(
      'identifier' => 'menu-resort-directory_challenge:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 10,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_family:<front>
  $menu_links['menu-resort-directory_family:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'family',
    'options' => array(
      'identifier' => 'menu-resort-directory_family:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 15,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_grooming:<front>
  $menu_links['menu-resort-directory_grooming:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'grooming',
    'options' => array(
      'identifier' => 'menu-resort-directory_grooming:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 20,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_lodging:<front>
  $menu_links['menu-resort-directory_lodging:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'lodging',
    'options' => array(
      'identifier' => 'menu-resort-directory_lodging:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 25,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_scenery:<front>
  $menu_links['menu-resort-directory_scenery:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'scenery',
    'options' => array(
      'identifier' => 'menu-resort-directory_scenery:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 30,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_service:<front>
  $menu_links['menu-resort-directory_service:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'service',
    'options' => array(
      'identifier' => 'menu-resort-directory_service:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 35,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_snowfall:<front>
  $menu_links['menu-resort-directory_snowfall:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'snowfall',
    'options' => array(
      'identifier' => 'menu-resort-directory_snowfall:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 40,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_state:<front>
  $menu_links['menu-resort-directory_state:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'state',
    'options' => array(
      'identifier' => 'menu-resort-directory_state:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 45,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_terrain:<front>
  $menu_links['menu-resort-directory_terrain:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'terrain',
    'options' => array(
      'identifier' => 'menu-resort-directory_terrain:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 50,
    'customized' => 0,
  );
  // Exported menu link: menu-resort-directory_value:<front>
  $menu_links['menu-resort-directory_value:<front>'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'value',
    'options' => array(
      'identifier' => 'menu-resort-directory_value:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 55,
    'customized' => 0,
  );
  // Exported menu link: menu-seo-menu_nastar:http://www.nastar.com
  $menu_links['menu-seo-menu_nastar:http://www.nastar.com'] = array(
    'menu_name' => 'menu-seo-menu',
    'link_path' => 'http://www.nastar.com',
    'router_path' => '',
    'link_title' => 'Nastar',
    'options' => array(
      'identifier' => 'menu-seo-menu_nastar:http://www.nastar.com',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -3,
    'customized' => 0,
  );
  // Exported menu link: menu-seo-menu_ski:http://www.skinet.com/ski
  $menu_links['menu-seo-menu_ski:http://www.skinet.com/ski'] = array(
    'menu_name' => 'menu-seo-menu',
    'link_path' => 'http://www.skinet.com/ski',
    'router_path' => '',
    'link_title' => 'Ski',
    'options' => array(
      'identifier' => 'menu-seo-menu_ski:http://www.skinet.com/ski',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -5,
    'customized' => 0,
  );
  // Exported menu link: menu-seo-menu_warren-miller:http://www.skinet.com/warrenmiller
  $menu_links['menu-seo-menu_warren-miller:http://www.skinet.com/warrenmiller'] = array(
    'menu_name' => 'menu-seo-menu',
    'link_path' => 'http://www.skinet.com/warrenmiller',
    'router_path' => '',
    'link_title' => 'Warren Miller',
    'options' => array(
      'identifier' => 'menu-seo-menu_warren-miller:http://www.skinet.com/warrenmiller',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -4,
    'customized' => 0,
  );
  // Exported menu link: menu-social-networks_facebook:node/71013
  $menu_links['menu-social-networks_facebook:node/71013'] = array(
    'menu_name' => 'menu-social-networks',
    'link_path' => 'node/71013',
    'router_path' => 'node/%',
    'link_title' => 'Facebook',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-social-networks_facebook:node/71013',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-social-networks_myspace:http://myspace.com/bonnier_site
  $menu_links['menu-social-networks_myspace:http://myspace.com/bonnier_site'] = array(
    'menu_name' => 'menu-social-networks',
    'link_path' => 'http://myspace.com/bonnier_site',
    'router_path' => '',
    'link_title' => 'MySpace',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-social-networks_myspace:http://myspace.com/bonnier_site',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-social-networks_twitter:node/71014
  $menu_links['menu-social-networks_twitter:node/71014'] = array(
    'menu_name' => 'menu-social-networks',
    'link_path' => 'node/71014',
    'router_path' => 'node/%',
    'link_title' => 'Twitter',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-social-networks_twitter:node/71014',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-video-categories_adventures:node/148260
  $menu_links['menu-video-categories_adventures:node/148260'] = array(
    'menu_name' => 'menu-video-categories',
    'link_path' => 'node/148260',
    'router_path' => 'node/%',
    'link_title' => 'Adventures',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-video-categories_adventures:node/148260',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-video-categories_culture:node/148261
  $menu_links['menu-video-categories_culture:node/148261'] = array(
    'menu_name' => 'menu-video-categories',
    'link_path' => 'node/148261',
    'router_path' => 'node/%',
    'link_title' => 'Culture',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-video-categories_culture:node/148261',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-video-categories_gear:node/148259
  $menu_links['menu-video-categories_gear:node/148259'] = array(
    'menu_name' => 'menu-video-categories',
    'link_path' => 'node/148259',
    'router_path' => 'node/%',
    'link_title' => 'Gear',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-video-categories_gear:node/148259',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-video-categories_more:node/148262
  $menu_links['menu-video-categories_more:node/148262'] = array(
    'menu_name' => 'menu-video-categories',
    'link_path' => 'node/148262',
    'router_path' => 'node/%',
    'link_title' => 'More',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-video-categories_more:node/148262',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('# of lifts');
  t('Accessories');
  t('Adventure');
  t('Adventures');
  t('All Gear Brands');
  t('Apparel');
  t('Apparel & Accessories');
  t('Athletes');
  t('Backcountry');
  t('Backcountry Gear');
  t('Bindings');
  t('Bindings Reviews');
  t('Boots');
  t('Car Racks');
  t('Culture');
  t('Facebook');
  t('Far Away Places');
  t('Gadgets');
  t('Gear');
  t('How To');
  t('Magazine');
  t('More');
  t('MySpace');
  t('Nastar');
  t('News');
  t('Newsletter');
  t('Poles');
  t('Resort Beta');
  t('STORIES');
  t('Site Map');
  t('Ski');
  t('Skiing Culture');
  t('Skiing Resorts');
  t('Skills');
  t('Skis');
  t('Subscribe');
  t('Subscribe!');
  t('The Scene');
  t('Twitter');
  t('VIDEO');
  t('Videos');
  t('Warren Miller');
  t('activities');
  t('challenge');
  t('family');
  t('grooming');
  t('lodging');
  t('scenery');
  t('service');
  t('snowfall');
  t('state');
  t('terrain');
  t('value');

  return $menu_links;
}
