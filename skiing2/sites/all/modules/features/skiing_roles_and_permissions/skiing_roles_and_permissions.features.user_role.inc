<?php
/**
 * @file
 * skiing_roles_and_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function skiing_roles_and_permissions_user_default_roles() {
  $roles = array();

  // Exported role: 24/7.
  $roles['24/7'] = array(
    'name' => '24/7',
    'weight' => 0,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 0,
  );

  // Exported role: blogger.
  $roles['blogger'] = array(
    'name' => 'blogger',
    'weight' => 0,
  );

  // Exported role: contributor.
  $roles['contributor'] = array(
    'name' => 'contributor',
    'weight' => 0,
  );

  // Exported role: dba.
  $roles['dba'] = array(
    'name' => 'dba',
    'weight' => 0,
  );

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => 0,
  );

  // Exported role: pcd.
  $roles['pcd'] = array(
    'name' => 'pcd',
    'weight' => 0,
  );

  // Exported role: photographer.
  $roles['photographer'] = array(
    'name' => 'photographer',
    'weight' => 0,
  );

  // Exported role: producer.
  $roles['producer'] = array(
    'name' => 'producer',
    'weight' => 0,
  );

  // Exported role: senior editor.
  $roles['senior editor'] = array(
    'name' => 'senior editor',
    'weight' => 0,
  );

  // Exported role: seo.
  $roles['seo'] = array(
    'name' => 'seo',
    'weight' => 0,
  );

  return $roles;
}
