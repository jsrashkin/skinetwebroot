<?php
/**
 * @file
 * skiing_blocks.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function skiing_blocks_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "boxes" && $api == "box") {
    return array("version" => "1");
  }
}
