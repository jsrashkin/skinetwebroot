<?php
/**
 * @file
 * skiing_pages.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function skiing_pages_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'gear_search';
  $page->task = 'page';
  $page->admin_title = 'Gear search';
  $page->admin_description = '';
  $page->path = 'gear/%display';
  $page->access = array(
    'logic' => 'and',
  );
  $page->menu = array(
    'type' => 'none',
    'title' => '',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array(
    'display' => array(
      'id' => 1,
      'identifier' => 'Display',
      'name' => 'string',
      'settings' => array(
        'use_tail' => 0,
      ),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_gear_search__panel';
  $handler->task = 'page';
  $handler->subtask = 'gear_search';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Gear search',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'string_equal',
          'settings' => array(
            'operator' => '=',
            'value' => 'skis',
            'case' => 0,
          ),
          'context' => 'argument_string_1',
          'not' => FALSE,
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'gear';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'filter' => NULL,
      'content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'caf5d9e3-97a0-4026-b60a-cda4e8612867';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f0a16cb2-1bcf-48d0-83c6-d4ffff71d277';
    $pane->panel = 'content';
    $pane->type = 'views_panes';
    $pane->subtype = 'gearfinder_filters-skis_block';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f0a16cb2-1bcf-48d0-83c6-d4ffff71d277';
    $display->content['new-f0a16cb2-1bcf-48d0-83c6-d4ffff71d277'] = $pane;
    $display->panels['content'][0] = 'new-f0a16cb2-1bcf-48d0-83c6-d4ffff71d277';
    $pane = new stdClass();
    $pane->pid = 'new-5602563c-ab48-4b42-9e4b-a67340b84d37';
    $pane->panel = 'filter';
    $pane->type = 'block';
    $pane->subtype = 'views-fa4e50eb087d8cd2d9fa7c68a36fd682';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'inherit_path' => 0,
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5602563c-ab48-4b42-9e4b-a67340b84d37';
    $display->content['new-5602563c-ab48-4b42-9e4b-a67340b84d37'] = $pane;
    $display->panels['filter'][0] = 'new-5602563c-ab48-4b42-9e4b-a67340b84d37';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_gear_search__boots';
  $handler->task = 'page';
  $handler->subtask = 'gear_search';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Boots',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'boots',
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'string_equal',
          'settings' => array(
            'operator' => '=',
            'value' => 'boots',
            'case' => 0,
          ),
          'context' => 'argument_string_1',
          'not' => FALSE,
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'gear';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'filter' => NULL,
      'content' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'caf5d9e3-97a0-4026-b60a-cda4e8612867';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0d7cde41-13bb-4435-ae3b-63eb06d35cb6';
    $pane->panel = 'content';
    $pane->type = 'views_panes';
    $pane->subtype = 'gearfinder_filters-boots';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0d7cde41-13bb-4435-ae3b-63eb06d35cb6';
    $display->content['new-0d7cde41-13bb-4435-ae3b-63eb06d35cb6'] = $pane;
    $display->panels['content'][0] = 'new-0d7cde41-13bb-4435-ae3b-63eb06d35cb6';
    $pane = new stdClass();
    $pane->pid = 'new-a4bebd7d-e4c6-4d2a-a6da-675ddf7d1c0a';
    $pane->panel = 'filter';
    $pane->type = 'block';
    $pane->subtype = 'views--exp-gearfinder_filters-boots';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'inherit_path' => 0,
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a4bebd7d-e4c6-4d2a-a6da-675ddf7d1c0a';
    $display->content['new-a4bebd7d-e4c6-4d2a-a6da-675ddf7d1c0a'] = $pane;
    $display->panels['filter'][0] = 'new-a4bebd7d-e4c6-4d2a-a6da-675ddf7d1c0a';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['gear_search'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'video_page';
  $page->task = 'page';
  $page->admin_title = 'Video page';
  $page->admin_description = '';
  $page->path = 'video';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_video_page__panel';
  $handler->task = 'page';
  $handler->subtask = 'video_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Video',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Video';
  $display->uuid = 'd08c4d60-0508-4b49-b4d6-06ea784a5d6a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-dcc65abd-0729-46ec-8d3f-24bbbf1c8425';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Brightcove html',
      'title' => '',
      'body' => '<!-- Start of Brightcove Player -->

<!--
By use of this code snippet, I agree to the Brightcove Publisher T and C
found at https://accounts.brightcove.com/en/terms-and-conditions/.
-->

<script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>

<object id="myExperience" class="BrightcoveExperience">
  <param name="bgcolor" value="#FFFFFF" />
  <param name="width" value="850" />
  <param name="height" value="1150" />
  <param name="playerID" value="1266944535001" />
  <param name="playerKey" value="AQ~~,AAAAAEwRm8I~,OusBxQT0iGjEhOPNUYu2kCgBeRGRwIHL" />
  <param name="isVid" value="true" />
  <param name="isUI" value="true" />
  <param name="dynamicStreaming" value="true" />

</object>

<!--
This script tag will cause the Brightcove Players defined above it to be created as soon
as the line is read by the browser. If you wish to have the player instantiated only after
the rest of the HTML is processed and the page load is complete, remove the line.
-->
<script type="text/javascript">brightcove.createExperiences();</script>

<!-- End of Brightcove Player -->',
      'format' => '2',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'dcc65abd-0729-46ec-8d3f-24bbbf1c8425';
    $display->content['new-dcc65abd-0729-46ec-8d3f-24bbbf1c8425'] = $pane;
    $display->panels['middle'][0] = 'new-dcc65abd-0729-46ec-8d3f-24bbbf1c8425';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-dcc65abd-0729-46ec-8d3f-24bbbf1c8425';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['video_page'] = $page;

  return $pages;

}
