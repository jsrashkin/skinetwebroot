<?php
/**
 * @file
 * skiing_other.brightcove.inc
 */

/**
 * Implements hook_brightcove_player().
 */
function skiing_other_brightcove_player() {
  $export = array();

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'default';
  $preset->display_name = 'Default';
  $preset->player_id = '1418450082001';
  $preset->player_key = 'AQ~~,AAAAAEwRm8I~,OusBxQT0iGjIFpH0hG_afvhbOWaUNIR3';
  $preset->responsive = TRUE;
  $export['default'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'desktop_default';
  $preset->display_name = 'Desktop-Default';
  $preset->player_id = '4649891406001';
  $preset->player_key = 'AQ~~,AAAAAEwRm8I~,OusBxQT0iGh5ivXIBqfTleGWyLCgB4JO';
  $preset->responsive = FALSE;
  $export['desktop_default'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'desktop_player';
  $preset->display_name = 'Desktop Player';
  $preset->player_id = '4649891410001';
  $preset->player_key = 'AQ~~,AAAAAEwRm8I~,OusBxQT0iGiup94ygXFo6axrzl7E12-1';
  $preset->responsive = FALSE;
  $export['desktop_player'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'homepage_videos';
  $preset->display_name = 'Homepage Videos';
  $preset->player_id = '4664172757001';
  $preset->player_key = 'AQ~~,AAAAAEwRm8I~,OusBxQT0iGiD6-JAKgR6ptfgW5F9UKDB';
  $preset->responsive = TRUE;
  $export['homepage_videos'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'responsive_player';
  $preset->display_name = 'Responsive Player';
  $preset->player_id = '1964542304001';
  $preset->player_key = 'AQ~~,AAAAAEwRm8I~,OusBxQT0iGh1UQjskO9PU6xQBk91cKuu';
  $preset->responsive = TRUE;
  $export['responsive_player'] = $preset;

  return $export;
}
