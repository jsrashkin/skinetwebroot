<?php
/**
 * @file
 * skiing_other.ad_manager_ad_default.inc
 */

/**
 * Implements hook_ad_manager_ad_default().
 */
function skiing_other_ad_manager_ad_default() {
  $export = array();

$size = new stdClass();
$size->{0} = NULL;
$size->{768} = array(
  0 => array(
    0 => 728,
    1 => 90,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'bottom',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'bottom';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['bottom'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 300,
    1 => 250,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'stoke';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => (object) array(),
  ),
);
  $export['stoke'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 320,
    1 => 50,
  ),
  1 => array(
    0 => 300,
    1 => 50,
  ),
);
$size->{768} = array(
  0 => array(
    0 => 728,
    1 => 90,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'top',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'top';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['top'] = $ad;

  return $export;
}
