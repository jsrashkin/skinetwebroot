<?php
/**
 * @file
 * skiing_other.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function skiing_other_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ad_manager" && $api == "ad_manager_ad_default") {
    return array("version" => "1");
  }
  if ($module == "brightcove" && $api == "brightcove") {
    return array("version" => "1");
  }
  if ($module == "video_embed_field" && $api == "default_video_embed_styles") {
    return array("version" => "1");
  }
}
