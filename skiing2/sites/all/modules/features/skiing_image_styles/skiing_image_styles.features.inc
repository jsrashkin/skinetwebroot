<?php
/**
 * @file
 * skiing_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function skiing_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: adventure_style.
  $styles['adventure_style'] = array(
    'label' => 'Adventure Style',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 375,
          'height' => 275,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: channel_380x155_.
  $styles['channel_380x155_'] = array(
    'label' => 'Channel(380x155)',
    'effects' => array(
      11 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 380,
          'height' => 155,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: gallery_pages.
  $styles['gallery_pages'] = array(
    'label' => 'Gallery pages',
    'effects' => array(
      12 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 970,
          'height' => 570,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: gear_and_how_to_style__380x480_.
  $styles['gear_and_how_to_style__380x480_'] = array(
    'label' => 'Gear and How to Style (380x480)',
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 380,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: gear_search_product.
  $styles['gear_search_product'] = array(
    'label' => 'gear search product(149x153)',
    'effects' => array(
      7 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 149,
          'height' => 153,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: node_main_image.
  $styles['node_main_image'] = array(
    'label' => 'Node main image',
    'effects' => array(
      8 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 850,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: node_related_content.
  $styles['node_related_content'] = array(
    'label' => 'node related content 265x194',
    'effects' => array(
      5 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 265,
          'height' => 194,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: popular_block.
  $styles['popular_block'] = array(
    'label' => 'Popular block (65x65)',
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 65,
          'height' => 65,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: popular_image_style.
  $styles['popular_image_style'] = array(
    'label' => 'Popular Image Style',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 61,
          'height' => 61,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: search_result.
  $styles['search_result'] = array(
    'label' => 'search result (117x117)',
    'effects' => array(
      6 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 117,
          'height' => 117,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: top_slider_image.
  $styles['top_slider_image'] = array(
    'label' => 'Top Slider Image',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1288,
          'height' => 495,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: video_sidebar.
  $styles['video_sidebar'] = array(
    'label' => 'Video sidebar',
    'effects' => array(
      10 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 265,
          'height' => 149,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
