<?php
/**
 * @file
 * skiing_main_menu.features.menu_uuid_links.inc
 */

/**
 * Implements hook_menu_default_menu_uuid_links().
 */
function skiing_main_menu_menu_default_menu_uuid_links() {
  $menu_uuid_links = array();

  // Exported menu link: 74b344c3-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b344c3-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'stoke',
    'router_path' => 'stoke',
    'link_title' => 'STORIES',
    'options' => array(
      'attributes' => array(
        'title' => 'Stoke',
      ),
      'identifier' => 'main-menu_stories:stoke',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'uuid' => '74b344c3-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b34510-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b34510-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure',
    'router_path' => 'adventure',
    'link_title' => 'Adventure',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_adventure:adventure',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b34510-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b34556-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b34556-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture',
    'router_path' => 'culture',
    'link_title' => 'The Scene',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_the-scene:culture',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -48,
    'uuid' => '74b34556-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b34599-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b34599-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'skiing-culture/how-ski',
    'router_path' => 'skiing-culture/how-ski',
    'link_title' => 'How To',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_how-to:skiing-culture/how-ski',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -47,
    'uuid' => '74b34599-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b34619-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b34619-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'router_path' => 'node/%',
    'link_title' => 'Magazine',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_magazine:node/139464',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -46,
    'uuid' => '74b34619-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/d6a1d079-ab49-45cc-ba75-34a54a2d038e',
  );
  // Exported menu link: 74b34832-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b34832-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'video',
    'router_path' => 'video',
    'link_title' => 'VIDEO',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_video:video',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'uuid' => '74b34832-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36c98-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36c98-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure/resorts',
    'router_path' => 'adventure/resorts',
    'link_title' => 'Skiing Resorts',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skiing-resorts:adventure/resorts',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b36c98-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34510-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36cfd-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36cfd-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure/backcountry',
    'router_path' => 'adventure/backcountry',
    'link_title' => 'Backcountry',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_backcountry:adventure/backcountry',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b36cfd-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34510-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36d43-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36d43-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture/athletes',
    'router_path' => 'culture/athletes',
    'link_title' => 'Athletes',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_athletes:culture/athletes',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b36d43-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34556-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36d85-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36d85-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture/ski-culture',
    'router_path' => 'culture/ski-culture',
    'link_title' => 'Skiing Culture',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skiing-culture:culture/ski-culture',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'uuid' => '74b36d85-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34556-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36ea4-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36ea4-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture/news',
    'router_path' => 'culture/news',
    'link_title' => 'News',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_news:culture/news',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b36ea4-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34556-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/all',
    'router_path' => 'gear/all',
    'link_title' => 'Gear',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_gear:gear/all',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36f7d-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36f7d-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/skis',
    'router_path' => 'gear/skis',
    'link_title' => 'Skis',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skis:gear/skis',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b36f7d-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36fc1-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36fc1-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/boots',
    'router_path' => 'gear/boots',
    'link_title' => 'Boots',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_boots:gear/boots',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b36fc1-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b37005-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b37005-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/bindings',
    'router_path' => 'gear/bindings',
    'link_title' => 'Bindings Reviews',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'uuid' => '74b37005-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b371eb-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b371eb-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/backcountry-gear',
    'router_path' => 'gear/backcountry-gear',
    'link_title' => 'Backcountry Gear',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_backcountry-gear:gear/backcountry-gear',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'uuid' => '74b371eb-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b37234-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b37234-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/brands',
    'router_path' => 'gear/brands',
    'link_title' => 'All Gear Brands',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'uuid' => '74b37234-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b37272-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b37272-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/apparel-and-accessories',
    'router_path' => 'gear/apparel-and-accessories',
    'link_title' => 'Apparel & Accessories',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_apparel--accessories:gear/apparel-and-accessories',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'uuid' => '74b37272-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b372b9-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b372b9-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure/destinations',
    'router_path' => 'adventure/destinations',
    'link_title' => 'Far Away Places',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_far-away-places:adventure/destinations',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'uuid' => '74b372b9-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34510-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b37473-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b37473-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'skiing-culture/how-ski',
    'router_path' => 'skiing-culture/how-ski',
    'link_title' => 'Resort Beta',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_resort-beta:skiing-culture/how-ski',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b37473-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34599-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b374b6-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b374b6-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'skiing-culture/diy-maintenance-fitness-and-nutrition',
    'router_path' => 'skiing-culture/diy-maintenance-fitness-and-nutrition',
    'link_title' => 'Skills',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skills:skiing-culture/diy-maintenance-fitness-and-nutrition',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b374b6-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34599-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b3760f-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b3760f-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'router_path' => 'node/%',
    'link_title' => 'Subscribe',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_subscribe:node/139464',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b3760f-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34619-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/d6a1d079-ab49-45cc-ba75-34a54a2d038e',
  );
  // Exported menu link: 74b37652-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b37652-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'http://media.aimmedia.com/skiing/skiingmag/newslettersignup.html?loc=topnav&newsletters[1610]=1&newsletters[1620]=1&newsletters[1630]=1&lnk=skiing',
    'router_path' => '',
    'link_title' => 'Newsletter',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_newsletter:http://media.aimmedia.com/skiing/skiingmag/newslettersignup.html?loc=topnav&newsletters[1610]=1&newsletters[1620]=1&newsletters[1630]=1&lnk=skiing',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b37652-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34619-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b39cd7-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b39cd7-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'router_path' => 'node/%',
    'link_title' => 'Videos',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_videos:node/148259',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'uuid' => '74b39cd7-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/237d0cff-6776-44f7-a480-33bb2304a564',
  );
  // Exported menu link: 74b39f1c-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b39f1c-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gears/apparel-and-accessories',
    'router_path' => 'gears/apparel-and-accessories',
    'link_title' => 'Apparel & Accessories',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_apparel--accessories:gears/apparel-and-accessories',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'uuid' => '74b39f1c-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Adventure');
  t('All Gear Brands');
  t('Apparel & Accessories');
  t('Athletes');
  t('Backcountry');
  t('Backcountry Gear');
  t('Bindings Reviews');
  t('Boots');
  t('Far Away Places');
  t('Gear');
  t('How To');
  t('Magazine');
  t('News');
  t('Newsletter');
  t('Resort Beta');
  t('STORIES');
  t('Skiing Culture');
  t('Skiing Resorts');
  t('Skills');
  t('Skis');
  t('Subscribe');
  t('The Scene');
  t('VIDEO');
  t('Videos');


  return $menu_uuid_links;
}
