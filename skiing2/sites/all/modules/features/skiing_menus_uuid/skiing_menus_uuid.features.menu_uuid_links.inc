<?php
/**
 * @file
 * skiing_menus_uuid.features.menu_uuid_links.inc
 */

/**
 * Implements hook_menu_default_menu_uuid_links().
 */
function skiing_menus_uuid_menu_default_menu_uuid_links() {
  $menu_uuid_links = array();

  // Exported menu link: 74b18243-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18243-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-social-networks',
    'router_path' => 'node/%',
    'link_title' => 'Twitter',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-social-networks_twitter:node/71014',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b18243-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/fdaedd2d-64a5-4b70-a95c-6aad0c6df9cf',
  );
  // Exported menu link: 74b1828a-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b1828a-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-social-networks',
    'router_path' => 'node/%',
    'link_title' => 'Facebook',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-social-networks_facebook:node/71013',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b1828a-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/1e402296-bfc9-40ef-a949-3581e48d37b8',
  );
  // Exported menu link: 74b182cb-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b182cb-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-social-networks',
    'link_path' => 'http://myspace.com/bonnier_site',
    'router_path' => '',
    'link_title' => 'MySpace',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-social-networks_myspace:http://myspace.com/bonnier_site',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'uuid' => '74b182cb-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b183da-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b183da-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-most-popular',
    'link_path' => 'taxonomy/term/3',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Skis',
    'options' => array(
      'identifier' => 'menu-most-popular_skis:taxonomy/term/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'uuid' => '74b183da-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b1855a-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b1855a-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/3',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Skis',
    'options' => array(
      'identifier' => 'menu-gear-directory_skis:taxonomy/term/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'uuid' => '74b1855a-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18599-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18599-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/9',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Boots',
    'options' => array(
      'identifier' => 'menu-gear-directory_boots:taxonomy/term/9',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 5,
    'uuid' => '74b18599-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b185d4-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b185d4-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/15',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Bindings',
    'options' => array(
      'identifier' => 'menu-gear-directory_bindings:taxonomy/term/15',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 10,
    'uuid' => '74b185d4-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18612-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18612-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/20',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Poles',
    'options' => array(
      'identifier' => 'menu-gear-directory_poles:taxonomy/term/20',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 15,
    'uuid' => '74b18612-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18650-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18650-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/24',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Backcountry Gear',
    'options' => array(
      'identifier' => 'menu-gear-directory_backcountry-gear:taxonomy/term/24',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 20,
    'uuid' => '74b18650-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18692-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18692-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/28',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Apparel',
    'options' => array(
      'identifier' => 'menu-gear-directory_apparel:taxonomy/term/28',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 25,
    'uuid' => '74b18692-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b186d1-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b186d1-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/36',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Accessories',
    'options' => array(
      'identifier' => 'menu-gear-directory_accessories:taxonomy/term/36',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 30,
    'uuid' => '74b186d1-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18711-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18711-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/41',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Car Racks',
    'options' => array(
      'identifier' => 'menu-gear-directory_car-racks:taxonomy/term/41',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 35,
    'uuid' => '74b18711-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18750-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18750-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-gear-directory',
    'link_path' => 'taxonomy/term/42',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Gadgets',
    'options' => array(
      'identifier' => 'menu-gear-directory_gadgets:taxonomy/term/42',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 40,
    'uuid' => '74b18750-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b187e7-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b187e7-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => '# of lifts',
    'options' => array(
      'identifier' => 'menu-resort-directory_-of-lifts:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'uuid' => '74b187e7-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b1882e-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b1882e-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'activities',
    'options' => array(
      'identifier' => 'menu-resort-directory_activities:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 5,
    'uuid' => '74b1882e-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18870-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18870-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'challenge',
    'options' => array(
      'identifier' => 'menu-resort-directory_challenge:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 10,
    'uuid' => '74b18870-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b188b6-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b188b6-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'family',
    'options' => array(
      'identifier' => 'menu-resort-directory_family:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 15,
    'uuid' => '74b188b6-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18b08-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18b08-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'grooming',
    'options' => array(
      'identifier' => 'menu-resort-directory_grooming:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 20,
    'uuid' => '74b18b08-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18b51-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18b51-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'lodging',
    'options' => array(
      'identifier' => 'menu-resort-directory_lodging:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 25,
    'uuid' => '74b18b51-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18b8f-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18b8f-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'scenery',
    'options' => array(
      'identifier' => 'menu-resort-directory_scenery:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 30,
    'uuid' => '74b18b8f-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18bcd-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18bcd-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'service',
    'options' => array(
      'identifier' => 'menu-resort-directory_service:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 35,
    'uuid' => '74b18bcd-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18d53-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18d53-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'snowfall',
    'options' => array(
      'identifier' => 'menu-resort-directory_snowfall:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 40,
    'uuid' => '74b18d53-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18dba-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18dba-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'state',
    'options' => array(
      'identifier' => 'menu-resort-directory_state:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 45,
    'uuid' => '74b18dba-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18df9-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18df9-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'terrain',
    'options' => array(
      'identifier' => 'menu-resort-directory_terrain:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 50,
    'uuid' => '74b18df9-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b18e34-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b18e34-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-resort-directory',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'value',
    'options' => array(
      'identifier' => 'menu-resort-directory_value:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 55,
    'uuid' => '74b18e34-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b2052d-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b2052d-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-seo-menu',
    'link_path' => 'http://www.skinet.com/ski',
    'router_path' => '',
    'link_title' => 'Ski',
    'options' => array(
      'identifier' => 'menu-seo-menu_ski:http://www.skinet.com/ski',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -5,
    'uuid' => '74b2052d-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b20570-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b20570-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-seo-menu',
    'link_path' => 'http://www.skinet.com/warrenmiller',
    'router_path' => '',
    'link_title' => 'Warren Miller',
    'options' => array(
      'identifier' => 'menu-seo-menu_warren-miller:http://www.skinet.com/warrenmiller',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -4,
    'uuid' => '74b20570-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b205b5-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b205b5-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-seo-menu',
    'link_path' => 'http://www.nastar.com',
    'router_path' => '',
    'link_title' => 'Nastar',
    'options' => array(
      'identifier' => 'menu-seo-menu_nastar:http://www.nastar.com',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -3,
    'uuid' => '74b205b5-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b344c3-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b344c3-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'stoke',
    'router_path' => 'stoke',
    'link_title' => 'STORIES',
    'options' => array(
      'attributes' => array(
        'title' => 'Stoke',
      ),
      'identifier' => 'main-menu_stories:stoke',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'uuid' => '74b344c3-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b34510-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b34510-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure',
    'router_path' => 'adventure',
    'link_title' => 'Adventure',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_adventure:adventure',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b34510-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b34556-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b34556-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture',
    'router_path' => 'culture',
    'link_title' => 'The Scene',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_the-scene:culture',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -48,
    'uuid' => '74b34556-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b34599-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b34599-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'skiing-culture/how-ski',
    'router_path' => 'skiing-culture/how-ski',
    'link_title' => 'How To',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_how-to:skiing-culture/how-ski',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -47,
    'uuid' => '74b34599-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b34619-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b34619-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'router_path' => 'node/%',
    'link_title' => 'Magazine',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_magazine:node/139464',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -46,
    'uuid' => '74b34619-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/d6a1d079-ab49-45cc-ba75-34a54a2d038e',
  );
  // Exported menu link: 74b34832-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b34832-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'video',
    'router_path' => 'video',
    'link_title' => 'VIDEO',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_video:video',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'uuid' => '74b34832-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36c98-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36c98-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure/resorts',
    'router_path' => 'adventure/resorts',
    'link_title' => 'Skiing Resorts',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skiing-resorts:adventure/resorts',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b36c98-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34510-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36cfd-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36cfd-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure/backcountry',
    'router_path' => 'adventure/backcountry',
    'link_title' => 'Backcountry',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_backcountry:adventure/backcountry',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b36cfd-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34510-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36d43-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36d43-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture/athletes',
    'router_path' => 'culture/athletes',
    'link_title' => 'Athletes',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_athletes:culture/athletes',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b36d43-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34556-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36d85-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36d85-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture/ski-culture',
    'router_path' => 'culture/ski-culture',
    'link_title' => 'Skiing Culture',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skiing-culture:culture/ski-culture',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'uuid' => '74b36d85-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34556-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36ea4-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36ea4-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'culture/news',
    'router_path' => 'culture/news',
    'link_title' => 'News',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_news:culture/news',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b36ea4-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34556-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/all',
    'router_path' => 'gear/all',
    'link_title' => 'Gear',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_gear:gear/all',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36f7d-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36f7d-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/skis',
    'router_path' => 'gear/skis',
    'link_title' => 'Skis',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skis:gear/skis',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b36f7d-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b36fc1-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b36fc1-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/boots',
    'router_path' => 'gear/boots',
    'link_title' => 'Boots',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_boots:gear/boots',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b36fc1-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b37005-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b37005-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/bindings',
    'router_path' => 'gear/bindings',
    'link_title' => 'Bindings Reviews',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'uuid' => '74b37005-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b371eb-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b371eb-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/backcountry-gear',
    'router_path' => 'gear/backcountry-gear',
    'link_title' => 'Backcountry Gear',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_backcountry-gear:gear/backcountry-gear',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'uuid' => '74b371eb-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b37234-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b37234-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/brands',
    'router_path' => 'gear/brands',
    'link_title' => 'All Gear Brands',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'uuid' => '74b37234-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b37272-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b37272-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gear/apparel-and-accessories',
    'router_path' => 'gear/apparel-and-accessories',
    'link_title' => 'Apparel & Accessories',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_apparel--accessories:gear/apparel-and-accessories',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'uuid' => '74b37272-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b372b9-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b372b9-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'adventure/destinations',
    'router_path' => 'adventure/destinations',
    'link_title' => 'Far Away Places',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_far-away-places:adventure/destinations',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'uuid' => '74b372b9-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34510-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b37473-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b37473-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'skiing-culture/how-ski',
    'router_path' => 'skiing-culture/how-ski',
    'link_title' => 'Resort Beta',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_resort-beta:skiing-culture/how-ski',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b37473-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34599-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b374b6-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b374b6-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'skiing-culture/diy-maintenance-fitness-and-nutrition',
    'router_path' => 'skiing-culture/diy-maintenance-fitness-and-nutrition',
    'link_title' => 'Skills',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_skills:skiing-culture/diy-maintenance-fitness-and-nutrition',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b374b6-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34599-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b3760f-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b3760f-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'router_path' => 'node/%',
    'link_title' => 'Subscribe',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_subscribe:node/139464',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b3760f-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34619-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/d6a1d079-ab49-45cc-ba75-34a54a2d038e',
  );
  // Exported menu link: 74b37652-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b37652-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'http://media.aimmedia.com/skiing/skiingmag/newslettersignup.html?loc=topnav&newsletters[1610]=1&newsletters[1620]=1&newsletters[1630]=1&lnk=skiing',
    'router_path' => '',
    'link_title' => 'Newsletter',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_newsletter:http://media.aimmedia.com/skiing/skiingmag/newslettersignup.html?loc=topnav&newsletters[1610]=1&newsletters[1620]=1&newsletters[1630]=1&lnk=skiing',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b37652-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b34619-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Exported menu link: 74b39abd-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b39abd-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-video-categories',
    'router_path' => 'node/%',
    'link_title' => 'Gear',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-video-categories_gear:node/148259',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'uuid' => '74b39abd-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/237d0cff-6776-44f7-a480-33bb2304a564',
  );
  // Exported menu link: 74b39b00-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b39b00-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-video-categories',
    'router_path' => 'node/%',
    'link_title' => 'More',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-video-categories_more:node/148262',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'uuid' => '74b39b00-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/6f70a045-3c94-4f48-9f27-89336026d6d3',
  );
  // Exported menu link: 74b39c51-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b39c51-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-video-categories',
    'router_path' => 'node/%',
    'link_title' => 'Adventures',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-video-categories_adventures:node/148260',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'uuid' => '74b39c51-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/4b868332-1a53-46a1-8e0e-ea4cdf68ea18',
  );
  // Exported menu link: 74b39c98-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b39c98-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'menu-video-categories',
    'router_path' => 'node/%',
    'link_title' => 'Culture',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-video-categories_culture:node/148261',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'uuid' => '74b39c98-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/22dd0786-c2bc-4a47-8a04-219774aa3c1a',
  );
  // Exported menu link: 74b39cd7-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b39cd7-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'router_path' => 'node/%',
    'link_title' => 'Videos',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'main-menu_videos:node/148259',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'uuid' => '74b39cd7-ad6d-11e5-ad9e-c0cab21e7d1c',
    'uuid_path' => 'node/237d0cff-6776-44f7-a480-33bb2304a564',
  );
  // Exported menu link: 74b39f1c-ad6d-11e5-ad9e-c0cab21e7d1c
  $menu_uuid_links['74b39f1c-ad6d-11e5-ad9e-c0cab21e7d1c'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'gears/apparel-and-accessories',
    'router_path' => 'gears/apparel-and-accessories',
    'link_title' => 'Apparel & Accessories',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_apparel--accessories:gears/apparel-and-accessories',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'uuid' => '74b39f1c-ad6d-11e5-ad9e-c0cab21e7d1c',
    'parent_uuid' => '74b36f37-ad6d-11e5-ad9e-c0cab21e7d1c',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('# of lifts');
  t('Accessories');
  t('Adventure');
  t('Adventures');
  t('All Gear Brands');
  t('Apparel');
  t('Apparel & Accessories');
  t('Athletes');
  t('Backcountry');
  t('Backcountry Gear');
  t('Bindings');
  t('Bindings Reviews');
  t('Boots');
  t('Car Racks');
  t('Culture');
  t('Facebook');
  t('Far Away Places');
  t('Gadgets');
  t('Gear');
  t('How To');
  t('Magazine');
  t('More');
  t('MySpace');
  t('Nastar');
  t('News');
  t('Newsletter');
  t('Poles');
  t('Resort Beta');
  t('STORIES');
  t('Ski');
  t('Skiing Culture');
  t('Skiing Resorts');
  t('Skills');
  t('Skis');
  t('Subscribe');
  t('The Scene');
  t('Twitter');
  t('VIDEO');
  t('Videos');
  t('Warren Miller');
  t('activities');
  t('challenge');
  t('family');
  t('grooming');
  t('lodging');
  t('scenery');
  t('service');
  t('snowfall');
  t('state');
  t('terrain');
  t('value');


  return $menu_uuid_links;
}
