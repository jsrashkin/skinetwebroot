<?php
/**
 * @file
 * skiing_menus_uuid.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function skiing_menus_uuid_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'Primary links are often used at the theme layer to show the major sections of a site. A typical representation for primary links would be tabs along the top.',
  );
  // Exported menu: menu-gear-directory.
  $menus['menu-gear-directory'] = array(
    'menu_name' => 'menu-gear-directory',
    'title' => 'Gear Directory',
    'description' => 'Gear Directory Menu - points to taxonomy',
  );
  // Exported menu: menu-most-popular.
  $menus['menu-most-popular'] = array(
    'menu_name' => 'menu-most-popular',
    'title' => 'Most Popular',
    'description' => 'Links to term pages that are most popular, will be manually controlled.',
  );
  // Exported menu: menu-resort-directory.
  $menus['menu-resort-directory'] = array(
    'menu_name' => 'menu-resort-directory',
    'title' => 'Resort Directory',
    'description' => 'Directory that points to resorts that can sorted by geographical region and rating',
  );
  // Exported menu: menu-seo-menu.
  $menus['menu-seo-menu'] = array(
    'menu_name' => 'menu-seo-menu',
    'title' => 'seo-menu',
    'description' => '',
  );
  // Exported menu: menu-social-networks.
  $menus['menu-social-networks'] = array(
    'menu_name' => 'menu-social-networks',
    'title' => 'Social Networks',
    'description' => 'Links to the site\\s pages on various social network sites.',
  );
  // Exported menu: menu-video-categories.
  $menus['menu-video-categories'] = array(
    'menu_name' => 'menu-video-categories',
    'title' => 'Video categories',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Directory that points to resorts that can sorted by geographical region and rating');
  t('Gear Directory');
  t('Gear Directory Menu - points to taxonomy');
  t('Links to term pages that are most popular, will be manually controlled.');
  t('Links to the site\\s pages on various social network sites.');
  t('Main menu');
  t('Most Popular');
  t('Primary links are often used at the theme layer to show the major sections of a site. A typical representation for primary links would be tabs along the top.');
  t('Resort Directory');
  t('Social Networks');
  t('Video categories');
  t('seo-menu');


  return $menus;
}
