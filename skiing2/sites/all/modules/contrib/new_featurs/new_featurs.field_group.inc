<?php
/**
 * @file
 * new_featurs.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function new_featurs_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basics|node|resort|form';
  $field_group->group_name = 'group_basics';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resort';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Contact Information',
    'weight' => '9',
    'children' => array(
      0 => 'field_resort_phone',
      1 => 'field_resort_website',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-basics field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_basics|node|resort|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_call_to_action|node|newsletter_block|form';
  $field_group->group_name = 'group_call_to_action';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'newsletter_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Call To Action',
    'weight' => '5',
    'children' => array(
      0 => 'field_newsletter_call_to_act',
      1 => 'field_newsletter_call_to_act_ref',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-call-to-action field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_call_to_action|node|newsletter_block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_details|node|prodcut_ski|form';
  $field_group->group_name = 'group_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'prodcut_ski';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Details',
    'weight' => '6',
    'children' => array(
      0 => 'field_product_brand',
      1 => 'field_product_model',
      2 => 'field_prodcut_year',
      3 => 'field_product_gender',
      4 => 'field_product_level',
      5 => 'field_product_price',
      6 => 'field_product_buy',
      7 => 'field_product_buy_now_price',
      8 => 'field_ski_lengths',
      9 => 'field_ski_tiptailwaist',
      10 => 'field_ski_binding',
      11 => 'field_new',
      12 => 'field_type',
      13 => 'field_product_map',
      14 => 'field_product_category',
      15 => 'field_product_tested',
      16 => 'field_product_notes',
      17 => 'field_product_review',
      18 => 'field_ski_waistwidth',
      19 => 'field_product_awards',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_details|node|prodcut_ski|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_details|node|prodcut|form';
  $field_group->group_name = 'group_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'prodcut';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Details',
    'weight' => '1',
    'children' => array(
      0 => 'field_product_brand',
      1 => 'field_product_model',
      2 => 'field_prodcut_year',
      3 => 'field_product_gender',
      4 => 'field_product_level',
      5 => 'field_product_price',
      6 => 'field_product_buy',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_details|node|prodcut|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_details|node|product_boot|form';
  $field_group->group_name = 'group_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_boot';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Details',
    'weight' => '6',
    'children' => array(
      0 => 'field_product_brand',
      1 => 'field_product_model',
      2 => 'field_prodcut_year',
      3 => 'field_product_gender',
      4 => 'field_product_level',
      5 => 'field_product_price',
      6 => 'field_product_buy',
      7 => 'field_product_buy_now_price',
      8 => 'field_new',
      9 => 'field_product_tested',
      10 => 'field_product_map',
      11 => 'field_product_review',
      12 => 'field_product_notes',
      13 => 'field_prodcut_sizes',
      14 => 'field_last_width',
      15 => 'field_boot_weight',
      16 => 'field_boot_flex',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_details|node|product_boot|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_details|node|resort|form';
  $field_group->group_name = 'group_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resort';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Resort Details',
    'weight' => '11',
    'children' => array(
      0 => 'field_resort_lifts',
      1 => 'field_resort_acres',
      2 => 'field_resort_snowfall',
      3 => 'field_resort_vertical',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-details field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_details|node|resort|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_digital_subscriptions|node|pcd|form';
  $field_group->group_name = 'group_digital_subscriptions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'pcd';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Digital Subscriptions',
    'weight' => '8',
    'children' => array(
      0 => 'field_dig_sub_title',
      1 => 'field_dig_sub_link',
      2 => 'field_dig_sub_header',
      3 => 'field_dig_sub_border',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-digital-subscriptions field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_digital_subscriptions|node|pcd|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_enhancements|node|resort|form';
  $field_group->group_name = 'group_enhancements';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resort';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Enhancements',
    'weight' => '15',
    'children' => array(
      0 => 'field_resort_trailmap_image',
      1 => 'field_resort_flash_tout',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-enhancements field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_enhancements|node|resort|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_form_block|node|pcd|form';
  $field_group->group_name = 'group_form_block';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'pcd';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Form Block',
    'weight' => '1',
    'children' => array(
      0 => 'field_pcd_header',
      1 => 'field_cover_thumbnail',
      2 => 'field_pcd_post',
      3 => 'field_from_code',
      4 => 'field_magazine_id',
      5 => 'field_pcd_links',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-form-block field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_form_block|node|pcd|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general_info|node|profile|form';
  $field_group->group_name = 'group_general_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'General Information',
    'weight' => '5',
    'children' => array(
      0 => 'field_sex',
      1 => 'field_birth_year',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-general-info field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_general_info|node|profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hp_featured_content|node|homepage|form';
  $field_group->group_name = 'group_hp_featured_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'homepage';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Featured Content',
    'weight' => '44',
    'children' => array(
      0 => 'field_featured_slideshow',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-hp-featured-content field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_hp_featured_content|node|homepage|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_just_for_fun|node|profile|form';
  $field_group->group_name = 'group_just_for_fun';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'About You',
    'weight' => '6',
    'children' => array(
      0 => 'field_skier_level',
      1 => 'field_skier_years',
      2 => 'field_skier_days',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-just-for-fun field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_just_for_fun|node|profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_main_photo|node|newsletter_block|form';
  $field_group->group_name = 'group_main_photo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'newsletter_block';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Main Photo Settings',
    'weight' => '7',
    'children' => array(
      0 => 'field_main_photo_max_width',
      1 => 'field_main_photo_max_height',
      2 => 'field_main_photo_url',
      3 => 'field_main_photo_url_ref',
      4 => 'field_main_photo_alt',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-main-photo field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_main_photo|node|newsletter_block|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_newsletter|node|profile|form';
  $field_group->group_name = 'group_newsletter';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Newsletter Preferences',
    'weight' => '1',
    'children' => array(
      0 => 'field_newsletter_preferences',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-newsletter field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_newsletter|node|profile|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pcd_menu|node|pcd|form';
  $field_group->group_name = 'group_pcd_menu';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'pcd';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Menu',
    'weight' => '3',
    'children' => array(
      0 => 'field_pcd_menu_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-pcd-menu field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_pcd_menu|node|pcd|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pcd_sub_offer|node|pcd|form';
  $field_group->group_name = 'group_pcd_sub_offer';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'pcd';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Sub Offer',
    'weight' => '2',
    'children' => array(
      0 => 'field_pcd_sub_offer_image',
      1 => 'field_pcd_sub_offer_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-pcd-sub-offer field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_pcd_sub_offer|node|pcd|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_print_subscriptions|node|pcd|form';
  $field_group->group_name = 'group_print_subscriptions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'pcd';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Print Subscriptions',
    'weight' => '9',
    'children' => array(
      0 => 'field_print_sub_title',
      1 => 'field_print_sub_link',
      2 => 'field_print_sub_header',
      3 => 'field_print_sub_border',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-print-subscriptions field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_print_subscriptions|node|pcd|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_rankings|node|resort|form';
  $field_group->group_name = 'group_rankings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resort';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Rankings',
    'weight' => '12',
    'children' => array(
      0 => 'field_resort_rank_skiing',
      1 => 'field_resort_rank_west',
      2 => 'field_resort_rank_east',
      3 => 'field_resort_rank_acres',
      4 => 'field_resort_rank_lifts',
      5 => 'field_resort_rank_snowfall',
      6 => 'field_resort_rank_vertical',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-rankings field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_rankings|node|resort|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ratings|node|prodcut_ski|form';
  $field_group->group_name = 'group_ratings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'prodcut_ski';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Ratings',
    'weight' => '10',
    'children' => array(
      0 => 'field_ski_rating_powderperform',
      1 => 'field_ski_rating_stabilityspeed',
      2 => 'field_ski_rating_longturns',
      3 => 'field_ski_rating_mediumturns',
      4 => 'field_ski_rating_shortturns',
      5 => 'field_ski_rating_maneuver',
      6 => 'field_ski_rating_hardsnowgrip',
      7 => 'field_ski_rating_crudperform',
      8 => 'field_ski_rating_corduroyperform',
      9 => 'field_ski_rating_mogulsperform',
      10 => 'field_ski_rating_forgiveness',
      11 => 'field_ski_rating_quickness',
      12 => 'field_ski_rating_reboundenergy',
      13 => 'field_ski_rating_touring_capabil',
      14 => 'field_ski_rating_versatility',
      15 => 'field_ski_rating_energylivelines',
      16 => 'field_ski_rating_averagescore',
      17 => 'field_ski_rating_balanceofskill',
      18 => 'field_ski_rating_flotation',
      19 => 'field_ski_rating_overall',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ratings field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ratings|node|prodcut_ski|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ratings|node|product_boot|form';
  $field_group->group_name = 'group_ratings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_boot';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Ratings',
    'weight' => '10',
    'children' => array(
      0 => 'field_boot_rating_toeboxfit',
      1 => 'field_boot_rating_forefootfit',
      2 => 'field_boot_rating_anklefit',
      3 => 'field_boot_rating_instepfit',
      4 => 'field_boot_rating_adjustments',
      5 => 'field_boot_rating_closure',
      6 => 'field_boot_rating_response',
      7 => 'field_boot_rating_support',
      8 => 'field_boot_rating_flex',
      9 => 'field_boot_rating_steering',
      10 => 'field_boot_rating_comfort',
      11 => 'field_boot_rating_overalimp',
      12 => 'field_boot_rating_averagescore',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ratings field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ratings|node|product_boot|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ratings|node|resort|form';
  $field_group->group_name = 'group_ratings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'resort';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Ratings',
    'weight' => '13',
    'children' => array(
      0 => 'field_resort_rating_snow',
      1 => 'field_resort_rating_grooming',
      2 => 'field_resort_rating_terrain',
      3 => 'field_resort_rating_challenge',
      4 => 'field_resort_rating_value',
      5 => 'field_resort_rating_liftservice',
      6 => 'field_resort_rating_service',
      7 => 'field_resort_rating_weather',
      8 => 'field_resort_rating_access',
      9 => 'field_resort_rating_onmtnfood',
      10 => 'field_resort_rating_dining',
      11 => 'field_resort_rating_lodging',
      12 => 'field_resort_rating_apresski',
      13 => 'field_resort_rating_offhill',
      14 => 'field_resort_rating_family',
      15 => 'field_resort_rating_scenery',
      16 => 'field_resort_rating_terrainpark',
      17 => 'field_resort_rating_overall',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ratings field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_ratings|node|resort|form'] = $field_group;

  return $export;
}
