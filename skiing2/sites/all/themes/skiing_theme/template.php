<?php

/**
 * @file
 * template.php
 */

/**
 * Implements template_preprocess_html()
 */
function skiing_theme_preprocess_html(&$vars) {
  if (!empty($vars['page']['content']['system_main']['nodes'])) {
    if (array_key_exists(variable_get('contact_us_node_nid', 57021), $vars['page']['content']['system_main']['nodes'])) {
      $vars['classes_array'][] = 'body-contact';
    }
  }
  $arg0 = arg(0, drupal_get_path_alias());
  switch ($arg0) {
    case 'sitemap' :
      $vars['classes_array'][] = 'sitemap';
      break;
    case 'search' :
      $vars['classes_array'][] = 'search-results';
      break;
  }
  if (!drupal_is_front_page()) {
    drupal_add_css(path_to_theme() . '/css/zozo.tabs.min.css', array('group' => CSS_THEME));
    drupal_add_css(path_to_theme() . '/css/jquery.bxslider.css', array('group' => CSS_THEME));
    drupal_add_js(path_to_theme() . '/js/zozo.tabs.min.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/js/jquery.bxslider.js', array('group' => JS_LIBRARY));
    drupal_add_js(path_to_theme() . '/js/isotope.js', array('group' => JS_LIBRARY));
  }
}

/**
 * Implements template_preprocess_page()
 */
function skiing_theme_preprocess_page(&$variables) {
  $variables['content_2_cols'] = $variables['not_title'] = FALSE;

  $arg0 = arg(0, drupal_get_path_alias());
  $sitemap = ($arg0 == 'sitemap');
  $search = ($arg0 == 'search');
  $variables['content_column_class'] = ' class="col-md-12 page-full-container"';
  if (drupal_is_front_page()) {
    $variables['content_column_class'] = ' class="col-md-12"';
  }
  if ($sitemap || !empty($variables['node'])) {
    $variables['content_2_cols'] = TRUE;
  }
  if ($sitemap || $search || (!empty($variables['node']) && $variables['node']->type == 'video')) {
    $variables['not_title'] = TRUE;
  }
  //map for contact page
  if (!empty($variables['node']) && $variables['node']->nid == variable_get('contact_us_node_nid', 57021)) {
    $variables['contact_us_iframe_url'] = url(variable_get('contact_us_iframe_url', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12093.821256948668!2d-74.00032534505492!3d40.730005716519614!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25990e23d7e8d%3A0xfa615edfd1b67e18!2sWashington+Square+Park!5e0!3m2!1ses!2sbo!4v1443740027020'));
  }
}

/**
 * Returns HTML for a themed site map box.
 *
 * @param array $variables
 *   An associative array containing:
 *   - title: The subject of the box.
 *   - content: The content of the box.
 *   - attributes:  Optional attributes for the box.
 *
 * @return string
 *   Returns sitemap display in DIV.
 */
function skiing_theme_site_map_box($variables) {
  $title = $variables['title'];
  $content = $variables['content'];
  $attributes = $variables['attributes'];
  $attributes['class'] = 'sitemap-item';
  $options = $variables['options'];

  $output = '';
  if (!empty($title) || !empty($content)) {
    $output .= '<div' . drupal_attributes($attributes) . '>';
    if (!empty($title) && isset($options['show_titles'])) {
      $output .= '<strong>' . $title . '</strong>';
    }
    if (!empty($content)) {
      $output .= $content;
    }
    $output .= '</div>';
  }

  return $output;
}

/**
 * Returns HTML for a wrapper for a menu sub-tree.
 *
 * This is a clone of the core theme_menu_tree() function with the exception of
 * the site_map specific class name used in the UL that also allow themers to
 * override the function only for the site map page.
 *
 * @param array $variables
 *   An associative array containing:
 *   - tree: An HTML string containing the tree's items.
 *
 * @return string
 *   Returns the html string with the <ul> for the menu tree.
 *
 * @see template_preprocess_menu_tree()
 * @ingroup themeable
 */
function skiing_theme_site_map_menu_tree($variables) {
  return '<ul>' . $variables['tree'] . '</ul>';
}

/**
 * Overrides theme_breadcrumb().
 *
 */
function skiing_theme_breadcrumb($variables) {
  $output = '';
  $items = array();
  $breadcrumb = $variables['breadcrumb'];

  // Determine if we are to display the breadcrumb.
  $bootstrap_breadcrumb = theme_get_setting('bootstrap_breadcrumb');
  if (($bootstrap_breadcrumb == 1 || ($bootstrap_breadcrumb == 2 && arg(0) == 'admin')) && !empty($breadcrumb)) {
    $menu_item = menu_get_item();
    $output = '<div class="breadcrumbs">';
    if (!empty($menu_item['page_arguments']) && is_object($menu_item['page_arguments'][0]) && !empty($menu_item['page_arguments'][0]->nid)) {
      $items[] = $breadcrumb[0];
      foreach ($menu_item['page_arguments'][0]->taxonomy_vocabulary_2['und'] as $site_channel) {
        $items[] = '<span>' . $site_channel['taxonomy_term']->name . '</span>';
      }
      $items[] = '<span>' . $breadcrumb[1]['data'] . '</span>';
    }
    else {
      foreach ($breadcrumb as $item) {
        $items[] = (!is_array($item)) ? $item : '<span>' . $item['data'] . '</span>';
      }
    }
    $output .= implode(' > ', $items);
    $output .= '</div>';
  }
  return $output;
}

/*/
 * Implements template_preprocess_block
 * */
function skiing_theme_preprocess_block(&$variables) {
  switch ($variables['block_html_id']) {
    case 'block-simple-weather-simple-weather-report' :
      $variables['classes_array'] = array('col-md-4', 'block-footer');
      break;
    case 'block-search-form' :
    case 'block-block-7' :
//      unset ($variables['classes_array']);
      $variables['theme_hook_suggestions'] = array('block__no_wrapper');
      break;
    case 'block-instagram-block-instagram-block-tag' :
      $variables['content'] = theme('skiing_site_instagram', array('content' => $variables['content']));
      break;
  }
}

/**
 * Implements THEMENAME_menu_tree__MENU_NAME()
 */
function skiing_theme_menu_tree__menu_footer_nav(&$variables){
  return '<ul class="footer-menu">' . $variables['tree'] . '</ul>';
}

/**
 * Implements THEMENAME_menu_tree__MENU_NAME()
 */
function skiing_theme_menu_tree__main_menu(&$variables){
  return '<ul class="nav navbar-nav">' . $variables['tree'] . '</ul>';
}
