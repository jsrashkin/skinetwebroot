
var skiingcustom = skiingcustom || {};

/**
 * Theme behavior - Drupal version.
 * Register our theme behavior so it can act on DOM.
 */
Drupal.behaviors.skiingcustom = {
  attach: function(context, settings) {
    skiingcustom.init(context, settings);
  }
};

(function ($) {

  /**
   * Attach handler.
   * Will be called on any DOM changes triggered by Drupal system.
   */
  skiingcustom.init = function (context, settings) {
    skiingcustom.ToggleButtonMore(context, settings);
  };

	/*$("header .genesis-nav-menu, .nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu").addClass("responsive-menu").before('<div class="responsive-menu-icon"></div>');

	$(".responsive-menu-icon").click(function(){
		$(this).next("header .genesis-nav-menu, .nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu").slideToggle();
	});

	$(window).resize(function(){
		if(window.innerWidth > 767) {
			$("header .genesis-nav-menu, .nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu, nav .sub-menu").removeAttr("style");
			$(".responsive-menu > .menu-item").removeClass("menu-open");
		}
	});*/

	$(".search-header .search-button").click(function(event){

		$(".search-header .search-field").toggleClass("open");
	});

	jQuery(window).bind("load", function() {
	   jQuery(".loader").addClass("load-end");
	});


	// filter buttons
	//$('.dropdown-filter select').change(function(){
	//	var $this = $(this);
    //
	//	// store filter value in object
	//	// i.e. filters.color = 'red'
	//	var group = $this.attr('data-filter-group');
    //
	//	filters[ group ] = $this.find(':selected').attr('data-filter-value');
	//	// console.log( $this.find(':selected') )
	//	// convert object into array
	//	var isoFilters = [];
	//	for ( var prop in filters ) {
	//	  	isoFilters.push(
	//	  		filters[ prop ]
	//	  	)
	//	}
	//	console.log(filters);
	//	var selector = isoFilters.join('');
	//	$container.isotope({
	//		filter: selector
	//	});
	//	return false;
	//});


  /**
   * Submit user registration form by second submit button (link)
   */
  skiingcustom.ToggleButtonMore = function (context, settings) {
    $(".toogle-btnmore", context).click(function(){
      $(".hide-block").slideToggle();
      $(this).toggleClass("opn-more");
      if ($('.toogle-btnmore').hasClass('opn-more')){
        $('.t-mero').html('Minimize');
        $('.sign').html('-');
      } else {
        $('.t-mero').html('Maximize');
        $('.sign').html('+');
      }
    });

    var $container = $('#gear-items'),
      filters = {};

    try {
      $container.isotope({
        itemSelector: '.gear-element',
        masonry: {
          columnWidth: 180,
          gutter: 20
        }
      });
    }
    catch (e) {}
  };

})(jQuery);

jQuery(document).ready(function ($) {
	setTimeout(function () {
      $("#tabbed-nav .z-tabs-nav li").removeClass('z-active');
      var href = window.location.pathname;
      if($("#tabbed-nav .z-tabs-nav li").find('a[href="'+href+'"]').length !=0){
          $("#tabbed-nav .z-tabs-nav li").find('a[href="'+href+'"]').parent().addClass('z-active');
      }
      else {
          $("#tabbed-nav .z-tabs-nav li").find('a:not([href])').parent().addClass('z-active');
      }


      $('.popup-with-zoom-anim').magnificPopup({
        type: 'inline',

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
      });
	}, 2000);
});

// jQuery(document).ready(function(){
//   jQuery( ".nav li a" ).hover(
//   function() {
//     jQuery(this).parent("li").removeClass("open");
//   }, function() {
//     jQuery(this).parent("li").addClass("open");
//   }
// )};

/* Top Menu ----------------------- */

jQuery(document).ready(function(){
	jQuery(".dropdown-toggle").removeAttr("data-toggle");

});	
jQuery(document).ready(function(){

	var text_ski = jQuery( ".page-gear .col-md-9 > h1" ).html();
	if(text_ski == 'Skis'){
	jQuery( ".col-md-9 > h1" ).html("<img src='http://skinet-construction.com/skiing/sites/all/themes/skiing_theme/images/ski.png' / ><span class='channel-title'>Skis</span>");
	}
	
	var text_boots = jQuery( ".page-gear .col-md-9 > h1" ).html();
	if(text_boots == 'Boots'){
	jQuery( ".col-md-9 > h1" ).html("<img src='http://skinet-construction.com/skiing/sites/all/themes/skiing_theme/images/boots1.png' / ><span class='channel-title'>Boots</span>");
	}
	
	var text_apprl = jQuery( ".page-gear .col-md-9 > h1" ).html();
	if(text_apprl == 'Apparel'){
	jQuery( ".col-md-9 > h1" ).html("<img src='http://skinet-construction.com/skiing/sites/all/themes/skiing_theme/images/appearel.png' / ><span class='channel-title'>Apparel</span>");
	}
	
	/* Video Menu Tabs* */
	
	var activeurl = window.location;
	jQuery('a[href="'+activeurl+'"]').parent('li').addClass('active');
	
	/* remove empty p-tag -article-content */
	
  //jQuery(".region-content p:empty").remove();
  
  jQuery('p').each(function(){
   var htm=jQuery(this).html();
  if(htm=='&nbsp;')
  {
    jQuery(this).remove();
  }
})
	
});	


