<?php
// Plugin definition
$plugin = array(
  'title' => t('Gear'),
  'category' => t('Skiing'),
  'icon' => 'onerowtwocols.png',
  'theme' => 'gear',
  'admin theme' => 'gear_admin',
  'admin css' => 'gear_admin.css',
  'regions' => array(
    'filter' => t('Filter'),
    'content' => t('Content'),
  )
);
