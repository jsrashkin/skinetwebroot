<div id="tabbed-nav yash" class="custom-navs <?php print $classes; ?>">
  <ul>
    <li><?php print l(t('Skis'), '/gear/skis')?></li>
    <li><?php print l(t('Boots'), '/gear/boots')?></li>
    <li><?php print l(t('How We Test'), '/node/119588'); ?></li>
    
  </ul>

  <div>
    <div>
      <div class="filters-container view-filters">
        <?php if ($content['filter']): ?>
          <?php print $content['filter']; ?>
        <?php endif; ?>
      </div>
      <?php if ($content['content']): ?>
        <?php print $content['content']; ?>
      <?php endif; ?>
    </div>
    <div></div>
    <div></div>
  </div>
</div>
<script>
  jQuery(document).ready(function ($) {
    setTimeout(function () {
      $("#tabbed-nav").zozoTabs({
        theme: "silver",
        orientation: "horizontal",
        position: "top-left"
      });
    }, 1000);
  });
</script>
