<?php
if (!empty($field_main_photo)) {
  print theme_image_style(
    array (
      "style_name" => "node_main_image",
      "path" => $field_main_photo[0]['node']->field_image['und'][0]['uri'],
      "height" => NULL,
      "width" => NULL,
    )
  );
}
print render($elements['body']);