<?php $username = $node->name;
	  $u_id = $node->uid;

// print "Author: <a href=/skiing/users/$username >$username</a>" ; ?>
<p><?php print render($content['field_author']) ?></p>
<p><?php print render($content['field_dek']) ?></p>
<?php
$uri = '';
if (!empty($field_main_photo[0])) {
  $uri = $field_main_photo[0]['node']->field_image['und'][0]['uri'];
}
elseif (!empty($field_thumbnail[0])) {
  $uri = $field_thumbnail[0]['node']->field_image['und'][0]['uri'];
}

if (!empty($uri)) {
  print theme_image_style(
    array (
      "style_name" => "node_main_image",
      "path" => $uri,
      "height" => NULL,
      "width" => NULL,
    )
  );
}
print render($elements['body']);
