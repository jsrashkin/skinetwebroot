<!-- Home instagram -->
<div class="row">

  <div class="instagram-container">
    <div class="wrap">
      <h3 class="section-title"><?php print t('Instagram Photos'); ?></h3>
      <div class="instagram-slider">
        <?php echo $content; ?>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function() {

      jQuery(".instagram-slider").owlCarousel({

        pagination: true,
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        //singleItem:true

        // "singleItem:true" is a shortcut for:
        items : 3
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

      });

    });
  </script>
</div>

<!-- End Home instagram -->
