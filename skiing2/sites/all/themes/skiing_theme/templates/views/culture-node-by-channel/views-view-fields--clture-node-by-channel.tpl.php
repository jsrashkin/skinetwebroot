<div class="color-title orange full-t">
    <div class="border-color-container">
       <div class="row little-padding">
           <div class="thumb-cat">
				<?php print $fields['field_image']->content; ?>
            </div>
			  <div class="content-article">
				   <div class="custom-title">
                          <h2><?php print $fields['title']->content; ?></h2>
                   </div>

				<?php print $fields['field_dek']->content; ?>
			  </div>
        </div>
    </div>
</div>
