<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="container-list">
<?php foreach ($rows as $id => $row): ?>
  <div class="list-item">
    <div class="container-fluid">
      <div class="row">
        <?php print $row; ?>
      </div>
    </div>
  </div>
<?php endforeach; ?>
</div>