<div class="block-sidebar">
  <h3><?php print t('Recent'); ?></h3>
  <?php if ($rows): ?>
    <?php print $rows; ?>
  <?php elseif ($empty): ?>
    <?php print $empty; ?>
  <?php endif; ?>
</div>