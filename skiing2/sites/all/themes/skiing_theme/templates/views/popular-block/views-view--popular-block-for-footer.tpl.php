<div class="col-md-4 block-footer">
  <div class="post-list">
    <div class="col-md-12">
      <h3><?php print t('POPULAR THIS MONTH'); ?></h3>
      <?php if ($rows): ?>
        <?php print $rows; ?>
      <?php elseif ($empty): ?>
        <?php print $empty; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
