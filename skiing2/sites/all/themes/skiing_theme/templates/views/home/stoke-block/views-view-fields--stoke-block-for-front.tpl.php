<?php
$class  = ($zebra == 'even') ? 'yellow' : 'blue';
$advert_content = '';
if ($fields['counter']->content == 3 && module_exists('ad_manager')) {
  $block = module_invoke('ad_manager', 'block_view', 'stoke');
  if (!empty($block)) {
    $advert_content = render($block['content']);
  }
}
?>
<div class="stoke-block <?php print $class; ?>">
  <?php if (!empty($advert_content)) : ?>
    <?php print $advert_content; ?>
  <?php else : ?>
    <?php print $fields['field_image']->content; ?>
    <div class="stoke-hidden">
      <h5><span><?php print $fields['type']->content; ?></span></h5>
      <div class="date"><?php print $fields['created']->content; ?></div>
      <p><?php print $fields['title']->content; ?></p>
      <?php print l(t('Read'), 'node/' . $fields['nid']->raw, array('attributes' => array('class' => 'read'))); ?>
    </div>
  <?php endif; ?>
</div>


<script type="text/javascript">
    jQuery(document).ready(function() {

      jQuery(".stoke-front_block").owlCarousel({

        pagination: true,
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        //singleItem:true

        // "singleItem:true" is a shortcut for:
        items : 3
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

      });

    });
  </script>
