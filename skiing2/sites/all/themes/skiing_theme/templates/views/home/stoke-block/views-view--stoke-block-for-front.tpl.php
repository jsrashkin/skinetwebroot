<!-- Home Stoke -->

<div class="row">
  <div class="wrap">
    <div class="block-area">
      <h3 class="section-title"><?php print t('STORIES'); ?></h3>
        <?php if ($rows): ?>
          <?php print $rows; ?>
        <?php elseif ($empty): ?>
          <?php print $empty; ?>
        <?php endif; ?>
    </div>
  </div>
</div>

<!-- End Home Stoke -->
