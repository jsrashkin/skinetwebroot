<!-- Home Videos -->
<div class="row">
  <div class="videos-container">
    <?php if ($rows): ?>
      <?php print $rows; ?>
    <?php elseif ($empty): ?>
      <?php print $empty; ?>
    <?php endif; ?>
  </div>
  <script type="text/javascript">
    jQuery(document).ready(function() {

      jQuery(".videos-slider").owlCarousel({

        pagination: true,
        navigation : false, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        //singleItem:true

        // "singleItem:true" is a shortcut for:
        items : 2
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

      });

    });
  </script>
</div>

  <!-- End Home Videos --