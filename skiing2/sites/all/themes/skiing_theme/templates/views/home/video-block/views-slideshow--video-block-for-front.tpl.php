<div class="wrap">
  <h3><?php print t('Videos'); ?></h3>
  <div class="videos-slider">
    <?php foreach ($rows as $id => $row): ?>
      <div class="item"><?php print $row; ?></div>
    <?php endforeach; ?>
  </div>
</div>
