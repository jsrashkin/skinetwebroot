<!-- Home Adventure -->
<div class="row">
  <div class="adventure-container">
    <div class="wrap">
      <h3 class="section-title"><?php print t('Adventure'); ?></h3>
        <?php if ($rows): ?>
          <?php print $rows; ?>
        <?php elseif ($empty): ?>
          <?php print $empty; ?>
        <?php endif; ?>
    </div>
  </div>

  <script type="text/javascript">
    jQuery(document).ready(function() {

      jQuery(".adventure-slider").owlCarousel({

        pagination: true,
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        //singleItem:true

        // "singleItem:true" is a shortcut for:
        items : 3
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

      });

    });
  </script>
</div>

<!-- End Home Adventure -->
