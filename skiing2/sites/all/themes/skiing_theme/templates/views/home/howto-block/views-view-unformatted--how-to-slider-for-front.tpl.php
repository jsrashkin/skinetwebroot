<div class="reviews-slider-h">
<?php foreach ($rows as $id => $row): ?>
  <div class="item">
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
</div>
<script type="text/javascript">
  jQuery(document).ready(function() {

    jQuery(".reviews-slider-h").owlCarousel({

      autoPlay: 5000,
      pagination: true,
      navigation : false, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true

      // "singleItem:true" is a shortcut for:
      // items : 1,
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false

    });

  });
</script>