<div class="big-slider">
<?php foreach ($rows as $id => $row): ?>
  <div class="item"><?php print $row; ?></div>
<?php endforeach; ?>
</div>

<script type="text/javascript">
  jQuery(document).ready(function() {

    jQuery(".big-slider").owlCarousel({

      autoPlay: 5000,
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      pagination : true,
      paginationNumbers : false,
      singleItem:true
      // "singleItem:true" is a shortcut for:
      // items : 1,
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false

    });

  });
</script>

