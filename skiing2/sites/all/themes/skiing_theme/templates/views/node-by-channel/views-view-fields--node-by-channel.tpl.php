<!-- <div class="resort-image">
  <?php // print $fields['field_image']->content; ?>
</div>
<div class="resort-dsc">
  <h5><?php // print $fields['title']->content; ?></h5>
  <p class="resort-rating">
    <?php // print $fields['field_dek']->content; ?>
  </p>
</div> -->

  <div class="color-title orange full-t">
    <div class="border-color-container">
       <div class="row little-padding">
           <div class="thumb-cat">
				<?php print $fields['field_image']->content; ?>
                  <div class="custom-title">
                          <h2><?php print $fields['title']->content; ?></h2>
                          <p><strong><?php print $fields['created']->content; ?></strong></p>
                   </div>
             </div>
				  <div class="content-article">
					<?php print $fields['field_dek']->content; ?>
					<p class="read-more-article"><?php print $fields['view_node']->content; ?></p>					
				  </div>
        </div>
    </div>
</div>


