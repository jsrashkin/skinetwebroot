<!-- Home Gear Reviews -->

<div class="row">
	<h3 class="section-title"><?php print t('Gear'); ?></h3>
  <?php if ($rows): ?>
    <?php print $rows; ?>
  <?php elseif ($empty): ?>
    <?php print $empty; ?>
  <?php endif; ?>
</div>

<!-- End Home Gear Reviews -->
