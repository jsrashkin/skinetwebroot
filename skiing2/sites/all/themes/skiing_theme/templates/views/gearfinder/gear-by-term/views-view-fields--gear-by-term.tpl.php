<div class="gear-image">
  <?php print $fields['field_image']->content; ?>
</div>
<div class="gear-dsc">
  <h5><?php print $fields['title']->content; ?></h5>
  <p class="gear-rating">
    <?php print t('Rating'); ?>: <strong><?php print $fields['field_ski_rating_overall']->content; ?><span> / </span> 5</strong>
  </p>
  <p class="gear-msrp"><?php print t('MSRP'); ?>: <strong>$<?php print $fields['field_product_price']->content; ?></strong></p>
</div>