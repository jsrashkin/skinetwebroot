<?php

/**
 * @file
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $sort_by: The select box to sort the view using an exposed form.
 * - $sort_order: The select box with the ASC, DESC options to define order. May be optional.
 * - $items_per_page: The select box with the available items per page. May be optional.
 * - $offset: A textfield to define the offset of the view. May be optional.
 * - $reset_button: A button to reset the exposed filter applied. May be optional.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */
$count = 0;

$top_filters = array(
  'filter-field_product_gender_value',
  'filter-field_product_year_value',
  'filter-field_product_category_value',
  'filter-taxonomy_vocabulary_5_tid',
  'filter-field_product_awards_value'
);
$top_widgets = array();

foreach ($top_filters as $key => $top_filter) {
  if (!empty($widgets[$top_filter])) {
    $top_widgets[$top_filter] = $widgets[$top_filter];
    unset($widgets[$top_filter]);
  }
}


?>
<?php if (!empty($q)): ?>
  <?php
  // This ensures that, if clean URLs are off, the 'q' is added first so that
  // it shows up first in the URL.
  print $q;

  ?>
<?php endif; ?>



    <h3><?php print t('Primary Filters'); ?></h3>

    <?php foreach ($top_widgets as $id => $widget): ?>
      <?php $count++; ?>
      <?php if ($count == 1): ?>
        <div class="row">
          <div class="col-md-6">
      <?php endif;?>

      <div id="<?php print $widget->id; ?>-wrapper" class="row views-exposed-widget views-widget-<?php print $id; ?>" style="margin-bottom:15px;">
        <div class="col-md-3">
          <?php
            $image = '';
            switch ($widget->id) {
              case 'edit-field-product-gender-value' :
                $image = 'gender.png';
                break;
              case 'edit-field-product-year-value' :
                $image = 'year.png';
                break;
              case 'edit-taxonomy-vocabulary-5-tid' :
                $image = 'brand.png';
                break;
              case 'edit-field-product-awards-value' :
                $image = 'awards.png';
                break;
              case 'edit-field-product-category-value' :
                $image = 'category.png';
                break;
            }
          ?>
          <?php if (!empty($image)) : ?>
            <img src="<?php print base_path() . path_to_theme(); ?>/images/<?php print $image?>">
          <?php endif; ?>
        </div>
        <div class="col-md-9">
          <?php if (!empty($widget->label)): ?>
            <strong><?php print $widget->label; ?></strong>
          <?php endif; ?>
          <div class="views-widget dropdown-filter">
            <?php print $widget->widget; ?>
          </div>
        </div>
      </div>

      <?php if ($count == 3): ?>
          </div>
          <div class="col-md-12" style="float: left">
            <div class="line-separator"></div>
          </div>
        </div>
      <?php endif;?>
    <?php endforeach; ?>


    <div class="more-filters">
      <h3><?php print t('More Filters'); ?></h3>
      <div class="btn-more">
        <div class="toogle-btnmore opn-more">
          <span class="t-mero"><?php // print t('Minimize'); ?></span>
          <span class="sign"><strong>-</strong></span>
        </div>
        <div class="hide-block">
          <?php foreach ($widgets as $id => $widget): ?>
            <?php $count++; ?>
            <div class="col-md-6">
              <div id="<?php print $widget->id; ?>-wrapper" class="row views-exposed-widget views-widget-<?php print $id; ?>" style="margin-bottom:15px;">
                <div class="">
                  <?php if (!empty($widget->label)): ?>
                    <strong><?php print $widget->label; ?></strong>
                  <?php endif; ?>
                  <div class="views-widget dropdown-filter">
                    <?php print $widget->widget; ?>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>




    <?php if (!empty($sort_by)): ?>
      <div class="views-exposed-widget views-widget-sort-by">
        <?php print $sort_by; ?>
      </div>
      <div class="views-exposed-widget views-widget-sort-order">
        <?php print $sort_order; ?>
      </div>
    <?php endif; ?>
    <?php if (!empty($items_per_page)): ?>
      <div class="views-exposed-widget views-widget-per-page">
        <?php print $items_per_page; ?>
      </div>
    <?php endif; ?>
    <?php if (!empty($offset)): ?>
      <div class="views-exposed-widget views-widget-offset">
        <?php print $offset; ?>
      </div>
    <?php endif; ?>
    <div class="views-exposed-widget views-submit-button">
      <?php print $button; ?>
    </div>
    <?php if (!empty($reset_button)): ?>
      <div class="views-exposed-widget views-reset-button">
        <?php print $reset_button; ?>
      </div>
    <?php endif; ?>

        <script>
		
		    var az = jQuery.noConflict();	
			
            az(document).ready(function(){
		
			az("div.row .hide-block").hide();
		
	        });
	        
	        
	        
	        
        
        </script>  
