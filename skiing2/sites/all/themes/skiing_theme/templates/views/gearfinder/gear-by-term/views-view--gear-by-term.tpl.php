<?php
// $Id$
/**
 * @file views-view--buying-guide-types.tpl.php
 * Overridden main view template to add compare functionality
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 * - $admin_links: A rendered list of administrative links
 * - $admin_links_raw: A list of administrative links suitable for theme('links')
 *
 * @ingroup views_templates
 */
?>
<div id="tabbed-nav" class="custom-navs <?php print $classes; ?>">
  <ul>
    <li class="skis-tab"><?php print l(t('Skis'), '/gear/skis')?></li>
    <li class="boots-tab"><?php print l(t('Boots'), '/gear/boots')?></li>
    <li class="apparel-tab"><?php print l(t('Apparel'), '/gear/apparel-and-accessories'); ?></li>
    <li class="how-tab"><?php print l(t('How We Test'), '/node/119588'); ?></li>
  </ul>

  <div>
    <div>
      <?php if ($exposed): ?>
        <div class="filters-container view-filters">
          <?php print $exposed; ?>
        </div>
      <?php endif; ?>

      <?php if ($rows): ?>
        <div class="results view-content" id="gear-items">
          <?php print $rows; ?>
        </div>
      <?php elseif ($empty): ?>
        <div class="view-empty">
          <?php print $empty; ?>
        </div>
      <?php endif; ?>

      <?php if ($pager): ?>
        <?php print $pager; ?>
      <?php endif; ?>

      <?php if ($attachment_after): ?>
        <div class="attachment attachment-after">
          <?php print $attachment_after; ?>
        </div>
      <?php endif; ?>

      <?php if ($more): ?>
        <?php print $more; ?>
      <?php endif; ?>

    </div>
    <div></div>
    <div></div>
  </div>
</div> <?php /* class view */ ?>
<script>
  jQuery(document).ready(function ($) {
    setTimeout(function () {
      $("#tabbed-nav").zozoTabs({
        theme: "silver",
        orientation: "horizontal",
        position: "top-left"
      });
    }, 1000);
  });
</script>
