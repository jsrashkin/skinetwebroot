<div class="gear-image">
  <?php print $fields['field_image']->content; ?>
</div>
<div class="gear-dsc">
  <h5><?php print $fields['title']->content; ?></h5>
  <p class="gear-rating">
    <?php print $fields['field_dek']->content; ?>
  </p>
</div>