<div class="gear-image">
  <?php print $fields['field_image']->content; ?>
</div>
<div class="gear-dsc">
  <h5 class="gear-title"><?php print $fields['title']->content; ?></h5>
  <p class="gear-rating">
    <?php print t('Rating'); ?>: <strong><?php print $fields['field_ski_rating_overall']->content; ?><span> / </span> 5</strong>
  </p>
  <p class="gear-msrp"><?php print t('MSRP'); ?>:<span class="dollar_sign">$</span><?php print $fields['field_product_price']->content; ?></p>
  <div class="compare-check">
    <input type="checkbox" id="check-compare-1" name="check-compare-1">
    <label for="check-compare-1"><?php print t('Compare');?></label>
  </div>
</div>
