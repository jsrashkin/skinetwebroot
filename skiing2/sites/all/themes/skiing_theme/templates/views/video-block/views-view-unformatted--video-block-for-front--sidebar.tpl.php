<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<div class="video-slider">
<?php foreach ($rows as $id => $row): ?>
  <div class="item"><?php print $row; ?></div>
<?php endforeach; ?>
</div>
<script type="text/javascript">
  jQuery(document).ready(function() {

    jQuery(".video-slider").owlCarousel({

      pagination: true,
      navigation : false, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true

      // "singleItem:true" is a shortcut for:
      //items : 3
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false

    });

  });
</script>