<div class="video-content">
  <div class="video-thumb">
    <?php print $fields['field_video_embed_code']->content; ?>
    <div>
      <img src="<?php print base_path() . path_to_theme(); ?>/images/videos/play.png">
    </div>
  </div>
  <p><?php print $fields['created']->content; ?></p>
  <?php print $fields['title']->content; ?>
</div>