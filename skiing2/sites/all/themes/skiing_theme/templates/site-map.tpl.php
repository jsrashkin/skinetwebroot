<?php
/**
 * @file
 * Theme implementation to display the site map.
 *
 * Available variables:
 * - $message:
 * - $rss_legend:
 * - $front_page:
 * - $blogs:
 * - $books:
 * - $menus:
 * - $faq:
 * - $taxonomys:
 * - $additional:
 *
 * @see template_preprocess()
 * @see template_preprocess_site_map()
 */
?>

<div class="sitemap-container">
  <div class="row">
    <?php if ($message): ?>
      <?php print $message; ?>
    <?php endif; ?>

    <?php if ($rss_legend): ?>
      <?php print $rss_legend; ?>
    <?php endif; ?>

    <?php if ($front_page): ?>
      <?php print $front_page; ?>
    <?php endif; ?>

    <?php if ($blogs): ?>
      <?php print $blogs; ?>
    <?php endif; ?>

    <?php if ($books): ?>
      <?php print $books; ?>
    <?php endif; ?>

    <?php if ($menus): ?>
      <?php print $menus; ?>
    <?php endif; ?>

    <?php if ($faq): ?>
      <?php print $faq; ?>
    <?php endif; ?>

    <?php if ($taxonomys): ?>
      <?php print $taxonomys; ?>
    <?php endif; ?>

    <?php if ($additional): ?>
      <?php print $additional; ?>
    <?php endif; ?>
</div>
</div>
