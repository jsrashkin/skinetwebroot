<?php

require 'AMS_GetSiteRegistrationByCustomer.php';

/**
 * Download Profile Image from Audience Management Service
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 * @package AMS
 */
final class AMS_GetProfileImage extends AMS_Object
{
    /**
     * The AMS Customer Id
     *
     * @var int
     */
    private $customerId;
    
    /**
     * Binary Contents of the Image File
     *
     * @var string
     */
    private $fileContents;
    
    /**
     * Displays the Image Type
     *
     * @var array
     */
    private $imageTypes;
    
    /**
     * Image Extensions
     *
     * @var array
     */
    private $imageExtensions;
    
    /**
     * Image MimeType Id
     *
     * @var int
     */
    private $imageMimeTypeId;
    
    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        
        // (1 = GIF, 2 = JPG, 3 = BMP, 5 = PNG)
        $this->imageTypes = array
        (
            0 => 'image/gif',
            1 => 'image/gif',
            2 => 'image/jpeg',
            3 => 'image/bmp',
            5 => 'image/png',      
        );
        
        // (1 = GIF, 2 = JPG, 3 = BMP, 5 = PNG)
        $this->imageExtensions = array
        (
            0 => 'gif',
            1 => 'gif',
            2 => 'jpg',
            3 => 'bmp',
            5 => 'png',      
        );
        
        $this->customerId = 0;
        
        $this->fileContents = null;
        
        $this->imageMimeTypeId = 0;
    }
    
    /**
     * Destructor
     *
     */
    public function __destruct()
    {
        if (true == AMS_PROFILE_PICTURE_DISPLAY_IN_DEBUG_MODE)
        {
            $message = "Request Made at " . date('Y-m-d H:i:s') . "\n";
            
            $message .= print_r($this->getErrorArray(), true) . "\n\n";
            
            error_log($message, 3, AMS_PROFILE_PICTURE_DISPLAY_LOGS);
            
            return;
        }
    }
    
    /**
     * Sets the AMS Customer Id
     *
     * @param int $customerId
     * @return bool
     */
    public function setCustomerId($customerId)
    {
        
        $this->customerId = (int) $customerId;
        
        if (0 == $this->customerId)
        {
            $this->addError(300, "Invalid Customer ID");
            
            return false;
        }
        
        return true;
    }
    
    /**
     * Returns the File Contents
     *
     * @return unknown
     */
    public function &getImageContents()
    {
        return $this->fileContents;
    }
    
    public function &getImageType()
    {
        return $this->imageMimeTypeId;
    }
    
    /**
     * Downloads the Image Data from AMS
     *
     * @return bool
     */
    public function downloadProfileImage()
    {
        $GetProfileImage = true;
        
        $AMS_Operation = new AMS_GetSiteRegistrationByCustomer();
        
        // Load the XML Object to be transmitted to AMS
        $AMS_Operation->load($this->customerId, AMS_DOMAIN_ID, $GetProfileImage);
        
        // Make the call to AMS
        $operationStatus = $AMS_Operation->execute();
        
        if (false === $operationStatus)
        {
            $this->addErrorArray($AMS_Operation->getErrorArray, false);
            
            $this->addError(301, "Error while making call to AMS");
            
            return false;
        }
        
        // Retrieve the response as an AMS_XMLObject
        $data = $AMS_Operation->getResponseNode();
        
        if (!method_exists($data->SiteRegistration->image, 'attributes'))
        {
            $this->addError(302, "Attributes() method could not be found on SiteRegistration->image property");
            
            return false;
        }
        
        $imageContents = (string) $data->SiteRegistration->image;
        
        $imageType     = (isset($data->SiteRegistration->image->attributes()->imageType) ? $data->SiteRegistration->image->attributes()->imageType : false);

        if (strlen($imageContents) < 5)
        {
            $this->addError(303, "Invalid Profile Image Contents");
            
            return false;
        }
        
        if (false == $imageType)
        {
            $this->addError(304, "Invalid Image Mime Type");
            
            return false;
        }
        
        $this->fileContents    = base64_decode($imageContents);
        
        $this->imageMimeTypeId = (int) $imageType;
    }
    
    public function displayImage()
    {
        if ($this->isError())
        {
            $this->fileContents = file_get_contents(AMS_PROFILE_PICTURE_DEFAULT_IMAGE);
            
            $this->imageMimeTypeId = 1;
        }
        
        // Gets a timestamp 18 years ago (Far enough)
        $no_cache_exp_gmt = gmdate("D, d M Y H:i:s", (time() - (3600 * 24 * 365 * 18) ) ) . " GMT";
        
        $filename_hash = sha1( 'file_name' . $this->customerId);
        
        $filename_hash = substr($filename_hash, 0, 8);
        
        // Sets the mime type to be sent to the browser
        header("Content-type: " . $this->imageTypes[$this->imageMimeTypeId]);
        header("Content-disposition: attachment; filename=" . $filename_hash . '.' . $this->imageExtensions[$this->imageMimeTypeId]);
                
        header("Expires: " . $no_cache_exp_gmt);
        header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1
        header("Cache-Control: post-check=0, pre-check=0", false);     // POST Checks
        header("Pragma: no-cache");
        
        print $this->fileContents;
        
        return true;     
    }
}
