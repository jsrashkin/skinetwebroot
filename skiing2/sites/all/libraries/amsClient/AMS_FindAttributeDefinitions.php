<?php

require_once('AMS_Operation.php');

/**
 * AMS_FindAttributeDefinitions
 *
 * This class searches for attribute definitions on a specified domain
 * 
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_FindAttributeDefinitions extends AMS_Operation
{
    /**
     * Loads the XML Data
     *
     * @param string $Name
     * @param int $Type
     * @param string $Description
     * @param int $DomainId
     */
    public function load($Name = null, $Type= null, $Description = null, $DomainId = null)
    {
        $this->setRequestNode('FindAttributeDefinitions', null, AMS_SERVICE_NAMESPACE);
        
        $AttributeDefinitions = new DOMElement('AttributeDefinitions');
        $AttributeDefinition  = new DOMElement('AttributeDefinition');
        
        $this->requestNode->appendChild($AttributeDefinitions);
        
        $AttributeDefinitions->appendChild($AttributeDefinition);
        
        if (!is_null($Name))
        {
            $AttributeDefinition->appendChild(new DOMElement('Name', $Name));
        }
        
        if (!is_null($Type))
        {
            $AttributeDefinition->appendChild(new DOMElement('Type', $Type));
        }
        
        if (!is_null($Description))
        {
            $AttributeDefinition->appendChild(new DOMElement('Description', $Description));
        }
        
        if (!is_null($DomainId))
        {
           $AttributeDefinition->appendChild(new DOMElement('DomainId', intval($DomainId))); 
        }
    }
}