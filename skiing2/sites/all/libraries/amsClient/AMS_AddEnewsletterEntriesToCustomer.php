<?php

require_once('AMS_Operation.php');

/**
 * AMS_AddEnewsletterEntriesToCustomer
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_AddEnewsletterEntriesToCustomer extends AMS_Operation
{
	
	private $CustomerId = 0;
	
	/**
	 * Customer XML Data
	 *
	 * @var DOMElement
	 */
	private $Customer;
	
	/**
	 * Subscriptions XML Data
	 *
	 * @var DOMElement
	 */
	private $Subscriptions;
	
	/**
	 * Attributes of the Customer Node
	 *
	 * @var DOMElement
	 */
	private $CustomerAttributes;
	
	/**
	 * Binds the data to be sent to AMS
	 *
	 * Needs the CustomerID, DomainId and Email Address of this customer
	 * 
	 * @return void
	 */
	public function load($CustomerId, $DomainId, $EmailAddress)
	{
		$this->setRequestNode('AddEnewsletterEntriesToCustomer', null, AMS_SERVICE_NAMESPACE);
		
		$this->Customer		 =& new DOMElement('Customer');		   
		$this->Subscriptions =& new DOMElement('Subscriptions');
		
		$this->requestNode->appendChild($this->Customer);
		$this->requestNode->appendChild(new DOMElement('domainId', $DomainId));
		$this->requestNode->appendChild($this->Subscriptions);
		
		$this->Customer->setAttribute('customerId', $CustomerId);
		$this->Customer->appendChild(new DOMElement('EmailAddress', $EmailAddress));
		
		$this->CustomerAttributes = new DOMElement('attributes');

		$this->CustomerId = $CustomerId;
	}
	
	/**
	 * Registers the Customer data
	 *
	 * @param string $FirstName
	 * @param string $LastName
	 * @param string $Address1
	 * @param string $Address2
	 * @param string $City
	 * @param string $State
	 * @param string $PostCode
	 * @param string $Country
	 * @param string $PhoneNumber
	 * @param string $WorkPhoneNumber
	 * @param string $FaxNumber
	 * @param string $MobilePhoneNumber
	 * @param string $DateOfBirth
	 * @param string $CompanyName
	 * @param string $Title
	 * @param string $WebSite
	 * @return bool
	 */
	public function loadCustomer($FirstName, $LastName, $Address1 = null, $Address2 = null, 
						 $City = null, $State = null, $PostCode = null,
						 $Country = null, $PhoneNumber = null, $WorkPhoneNumber = null,
						 $FaxNumber = null, $MobilePhoneNumber = null, 
						 $DateOfBirth = null, $CompanyName = null, 
						 $Title = null, $WebSite = null)
	{
		$this->Customer->appendChild(new DOMElement('FirstName', $FirstName));
		
		$this->Customer->appendChild(new DOMElement('LastName', $LastName));
		
		if ($Address1)
		{
			$this->Customer->appendChild(new DOMElement('Address1', $Address1));
		}
		
		if ($Address2)
		{
			$this->Customer->appendChild(new DOMElement('Address2', $Address2));
		}
		
		if ($City)
		{
			$this->Customer->appendChild(new DOMElement('City', $City));
		}
		
		if ($State)
		{
			$this->Customer->appendChild(new DOMElement('State', $State));
		}
		
		if ($PostCode)
		{
			$this->Customer->appendChild(new DOMElement('PostCode', $PostCode));
		}
		
		if ($Country)
		{
			$this->Customer->appendChild(new DOMElement('Country', $Country));
		}
		
		if ($PhoneNumber)
		{
			$this->Customer->appendChild(new DOMElement('PhoneNumber', $PhoneNumber));
		}
		
		if ($WorkPhoneNumber)
		{
			$this->Customer->appendChild(new DOMElement('WorkPhoneNumber', $WorkPhoneNumber));
		}
		
		if ($FaxNumber)
		{
			$this->Customer->appendChild(new DOMElement('FaxNumber', $FaxNumber));
		}
		
		if ($MobilePhoneNumber)
		{
			$this->Customer->appendChild(new DOMElement('MobilePhoneNumber', $MobilePhoneNumber));
		}
		
		if ($DateOfBirth)
		{
			$this->Customer->appendChild(new DOMElement('DateOfBirth', $DateOfBirth));
		}
		/* Date of birth is optional according to AMS. We shouldn't be setting a bogus value.
		 * - Kevin Hallmark 06/15/2009
		else 
		{
			$this->Customer->appendChild(new DOMElement('DateOfBirth', '1970-01-01'));
		}*/
				
		if ($CompanyName)
		{
			$this->Customer->appendChild(new DOMElement('CompanyName', $CompanyName));
		}
		
		if ($Title)
		{
			$this->Customer->appendChild(new DOMElement('Title', $Title));
		}
		
		if ($WebSite)
		{
			$this->Customer->appendChild(new DOMElement('WebSite', $WebSite));
		}
		
		$this->Customer->appendChild(new DOMElement('status', null));
		
		$this->Customer->appendChild($this->CustomerAttributes);
		
		
		return true;
	}
	/**
	 * Adds a subscription entry for this customer
	 * 
	 * This method can be called multiple times for each Enewsletter Id
	 *
	 * @param int $EnewsletterId
	 * @param string $EmailFormat HTML or PLAIN_TEXT
	 * @param int $Status
	 * @param dateTime $OptInDate Timestamp used to create YYYY-MM-DD HH:MM:SS
	 * @param string $OptInSrc
	 * @param string $OptInQualifier
	 * @param dateTime $OptOutDate YYYY-MM-DD HH:MM:SS
	 * @param string $OptOutReason
	 */
	public function addSubscription($EnewsletterId, $EmailFormat = null, $Status = null, 
									$OptInDate = null, $OptInSrc = null, $OptInQualifier = null,
									$OptOutDate = null, $OptOutReason = null, $RemoteHost = null)
	{
		$Subscription = new DOMElement('Subscription');
		
		$this->Subscriptions->appendChild($Subscription);
		
		$Subscription->appendChild(new DOMElement('EnewsletterId', $EnewsletterId));
		
		if (!is_null($EmailFormat))
		{
			$Subscription->appendChild(new DOMElement('EmailFormat', $EmailFormat));
		}
		
		if (!is_null($Status))
		{
			$Subscription->appendChild(new DOMElement('Status', $Status));
		}
		
		if (!is_null($OptInDate))
		{
			$Subscription->appendChild(new DOMElement('OptInDate', date('c', strtotime($OptInDate))));
		}
		
		if (!is_null($OptInSrc))
		{
			$Subscription->appendChild(new DOMElement('OptInSrc', intval($OptInSrc)));
		}
		
		if (!is_null($OptInQualifier))
		{
			$Subscription->appendChild(new DOMElement('OptInQualifier', $OptInQualifier));
		}
		
		if (!is_null($OptOutDate))
		{
			$Subscription->appendChild(new DOMElement('OptOutDate', date('c', strtotime($OptOutDate))));
		}
		
		if (!is_null($OptOutReason))
		{
			$Subscription->appendChild(new DOMElement('OptOutReason', $OptOutReason));
		}
    if (is_null($RemoteHost))
    {
        $RemoteHost = ams_customer_ip_address();
    }
    $Subscription->appendChild(new DOMElement('RemoteHost', $RemoteHost));

	}
	
	/**
	 * Adds an attribute
	 *
	 * Must only be called after load() has been called
	 * 
	 * @param int $attributeId
	 * @param string $attributeValue
	 * @return bool
	 */
	public function addCustomerAttribute($attributeId, $attributeValue)
	{
		$attribute = new DOMElement('attribute');		 
		$this->CustomerAttributes->appendChild($attribute);
		
		$attribute->appendChild(new DOMElement('attributeId', $attributeId));
		
		// Since the ratio of attributes to customer is one to one
		// This will always be 1 for now.
		$attribute->appendChild(new DOMElement('instance', 1)); 
		$attribute->appendChild(new DOMElement('value', $attributeValue));
		
		return true;
	}
}
