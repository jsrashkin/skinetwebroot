<?php


require_once('AMS_Operation.php');

/**
 * AMS_FindSiteRegistrations
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_FindSiteRegistrations extends AMS_Operation
{
   
    /**
     * Site Registration Element
     *
     * @var DOMElement
     */
    private $SiteRegistration;
    
    /**
     * Loads the DATA to be sent to AMS
     *
     * @param int $StartIndex Index to start
     * @param int $EndIndex Index to end
     * @param bool $GetImage
     */
    public function load($StartIndex, $EndIndex, $GetImage = false)
    {
        $this->setRequestNode('FindSiteRegistrations', null, AMS_SERVICE_NAMESPACE);
        
        $GetImageValue = ( ($GetImage) ? 'true' : 'false' );
        
        $this->SiteRegistration = new DOMElement('SiteRegistration');
        
        $this->requestNode->appendChild($this->SiteRegistration);
       
        $this->requestNode->appendChild(new DOMElement('GetImage', $GetImageValue));
        $this->requestNode->appendChild(new DOMElement('Start', $StartIndex));
        $this->requestNode->appendChild(new DOMElement('End', $EndIndex));
        
        $this->SiteRegistration->appendChild(new DOMElement('domainId', AMS_DOMAIN_ID));
    }
}