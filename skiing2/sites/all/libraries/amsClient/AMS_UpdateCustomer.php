<?php

require_once('AMS_Operation.php');

/**
 * AMS_AddSiteRegistration
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_UpdateCustomer extends AMS_Operation
{
    /**
     * Customer Node
     *
     * @var DOMElement
     */
    private $Customer;
    
    /**
     * Attributes of the Customer Node
     *
     * @var DOMElement
     */
    private $CustomerAttributes;
    
    /**
     * Domain
     *
     * @var int
     */
    private $DomainId = 0;
    
    /**
     * Customer Identification Number
     *
     * @var int
     */
    private $CustomerId = 0;
    
    /**
     * Initializer
     *
     * @param int $DomainId
     */
    public function load($DomainId, $CustomerId)
    {
        $this->DomainId = $DomainId;
        
        $this->CustomerId = $CustomerId;
        
        $this->Customer = new DOMElement('Customer');
        
        $this->CustomerAttributes = new DOMElement('attributes');
        
        $this->setRequestNode('UpdateCustomer', null, AMS_SERVICE_NAMESPACE);
        
        $this->requestNode->appendChild($this->Customer);
    }
    
    /**
     * Registers the Customer data
     *
     * @param string $FirstName
     * @param string $LastName
     * @param string $EmailAddress
     * @param string $Address1
     * @param string $Address2
     * @param string $City
     * @param string $State
     * @param string $PostCode
     * @param string $Country
     * @param string $PhoneNumber
     * @param string $WorkPhoneNumber
     * @param string $FaxNumber
     * @param string $MobilePhoneNumber
     * @param string $DateOfBirth
     * @param string $CompanyName
     * @param string $Title
     * @param string $WebSite
     * @param string $status
     * @return bool
     */
    public function loadCustomer($FirstName = null, $LastName = null,
                         $EmailAddress = null, $Address1 = null, $Address2 = null, 
                         $City = null, $State = null, $PostCode = null, 
                         $Country = null, $PhoneNumber = null, $WorkPhoneNumber = null,
                         $FaxNumber = null, $MobilePhoneNumber = null, 
                         $DateOfBirth = null, $CompanyName = null, 
                         $Title = null, $WebSite = null, $status = false)
    {
        $this->Customer->setAttribute('customerId', $this->CustomerId);
        
        //$this->Customer->appendChild(new DOMElement('password', $FirstName));
        
        if ($FirstName)
        {
            $this->Customer->appendChild(new DOMElement('FirstName', $FirstName));
        }
        
        if ($LastName)
        {
            $this->Customer->appendChild(new DOMElement('LastName', $LastName));
        }
        
        if ($Address1)
        {
            $this->Customer->appendChild(new DOMElement('Address1', $Address1));
        }
        
        if ($Address2)
        {
            $this->Customer->appendChild(new DOMElement('Address2', $Address2));
        }
        
        if ($City)
        {
            $this->Customer->appendChild(new DOMElement('City', $City));
        }
        
       /* commented out conditional for state field
        * If country other than US/Canada is chosen, it hides state field
        * If state field was previously populated, it never got updated
       */
       // if ($State) 
       // {
            $this->Customer->appendChild(new DOMElement('State', $State));
       // }
        
        if ($PostCode)
        {
            $this->Customer->appendChild(new DOMElement('PostCode', $PostCode));
        }
        
        if ($Country)
        {
            $this->Customer->appendChild(new DOMElement('Country', $Country));
        }
        
        if ($EmailAddress)
        {
            $this->Customer->appendChild(new DOMElement('EmailAddress', $EmailAddress));
        }
        
        if ($PhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('PhoneNumber', $PhoneNumber));
        }
        
        if ($WorkPhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('WorkPhoneNumber', $WorkPhoneNumber));
        }
        
        if ($FaxNumber)
        {
            $this->Customer->appendChild(new DOMElement('FaxNumber', $FaxNumber));
        }
        
        if ($MobilePhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('MobilePhoneNumber', $MobilePhoneNumber));
        }
        
        if ($DateOfBirth)
        {
            $this->Customer->appendChild(new DOMElement('DateOfBirth', date('c', $DateOfBirth)));
            
        }/*
        else 
        {
            
            $this->Customer->appendChild(new DOMElement('DateOfBirth', date('c', '1970-01-01')));
        }*/
                
        if ($CompanyName)
        {
            $this->Customer->appendChild(new DOMElement('CompanyName', $CompanyName));
        }
        
        if ($Title)
        {
            $this->Customer->appendChild(new DOMElement('Title', $Title));
        }
        
        if ($WebSite)
        {
            $this->Customer->appendChild(new DOMElement('WebSite', $WebSite));
        }
        
        if ($status !== false)
        {
            $this->Customer->appendChild(new DOMElement('status', $status));
        }
        
        //$this->Customer->appendChild($this->CustomerAttributes);
        
        return true;
    }
    
    /**
     * Adds an attribute
     *
     * Must only be called after loadCustomer() has been called
     * 
     * @param int $attributeId
     * @param string $attributeValue
     * @return bool
     */
    public function updateAttribute($attributeId, $attributeValue)
    {
        $attribute = new DOMElement('attribute');        
        $this->CustomerAttributes->appendChild($attribute);
        
        $attribute->appendChild(new DOMElement('attributeId', $attributeId));
        $attribute->appendChild(new DOMElement('instance', 1));
        $attribute->appendChild(new DOMElement('value', $attributeValue));
        
        return true;
    }
}
