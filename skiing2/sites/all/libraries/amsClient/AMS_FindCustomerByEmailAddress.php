<?php


require_once('AMS_Operation.php');

/**
 * AMS_FindSiteRegistrationsByCustomerIds
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_FindCustomerByEmailAddress extends AMS_Operation
{
    
    /**
     * Loads the XML Request Data
     *
     * @param array $CustomerIds
     * @param int $StartIndex
     * @param int $EndIndex
     * @param bool $GetImage
     */
    public function load( $EmailAddress )
    {
        $this->setRequestNode('FindCustomerByEmailAddress', null, AMS_SERVICE_NAMESPACE);
        
        $EmailElement = new DOMElement('emailAddress', $EmailAddress);
        $this->requestNode->appendChild($EmailElement);
     
    }

	/**
	 * customerId
	 * 
	 * Returns the customerId from the response object on success, or null if doesn't exist
	 *
	 * @return void
	 * @author Kevin Hallmark
	 */
	public function getCustomerId() {
		$respXML = $this->getResponseXMLObject();
		
		if($respXML === null || $this->isSoapFault() === true) {
			return null;
		}
		
		//dpm($respXML);
		$customerId = null;
		
		$resp_attr = $respXML->Body->CustomerOutput->attributes();
		$found = (string)$resp_attr['found'];
		if($found == 'true') {
			$customer = $respXML->Body->CustomerOutput->Customer;
			$attr = $customer->attributes();
			$customerId = (string)$attr["customerId"];	
		}
		
		return $customerId;
	}

  public function getCustomer() {
    $customer = array();
    $respXML = $this->getResponseXMLObject();

		if($respXML === null || $this->isSoapFault() === true) {
			return null;
		}

		//dpm($respXML);

		$resp_attr = $respXML->Body->CustomerOutput->attributes();
		$found = (string)$resp_attr['found'];
    $customer = array();
		if($found == 'true') {
			$customer = (array) $respXML->Body->CustomerOutput->Customer;
      $customerId = array_pop($customer['@attributes']);
      // Get rid of attributes
      $attributes = (array) array_pop($customer);
      $customer['attributes'] = array();
      if ($attributes['attribute']) {
        foreach ($attributes['attribute'] as $attribute) {
          $attribute = (array) $attribute;
          if ($attribute['value']) {
            $customer['attributes'][$attribute['attributeId']] = $attribute['value'];
          }
        }
      }
      // Get rid of customer attr
      $customerid_attr = (array) array_shift($customer);
      $customer['customerId'] = $customerId;
		}

		return $customer;
  }
}
