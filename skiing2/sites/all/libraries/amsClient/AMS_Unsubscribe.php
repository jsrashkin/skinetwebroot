<?php


require_once('AMS_Operation.php');

/**
 * AMS_Unsubscribe
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_Unsubscribe extends AMS_Operation
{
    /**
     * Binds the data to be sent to AMS
     *
     * @param int $SubscriptionId
     */
    public function load($SubscriptionId)
    {
        $this->setRequestNode('Unsubscribe', null, AMS_SERVICE_NAMESPACE);
        
        $this->requestNode->appendChild(new DOMElement('SubscriptionId', $SubscriptionId));
    }
}