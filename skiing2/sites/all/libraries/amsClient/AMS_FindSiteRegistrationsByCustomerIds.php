<?php


require_once('AMS_Operation.php');

/**
 * AMS_FindSiteRegistrationsByCustomerIds
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_FindSiteRegistrationsByCustomerIds extends AMS_Operation
{
    
    /**
     * Loads the XML Request Data
     *
     * @param array $CustomerIds
     * @param int $StartIndex
     * @param int $EndIndex
     * @param bool $GetImage
     */
    public function load(array $CustomerIds, $StartIndex, $EndIndex, $GetImage = false)
    {
        $this->setRequestNode('FindSiteRegistrationsByCustomerIds', null, AMS_SERVICE_NAMESPACE);
        
        $CustomerIdsElement = new DOMElement('CustomerIds');
        
        $GetImageValue = ( ($GetImage) ? 'true' : 'false' );
        
        $this->requestNode->appendChild(new DOMElement('DomainId', AMS_DOMAIN_ID));
        $this->requestNode->appendChild($CustomerIdsElement);
        $this->requestNode->appendChild(new DOMElement('GetImage', $GetImageValue));
        $this->requestNode->appendChild(new DOMElement('Start', $StartIndex));
        $this->requestNode->appendChild(new DOMElement('End', $EndIndex));
        
        for ($i = 0; $i < sizeof($CustomerIds); $i++)
        {
            $CustomerId = (int) $CustomerIds[$i];
            
            $CustomerIdsElement->appendChild(new DOMElement('CustomerId', $CustomerId));          
        }        
    }
}