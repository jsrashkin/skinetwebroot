<?php

require_once('AMS_Operation.php');

/**
 * AMS_Adds Attributes to a Customer
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_AddAttributesToCustomer extends AMS_Operation
{    
    /**
     * Attributes of the Customer Node
     *
     * @var DOMElement
     */
    private $CustomerAttributes;
    
    
	/**
	 * Initializer
	 *
	 * @param int $CustomerId
	 */
	public function load($CustomerId, $updateIfExists = false)
	{
		$updateIfExists = "false";
		if($updateIfExists == true) {
			$updateIfExists = "true";
		}
		
		$this->CustomerAttributes = new DOMElement('Attributes');
		
		$this->setRequestNode('AddAttributesToCustomer', null, AMS_SERVICE_NAMESPACE);
		
		$this->requestNode->appendChild(new DOMElement('CustomerId', $CustomerId));
		$this->requestNode->appendChild($this->CustomerAttributes);
		$this->requestNode->setAttribute('UpdateIfExists', $updateIfExists);
	}
    
    /**
     * Adds an attribute
     *
     * Must only be called after load() has been called
     * 
     * @param int $attributeId
     * @param string $attributeValue
     * @return bool
     */
    public function addCustomerAttribute($attributeId, $attributeValue, $attributeInstance = 1)
    {
        $attribute = new DOMElement('attribute');        
        $this->CustomerAttributes->appendChild($attribute);
        
        $attribute->appendChild(new DOMElement('attributeId', $attributeId));
        
        // Since the ratio of attributes to customer is one to one
        // This will always be 1 for now.
        $attribute->appendChild(new DOMElement('instance', $attributeInstance)); 
        $attribute->appendChild(new DOMElement('value', $attributeValue));
        
        return true;
    }

	public function getAttributeDataAsArray() {
		//var_dump($this->getResponseXMLString());
		//die;
	}
}