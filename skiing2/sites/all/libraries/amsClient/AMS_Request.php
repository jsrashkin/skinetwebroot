<?php

/**
 * Loads the defined constants
 * 
 * @name AMS_Configuration_Values
 */

/**
 * Loads the AMS_Object class
 */
require_once('AMS_Object.php');
// This should never be modified. Please do not change this constant
define('AMS_SERVICE_NAMESPACE', 'http://ams.webhub.bonniercorp.com/facade/schema');
/**
 * Audience Management Services
 * 
 * AMS_Request Object
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
class AMS_Request extends AMS_Object
{
    /**
     * Current version of the library
     *
     * @var string
     */
    const LIBRARY_VERSION       = '2.0';
    
    /**
     * Carriage Return and Line Feed
     *
     * @var string
     */
    const HTTP_CRLF             = "\r\n";
    
    /**
     * Line Feed Character
     *
     * @var string
     */
    const HTTP_LF               = "\n";
    
    /**
     * HTTP Protocol Version
     *
     * @var string
     */
    const HTTP_VERSION          = 'HTTP/1.0';
    
    /**
     * HTTP Request Method
     *
     * @var string
     */
    const HTTP_REQUEST_METHOD   = 'POST';
    
    /**
     * Request Content Type
     *
     * @var string
     */
    const HTTP_CONTENT_TYPE     = 'text/xml; charset=UTF-8';
    
    /**
     * Acceptable response mime-types
     *
     * @var string
     */
    const HTTP_CONTENT_ACCEPT   = 'text/xml,application/xml; charset=UTF-8';
    
    /**
     * Name of the User-Agent
     *
     * @var string
     */
    const HTTP_USER_AGENT       = 'PHP_AMS_Request_Library ';
    
    /**
     * Timeout 
     * 
     * How long we should wait before giving up
     *
     * Recommended value is 30 seconds, especially for slow servers
     * 
     * @var string
     */
    const HTTP_REQUEST_TIMEOUT  = 30;
    
    /**
     * The read length of data from the socket
     *
     * Specific number of bytes to grab in a given chunck.
     * 
     * @var string
     */
    const HTTP_CHUNCK_READ_SIZE = 256;
    
    /**
     * IP Address of the Endpoint
     *
     * @var string
     */
    private $hostName;
    
    /**
     * Port Number of the endpoint
     *
     * @var int
     */
    private $portNumber;
    
    /**
     * Absolute path of the Endpoint
     *
     * @var string
     */
    private $requestPath;
    
    /**
     * AMS Authentication Username
     *
     * @var string
     */
    private $authUsername;
    
    /**
     * AMS Authentication Password
     *
     * @var unknown_type
     */
    private $authPassword;
    
    /**
     * An Array of Request Headers
     *
     * @var array
     */
    private $requestArray;
    
    /**
     * The Socket Request Headers
     *
     * @var string
     */
    private $requestHeader;
    
    /**
     * The Socket Request Body
     *
     * @var string
     */
    private $requestString;
    
    /**
     * The Entire Socket Request
     *
     * This includes the header and the body
     * 
     * @var string
     */
    private $socketRequestContent;

    /**
     * The Entire Socket Response string
     *
     * @var string
     */
    private $responseString;
    
    /**
     * The socket error number
     *
     * @var int
     */
    private $socketErrorNumber;
    
    /**
     * The socket error string
     *
     * @var string
     */
    private $socketErrorString;
    
    /**
     * Is Ready
     * 
     * Whether the packet/payload has been prepared or not
     *
     * @var bool
     */
    private $isReady;
    
    /**
     * The Response headers
     *
     * @var string
     */
    private $responseHeaders;
    
    /**
     * The Response body
     *
     * @var string
     */
    private $responseBody;
    
    /**
     * The HTTP Response code
     *
     * @var int
     */
    private $responseCode;
    
    /**
     * How long did this request take
     *
     * @var float
     */
    private $requestTime;
    
    /**
     * The constructor
     *
     */
    public function __construct()
    {   
		parent::__construct();
		
        // Translate the domain name into a v4 IP Address
        //$this->hostName     = gethostbyname(AMS_SERVICE_HOST);
        $this->hostName = AMS_SERVICE_HOST;
        
        $this->portNumber   = AMS_SERVICE_PORT;
        
        $this->requestPath  = AMS_SERVICE_PATH;
        
        $this->authUsername = AMS_USERNAME;
        
        $this->authPassword = AMS_PASSWORD;
        
        $this->requestArray = array();
        
        $this->requestString  = null;
        
        $this->responseString = null; 
        
        $this->socketRequestContent = null;
        
        $this->socketErrorNumber    = 0;
        
        $this->socketErrorString    = null;
        
        $this->isReady = false; 
        
        $this->responseCode = 0;
        
        $this->responseBody = null;
        
        $this->responseHeaders = null;
        
        $this->requestTime = 0;
              
    }
    
    /**
     * Adds a line item for the request
     *
     * @param string $lineItem
     */
    private function addRequestLine($lineItem)
    {
        $this->requestArray[] = $lineItem;
    }
    
    /**
     * Sets the Request String
     *
     * @param string $requestString
     * @param bool $bind
     */
    public function setRequestString($requestString, $bind = false)
    {
        $this->requestString = trim($requestString);
        
        if ($bind === true)
        {
            $this->bind();
        }
    }
    
    /**
     * Returns the request headers
     *
     * @return string
     */
    public function &getRequestHeaders()
    {
        return $this->requestHeader;
    }
    
    /**
     * Returns the Request String
     *
     * @return string
     */
    public function &getRequestString()
    {
        return $this->requestString;
    }
    
    /**
     * Returns the Raw response string
     *
     * @return string
     */
    public function &getResponseString()
    {
        return $this->responseString;
    }
    
    /**
     * Returns the Raw Socket Request String
     *
     * @return string
     */
    public function &getSocketRequestString()
    {
        return $this->socketRequestContent;
    }
    
    /**
     * Returns the HTTP Response Code
     *
     * @return int
     */
    public function &getResponseCode()
    {
        return $this->responseCode;
    }
    
    /**
     * Returns the response headers
     *
     * @return string
     */
    public function &getResponseHeaders()
    {
        return $this->responseHeaders;
    }
    
    /**
     * Returns the response body
     *
     * @return string
     */
    public function &getResponseBody()
    {
        return $this->responseBody;
    }
    
    /**
     * Returns how long the operation took
     *
     * @return float
     */
    public function &getRequestTime()
    {
        return $this->requestTime;
    }
    
    /**
     * Prepares the Payload
     *
     * @return void
     */
    private function bind()
    {    
        $this->addRequestLine(self::HTTP_REQUEST_METHOD . ' ' . 
                              
                              $this->requestPath . ' ' . self::HTTP_VERSION .
                              
                              self::HTTP_CRLF);
                              
        $this->addRequestLine('Host: ' . $this->hostName . self::HTTP_CRLF);
        
        $this->addRequestLine('Authorization: Basic ' . 
        
                base64_encode($this->authUsername . ':' . $this->authPassword) .
                
                self::HTTP_CRLF);
                              
        $this->addRequestLine('Content-Length: ' . strlen($this->requestString) .
         
                self::HTTP_LF);
        
        $this->addRequestLine('Content-Type: ' . self::HTTP_CONTENT_TYPE .

                self::HTTP_LF);
        
        $this->addRequestLine('Accept: ' . self::HTTP_CONTENT_ACCEPT . 
        
                self::HTTP_LF);
        
        $this->addRequestLine('User-Agent: ' . self::HTTP_USER_AGENT . 
        
                self::LIBRARY_VERSION  . self::HTTP_LF);
        
        $this->addRequestLine('Connection: Close' . self::HTTP_LF . self::HTTP_LF);
        $this->requestHeader = implode('', $this->requestArray);
        
        // Concatenating the request headers with the request body to create the payload
        $this->socketRequestContent = $this->requestHeader . $this->requestString .
                                    
               self::HTTP_LF . self::HTTP_LF ;
        
        // flag that the payload has been prepared     
        $this->isReady = true;
    }
    
    /**
     * Sends the Request
     * 
     * Transmits the raw request string together with the headers
     * to the TCP/IP Socket
     *
     * @return bool
     */
    public function send()
    {
        // If the packet has not been prepared, prepare it
        if (false == $this->isReady)
        {
            $this->bind();
        }
        
        $startTimeStamp = microtime(true);
        
        
        // Create a PHP Socket Connection to the endpoint
        /*
        CANCEL THIS: Do not even try to connect. 
        $socketConnection = @fsockopen($this->hostName, $this->portNumber,
                          
                            $this->socketErrorNumber, $this->socketErrorString,
                            
                            self::HTTP_REQUEST_TIMEOUT);
        */
        
        $stopTimeStamp = microtime(true);
        
        // Set the type to anything other than "stream" so that the next
        // conditional is not met.
        //$socketResourceType = @get_resource_type($socketConnection);
        $socketResourceType = 'do_not_try';
        $this->socketErrorString = sprintf('Did not attempt connection. See line %d in %s.', __LINE__, __FILE__);
        
        if ('stream' !== $socketResourceType)
        {
            $errorString = "Unable to establish a valid socket connection\n" .
            
                           "Socket Error Number: " . $this->socketErrorNumber . "\n" .
                           
                           "Socket Error String: " . $this->socketErrorString;
                           
            $this->addError(800, $errorString);

            $this->requestTime = ($stopTimeStamp - $startTimeStamp);
            
            return false;           
        }
        
        // Write the request to the socket
        $writeStatus = fwrite($socketConnection, $this->socketRequestContent);
        
        $stopTimeStamp = microtime(true);
        
        // If we were unable to write to the socket
        if (!$writeStatus)
        {
            $this->addError(801, "Unable to write to socket");
            
            fclose($socketConnection);
            
            $this->requestTime = ($stopTimeStamp - $startTimeStamp);
            
            return false;
        }
        
        $this->responseString = '';
        
        // While there is still data to read
        while (!feof($socketConnection)) 
        {
            // Grab the response string line by line
        	$this->responseString .= fgets($socketConnection, self::HTTP_CHUNCK_READ_SIZE);            
        }
        
        $stopTimeStamp = microtime(true);
        
        $this->requestTime = ($stopTimeStamp - $startTimeStamp);
        
        fclose($socketConnection);
        
        // Split the response into headers and body
        // Also capture the response code
        $this->parseResponse();
        
        return true;        
    }
    
    /**
     * Parses the response
     * 
     * This method splits the response string into headers and body.
     * 
     * It also captures the HTTP Response code
     * 
     * @return void
     */
    private function parseResponse()
    {
        // Split the response string into 2 parts
        $response_array = explode(self::HTTP_CRLF . self::HTTP_CRLF , $this->responseString, 2);
        
        // Figure out the length of the first part
        $header_first_line_length = strpos($response_array[0], self::HTTP_CRLF);

        // Grab the first line
        $header_first_line = substr($response_array[0], 0, $header_first_line_length);

        // Grabs a 3-digit integer in the $header_first_line variable
        preg_match("/[0-9]{3}/", $header_first_line, $matches);

        // Set the response headers
        $this->responseHeaders = trim($response_array[0]);
        
        // Set the HTTP Response code
        $this->responseCode = (int) $matches[0];

        // Grab the response body
        $this->responseBody = trim($response_array[1]);       
    }
}
