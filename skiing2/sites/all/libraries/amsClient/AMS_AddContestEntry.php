<?php

require_once('AMS_Operation.php');

/**
 * AMS_Adds Contest Entry to a Customer
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */

/*
Example XML:


<AddContestEntryInput xmlns="http://ams.webhub.bonniercorp.com/facade/schema">
    <CustomerId>1234</CustomerId>
    <ContestId>5678</ContestId>
    <EntryDate>2005-03-29T13:24:36.000</EntryDate>
</AddContestEntryInput>

*/

final class AMS_AddContestEntry extends AMS_Operation
{    
	/**
	 * Initializer
	 *
	 * @param int $CustomerId
	 */
	public function load($CustomerId, $ContestId)
	{		
		$this->setRequestNode('AddContestEntry', null, AMS_SERVICE_NAMESPACE);
		
		$this->requestNode->appendChild(new DOMElement('CustomerId', $CustomerId));
		$this->requestNode->appendChild(new DOMElement('ContestId', $ContestId));
		$this->requestNode->appendChild(new DOMElement('EntryDate', date('Y-m-d\TH:i:s.000P')));
		
	}
}