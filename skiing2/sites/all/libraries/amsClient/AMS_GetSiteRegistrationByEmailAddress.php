<?php

require_once('AMS_Operation.php');

/**
 * AMS_GetSiteRegistrationByEmailAddress
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_GetSiteRegistrationByEmailAddress extends AMS_Operation
{
   
    /**
     * Loads the DATA to be sent to AMS
     *
     * @param string $EmailAddress
     * @param int $DomainId
     * @param bool $GetImage
     */
    public function load($EmailAddress, $DomainId, $GetImage = false)
    {
        $this->setRequestNode('GetSiteRegistrationByEmailAddress', null, AMS_SERVICE_NAMESPACE);
        
        $this->setResponseOutputName('SiteRegistration');
        
        $GetImageValue = ( ($GetImage) ? 'true' : 'false' );
        
        $this->requestNode->appendChild(new DOMElement('EmailAddress', $EmailAddress));
        $this->requestNode->appendChild(new DOMElement('DomainId', $DomainId));
        $this->requestNode->appendChild(new DOMElement('GetImage', $GetImageValue));
    }
}