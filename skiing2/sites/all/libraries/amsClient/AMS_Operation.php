<?php

require_once('AMS_Bridge.php');
require_once('AMS_Helper.php');

/**
 * Base Class for Audience Management Services Operations
 *
 * Every child AMS Operations Object must extend this class
 * 
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
abstract class AMS_Operation extends AMS_Bridge
{
    /**
     * Constructor
     *
     * Invokes the parent construct
     * 
     * Child methods should not implement the constructor
     * 
     * If they do, then they have to call this construct to pass on the 
     * chain invokation.
     * 
     * @return AMS_Bridge
     */
    public function __construct()
    {
        parent::__construct();        
    }
    
    /**
     * Destructor
     *
     */
    public function __destruct()
    {
        parent::__destruct();
    }
    
    /**
     * Loads the Data to be sent to AMS
     * 
     * Every child object must implement this method
     *
     * This method must call the setRequestNode() method
     * 
     * Below are the parameters you should pass to the setRequestNode() method
     * 
     * - The first parameter is a string representing the name of the input operation
     * 
     * - The second parameter is the value of the node which is usually null for most operations
     * 
     * - The final parameter is the namespace URI for this node
     * 
     * The parameters for this method load() varies from operation to operations
     * 
     * @return bool
     */
    public function load()
    {
        
    }
}

