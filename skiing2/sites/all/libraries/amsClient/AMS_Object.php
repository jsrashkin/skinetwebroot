<?php

/**
 * Base class for Audience Management Services Tools
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 * @package AMS
 */
abstract class AMS_Object
{
	
	/**
	 * Stores an open log file handle, if logging is enabled.
	 */
	protected $logFileHandle = null;
	
	/**
	 * Tells me if logging is currently enabled
	 */
	protected $loggingEnabled = null;
	
    /**
     * Stores all the errors that occur
     *
     * @var array
     */
    protected $errorArray = array();
    
    /**
     * Constructor
     *
     */
    public function __construct()
    {	
		//Insure no error if not defined
		if(!defined("AMS_LOGGING")) {
			define("AMS_LOGGING", false);
		}
		
		//Enable logging mode and open the file handle
		$this->loggingEnabled = AMS_LOGGING;
		if($this->loggingEnabled) {
			if(!defined("AMS_LOG_FILE")) {
				define("AMS_LOG_FILE", "./ams.log");
			}
			
			$file = fopen(AMS_LOG_FILE, 'a');
			if($file === false) {
				$this->loggingEnabled = false;
			} else {
				$this->logFileHandle = $file;
			}
		}
    }
    
    /**
     * Logs an error that has occured
     *
     * @param int $errorId
     * @param mixed $errorMessage
     * @return bool
     */
    protected function addError($errorCode, $errorMessage)
    {
        if (is_numeric($errorCode))
        {
            $trace = debug_backtrace();
        
            $errorFunction = isset($trace[1]['function']) ? $trace[1]['function'] : '';
            $errorClass    = isset($trace[1]['class']) ? $trace[1]['class'] : '';
            $errorLine     = isset($trace[0]['line']) ? $trace[0]['line'] : ''; 
            
            $calledBy = '';
    
            if (isset($trace[2]))
            {        
                $errorCalledByFunction = isset($trace[2]['function']) ? $trace[2]['function'] : '';
                $errorCalledByClass    = isset($trace[2]['class']) ? $trace[2]['class'] : '';
                $errorCalledByLine     = isset($trace[1]['line']) ? $trace[1]['line'] : '';
                
                $calledBy = "| CALLED BY $errorCalledByClass::$errorCalledByFunction() L:$errorCalledByLine";
            }
            
            $errorMessage  = "[[ERROR IN $errorClass::$errorFunction() L:$errorLine $calledBy]] - ".$errorMessage;
    
            $this->errorArray[$errorCode] = $errorMessage;
    
            return true;           
        }
        
        return false;          
    }
    
    /**
     * Adds an error list to the list of errors
     *
     * @param array $array
     * @param bool $overwrite
     * @return bool
     */
    protected function addErrorArray($array, $overWrite = false)
    {
        if (is_array($array))
        {
        
            if ($overWrite === true)
            {            
                $this->errorArray = $array;            
            }
            else 
            {            
                $this->errorArray = array_merge($this->errorArray, $array);            
            }
            
            return true;        
        }
        
        return false;        
    }

    /**
     * Whether or not an error occured
     *
     * @return bool
     */
    public function isError()
    {
        // If there was an error the array will contain at least one element
        return ( sizeof($this->errorArray) > 0);
    }
    
    /**
     * Returns a list of errors
     *
     * @return array
     */
    public function &getErrorArray()
    {
        return $this->errorArray;
    }
    
    /**
     * Resets all the errors
     *
     */
    public function resetErrors()
    {
        $this->errorArray = array();
    }

	/**
	 * Logs a message to the AMS Log File
	 *
	 * @param string $message The message to be logged
	 * @return void
	 * @author Kevin Hallmark
	 */
	public function logMessage($message = null) {
		if($this->loggingEnabled) {
			fwrite($this->logFileHandle, $message);
		}
	}
	
	public function isLoggingEnabled() {
		return $this->loggingEnabled;
	}
}