<?php


require_once('AMS_Operation.php');

/**
 * AMS_FindSiteRegistrationsByCustomerIds
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_GetCustomer extends AMS_Operation
{
    
    /**
     * Loads the XML Request Data
     *
     * @param array $CustomerIds
     * @param int $StartIndex
     * @param int $EndIndex
     * @param bool $GetImage
     */
    public function load( $CustomerId )
    {
        $this->setRequestNode('GetCustomer', null, AMS_SERVICE_NAMESPACE);
        
        $CustomerIdElement = new DOMElement('customerId', $CustomerId);
        $this->requestNode->appendChild($CustomerIdElement);
     
    }
}