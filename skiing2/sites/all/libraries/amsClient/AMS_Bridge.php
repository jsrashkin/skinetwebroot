<?php

require_once('AMS_Request.php');

require_once('AMS_Object.php');

require_once('AMS_XMLObject.php');

/**
 * Audience Management Services Operations need the methods implemented here
 *
 * This object provides a bridge to communicate with AMS
 * 
 * It has methods that prepares the XML data to be sent and parses the XML
 * response into AMS_XMLObjects.
 * 
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
abstract class AMS_Bridge extends AMS_Object
{
    /**
     * XML Document Instance
     *
     * @var DOMDocument
     */
    protected $document;
    
    /**
     * This will store the main request node
     *
     * @var DOMElement
     */
    protected $requestNode;
    
    /**
     * Envelope of Soap Request
     *
     * @var DOMElement
     */
    protected $soapEnvelope;
    
    /**
     * Body of Soap Request
     *
     * @var DOMElement
     */
    protected $soapBody;
    
    /**
     * AMS Request Object
     *
     * @var AMS_Request
     */
    protected $requestObject;
    
    /**
     * Zend_Http_Response
     *
     * @var Zend_Http_Response
     */
    protected $responseObject;
    
    /**
     * Stores the XML response as a string
     *
     * @var string
     */
    protected $responseXMLString;
    
    /**
     * Raw Response XML String
     *
     * @var string
     */
    protected $rawResponseXMLString;
    
    /**
     * Stores the response as a SimpleXMLElement object
     *
     * @var AMS_XMLObject
     */
    protected $responseXMLObject;
    
    /**
     * Stores the outgoing request string
     *
     * @var string
     */
    protected $requestXMLString;
    
    /**
     * Request XML Object
     *
     * @var AMS_XMLObject
     */
    protected $requestXMLObject;
    
    /**
     * Name of the request node
     *
     * @var string
     */
    protected $requestInputName;
    
    /**
     * Name of the Response node
     *
     * @var string
     */
    protected $responseOuputName;
    
    /**
     * Whether the Payload has been prepared
     *
     * @var bool
     */
    protected $isReady;
    
    /**
     * Constructor for the AMS_Brigde class
     *
     * @return AMS_Brigde
     */
    public function __construct()
    {
		parent::__construct();
        $this->init();
    }
    
    
    
    /**
     * Enter description here...
     *
     */
    public function __destruct()
    {
        
    }    
    
    protected function setResponseName($ElementName)
    {
        $this->responseOuputName = trim($ElementName);        
    }
    
    /**
     * Initializes internal variables
     * 
     * Initializes internal variables in preparation for a request
     *
     */
    protected function init()
    {
        $this->isReady = false;
        
        $this->document = null;
        
        $this->requestObject = null;
        
        $this->document = new DOMDocument('1.0');
        
        $this->document->encoding = 'UTF-8';
        
        $this->document->formatOutput = true;
        
        $this->document->preserveWhiteSpace = false;
        
        $this->setEnvelope();
        
        $this->setBody();
        
        $this->requestObject = new AMS_Request(); 
        
        $this->responseXMLObject = null;
        
        $this->requestXMLObject = null;
        
        $this->responseXMLString = null;
        
        $this->rawResponseXMLString = null;     
    }
    
    /**
	 * Prepares the SOAP Envelope
	 *
	 * @return bool
	 */
    private function setEnvelope()
    {
        $this->document->appendChild(new DOMElement('S:Envelope', null, 'http://schemas.xmlsoap.org/soap/envelope/'));
        
        $this->soapEnvelope = $this->document->firstChild;
        
        return true;
    }
    
    /**
     * Prepares the SOAP Body
     *
     * @return bool
     */
    private function setBody()
    {
        $this->soapEnvelope->appendChild(new DOMElement('S:Body', null, 'http://schemas.xmlsoap.org/soap/envelope/'));
        
        $this->soapBody = $this->soapEnvelope->firstChild;
        
        return true;
    }
    
    public function bind()
    {
         // converts the DOMDocument object to an XML string
        $this->requestXMLString = $this->document->saveXML();
        
        // Sets the request string and prepares the payload
        $this->requestObject->setRequestString($this->requestXMLString, true);
        
        $this->isReady = true;
    }
    
    /**
     * Sends the request to the Webservice
     *
     * @return bool
     */
    public final function execute()
    {
        if ($this->isReady == false)
        {
            $this->bind();
        }
  
        try 
        {
          $this->logMessage("\nStart: " . date('r') . "\n");
          $this->logMessage($this->requestObject->getRequestString());
          $this->logMessage("\nAfter Request: " . date('r') . "\n");
          // Send the request to the socket as an XML post
          $status = $this->requestObject->send();
          $this->logMessage($this->requestObject->getResponseString());
          $this->logMessage("\nAfter Response: " . date('r') . "\n");

            if (false === $status || true == $this->requestObject->isError())
            {
                $this->addErrorArray($this->requestObject->getErrorArray());
                
                $this->addError(410, "Error while sending payload");
                
                return false;
            }
            //print_r($this->requestObject);
            //echo '<br>';
            
            // Finds out what the HTTP Response code is
            $http_status_code = $this->requestObject->getResponseCode();
            // Converts the response into a string if it is not already a string
            $this->responseXMLString = strval($this->requestObject->getResponseBody());
            // Attempting to capture the raw XML response.
            if (strlen($this->responseXMLString))
            {
                // This variable will be used to parse the Response XML
                $rawResponseDocument = new DOMDocument('1.0');
            
                $rawResponseDocument->encoding = 'UTF-8';
            
                $rawResponseDocument->formatOutput = true;
            
                $rawResponseDocument->preserveWhiteSpace = false;
                
                $successfull = false;
                
                $successfull = $rawResponseDocument->loadXML($this->responseXMLString);
                
                // if the XML Document was successfully loaded
                if (true == $successfull)
                {
                    $this->rawResponseXMLString = $rawResponseDocument->saveXML();
                }
            }
            
            // Striping out prefixes that woud cause Simple XML to have errors
            $this->responseXMLString = $this->sanitizeXML($this->responseXMLString);
            // Checking for errors in the response code and response body
            if (403 == $http_status_code)
            {
                $this->addError(100, "A 403 Error has occurred. Forbidden");
                
                return false;
            }
            
            if (404 == $http_status_code)
            {
                $this->addError(101, "A 404 Error has occurred. Requested end point not found");
                
                return false;
            }
            
            if (401 == $http_status_code)
            {
                $this->addError(102, "A 401 Error has occurred. Unauthorized");
                
                return false;
            }
            if (strlen($this->requestXMLString) == 0)
            {
                $this->addError(103, "An empty response was received from the web service");
                
                return false;
            }
            
            if ($this->isSoapFault())
            {
                // Converts the XML string into a SimpleXMLElement object
                $this->responseXMLObject = simplexml_load_string($this->responseXMLString, 'AMS_XMLObject');
               	if('AMS-03.01.02' == $this->responseXMLObject->Body->Fault->detail->CreateCustomerFault->ErrorCode)
            	{
            		$this->addError(1001,'User already exists');
            		return 1001;
            	}
            	//if('ClientAddSiteRegistrationFaultCRM-02.12.10Username already exists in this domain' ==$this->responseXMLString)
              if('AMS-02.12.10' == $this->responseXMLObject->Body->Fault->detail->AddSiteRegistrationFault->ErrorCode)
            	{
            		$this->addError(106,"Username already exits");
            		return 106;
            	}
     			if('Site Registration already exists' == $this->responseXMLString)
     			{
     				$this->addError(108,'Email already in use');
     				return 108;
     			}
                $this->addError(104, "A Soap Fault was encountered\n\n" . $this->responseXMLString);
                
            
                return false;
            }
            
            if (500 == $http_status_code)
            {
                $this->addError(105, "A 500 Error has occurred. Internal Server Error\n\n" . $this->responseXMLString);
                
                return false;
            }        

            $requestXML = $this->requestXMLString;
            
            $requestXML = $this->sanitizeXML($requestXML);
            
            // Converts the response XML string into a SimpleXMLElement object
            $this->responseXMLObject = simplexml_load_string($this->responseXMLString, 'AMS_XMLObject');
            
            $this->requestXMLObject  = simplexml_load_string($requestXML, 'AMS_XMLObject');
                        
        }              
        catch (Exception $e)
        {
            // Logs the exception that has occured in the error array
            $this->addError(107, "An exception was encountered " . print_r($e, true));
            
            return false;
        }
        return true;               
    }
    
    /**
     * Removes namespace prefixes that causes issues
     *
     * This method gets rid of namespace prefixes that could cause problems with simple xml
     * 
     * The string is passed by reference so that a new copy will not be made
     * 
     * @param string $xmlString
     * @return string
     */
    protected function &sanitizeXML(&$xmlString)
    {
        $problemPatterns = array('SOAP-ENV:', 'ns1:');
        
        $cleanXML = str_replace($problemPatterns, '', $xmlString);
        
        $cleanXML = trim($cleanXML);
        
        return $cleanXML;
    }
    
    /**
     * Checks if the response was a SOAP Fault
     *
     * @return bool
     */
    protected function isSoapFault()
	{
		$simplexml = new AMS_XMLObject($this->responseXMLString);
		
		$xpath = $simplexml->xpath('/Envelope/Body/Fault');
		
		if ($xpath) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}
	
	/**
	 * Sets the Primary request node
	 *
	 * @param string $elementName the name of the input operation we wish to invoke (without "Input")
	 * @param string $nodeValue This is usually left as null
	 * @param string $namespaceURI The URI for representing the name space for this node
	 * @return bool
	 */
	protected function setRequestNode($elementName, $nodeValue = null, $namespaceURI = null)
	{
	    $this->requestInputName  = $elementName . 'Input';
	    
	    $this->responseOuputName = $elementName . 'Output';
	    
	    $this->requestNode = new DOMElement($this->requestInputName, $nodeValue, $namespaceURI);
	    
	    $this->soapBody->appendChild($this->requestNode);
	    
	    return true;
	}
	
	/**
	 * Sets the Response output element name
	 *
	 * @param string $elementName
	 * @param bool $useAsIs
	 */
	public function setResponseOutputName($elementName, $useAsIs = false)
	{
	    $this->responseOuputName = $elementName . 'Output';
	    
	    if ($useAsIs)
	    {
	        $this->responseOuputName = $elementName;
	    }
	}
    
    /**
     * Returns the Response XML Object
     *
     * @return AMS_XMLObject
     */
    public function &getResponseXMLObject()
    {
        return $this->responseXMLObject;
    }
    
    /**
     * Returns the processed Response String
     *
     * This is the XML response back from the webservice
     * @return string
     */
    public function &getResponseXMLString()
    {
        return $this->responseXMLString;
    }
    
    /**
     * Returns the Raw Response String
     *
     * This is the XML response back from the webservice
     * @return string
     */
    public function &getRawResponseXMLString()
    {
        return $this->rawResponseXMLString;
    }
    
    /**
     * Returns the Request String
     *
     * This is the XML document that was submitted to the webservice
     * @return string
     */
    public function &getRequestXMLString()
    {
        return $this->requestXMLString;
    }
    
    /**
     * Returns the XML Object
     *
     * @return AMS_XMLObject
     */
    public function &getRequestXMLObject()
    {
        return $this->requestXMLObject;
    }
    
    /**
     * Returns the response body
     *
     * @return AMS_XMLObject
     */
    public function &getResponseBody()
    {
        if (isset($this->responseXMLObject->Body))
        {
            return $this->responseXMLObject->Body;
        }
        
        $null = null;
        
        return $null;
    }
    
    /**
     * Returns the target response node
     *
     * @return AMS_XMLObject
     */
    public function &getResponseNode()
    {
        $targetNodeName = $this->responseOuputName;
        
        if (isset($this->responseXMLObject->Body->$targetNodeName))
        {
            return $this->responseXMLObject->Body->$targetNodeName;
        }
        
        $null = null;
        
        return $null;
    }
    
    /**
     * Retrieves the Request Headers
     *
     * Used during debugging
     * 
     * @return string
     */
    public function &getRequestHeaders()
    {
        return $this->requestObject->getRequestHeaders();
    }
    
    /**
     * Retrieves the XML Payload
     *
     * Used during debugging
     * 
     * @return string
     */
    public function &getSocketPayload()
    {
        return $this->requestObject->getSocketRequestString();
    }
    
    /**
     * Retrieves the raw socket response
     * 
     * Used during debugging
     *
     * @return string
     */
    public function &getSocketResponse()
    {
        return $this->requestObject->getResponseString();
    }
    
    /**
     * Retrieves the response headers
     *
     * @return string
     */
    public function &getResponseHeaders()
    {
        return $this->requestObject->getResponseHeaders();
    }
    
    /**
     * Retrieves how long the request took
     *
     * @return float
     */
    public function &getResponseTime()
    {
        return $this->requestObject->getRequestTime();
    }  
}
