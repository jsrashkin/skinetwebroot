<?php

require_once('AMS_Operation.php');

/**
 * AMS_AddEnewsletterEntriesToCustomer
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_UpdateSiteRegistration extends AMS_Operation
{
    /**
     * Loading the XML Data
     *
     * @param int $registrationId ( required )
     * @param int $customerId ( required )
     * @param string $username ( required )
     * @param string $password
     * @param string $nickname
     * @param datetime $suspendDate YYYY-MM-DD HH:MM:SS
     * @param datetime $allowAccessDate YYYY-MM-DD HH:MM:SS
     * @param bool $active
     * @param string $suspendComments usually ignored
     * @param string $imageFilename absolution path to a file name
     * @param int $imageType  (1 = GIF, 2 = JPG, 3 = BMP, 5 = PNG)
     * @return bool
     */
    public function load($registrationId, $customerId, $username, $password = null, $nickname = null, $suspendDate = null, $allowAccessDate = null, $active = null, $suspendComments = null, $imageFilename = null, $imageType = null)
    { 
        // Register the name of the input operation
        $this->setRequestNode('UpdateSiteRegistration', null, AMS_SERVICE_NAMESPACE);
        
        // Creating the 'SiteRegistration' node
        $SiteRegistration = new DOMElement('SiteRegistration');
        
        // Append this node to the XML so that you can add more to it
        // the 'SiteRegistration' node will be read only until you do so
        $this->requestNode->appendChild($SiteRegistration);
        
        $SiteRegistration->setAttribute('registrationId', $registrationId);
        
        $SiteRegistration->appendChild(new DOMElement('customerId', $customerId));
        
        $SiteRegistration->appendChild(new DOMElement('username', $username));
        
        $SiteRegistration->appendChild(new DOMElement('password', $password));
        
        $SiteRegistration->appendChild(new DOMElement('nickname', $nickname));
        
        $SiteRegistration->appendChild(new DOMElement('createdDate', null));
        
        $SiteRegistration->appendChild(new DOMElement('lastAccessDate', date('c')));
        
        $suspendDate = ($suspendDate) ? date('c', strtotime($suspendDate)) : null;
        
        $SiteRegistration->appendChild(new DOMElement('suspendDate', $suspendDate));
        
        $allowAccessDate = ($allowAccessDate)? date('c', strtotime($allowAccessDate)) : null;
        
        $SiteRegistration->appendChild(new DOMElement('allowAccessDate', $allowAccessDate));
        
        $SiteRegistration->appendChild(new DOMElement('domainId', AMS_DOMAIN_ID));
        
        if (!is_null($active))
        {
            $activeString = ($active) ? 'true' : 'false';
            
            $SiteRegistration->appendChild(new DOMElement('active', $activeString));
        }
        
        $SiteRegistration->appendChild(new DOMElement('suspendComments', $suspendComments));
        
        // If there is an image
        if (strlen($imageFilename) && intval($imageType))
        {            
            if (file_exists($imageFilename))
            {
                if (is_readable($imageFilename))
                {
                    $fileContents = file_get_contents($imageFilename);
           
                    $Image = new DOMElement('image', base64_encode($fileContents)); 
                     
                    $SiteRegistration->appendChild($Image);
                    
                    $Image->setAttribute('imageType', $imageType);
                }
            }
            
        }
        
        return true;
    }
}