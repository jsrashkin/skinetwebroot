<?php
require_once('AMS_Operation.php');

/**
 * AMS_CreateCustomer - This class is very similar to AMS_AddSiteRegistration
 * except this does not require a SiteRegistration node - useful for "anonymous" 
 * newsletter sign-ups
 *
 * @author Bensan George <bensan.george@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */

final class AMS_CreateCustomer extends AMS_Operation
{
    /**
     * 
     *
     */
	public function load($updateIfExists = false)
	{
		if($updateIfExists == true) {
			$updateIfExists = "true";
		} else {
			$updateIfExists = "false";
		}
		
		$this->Customer = new DOMElement('Customer');
		$this->Subscriptions = new DOMElement('Subscriptions');

		$this->setRequestNode('CreateCustomer', null, AMS_SERVICE_NAMESPACE);

		$this->requestNode->appendChild($this->Customer);
		$this->requestNode->appendChild($this->Subscriptions);
		//$this->requestNode->appendChild(new DOMElement('updateIfExists', true));
		$this->requestNode->setAttribute('UpdateIfExists', $updateIfExists);
	}

    /**
     * Registers the Customer data
     *
     * @param string $FirstName
     * @param string $LastName
     * @param string $EmailAddress
     * @param string $Address1
     * @param string $Address2
     * @param string $City
     * @param string $State
     * @param string $PostCode
     * @param string $Country
     * @param string $PhoneNumber
     * @param string $WorkPhoneNumber
     * @param string $FaxNumber
     * @param string $MobilePhoneNumber
     * @param string $DateOfBirth
     * @param string $CompanyName
     * @param string $Title
     * @param string $WebSite
     * @return bool
     */
    public function loadCustomer($FirstName, $LastName,$EmailAddress, $Address1 = null, $Address2 = null, 
                         $City = null, $State = null, $PostCode = null,
                         $Country = null, $PhoneNumber = null, $WorkPhoneNumber = null,
                         $FaxNumber = null, $MobilePhoneNumber = null, 
                         $DateOfBirth = null, $CompanyName = null, 
                         $Title = null, $WebSite = null)
    {
        $this->Customer->appendChild(new DOMElement('FirstName', $FirstName));
        
        $this->Customer->appendChild(new DOMElement('LastName', $LastName));
        
        if ($Address1)
        {
            $this->Customer->appendChild(new DOMElement('Address1', $Address1));
        }
        
        if ($Address2)
        {
            $this->Customer->appendChild(new DOMElement('Address2', $Address2));
        }
        
        if ($City)
        {
            $this->Customer->appendChild(new DOMElement('City', $City));
        }
        
        if ($State)
        {
            $this->Customer->appendChild(new DOMElement('State', $State));
        }
        
        if ($PostCode)
        {
            $this->Customer->appendChild(new DOMElement('PostCode', $PostCode));
        }
        
        if ($Country)
        {
            $this->Customer->appendChild(new DOMElement('Country', $Country));
        }
        
        if ($EmailAddress)
        {
            $this->Customer->appendChild(new DOMElement('EmailAddress', $EmailAddress));
        }
        
        if ($PhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('PhoneNumber', $PhoneNumber));
        }
        
        if ($WorkPhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('WorkPhoneNumber', $WorkPhoneNumber));
        }
        
        if ($FaxNumber)
        {
            $this->Customer->appendChild(new DOMElement('FaxNumber', $FaxNumber));
        }
        
        if ($MobilePhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('MobilePhoneNumber', $MobilePhoneNumber));
        }
        
        if ($DateOfBirth)
        {
            $this->Customer->appendChild(new DOMElement('DateOfBirth', $DateOfBirth));
        }
        /* Date of birth is optional according to AMS. We shouldn't be setting a bogus value.
         * - Kevin Hallmark 06/15/2009
        else 
        {
            $this->Customer->appendChild(new DOMElement('DateOfBirth', '1970-01-01'));
        }*/
                
        if ($CompanyName)
        {
            $this->Customer->appendChild(new DOMElement('CompanyName', $CompanyName));
        }
        
        if ($Title)
        {
            $this->Customer->appendChild(new DOMElement('Title', $Title));
        }
        
        if ($WebSite)
        {
            $this->Customer->appendChild(new DOMElement('WebSite', $WebSite));
        }
        
        $this->Customer->appendChild(new DOMElement('status', null));
        
        return true;
    }

    
    /**
     * Adds a subsciption entry for each user
     *
     * @param int $EnewsletterId
     * @param string $EmailFormat PLAIN or HTML
     * @param int $Status
     * @param int $OptInDate Time Stamp YYYY-MM-DD HH:MM:SS
     * @param int $OptInSrc
     * @param string $OptInQualifier
     * @param int $OptOutDate Timestamp YYYY-MM-DD HH:MM:SS
     * @param string $OptOutReason
     * @param string $RemoteHost Users remote IP
     */
    public function addSubscription($EnewsletterId, $EmailFormat = null, $Status = null, 
                                    $OptInDate = null, $OptInSrc = null, $OptInQualifier = null,
                                    $OptOutDate = null, $OptOutReason = null, $RemoteHost = null)
    {
        $Subscription = new DOMElement('Subscription');
        
        $this->Subscriptions->appendChild($Subscription);
        
        $Subscription->appendChild(new DOMElement('EnewsletterId', $EnewsletterId));
        
        if (!is_null($EmailFormat))
        {
            $Subscription->appendChild(new DOMElement('EmailFormat', $EmailFormat));
        }
        else
        {
        	$Subscription->appendChild(new DOMElement('EmailFormat', 'HTML'));
        }
        
        if (!is_null($Status))
        {
            $Subscription->appendChild(new DOMElement('Status', $Status));
        }
        
        if (!is_null($OptInDate))
        {
            $Subscription->appendChild(new DOMElement('OptInDate', date('c', $OptInDate)));
        }
        
        if (!is_null($OptInSrc))
        {
            $Subscription->appendChild(new DOMElement('OptInSrc', intval($OptInSrc)));
        }
        
        if (!is_null($OptInQualifier))
        {
            $Subscription->appendChild(new DOMElement('OptInQualifier', $OptInQualifier));
        }
        
        if (!is_null($OptOutDate))
        {
            $Subscription->appendChild(new DOMElement('OptOutDate', date('c', $OptOutDate)));
        }
        
        if (!is_null($OptOutReason))
        {
            $Subscription->appendChild(new DOMElement('OptOutReason', $OptOutReason));
        }
        if (is_null($RemoteHost))
        {
            $RemoteHost = ams_customer_ip_address();
        }
        $Subscription->appendChild(new DOMElement('RemoteHost', $RemoteHost));

    }

	/**
	 * customerId
	 * 
	 * Returns the customerId from the response object on success, or null if doesn't exist
	 *
	 * @return void
	 * @author Kevin Hallmark
	 */
	public function customerId() {
		$respXML = $this->getResponseXMLObject();
		
		if($respXML === null || $this->isSoapFault() === true) {
			return null;
		}
		
		//var_dump($respXML);

		$customer = $respXML->Body->CustomerOutput->Customer;
		$attr = $customer->attributes();
		
		return (string)$attr["customerId"];
	}
}
