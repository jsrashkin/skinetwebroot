<?php

require_once('AMS_Operation.php');

/**
 * AMS_AddEnewsletterEntriesToCustomer
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_FindEnewsletters extends AMS_Operation
{
    

    /**
     * Loads the XML data for the request
     *
     * @param int $DomainId
     * @param string $Name
     * @param string $Description
     * @param int $TypeId
     * @param string $DoubleOptIn
     */
    public function load($DomainId = null, $Name = null, $Description = null, $TypeId = null, $DoubleOptIn = null)
    {
        $this->setRequestNode('FindEnewsletters', null, AMS_SERVICE_NAMESPACE);
       
        $DoubleOptInString = ($DoubleOptIn)? 'true' : 'false';
        
        $NewsLetter      =& new DOMElement('Newsletter'); 
        
        $this->requestNode->appendChild($NewsLetter);
        
        if (!is_null($Name))
        {
            $NewsLetter->appendChild(new DOMElement('name', trim($Name)));
        }
        
        if (!is_null($Description))
        {
            $NewsLetter->appendChild(new DOMElement('description', trim($Description)));
        }
        
        if (!is_null($DomainId))
        {
            $NewsLetter->appendChild(new DOMElement('domainId', $DomainId));
        }
        
        if (!is_null($TypeId))
        {
            $NewsLetter->appendChild(new DOMElement('typeId', $TypeId));
        }
        
        if (!is_null($DoubleOptIn))
        {
            $NewsLetter->appendChild(new DOMElement('doubleOptIn', $DoubleOptInString));
        }        
    }

  /**
	 * customerId
	 *
	 * Returns an array of newsletters returned from the xml.
	 *
	 * @return array
	 * @author Matt Poole
	 */
	public function getNewsletters() {
		$respXML = $this->getResponseXMLObject();

		if($respXML === null || $this->isSoapFault() === true) {
			return null;
		}

    $newsletters = $respXML->Body->FindEnewslettersOutput->Newsletters->Newsletter;
    $return = array();
		if ($newsletters) {
      foreach ($newsletters as $newsletter) {
        $enewsletterId = (string) $newsletter->attributes();
        if ($enewsletterId) {
          $newsletter = (array) $newsletter;
          unset($newsletter['@attributes']);
          $newsletter['enewsletterId'] = $enewsletterId;
          $return[] = $newsletter;
        }
      }
    }
    return $return;
	}
}