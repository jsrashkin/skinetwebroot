<?php

require_once('AMS_Operation.php');

/**
 * AMS_RemoveAttributeValue
 *
 * Deletes an attribute for a customer
 * 
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_RemoveAttributeValue extends AMS_Operation
{    
    
    /**
     * Initializer
     *
     * @param int $CustomerId
     */
    public function load($AttributeValueId)
    {        
        $this->setRequestNode('RemoveAttributeValue', null, AMS_SERVICE_NAMESPACE);
        
        $this->requestNode->appendChild(new DOMElement('AttributeValueId', $AttributeValueId));
    }
}