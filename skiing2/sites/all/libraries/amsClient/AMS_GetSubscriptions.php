<?php

require_once('AMS_Operation.php');

/**
 * AMS_GetSubscriptions
 *
 * Get all enewsletter entries for a customer in a specific domain
 * 
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_GetSubscriptions extends AMS_Operation
{
    /**
     * Loads the XML node for GetSubscriptions
     *
     * @param int $CustomerId
     * @param int $DomainId
     */
    public function load($CustomerId, $DomainId)
    {
        $this->setRequestNode('GetSubscriptions', null, AMS_SERVICE_NAMESPACE);
        
        $this->requestNode->appendChild(new DOMElement('CustomerId', $CustomerId));
        $this->requestNode->appendChild(new DOMElement('DomainId', $DomainId));
    }
}