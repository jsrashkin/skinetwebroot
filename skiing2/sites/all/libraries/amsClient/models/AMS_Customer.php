<?php

/**
 * This is a stub file for the AMS_Customer Model object. One of the biggest 
 * problems with the AMS Library is that it requires the same XML elements for 
 * almost all the requests. However, each function file has to duplicates the same
 * code to control the Customer. The idea is that this file creates an XML tree,
 * then it can be added to any of the different operations. This way, we don't
 * have to duplicate the Customer code over, and over, and over. DRY - Kevin 
 */


class AMS_Customer {
	/**
	 * This is the private Customer array. It stores all the values needed.
	 *
	 * @var string
	 */
	private $Customer = array();

	
	public function __construct() {
		$this->Customer = new DOMElement('Customer');
	}
	
	public static function get($CustomerId) {
				
	}
	
	public static function findByEmailAddress() {
		
	}
	
	/**
     * Registers the Customer data
     *
     * @param string $FirstName
     * @param string $LastName
     * @param string $EmailAddress
     * @param string $Address1
     * @param string $Address2
     * @param string $City
     * @param string $State
     * @param string $PostCode
     * @param string $Country
     * @param string $PhoneNumber
     * @param string $WorkPhoneNumber
     * @param string $FaxNumber
     * @param string $MobilePhoneNumber
     * @param string $DateOfBirth
     * @param string $CompanyName
     * @param string $Title
     * @param string $WebSite
     * @return bool
     */
    public function loadCustomer($FirstName, $LastName,
                         $EmailAddress, $Address1 = null, $Address2 = null, 
                         $City = null, $State = null, $PostCode = null, 
                         $Country = null, $PhoneNumber = null, $WorkPhoneNumber = null,
                         $FaxNumber = null, $MobilePhoneNumber = null, 
                         $DateOfBirth = null, $CompanyName = null, 
                         $Title = null, $WebSite = null)
    {
        $this->Customer->appendChild(new DOMElement('FirstName', $FirstName));
        
        $this->Customer->appendChild(new DOMElement('LastName', $LastName));
        
        if ($Address1)
        {
            $this->Customer->appendChild(new DOMElement('Address1', $Address1));
        }
        
        if ($Address2)
        {
            $this->Customer->appendChild(new DOMElement('Address2', $Address2));
        }
        
        if ($City)
        {
            $this->Customer->appendChild(new DOMElement('City', $City));
        }
        
        if ($State)
        {
            $this->Customer->appendChild(new DOMElement('State', $State));
        }
        
        if ($PostCode)
        {
            $this->Customer->appendChild(new DOMElement('PostCode', $PostCode));
        }
        
        if ($Country)
        {
            $this->Customer->appendChild(new DOMElement('Country', $Country));
        }
        
        if ($EmailAddress)
        {
            $this->Customer->appendChild(new DOMElement('EmailAddress', $EmailAddress));
        }
        
        if ($PhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('PhoneNumber', $PhoneNumber));
        }
        
        if ($WorkPhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('WorkPhoneNumber', $WorkPhoneNumber));
        }
        
        if ($FaxNumber)
        {
            $this->Customer->appendChild(new DOMElement('FaxNumber', $FaxNumber));
        }
        
        if ($MobilePhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('MobilePhoneNumber', $MobilePhoneNumber));
        }
        
        if ($DateOfBirth)
        {
            $this->Customer->appendChild(new DOMElement('DateOfBirth', $DateOfBirth));
            
        }

		/* Default DOB is BAD - Kevin (07/09/2009)
        else 
        {
            
            $this->Customer->appendChild(new DOMElement('DateOfBirth', '1970-01-01'));
        }*/
                
        if ($CompanyName)
        {
            $this->Customer->appendChild(new DOMElement('CompanyName', $CompanyName));
        }
        
        if ($Title)
        {
            $this->Customer->appendChild(new DOMElement('Title', $Title));
        }
        
        if ($WebSite)
        {
            $this->Customer->appendChild(new DOMElement('WebSite', $WebSite));
        }
        
        $this->Customer->appendChild(new DOMElement('status', null));
        
        $this->Customer->appendChild($this->CustomerAttributes);
        
        return true;
    }
}