<?php

/**
 * This is a stub file for the AMS_SiteRegistration Model object. One of the biggest 
 * problems with the AMS Library is that it requires the same XML elements for 
 * almost all the requests. However, each function file has to duplicates the same
 * code to control the Customer. The idea is that this file creates an XML tree,
 * then it can be added to any of the different operations. This way, we don't
 * have to duplicate the Customer code over, and over, and over. DRY - Kevin 
 */


class AMS_SiteRegistration {
	
	const AMS_USERNAME = "Bob";
	
	
	public function loadSiteRegistrationData() {
		
	}
}