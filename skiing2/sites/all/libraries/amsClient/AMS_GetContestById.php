<?php

require_once('AMS_Operation.php');

/**
 * AMS_GetContestById
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_GetContestById extends AMS_Operation
{

    /**
     * Loads the XML Data
     *
     * @param int $ContestId The ID of the Contest
     */
    public function load($ContestId)
    {
        $this->setRequestNode('GetContestById', $ContestId, AMS_SERVICE_NAMESPACE);
    }
}