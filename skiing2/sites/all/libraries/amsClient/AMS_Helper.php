<?php
function ams_customer_ip_address() {
  static $ip_address = NULL;
  if (!isset($ip_address)) {
    $ip_address = $_SERVER['REMOTE_ADDR'];
    if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
      $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
  }
  return $ip_address;
}
