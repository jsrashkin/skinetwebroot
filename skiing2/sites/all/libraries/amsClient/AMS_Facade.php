<?php
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(dirname(dirname(__FILE__))) . '/lib/');
require_once('amsClient/AMS_UpdateSiteRegistration.php');
require_once('amsClient/AMS_UpdateCustomer.php');
require_once('amsClient/AMS_AddEnewsletterEntriesToCustomer.php');
require_once('amsClient/AMS_AddSiteRegistration.php');
require_once('amsClient/AMS_GetSiteRegistrationByEmailAddress.php');
require_once('amsClient/AMS_GetSiteRegistrationByCustomer.php');
require_once('amsClient/AMS_GetCustomer.php');

/**
 * This Facade class simplifies the interface to the nStein platform. A developer shall be using to call to the 
 * corresponding AMS classes.
 * 
 */
class AMS_Facade {
	private $addNewsletterEntry;
	
	private $addSiteRegistration;
	
	private $createNewsletter;
	
	private $getEnewslettersForDomain;
	
	private $getContestsInDomain;

	private $getContestId;
	
	private $getSiteRegByEmail;
	
	private $getSiteRegById;
	
	private $unsubscribe;
	
	private $updateCustomer;
	
	private $updateSiteReg;
	
	private $getServerInfo;
	
	private $getEnewsletterForCustomer;

  private $addEnewsletterToCustomer;
  
  private $subscribeToNewsletterAnon;

	/**
	 * Gets AMS Server information
	 *
	 * @author Bensan George
	 */
	public function getServerInfo()
	{
		$this->getServerInfo = new AMS_GetServerInfo();
		$this->getServerInfo->load();
		$this->getServerInfo->execute();
		
		echo '<pre>';
		print_r($this->getServerInfo->getResponseXMLObject());
		echo '</pre>';
	}

  /**
	 * Gets A customer id by an email address
	 *
	 * @author Matt Poole
	 */
  public function getCustomerIdByEmailAddress($email) {
    require_once('amsClient/AMS_FindCustomerByEmailAddress.php');
    $ams_find = new AMS_FindCustomerByEmailAddress();
    $ams_find->load($email);
    $ams_find->execute();
    return $ams_find->getCustomerId();
  }

  /**
	 * Gets A customer record by an email address
	 *
	 * @author Matt Poole
	 */
  public function getCustomerByEmailAddress($email) {
    require_once('amsClient/AMS_FindCustomerByEmailAddress.php');
    $ams_find = new AMS_FindCustomerByEmailAddress();
    $ams_find->load($email);
    $ams_find->execute();
    return $ams_find->getCustomer();
  }

	/**
	 * Gets customer information
	 *
	 * @param string $customerEmail
	 * @param integer $domainId
	 * @param bool $getImage
	 * @return An associative array with the following customer information if found
	 * 	found status, registration id, customer id, username, password, created date, last access date, domain id, active,
	 *  customer id, first name, last name, address, city, state, postal code, country, country, email address;
	 * 	returns false if no customer information found
	 * 
	 */
	public function getCustomerInfoByEmailAddress($customerEmail, $domainId, $getImage = false)
	{
		$this->getSiteRegByEmail = new AMS_GetSiteRegistrationByEmailAddress();
		$this->getSiteRegByEmail->load($customerEmail, $domainId, $getImage);
		$status = $this->getSiteRegByEmail->execute();
    $errorArray = $this->getSiteRegByEmail->getErrorArray();
    //make sure we have some errors
    if(!empty($errorArray) && is_array($errorArray)) {
      //we have some sort of error so check against array keys to identify
      foreach($errorArray as $key=>$value) {
        //we only want to catch error 104 for username already exists
        if($key == 410) {
          //lets set status to merge to let us know to move the use to ams_queue_merge table
          return 'ams_down';
        }
      }
    }


		if($status)
		{
			$siteRegistrationOutput = $this->getSiteRegByEmail->getResponseXMLObject()->Body->SiteRegistrationOutput;
			$found = (string) $siteRegistrationOutput->attributes()->found;
			if((string) $siteRegistrationOutput->attributes()->found != 'false')
			{
				$siteRegistration = $siteRegistrationOutput->SiteRegistration;
				$customer = $siteRegistrationOutput->Customer;
				
				//$return['found'] = (string) $siteRegistrationOutput->attributes()->found;
				$return['registrationId'] = (string) $siteRegistration->attributes()->registrationId;
				$return['customerId'] = (string) $siteRegistration->customerId;
				$return['username'] = (string) $siteRegistration->username;
				//$return['nickname'] = (string) $siteRegistration->nickname;
				$return['password'] = (string) $siteRegistration->password;
				//$return['createdDate'] = (string) $siteRegistration->createdDate;
				//$return['lastAccessDate'] = (string) $siteRegistration->lastAccessDate;
				//$return['domainId'] = (string) $siteRegistration->domainId;
				//$return['active'] = (string) $siteRegistration->active;
				//$return['firstName'] = (string) $customer->FirstName;
				//$return['lastName'] = (string) $customer->LastName;
				//$return['address1'] = (string) $customer->Address1;
				//$return['city'] = (string) $customer->City;
				//$return['state'] = (string) $customer->State;
				//$return['postalCode'] = (string) $customer->PostCode;
				//$return['country'] = (string) $customer->Country;
				//$return['emailAddress'] = (string) $customer->EmailAddress;
				$return['attributes'] = $customer->attributes;

				return $return;
			}
			else
			{
				return false;
			}
		}
		return false;
	}

	
	/**
	 * Retrieves the site registration by id
	 *
	 * @return void
	 * @author Bensan George
	 **/
	public function getCustomerInfoById($customerId, $domainId, $getImage = false, $email = false)
	{
	    $this->getSiteRegById = new AMS_GetSiteRegistrationByCustomer();
	    $this->getSiteRegById->load($customerId, $domainId, $getImage);
	    $status = $this->getSiteRegById->execute();
	    
	    if($status)
	    {
	        $siteRegistrationOutput = $this->getSiteRegById->getResponseXMLObject()->Body->SiteRegistrationOutput;
	        $siteRegistration = $siteRegistrationOutput->SiteRegistration;
	        if ($email === false)
	  		{
				return $siteRegistration;
			}
	
	        // we are assuming here that the email address is the same as the username
	        // as per the CTL specifications
	        $emailAddress = $siteRegistration->username;
	        
	        if(isset($emailAddress))
	        {
	            $return = $this->getCustomerInfoByEmailAddress($emailAddress, $domainId, $getImage);
	            return $return;
	        }
	    }
	    return false;
	}
	
	
	/**
	 * Retrieves the customer information by id
	 *
	 * @return void
	 **/
	public function getCustomerById($customerId)
	{
		$this->getCustomerById = new AMS_GetCustomer();
    	$this->getCustomerById->load($customerId);
    	$status = $this->getCustomerById->execute();

		if ($status)
		{
      		$customerOutput = $this->getCustomerById->getResponseXMLObject()->Body->CustomerOutput;
      		$siteCustomer = $customerOutput->Customer;
			return $siteCustomer;
		}
		
		return false;
	}
	
	public function getSiteRegistrationById($customerId, $domainId, $getImage = false, $email = false)
	{
	 	return self::getCustomerInfoById($customerId, $domainId, $getImage, $email);
	}
	
	
	public function updateSiteRegistration($registrationId, $customerId, $username = null, $password = null, $nickname = null, 
	$suspendDate = null, $allowAccessDate = null, $active = null, $suspendComments = null, $imageFilename = null, $imageType = null)
	{
		// do not encrypt password if syncing with AMS...stores in plain text
		if (AMS_SYNC == 0)
		{
			$password = md5($password);
		}
		else
		{
			$password = $password;
		}
		
		$this->updateSiteReg = new AMS_UpdateSiteRegistration();
		$this->updateSiteReg->load($registrationId, $customerId, $username, $password, $nickname, 
		$suspendDate, $allowAccessDate, $active, $suspendComments, $imageFilename, $imageType);
		if($this->updateSiteReg->execute())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Retrieve contests in AMS
	 * @param $domainId domain id of contests
	 * @param $includeExpiredContent include expired contests in search results
	 * @return array $contests an associative array of contests; key is the contest id
	 *
	 * @author Bensan George
	 */
	public function getContestsInDomain($domainId, $includeExpiredContent)
	{
		$this->getContestsInDomain = new AMS_GetContestsInDomain();
        $this->getContestsInDomain->load(AMS_DOMAIN_ID, $includeExpiredContent);
        $status = $this->getContestsInDomain->execute();

				if ($status === false)
				{
					print_r($this->getContestsInDomain->getErrorArray()); 
					return ;
				}

				$contestOutput = $this->getContestsInDomain->getResponseNode()->Contests->Contest;

				foreach($contestOutput as $amsContest){
					$contestId = (string) $amsContest->attributes()->contestId;
					$return[$contestId]['contestId'] = $contestId;
					$return[$contestId]['name'] = (string) $amsContest->Name;
					$return[$contestId]['blurb'] = (string) $amsContest->Blurb;
					$return[$contestId]['domainId'] = (string) $amsContest->DomainId;
					$return[$contestId]['rulesId'] = (string) $amsContest->RulesId;
					$return[$contestId]['startDate'] = (string) $amsContest->StartDate;
					$return[$contestId]['endDate'] = (string) $amsContest->EndDate;
					$return[$contestId]['allowMultiple'] = (string) $amsContest->AllowMultiple;
					$return[$contestId]['expired'] = (string) $amsContest->Expired;
					$return[$contestId]['smallImageId'] = (string) $amsContest->SmallImageId;
					$return[$contestId]['largeImageId'] = (string) $amsContest->LargeImageId;
					$return[$contestId]['imageURL'] = (string) $amsContest->ImageURL;
				}
				
				return $return;
	}
	
	/**
	 * Retrieves a contest by the contest id
	 *
	 * @param integer $contestId
	 * @return array
	 */
	public function getContestById($contestId)
	{
		$this->getContestId = new AMS_GetContestById();
		$this->getContestId->load($contestId);
		$this->getContestId->execute();
		
		$contestOutput = $this->getContestId->getResponseNode();
		$contest = $contestOutput->Contest;

		$return['found'] = (string) $contestOutput->attributes()->found;
		$return['contestId'] = (string) $contest->attributes()->contestId;
		$return['name'] = (string) $contest->Name;
		$return['blurb'] = (string) $contest->Blurb;
		$return['domainId'] = (string) $contest->DomainId;
		$return['issueId'] = (string) $contest->IssueId;
		$return['rulesId'] = (string) $contest->RulesId;
		$return['startDate'] = (string) $contest->StartDate;
		$return['endDate'] = (string) $contest->EndDate;
		$return['allowMultiple'] = (string) $contest->AllowMultiple;
		$return['expired'] = (string) $contest->Expired;
		$return['smallImageId'] = (string) $contest->SmallImageId;
		$return['largeImageId'] = (string) $contest->LargeImageId;

		if(isset($contest->ImageURL))
		{
			$return['imageURL'] = (string) $contest->ImageURL;
		}
		else
		{
			$return['imageURL'] = 'http://ctl/img/no_image_found_140x95.jpg';
		}
		if(isset($contest->Rules))
		{
		    $return['rules'] = (string) $contest->Rules;
		}
		else
		{
		    $return['rules'] = null;
		}
		return $return;
	}
	
	
	/**
	 * Retrieves username and password
	 * @param $username username
	 * @param $password password
	 * @return bool returns true if successfully authenticated, returns false if authentication fails 
	 * @author Bensan George
	 * Edited by Jeff
	 * Changes:  just return customerinfo even if user not found.  We need to be able to handle errors
	 */
	public function getLoginInfo($username, $password)
	{
	    $customerInfo = $this->getCustomerInfoByEmailAddress($username, AMS_DOMAIN_ID, false);
	    if($customerInfo['username'] == $username && $customerInfo['password'] == $password)
	    {
	    	$customerInfo['found'] = true;
	    }
	    else
	    {
	  		$customerInfo['found'] == false;
	    }
	    return $customerInfo;
	}
	
	/**
	 * Register customer to a site
	 * @param array $customerInfoArray array of customer information
	 * @param array $siteRegInfoArray array of site registration information for customer
	 * @param array $subscriptionAttrArray array of subscriptions for this customer
	 * @param array $customerAttrArray array of customer attributes for this customer
	 * @author Bensan George
	 */
	public function addSiteRegistration($customerInfoArray, $siteRegInfoArray, $subscriptionAttrArray = null, $customerAttrArray = null)
	{
	    $this->addSiteRegistration = new AMS_AddSiteRegistration();
	    $this->addSiteRegistration->load(AMS_DOMAIN_ID);
	    // load customer information
	    $this->addSiteRegistration->loadCustomer($customerInfoArray['firstname'], $customerInfoArray['lastname'], $customerInfoArray['email'],
	    $customerInfoArray['address1'], $customerInfoArray['address2'], $customerInfoArray['city'], $customerInfoArray['state'], $customerInfoArray['postalcode'],
	    $customerInfoArray['country'], $customerInfoArray['phone'], $customerInfoArray['workphone'], $customerInfoArray['fax'], $customerInfoArray['mobilephone'],
	    $customerInfoArray['date_of_birth'], $customerInfoArray['company'], $customerInfoArray['title'], $customerInfoArray['website']);
	    	    
	    // load site registration information
	    $this->addSiteRegistration->loadSiteRegistration($siteRegInfoArray['username'], $siteRegInfoArray['password'], $siteRegInfoArray['nickname'], 
	    $siteRegInfoArray['profilePhotoPath'], $siteRegInfoArray['imageType']);
	    
	    // load subscriptions for customer
	    if($subscriptionAttrArray != null)
	    {
	        foreach($subscriptionAttrArray as $subscriptionNum => $subscriptionFormat)
	        {
	            $this->addSiteRegistration->addSubscription($subscriptionNum, $subscriptionFormat);
	        }
	    }
	    
	    // load customer attributes
	    if($customerAttrArray != null)
	    {
	        foreach($customerAttrArray as $customerAttrNum => $customerAttrLabel)
	        {
	            $this->addSiteRegistration->addCustomerAttribute($customerAttrNum, $customerAttrLabel);
	        }
	    }
	    $status = $this->addSiteRegistration->execute();
	    if($status === false || $status === 106)
	    {
	        $return['customerid'] = null;
	        $return['registrationId'] = null;
	        $return['emailTaken'] = false;
	        $return['usernameTaken'] = true;
	        $return['status'] = $status;
	        return $return;
	    }
	    else if($status === 108)
	    {
	    	$return['customerid'] = null;
	        $return['registrationId'] = null;
	        $return['emailTaken'] = true;
	        $return['usernameTaken'] = false;
	        return $return;
	    }
	    else
	    {
		    $response = $this->addSiteRegistration->getResponseNode();
		    $return['customerId'] = $response->SiteRegistration->customerId;
		    $return['registrationId'] = $response->SiteRegistration->registrationId;
		    $return['emailTaken'] = false;
		    $return['usernameTaken'] = false;
	    }
	    //print_r($return);
	    //die;
	    return $return;
	}

  /**
	 * Retrieves all the attribute definitions
	 *
	 * @return array
	 * @author Matt Poole
	 **/
	public function getAllAttributeDefinitions($Name = null, $Type= null, $Description = null, $DomainId = null)
	{
	  require_once('amsClient/AMS_FindAttributeDefinitions.php');
		$this->getAttributeDefinitions = new AMS_FindAttributeDefinitions();
		$this->getAttributeDefinitions->load($Name, $Type, $Description, $DomainId);
		$status = $this->getAttributeDefinitions->execute();

		if($status === false)
		{
			if(AMS_DEBUG) {
				print_r($this->getAttributeDefinitions->getErrorArray());
				print $this->getAttributeDefinitions->getRequestXMLString();
			}
			return array();
		}

		$response = $this->getAttributeDefinitions->getResponseNode();
    $attributes = $response->AttributeDefinitions->AttributeDefinition;
    $return = array();
    foreach ($attributes as $attribute) {
      $attribute = (array) $attribute;
      $attribute['AttributeId'] = array_pop($attribute['@attributes']);
      unset($attribute['@attributes']);
      $return[] = $attribute;
    }
    return $return;
	}

	/**
	 * Retrieves all the newsletters for a specific domain id
	 *
	 * @return void
	 * @author Bensan George
	 **/
	public function getAllEnewsletters()
	{
    require_once('amsClient/AMS_FindEnewsletters.php');
    $this->getEnewslettersForDomain = new AMS_FindEnewsletters();
    $this->getEnewslettersForDomain->load(AMS_DOMAIN_ID);

    $status = $this->getEnewslettersForDomain->execute();

    if($status === false)
    {
      if(AMS_DEBUG) {
				print_r($this->getEnewslettersForDomain->getErrorArray());
				print $this->getEnewslettersForDomain->getRequestXMLString();
			}
			return ;
    }
    else {
      return $this->getEnewslettersForDomain->getNewsletters();
    }
}
	
	/**
	 * Retrieves all newsletters for a customer
	 * @param $customerId
	 * @return void
	 * @author Bensan George
	 **/
	public function getEnewsletterForCustomer($customerId)
	{
		require_once('amsClient/AMS_GetSubscriptions.php');
		$this->getEnewsletterForCustomer = new AMS_GetSubscriptions();
		$this->getEnewsletterForCustomer->load($customerId, AMS_DOMAIN_ID);
		$status = $this->getEnewsletterForCustomer->execute();
		if($status === false)
		{
			if(AMS_DEBUG) {
				print_r($this->getEnewsletterForCustomer->getErrorArray()); 
				print $this->getEnewsletterForCustomer->getRequestXMLString();
			}
			return ;
		}
		
		$response = $this->getEnewsletterForCustomer->getResponseNode();
		$newsletters = $response->Subscriptions->Subscription;
		if($newsletters)
		{
			foreach($newsletters as $newsletter)
			{
				//var_dump($newsletter);
				
				$newsletterId = (int) $newsletter->EnewsletterId;
				$return[$newsletterId]['id'] = (string) $newsletter->attributes()->enewsletterEntryId;
				$return[$newsletterId]['eNewsletterId'] = (int) $newsletter->EnewsletterId;
				$return[$newsletterId]['customerId'] = (int) $newsletter->CustomerId;
				$return[$newsletterId]['emailFormat'] = (string) $newsletter->EmailFormat;
				$return[$newsletterId]['status'] = (int)$newsletter->Status;
				$return[$newsletterId]['optInDate'] = (string)$newsletter->OptInDate;
				$return[$newsletterId]['optInSrc'] = (string)$newsletter->OptInSrc;
				$return[$newsletterId]['optOutDate'] = (string)$newsletter->OptOutDate;
				
			}
		}
		return $return;
	}
	
	/**
	 * Add a newsletter to customer
	 *
	 * @return void
	 * @author Bensan George
	 **/
	public function addEnewsletterToCustomer($customerid, $emailAddress, $subscriptions = null) {
		$this->addEnewsletterToCustomer = new AMS_AddEnewsletterEntriesToCustomer();
		
		$this->addEnewsletterToCustomer->load($customerid, AMS_DOMAIN_ID, $emailAddress);
		
		if(isset($subscriptions)) {
			foreach($subscriptions as $subscriptionId => $subscriptionFormat) {
				$this->addEnewsletterToCustomer->addSubscription($subscriptionId, $subscriptionFormat);
			}
		}
		$status = $this->addEnewsletterToCustomer->execute();
		
		if ($status === false)
		{
			print_r($this->addEnewsletterToCustomer->getErrorArray());
			print $this->addEnewsletterToCustomer->getRequestXMLString();
			return ;
		}
		
		$response = $this->addEnewsletterToCustomer->getResponseNode();
		$subscription = $response->Subscriptions->Subscription;
		$return = array();
		
		foreach($subscription as $sub)
		{
			$subId = (string) $sub->attributes()->enewsletterEntryId;
			$return[$subId]['id'] = $subId;
			$return[$subId]['eNewsletterId'] = (string) $sub->ENewsletterId;
			$return[$subId]['customerId'] = (string) $sub->CustomerId;
			$return[$subId]['emailFormat'] = (string) $sub->EmailFormat;
			$return[$subId]['status'] = (string) $sub->Status;
			$return[$subId]['optInDate'] = (string) $sub->OptInDate;
			$return[$subId]['optInSrc'] = (string) $sub->OptInSrc;
		}
		return $return;
	}

	/**
	 * Function allows you to subscribe to newsletters anonymously
	 *
	 * @return void
	 * @author Bensan George
	 **/
	public function subscribeToNewsletterAnon($emailAddress, $firstName, $lastName, $subscriptions)
	{
			$this->subscribeToNewsletterAnon = new AMS_CreateCustomer();
			$this->subscribeToNewsletterAnon->load();
			$this->subscribeToNewsletterAnon->loadCustomer($firstName, $lastName, $emailAddress);
      foreach($subscriptions as $subscriptionId => $subscriptionFormat)
      {
          $this->subscribeToNewsletterAnon->addSubscription($subscriptionId, $subscriptionFormat, 0, null, 0, null, null, null, null);
      }
			
			$this->subscribeToNewsletterAnon->execute();
	}
}
