<?php

require_once('AMS_Operation.php');

/**
 * AMS_FindSubscription
 *
 * Get all enewsletter entries for a customer in a specific domain
 * 
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_FindSubscription extends AMS_Operation
{
    
    /**
     * Loads the XML Data
     *
     * @param int $CustomerId
     * @param int $EnewsletterId
     */
    public function load($CustomerId, $EnewsletterId)
    {
        $this->setRequestNode('FindSubscription', null, AMS_SERVICE_NAMESPACE);
        
        $this->requestNode->appendChild(new DOMElement('EnewsletterId', $EnewsletterId));
        $this->requestNode->appendChild(new DOMElement('CustomerId', $CustomerId));
    }
}