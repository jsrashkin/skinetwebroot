<?php

require_once('AMS_Operation.php');

/**
 * AMS_UpdateCustomerAttributes
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_UpdateCustomerAttributes extends AMS_Operation
{    
    /**
     * Attributes of the Customer Node
     *
     * @var DOMElement
     */
    private $CustomerAttributes;
    
    
    /**
     * Initializer
     *
     * @param int $CustomerId
     */
    public function load($CustomerId)
    {
        
        $this->CustomerAttributes = new DOMElement('Attributes');
        
        $this->setRequestNode('UpdateCustomerAttributes', null, AMS_SERVICE_NAMESPACE);
        
        $this->requestNode->appendChild(new DOMElement('CustomerId', $CustomerId));
        $this->requestNode->appendChild($this->CustomerAttributes);
    }
    
    /**
     * Updates an attribute
     *
     * Must only be called after load() has been called
     * 
     * @param int $attributeValueId
     * @param int $attributeId
     * @param string $attributeValue
     * @return bool
     */
    public function updateCustomerAttribute($attributeValueId, $attributeId, $attributeValue)
    {
        $attribute = new DOMElement('attribute'); 
               
        $this->CustomerAttributes->appendChild($attribute);
        
        $attribute->appendChild(new DOMElement('attributeId', $attributeId));
        
        // Since the ratio of attributes to customer is one to one
        // This will always be 1 for now.
        $attribute->appendChild(new DOMElement('instance', 1)); 
        $attribute->appendChild(new DOMElement('value', $attributeValue));
        
        $attribute->setAttribute('attributeValueId', $attributeValueId);
        
        return true;
    }
}