<?php

/**
 * AMS_XMLObject Audience Management Services
 *
 * This object will be modified in the future to parse the XML responses.
 * 
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
class AMS_XMLObject extends SimpleXMLElement implements ArrayAccess
{
    /**
     * Cached attribute values
     *
     * @var array
     */
    public $cache = array();
    
    /**
     * Enter description here...
     *
     * @param unknown_type $offset
     * @return unknown
     */
    public function offsetExists($offset)
    {
        if (isset($cache[$offset]))
        {
            return true;
        }
        
        if (isset($this->attributes()->$offset))
        {
            return true;
        }
        
        return false;
    }
    
    public function offsetGet($offset)
    {
        if (isset($this->cache[$offset]))
        {
            return $this->cache[$offset];
        }
        
        if (isset($this->attributes()->$offset))
        {
            $keyValue = $this->attributes()->$offset;
            
            $this->cache[$offset] = (string) $keyValue;
            
            return (string) $this->cache[$offset];
        }
        
        return null;   
    }
    
    public function offsetSet($offset, $value)
    {
        $this->cache[$offset]  = $value;
        
        $this->attributes()->$offset = $value;
        
        return true;
    }
    
    public function offsetUnset($offset)
    {        
        $this->cache[$offset] = null;
        
        unset($this->cache[$offset]);
        
        return true;
    }
}