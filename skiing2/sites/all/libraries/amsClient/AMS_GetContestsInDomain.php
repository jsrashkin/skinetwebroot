<?php

require_once('AMS_Operation.php');

/**
 * AMS_GetContestsInDomain
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_GetContestsInDomain extends AMS_Operation
{
    /**
     * Loads Data
     *
     * @param int $DomainId Domain Id of the site
     * @param bool $IncludeExpiredContents Whether or not to include expired contents
     */
    public function load($DomainId, $IncludeExpiredContents = false)
    {
        $this->setRequestNode('GetContestsInDomain', null, AMS_SERVICE_NAMESPACE);
        
        $ExpiredString = (($IncludeExpiredContents) ? 'true' : 'false');
        
        $this->requestNode->appendChild(new DOMElement('DomainId', $DomainId));
        $this->requestNode->appendChild(new DOMElement('Expired', $ExpiredString));
    }
}