<?php

require_once('AMS_Operation.php');

/**
 * AMS_GetServerInfo
 *
 * Retrieves information about the AMS server.
 * 
 * Just returns the server version for now
 * 
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_GetServerInfo extends AMS_Operation
{
    /**
     * Binds the data to be sent to AMS
     *
     */
    public function load()
    {
        $this->setRequestNode('GetServerInfo', null, AMS_SERVICE_NAMESPACE);
    }
}