<?php

require_once('AMS_Operation.php');

/**
 * AMS_AddSiteRegistration
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_AddSiteRegistration extends AMS_Operation
{
    /**
     * Customer Node
     *
     * @var DOMElement
     */
    private $Customer;
    
    /**
     * Subscriptions
     *
     * @var DOMElement
     */
    private $Subscriptions;
    
    /**
     * SiteRegistration
     *
     * @var DOMElement
     */
    private $SiteRegistration;
    
    /**
     * Attributes of the Customer Node
     *
     * @var DOMElement
     */
    private $CustomerAttributes;
    
    /**
     * Domain
     *
     * @var int
     */
    private $DomainId = 0;
    
	private $attribute_count = 0;
    private $subscription_count = 0;
    /**
     * Initializer
     *
     * @param int $DomainId
     */
    public function load($DomainId)
    {
        $this->DomainId = $DomainId;
        
        $this->Customer = new DOMElement('Customer');
        
        $this->Subscriptions = new DOMElement('Subscriptions');
        
        $this->SiteRegistration = new DOMElement('SiteRegistration');
        
        $this->CustomerAttributes = new DOMElement('attributes');
        
        $this->setRequestNode('AddSiteRegistration', null, AMS_SERVICE_NAMESPACE);
        
        $this->requestNode->appendChild($this->Customer);

        $this->requestNode->appendChild($this->SiteRegistration);
    }
    
    /**
     * Registers the Customer data
     *
     * @param string $FirstName
     * @param string $LastName
     * @param string $EmailAddress
     * @param string $Address1
     * @param string $Address2
     * @param string $City
     * @param string $State
     * @param string $PostCode
     * @param string $Country
     * @param string $PhoneNumber
     * @param string $WorkPhoneNumber
     * @param string $FaxNumber
     * @param string $MobilePhoneNumber
     * @param string $DateOfBirth
     * @param string $CompanyName
     * @param string $Title
     * @param string $WebSite
     * @return bool
     */
    public function loadCustomer($FirstName, $LastName,
                         $EmailAddress, $Address1 = null, $Address2 = null, 
                         $City = null, $State = null, $PostCode = null, 
                         $Country = null, $PhoneNumber = null, $WorkPhoneNumber = null,
                         $FaxNumber = null, $MobilePhoneNumber = null, 
                         $DateOfBirth = null, $CompanyName = null, 
                         $Title = null, $WebSite = null)
    {
        $this->Customer->appendChild(new DOMElement('FirstName', $FirstName));
        
        $this->Customer->appendChild(new DOMElement('LastName', $LastName));
        
        if ($Address1)
        {
            $this->Customer->appendChild(new DOMElement('Address1', $Address1));
        }
        
        if ($Address2)
        {
            $this->Customer->appendChild(new DOMElement('Address2', $Address2));
        }
        
        if ($City)
        {
            $this->Customer->appendChild(new DOMElement('City', $City));
        }
        
        if ($State)
        {
            $this->Customer->appendChild(new DOMElement('State', $State));
        }
        
        if ($PostCode)
        {
            $this->Customer->appendChild(new DOMElement('PostCode', $PostCode));
        }
        
        if ($Country)
        {
            $this->Customer->appendChild(new DOMElement('Country', $Country));
        }
        
        if ($EmailAddress)
        {
            $this->Customer->appendChild(new DOMElement('EmailAddress', $EmailAddress));
        }
        
        if ($PhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('PhoneNumber', $PhoneNumber));
        }
        
        if ($WorkPhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('WorkPhoneNumber', $WorkPhoneNumber));
        }
        
        if ($FaxNumber)
        {
            $this->Customer->appendChild(new DOMElement('FaxNumber', $FaxNumber));
        }
        
        if ($MobilePhoneNumber)
        {
            $this->Customer->appendChild(new DOMElement('MobilePhoneNumber', $MobilePhoneNumber));
        }
        
        if ($DateOfBirth)
        {
            $this->Customer->appendChild(new DOMElement('DateOfBirth', $DateOfBirth));
            
        }

		/* Default DOB is BAD - Kevin (07/09/2009)
        else 
        {
            
            $this->Customer->appendChild(new DOMElement('DateOfBirth', '1970-01-01'));
        }*/
                
        if ($CompanyName)
        {
            $this->Customer->appendChild(new DOMElement('CompanyName', $CompanyName));
        }
        
        if ($Title)
        {
            $this->Customer->appendChild(new DOMElement('Title', $Title));
        }
        
        if ($WebSite)
        {
            $this->Customer->appendChild(new DOMElement('WebSite', $WebSite));
        }
        
        $this->Customer->appendChild(new DOMElement('status', null));
        
		if($this->attribute_count > 0) {
			$this->Customer->appendChild($this->CustomerAttributes);	
		}
        return true;
    }
    
    /**
     * Adds an attribute
     *
     * Must only be called after loadCustomer() has been called
     * 
     * @param int $attributeId
     * @param string $attributeValue
     * @return bool
     */
    public function addCustomerAttribute($attributeId, $attributeValue)
    {
		$this->attribute_count++;
		
        $attribute = new DOMElement('attribute');        
        $this->CustomerAttributes->appendChild($attribute);
        
        $attribute->appendChild(new DOMElement('attributeId', $attributeId));
        
        // Since the ratio of attributes to customer is one to one
        // This will always be 1 for now.
        $attribute->appendChild(new DOMElement('instance', 1)); 
        $attribute->appendChild(new DOMElement('value', $attributeValue));
        
        return true;
    }
    
    /**
     * Adds a subsciption entry for this user
     *
     * @param int $EnewsletterId
     * @param string $EmailFormat PLAIN or HTML
     * @param int $Status
     * @param int $OptInDate Time Stamp YYYY-MM-DD HH:MM:SS
     * @param int $OptInSrc
     * @param string $OptInQualifier
     * @param int $OptOutDate Timestamp YYYY-MM-DD HH:MM:SS
     * @param string $OptOutReason
     */
    public function addSubscription($EnewsletterId, $EmailFormat = null, $Status = null, 
                                    $OptInDate = null, $OptInSrc = null, $OptInQualifier = null,
                                    $OptOutDate = null, $OptOutReason = null, $RemoteHost = null)
    {
        $Subscription = new DOMElement('Subscription');
        
        $this->Subscriptions->appendChild($Subscription);
        
        $Subscription->appendChild(new DOMElement('EnewsletterId', $EnewsletterId));
        
        if (!is_null($EmailFormat))
        {
            $Subscription->appendChild(new DOMElement('EmailFormat', $EmailFormat));
        }
        
        if (!is_null($Status))
        {
            $Subscription->appendChild(new DOMElement('Status', $Status));
        }
        
        if (!is_null($OptInDate))
        {
            $Subscription->appendChild(new DOMElement('OptInDate', date('c', $OptInDate)));
        }
        
        if (!is_null($OptInSrc))
        {
            $Subscription->appendChild(new DOMElement('OptInSrc', intval($OptInSrc)));
        }
        
        if (!is_null($OptInQualifier))
        {
            $Subscription->appendChild(new DOMElement('OptInQualifier', $OptInQualifier));
        }
        
        if (!is_null($OptOutDate))
        {
            $Subscription->appendChild(new DOMElement('OptOutDate', date('c', $OptOutDate)));
        }
        
        if (!is_null($OptOutReason))
        {
            $Subscription->appendChild(new DOMElement('OptOutReason', $OptOutReason));
        }
        if (is_null($RemoteHost))
        {
            $RemoteHost = ams_customer_ip_address();
        }
        $Subscription->appendChild(new DOMElement('RemoteHost', $RemoteHost));
        

		$this->subscription_count++;
		if($this->subscription_count == 1) {
			$this->requestNode->appendChild($this->Subscriptions);
		}
    }
    
    /**
     * Loads the Site Registration data
     *
     * @param string $username
     * @param string $password
     * @param string $nickname
     * @param bool $active
     * @param string $imageFilename Path to the image filename for the profile image
     * @param int $imageType (1 = GIF, 2 = JPG, 3 = BMP, 5 = PNG)
     * @return bool 
     */
    public function loadSiteRegistration($username, $password, $nickname = null, $active = null, $imageFilename = null, $imageType = null)
    {
        $this->SiteRegistration->appendChild(new DOMElement('username', $username));
        
        $this->SiteRegistration->appendChild(new DOMElement('password', $password));
        
        if (strlen($nickname))
        {
            $this->SiteRegistration->appendChild(new DOMElement('nickname', $nickname));
        }
        
        $this->SiteRegistration->appendChild(new DOMElement('domainId', $this->DomainId));
        
        if (!is_null($active))
        {
            $activeString = ($active) ? 'true' : 'false';
            
            $this->SiteRegistration->appendChild(new DOMElement('active', $activeString));
        }
        
        
        if (strlen($imageFilename) && intval($imageType))
        {            
            if (file_exists($imageFilename))
            {
                if (is_readable($imageFilename))
                {
                    $fileContents = file_get_contents($imageFilename);
           
                    $Image = new DOMElement('image', base64_encode($fileContents)); 
                     
                    $this->SiteRegistration->appendChild($Image);
                    
                    $Image->setAttribute('imageType', $imageType);
                }
            }
            
        }
        
        return true;
    }
}
