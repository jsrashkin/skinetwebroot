<?php


require_once('AMS_Operation.php');

/**
 * AMS_GetSiteRegistrationByCustomer
 * 
 * Retrieves the site registration information using the customer id
 *
 * @author Israel Ekpo <israel.ekpo@bonniercorp.com>
 * @copyright Bonnier Corporation
 * @version 0.2.0
 */
final class AMS_GetSiteRegistrationByCustomer extends AMS_Operation
{
    
    /**
     * Loads the DATA to be sent to AMS
     *
     * @param int $CustomerId
     * @param int $DomainId
     * @param bool $GetImage Whether or not we should return the profile image
     */
    public function load($CustomerId, $DomainId, $GetImage = false)
    {
        $this->setRequestNode('GetSiteRegistrationByCustomer', null, AMS_SERVICE_NAMESPACE);
        
        // We need to override the default value which is GetSiteRegistrationByCustomerOuput
        // Appends the string Output automatically to the string SiteRegistration
        $this->setResponseOutputName('SiteRegistration');
        
        $GetImageValue = ( ($GetImage) ? 'true' : 'false' );
       
        $this->requestNode->appendChild(new DOMElement('CustomerId', $CustomerId));
        $this->requestNode->appendChild(new DOMElement('DomainId', $DomainId));
        $this->requestNode->appendChild(new DOMElement('GetImage', $GetImageValue));
    }
}