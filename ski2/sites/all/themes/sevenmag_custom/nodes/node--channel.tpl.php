
<div class="g-tab">
<?php if ($tabs = render($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
</div>
<?php /*if ($menu): ?>
  <div class="channel-menu">
  <?php print $menu; ?>
  </div>
<?php endif; ?>

<?php if ($feed): ?>
  <div class="channel-feed-container">
  <?php print $feed; ?>
  </div>
<?php endif; ?>

<?php if ($show_more): ?>
  <?php print $show_more; ?>
<?php endif; */
$path = drupal_get_path('theme', 'sevenmag_custom');



if (arg(0) == 'node' && is_numeric(arg(1))) {
  $nid = arg(1);
  if ($nid) {
    $node = node_load($nid);

  }
}

$ContentType='resort_life';
if(preg_match('#performance#',strtolower($node->title)))
	$ContentType='performance';
else
	$ContentType=str_replace(' ','_',strtolower($node->title));	

$CatQ1 = new EntityFieldQuery();
$CatQ1->entityCondition('entity_type', 'node')
  ->entityCondition('bundle',$ContentType)
  ->propertyOrderBy('nid','desc')
  ->range(0,6)
  ->propertyCondition('status', NODE_PUBLISHED); // Run the query as user 1.
$CatQ = $CatQ1->execute();
//type



?>
<div class="wrap">
	<div class="color-title orange full-t">
		<h3><span><a href="">Home</a> &gt; Ski <?php echo $node->title?></span></h3>              
        </div>
	<div class="row">
		<div class="col-md-4">
                <?php if(!empty($CatQ)):?>
			<?php $i=0;foreach($CatQ['node'] as $rev):?>
				<?php 
			if($i==2)
			{
				echo '<img src="'.$path.'/images/b300.jpg" style="width:100%;">';
				$i=0;
				echo '</div><div class="col-md-4">';
			}	
			else
			{
				$revi= node_load($rev->nid);?>	
				<?php 
				$Img=$revi->field_resort_life_image;
				if($ContentType=='performance')
					$Img=$revi->field_performance_image;

				
				?>
				<?php $Desc=@$revi->body['und'][0]['value'];?>	
				<div class="color-title orange full-t">
					<div class="border-color-container">
			            		<div class="row little-padding">
		 	                     		<div class="thumb-cat">
                        					<img src="<?php echo $GLOBALS['base_url'].'/files/'.$Img['und'][0]['filename'];?>">
					                        <div class="custom-title">
                        				 		<h2><?php echo $revi->title?></h2>
				                          		<p><strong><?php echo date('m/d/Y',$revi->created);?></strong></p>
			                        		</div>
			                      		</div>
					      		<div class="content-article">
								<p><?php echo substr($Desc,0,150)?>...</p>
								<a href="<?php echo 
										empty($revi->page_title) ? 
										$GLOBALS['base_url'].'/node/'.$rev->nid : $revi->page_title;?>" class="read-more">Read More</a>
					      		</div>
			                   	 </div>
                		  	</div>
        	       		 </div>
			<?php }
$i++;
endforeach;?>
		<?php else:?>
		<div class="color-title orange full-t"><div class="border-color-container">No article found</div></div>
		<?php endif;?>
</div>
</div>


<?php 
$RelCat = new EntityFieldQuery();
$RelCat->entityCondition('entity_type', 'node')
  ->entityCondition('bundle',$ContentType)
  ->propertyOrderBy('nid','desc')
  ->propertyCondition('status', NODE_PUBLISHED)
  ->addTag('random')
  ->range(0, 2); // Run the query as user 1.	
$RelCatQ =$RelCat->execute();

?>
	<div class="row">
		<div class="col-md-12">
	                <div class="color-title black" style="width:100%;">
	                  <h3><span>Related Posts</span></h3>
                  <div class="border-color-container cat-border">
                    
                    <div class="content-related">
		 <?php if(!empty($RelCatQ)):?>
			<?php foreach($RelCatQ['node'] as $rev1):?>
			<?php $revi1= node_load($rev1->nid);?>
			<?php 
			$Img=$revi1->field_resort_life_image;
			if($ContentType=='performance')
				$Img=$revi1->field_performance_image;
			?>
				<div class="col-md-6">
                		       <div class="thumb-cat">
			<a href="<?php echo empty($revi1->page_title) ? $GLOBALS['base_url'].'/node/'.$rev->nid : $revi1->page_title;?>"><img width="483" height="242" src="<?php echo $GLOBALS['base_url'].'/files/'.$Img['und'][0]['filename'];?>"></a>
                			            	<div class="custom-title">
                        					<h4><?php echo $revi->title?></h4>
                        				    	<p><?php echo date('m/d/Y',$revi->created);?></p>
                        			  	</div>
		                          	</div>
                	      </div>
			<?php endforeach;?>
		<?php endif;?>
                    
                    </div>

                  </div>
                </div>
                
              </div>
            </div>
              <!--  <div class="color-title orange full-t">
                  <div class="border-color-container">
                    <div class="row little-padding">
                      <div class="thumb-cat">
                        <img src="<?php echo $path . '/';?>images/category/2.jpg">
                        <div class="custom-title">
                          <h2>How We Test Ski Boots</h2>
                          <p><strong>08/14/2015</strong></p>
                        </div>
                      </div>
                      <div class="content-article">
                        <p>To get the best ski gear, start at the bottom and work up.</p>
                        <a href="" class="read-more">Read More</a>
                      </div>

                    </div>
                  </div>
                </div>

                <img src="<?php echo $path . '/';?>images/b300.jpg" style="width:100%;">

                <div class="color-title orange full-t">
                  <div class="border-color-container">
                    <div class="row little-padding">
                      <div class="thumb-cat">
                        <img src="<?php echo $path . '/';?>images/category/3.jpg">
                        <div class="custom-title">
                          <h2>CMH: A Heli-Skiing History ...</h2>
                          <p><strong>08/14/2015</strong></p>
                        </div>
                      </div>
                      <div class="content-article">
                        <p>As one of the most well-known heli-ski operations in North America, CMH just ...</p>
                        <a href="" class="read-more">Read More</a>
                      </div>

                    </div>
                  </div>
                </div>

              </div>


              <div class="col-md-4">

                <div class="color-title orange full-t">
                  <div class="border-color-container">
                    <div class="row little-padding">
                      <div class="thumb-cat">
                        <img src="<?php echo $path . '/';?>images/category/4.jpg">
                        <div class="custom-title">
                          <h2>Lake Louise Eyes New ...</h2>
                          <p><strong>08/14/2015</strong></p>
                        </div>
                      </div>
                      <div class="content-article">
                        <a href="" class="read-more">Read More</a>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="color-title orange full-t">
                  <div class="border-color-container">
                    <div class="row little-padding">
                      <div class="thumb-cat">
                        <img src="<?php echo $path . '/';?>images/category/5.jpg">
                        <div class="custom-title">
                          <h2>Hey Kids: Get Out! Vail ...</h2>
                          <p><strong>08/14/2015</strong></p>
                        </div>
                      </div>
                      <div class="content-article">
                        <a href="" class="read-more">Read More</a>
                      </div>

                    </div>
                  </div>
                </div>

                <img src="<?php echo $path . '/';?>images/b300.jpg" style="width:100%;">

                <div class="color-title orange full-t">
                  <div class="border-color-container">
                    <div class="row little-padding">
                      <div class="thumb-cat">
                        <img src="<?php echo $path . '/';?>images/category/6.jpg">
                        <div class="custom-title">
                          <h2>GO: Steamboat, Colo.</h2>
                          <p><strong>08/14/2015</strong></p>
                        </div>
                      </div>
                      <div class="content-article">
                        <p>From the rugged slopes to the colorful downtown, this Colorado resort keeps ...</p>
                        <a href="" class="read-more">Read More</a>
                      </div>

                    </div>
                  </div>
                </div>
                

              </div>
              <div class="col-md-4">
                
                <div class="color-title orange full-t">
                  <div class="border-color-container">
                    <div class="row little-padding">
                      <div class="thumb-cat">
                        <img src="<?php echo $path . '/';?>images/category/7.jpg">
                        <div class="custom-title">
                          <h2>Beijing to Host 2022 ...</h2>
                          <p><strong>08/14/2015</strong></p>
                        </div>
                      </div>
                      <div class="content-article">
                        <a href="" class="read-more">Read More</a>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="color-title orange full-t">
                  <div class="border-color-container">
                    <div class="row little-padding">
                      <div class="thumb-cat">
                        <img src="<?php echo $path . '/';?>images/category/8.jpg">
                        <div class="custom-title">
                          <h2>Colorado's Echo Mountain ...</h2>
                          <p><strong>08/14/2015</strong></p>
                        </div>
                      </div>
                      <div class="content-article">
                        <a href="" class="read-more">Read More</a>
                      </div>

                    </div>
                  </div>
                </div>

                <img src="<?php echo $path . '/';?>images/b300.jpg" style="width:100%;">

                <div class="color-title orange full-t">
                  <div class="border-color-container">
                    <div class="row little-padding">
                      <div class="thumb-cat">
                        <img src="<?php echo $path . '/';?>images/category/9.jpg">
                        <div class="custom-title">
                          <h2>Snowbird Founder Dies ...</h2>
                          <p><strong>08/14/2015</strong></p>
                        </div>
                      </div>
                      <div class="content-article">
                        <a href="" class="read-more">Read More</a>
                      </div>

                    </div>
                  </div>
                </div>

              </div>




            </div>

            <div class="row">
              <div class="col-md-12">

                <div class="color-title black" style="width:100%;">
                  <h3><span>Related Posts</span></h3>
                  <div class="border-color-container cat-border">
                    
                    <div class="content-related">
                      <div class="col-md-6">
                        <div class="row">
                          <div class="thumb-cat">
                            <img src="<?php echo $path . '/';?>images/popular/1r.jpg">
                            <div class="custom-title">
                            <h4>How We Test Ski Boots</h4>
                            <p>08/14/2015</p>
                          </div>
                          </div>
                          
                        </div>
                      
                      </div>
                      <div class="col-md-6">
                        
                        <div class="row">
                          <div class="thumb-cat">
                            <img src="<?php echo $path . '/';?>images/popular/2r.jpg">
                            <div class="custom-title">
                            <h4>Hey Kids: Get Out! Vail Resorts ...</h4>
                            <p>08/14/2015</p>
                          </div>
                          </div>
                          
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
                
              </div>
            </div>



          </div>-->
</div>
