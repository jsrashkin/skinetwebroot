<?php
/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
 global $base_url;
?>
<?php $formatted_date = format_date($node->created, 'custom', 'd/m/Y');?>
<?php
$ski_model = $content['field_product_price']['#object']->field_product_model['und'][0]['value']; 
$ski_brand = $content['field_product_price']['#object']->field_product_brand['und'][0]['entity']->title; 
$ski_body = $content['field_product_price']['#object']->body['und'][0]['value']; 
$ski_product_image = $content['field_product_price']['#object']->field_thumbnail['und'][0]['entity']->title;
$ski_product_image1 = $content['field_product_price']['#object']->field_thumbnail['und'][0]['entity']->field_image['und'][0]['uri'];
$product_price = $content['field_product_price']['#object']->field_product_price['und'][0]['value'];
$product_year = $content['field_product_price']['#object']->field_product_year['und'][0]['value'];
$product_gender = $content['field_product_price']['#object']->field_product_gender['und'][0]['value'];
$product_flex = $content['field_product_price']['#object']->field_boot_flex['und'][0]['value'];
$product_waistwidth = $content['field_product_price']['#object']->field_ski_waistwidth['und'][0]['value'];
$product_size = $content['field_product_price']['#object']->field_boot_sizes['und'][0]['value'];
$product_review = $content['field_product_price']['#object']->field_product_review['und'][0]['value'];
$product_speed = $content['field_product_price']['#object']->field_ski_rating_stabilityspeed['und'][0]['value'];
$product_snow_performance = $content['field_product_price']['#object']->field_ski_rating_hardsnowgrip['und'][0]['value'];
$product_crud_performance = $content['field_product_price']['#object']->field_ski_rating_crudperform['und'][0]['value'];
$product_over_floting = $content['field_product_price']['#object']->field_ski_rating_overall['und'][0]['value'];
$product_floting = $content['field_product_price']['#object']->field_ski_rating_flotation['und'][0]['value'];
$product_forgivness = $content['field_product_price']['#object']->field_ski_rating_forgiveness['und'][0]['value'];
$product_tip_tail_waist = $content['field_product_price']['#object']->field_ski_tiptailwaist['und'][0]['value'];
$product_length = $content['field_product_price']['#object']->field_ski_lengths['und'][0]['value'];

$uri = 'public://';
$img_path= file_create_url($ski_product_image1)
?>
<?php print views_embed_view('snow_feeds'); ?>
<?php
global $user;
 if (in_array('administrator', $user->roles)) {
	print "<div class='contextual-tabs'>". views_embed_view('node', 'contextual_link'). "</div>";
} ?>


<div class="left-box col-md-9 col-sm-8 col-xs-6">
  <div class="video-node-content product-detail-wrap">
    <div class="product-title">
      <h1><?php print $title?></h1>
    </div>
    <div class="breadcrumb-box">
      <div class="breadcrumb-inner">
        <?php   
				$bcarr = drupal_get_breadcrumb();
				$bcarr [] = drupal_get_title();
				print  theme('breadcrumb', array('breadcrumb'=>$bcarr));
				?>
      </div>
    </div>
    <?php //  echo"<pre>"; print_r($content);exit; ?>
    <div class="product-node-content content">
    	<div class="row">
      <div class="ski-posted col-sm-12"><span>Posted: </span><?php print $formatted_date; ?> </div>
      </div>
      <!--	<div class="ski-img"><img src="http://skinet-construction.com/ski/files/_images/201506/<?php // print $ski_product_image ?>"  width="80" height="490">  </div>	-->
      <div class="row">
      <div class="product-img col-md-5 col-sm-12 col-xs-12">
        <div class="ski-img"><img src="<?php print $img_path ?>"  width="80" height="490"> </div>
      </div>
      <div class="brand-detail col-md-7 col-sm-12 col-xs-12">
          <div class="ski-title"><h1><?php print $ski_model?></h1></div>
          <div class="ski-price"><span>Price: </span><?php print $product_price?> </div>
          <div class="buy-btn"><a href="#"><img src="<?php print $base_url ?>/sites/all/themes/sevenmag_custom/images/BUYITNOW_ski_btn.png"> </a></div>
          <div class="ski-year"><span>year: </span><?php print $product_year?> </div>
          <div class="ski-gender"><span>Gender: </span><?php print $product_gender?> </div>
          <div class="ski-last_width"><span>Width: </span><?php print $product_waistwidth?> </div>
          <div class="ski-tip_tail_waist"><span>Tip/Tail/waist: </span><?php print $product_tip_tail_waist?> </div>
          <div class="ski-length"><span>Length: </span><?php print $product_length?> </div>
          <div class="ski-brand"><span>Brand: </span><?php print $ski_brand?></div>
          <div class="ski-speed"><span>Speed: </span><?php print $product_speed?> </div>
          <div class="ski-snow_perf"><span>Snow Performance: </span><?php print $product_snow_performance?> </div>
          <div class="ski-crud_perf"><span>Crud Performance: </span><?php print $product_crud_performance?> </div>
          <div class="ski-forgivness"><span>Forgivness: </span><?php print $product_forgivness?> </div>
          <div class="ski-overall"><span>Overall: </span><?php print $product_over_floting?> </div>
          <div class="ski-floting"><span>Floting: </span><?php print $product_floting?> </div>
        </div>
      </div>
      
      <div class="row">
      <div class="ski-body col-sm-12"><?php print $ski_body?> </div>
      <?php // echo"<pre>"; print_r($content); exit; ?>
      </div>
    </div>
    
    <!--    <div class="post-box black">
          <?php // print views_embed_view('related_post', 'related_posts');   ?>
		</div>		--> 
  </div>
</div>
<div class="right-box col-md-3 col-sm-4 col-xs-6 no-r-padding">
  <div class="nav-sidevar innr-box">
    <div class="innr-list">
      <?php $block = module_invoke('block', 'block_view', '4');
				print render($block['content']); ?>
    </div>
  </div>
  <div class="color-title pink isotope-element video-home">
    <h3><span>Most Recent SKI Videos</span></h3>
    <div class="video-container">
      <div class="pane-content">
        <p><!-- Start of Brightcove Player --><!-- Start of Brightcove Player --></p>
        <!--
By use of this code snippet, I agree to the Brightcove Publisher T and C
found at <a href="https://accounts.brightcove.com/en/terms-and-conditions/" title="https://accounts.brightcove.com/en/terms-and-conditions/">https://accounts.brightcove.com/en/terms-and-conditions/</a>.
--><!--
By use of this code snippet, I agree to the Brightcove Publisher T and C
found at <a href="https://accounts.brightcove.com/en/terms-and-conditions/" title="https://accounts.brightcove.com/en/terms-and-conditions/">https://accounts.brightcove.com/en/terms-and-conditions/</a>.
--> 
        <script src="http://admin.brightcove.com/js/BrightcoveExperiences.js" type="text/javascript" language="JavaScript"></script>
        <object width="300" height="592" type="application/x-shockwave-flash" data="http://c.brightcove.com/services/viewer/federated_f9?&amp;width=300&amp;height=592&amp;flashID=myExperience&amp;bgcolor=%23FFFFFF&amp;playerID=1964542304001&amp;playerKey=AQ~~%2CAAAAAEwRm8I~%2COusBxQT0iGh1UQjskO9PU6xQBk91cKuu&amp;isVid=true&amp;isUI=true&amp;dynamicStreaming=true&amp;autoStart=&amp;debuggerID=&amp;startTime=1447152433812" id="myExperience" class="BrightcoveExperience" seamlesstabbing="undefined">
          <param name="allowScriptAccess" value="always">
          <param name="allowFullScreen" value="true">
          <param name="seamlessTabbing" value="false">
          <param name="swliveconnect" value="true">
          <param name="wmode" value="window">
          <param name="quality" value="high">
          <param name="bgcolor" value="#FFFFFF">
        </object>
        <!--
This script tag will cause the Brightcove Players defined above it to be created as soon
as the line is read by the browser. If you wish to have the player instantiated only after
the rest of the HTML is processed and the page load is complete, remove the line.
--><!--
This script tag will cause the Brightcove Players defined above it to be created as soon
as the line is read by the browser. If you wish to have the player instantiated only after
the rest of the HTML is processed and the page load is complete, remove the line.--><script type="text/javascript">brightcove.createExperiences();</script><!-- End of Brightcove Player --><!-- End of Brightcove Player -->
        <p></p>
      </div>
      
      <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

                    asd2('.video-bxslider').bxSlider({
                      //pagerCustom: '#video-bx-pager',
                      //controls: false
                    });*/
                    
                    
					/*var slider = $('.bxslider').bxSlider({
						pagerCustom: '#bx-pager'
					});	*/	
									--> 
      
    </div>
  </div>
  <div class="innr-box">
    <h3><span>Popular</span></h3>
    <div class="innr-list">
      <div class="block">
        <?php
					$view = views_get_view('popular_at_homepage');
					print $view->execute_display('default', NULL);
			//		print views_embed_view('popular_at_homepage','block_popular');
			//		print views_embed_view('sidebar_content', 'block'); ?>
      </div>
    </div>
  </div>
  <div class="innr-box">
    <div class="innr-box">
      <h3><span>Recent</span></h3>
      <div class="innr-list">
        <div class="block">
          <?php
				/*		$view = views_get_view('recent_posts');
						print $view->execute_display('default', NULL); */
						
						print views_embed_view('sidebar_content', 'recent_post'); 
						?>
        </div>
      </div>
    </div>
  </div>
<div class="google-ad-custom-block-right_3 hide">
			<div class="innr-box">       	
				<?php $block = module_invoke('ad_manager', 'block_view', 'right3');
						print render($block['content']); ?> 
			</div>
		</div>
		
		<div class="google-ad-custom-block-right_3">
			<div class="innr-box">       	
				<?php $block = module_invoke('ad_manager', 'block_view', 'right3');
						print render($block['content']); ?> 
			</div>
		</div>
		
		<div class="google-ad-custom-block-right_3_clone">
			<div class="innr-box">       	
				<?php $block = module_invoke('ad_manager', 'block_view', 'right3');
						print render($block['content']); ?> 
			</div>
		</div>	
</div>
