<?php
/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>

          
<?php $formatted_date = format_date($node->created, 'custom', 'd/m/Y');?>

<?php print views_embed_view('snow_feeds'); ?>
<?php
global $user;
 if (in_array('administrator', $user->roles)) {
	print "<div class='contextual-tabs'>". views_embed_view('node', 'contextual_link'). "</div>";
} ?>

<div class="left-box col-md-9 col-sm-8 col-xs-6">
		<div class="video-node-content">
			<div class="breadcrumb-box">
				<div class="breadcrumb-inner">
					<?php   
					$bcarr = drupal_get_breadcrumb();
					$bcarr [] = drupal_get_title();
					print  theme('breadcrumb', array('breadcrumb'=>$bcarr));
					?>
				</div>
			</div>
			<div class="article-title">	<h1><?php print $title; ?></h1></div>
			<div class="dek"><p><?php print $content['field_sponsor']['#object']->field_dek['und'][0]['value'];?></p></div>
			<div class="posted"><p><?php print $formatted_date; ?></p></div>						
			<div class="main_body"><?php print $content['field_sponsor']['#object']->body['und'][0]['value']; ?></div>		
		</div>	
</div>

<div class="right-box col-md-3 col-sm-4 col-xs-6 no-r-padding">
		<div class="nav-sidevar">
		<?php $block = module_invoke('block', 'block_view', '4');
			print render($block['content']); ?> 
		</div>
		
		<div class="color-title pink isotope-element video-home">
	<h3><span>Most Recent SKI Videos</span></h3>
	<div class="video-container">
		
		
		<div class="pane-content">
<p><!-- Start of Brightcove Player --><!-- Start of Brightcove Player --></p>
<!--
By use of this code snippet, I agree to the Brightcove Publisher T and C
found at <a href="https://accounts.brightcove.com/en/terms-and-conditions/" title="https://accounts.brightcove.com/en/terms-and-conditions/">https://accounts.brightcove.com/en/terms-and-conditions/</a>.
--><!--
By use of this code snippet, I agree to the Brightcove Publisher T and C
found at <a href="https://accounts.brightcove.com/en/terms-and-conditions/" title="https://accounts.brightcove.com/en/terms-and-conditions/">https://accounts.brightcove.com/en/terms-and-conditions/</a>.
-->
<script src="http://admin.brightcove.com/js/BrightcoveExperiences.js" type="text/javascript" language="JavaScript"></script>
<object width="300" height="592" type="application/x-shockwave-flash" data="http://c.brightcove.com/services/viewer/federated_f9?&amp;width=300&amp;height=592&amp;flashID=myExperience&amp;bgcolor=%23FFFFFF&amp;playerID=1964542304001&amp;playerKey=AQ~~%2CAAAAAEwRm8I~%2COusBxQT0iGh1UQjskO9PU6xQBk91cKuu&amp;isVid=true&amp;isUI=true&amp;dynamicStreaming=true&amp;autoStart=&amp;debuggerID=&amp;startTime=1447152433812" id="myExperience" class="BrightcoveExperience" seamlesstabbing="undefined"><param name="allowScriptAccess" value="always"><param name="allowFullScreen" value="true"><param name="seamlessTabbing" value="false"><param name="swliveconnect" value="true"><param name="wmode" value="window"><param name="quality" value="high"><param name="bgcolor" value="#FFFFFF"></object><!--
This script tag will cause the Brightcove Players defined above it to be created as soon
as the line is read by the browser. If you wish to have the player instantiated only after
the rest of the HTML is processed and the page load is complete, remove the line.
--><!--
This script tag will cause the Brightcove Players defined above it to be created as soon
as the line is read by the browser. If you wish to have the player instantiated only after
the rest of the HTML is processed and the page load is complete, remove the line.--><script type="text/javascript">brightcove.createExperiences();</script><!-- End of Brightcove Player --><!-- End of Brightcove Player --><p></p>
  </div>
		
		
		          <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

                    asd2('.video-bxslider').bxSlider({
                      //pagerCustom: '#video-bx-pager',
                      //controls: false
                    });*/
                    
                    
					/*var slider = $('.bxslider').bxSlider({
						pagerCustom: '#bx-pager'
					});	*/	
									-->			
		
    </div>
</div>	
	
		<div class="innr-box">		
			<h3><span>Popular</span></h3>
			<div class="innr-list">
				 <div class="block">
					<?php
					$view = views_get_view('popular_at_homepage');
					print $view->execute_display('default', NULL);
			//		print views_embed_view('popular_at_homepage','block_popular');
			//		print views_embed_view('sidebar_content', 'block'); ?>


					
				</div>
			</div>
		</div>

		<div class="innr-box">
			<div class="innr-box">	
				<h3><span>Recent</span></h3>
				<div class="innr-list">
					<div class="block">
						<?php
				/*		$view = views_get_view('recent_posts');
						print $view->execute_display('default', NULL); */
						
						print views_embed_view('sidebar_content', 'recent_post'); 
						?>
					</div> 
				</div>
			</div>
		</div>

		<div class="innr-box">
				<span class="img">
							<img alt="" src="./sites/all/themes/sevenmag_custom/images/b300.jpg">
					 </span>
			</div>


		<div class="innr-box">
				<span class="img">
							<img alt="" src="./sites/all/themes/sevenmag_custom/images/b300.jpg">
					 </span>
			</div>

	
			
			
</div>



