<?php
/**
 * Implements theme_menu_tree_MENU_NAME.
 */
function sevenmag_custom_menu_tree__main_menu($variables) {
  if (!preg_match("/\bfa-angle-down\b/i", $variables['tree'])) {
    return '<ul class="dropdown-menu">' . $variables['tree'] . '</ul>';
  }
  return '<ul class="nav navbar-nav">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_menu_link.
 */
function sevenmag_custom_menu_link($variables) {
  $element = $variables['element'];
  $sub_menu = '';
  
  $element['#attributes']['class'][] = 'dropdown menu-' .  $element['#original_link']['mlid'];
  
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $li_class = '';
  if (($element['#href'] == $_GET['q'] || ($element['#href'] == '<front>' && drupal_is_front_page()))) {
    $li_class = ' class="active"';
  }

  if ($element['#href'] == '<front>') {
    $element['#localized_options']['html'] = TRUE;
    $output = l('<i class="fa fa-home"></i>', $element['#href'], $element['#localized_options']);
    return '<li' . $li_class . '>' . $output . $sub_menu . "</li>\n";
  }
  elseif (!$sub_menu) {
    $output = l($element['#title'] , $element['#href'], $element['#localized_options']);
    return '<li' . $li_class . '>' . $output . $sub_menu . "</li>\n";
  }
  else {
    $element['#localized_options']['html'] = TRUE;
    $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
  //  $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
  //  $element['#localized_options']['attributes']['role'] = 'button';
   // $element['#localized_options']['attributes']['aria-haspopup'] = 'true';
  //  $element['#localized_options']['attributes']['aria-expanded'] = 'false';
    $add = ($sub_menu) ? ' <i class="fa fa-angle-down"></i>' : '';
    $output = l($element['#title'] . $add, $element['#href'], $element['#localized_options']);
    return '<li '.drupal_attributes($element['#attributes']).'>' . $output . $sub_menu . "</li>\n";
  }

}

/**
 * Implements theme_menu_tree_MENU_NAME.
 */
function sevenmag_custom_menu_tree__menu_footer_nav($variables) {
  return '<ul class="footer-menu">' . $variables['tree'] . '</ul>';
}


function sevenmag_custom_preprocess_node(&$vars, $hook){  
    $vars['template_files'][] = 'node-'. $vars['node']->nid; 
    

	 
if (isset($vars['node'])) {
        // mod rpc, inject renderable version of $tabs into node object
        $vars['node']->tabs=$vars['tabs']; 
        $vars['page']['changed'] = $vars['node']->changed;
        $vars['page']['name'] = preg_replace("/.*? by (.*?)\./", '\1', $vars['node']->log); //$vars['node']->name;
      $vars['theme_hook_suggestions'][] = 'page__'. $vars['node']->type;
   }

}

function sevenmag_custom_preprocess_page(&$variables, $hook)
{
	  $variables['tabs'] = theme('menu_local_tasks');

	   print_r($variables['tabs']);
	   exit;
	  
      
  /* if (!empty($variables['node']) && $variables['node']->type == 'article' && in_array('editor', $variables['user']->  roles))
    {
    $to_be_removed = array('node/%/edit', 'node/%/view');
    foreach ($variables['tabs'] as $group_key =>$tab_group)
        {
            if (is_array($tab_group))
            {
                foreach ($tab_group as $key =>$tab)
                {
                    if (isset($tab['#link']['path']) && in_array($tab['#link']['path'], $to_be_removed))
                    {
                    unset($variables['tabs'][$group_key][$key]);
                    }
                }
            }
        }
    }*/
}





