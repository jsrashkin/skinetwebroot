<!-- Header -->
<div id="">

  <div class="wrap">

    <!-- Top header elements -->

    <div class="top-header">

      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2 logo">
            <?php
            if($logo){
              ?>
              <a href="<?php print check_url($front_page); ?>"><img src="<?php print $logo; ?>"/></a>
              <?php }?>
            </div>

            <div class="col-md-5">
              <img src="<?php echo path_to_theme();?>/images/advertise.png" class="advertise">
            </div>
            <div class="col-md-5 register">
              <div class="container-fluid p-socials">
                <div class="col-md-6">
                  <div class="top-socials">
                    <a href="#" class="facebook">
                      <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="twitter">
                      <i class="fa fa-twitter"></i>
                    </a>
                    <a href="#" class="dribbble">
                      <i class="fa fa-dribbble"></i>
                    </a>
                    <a href="#" class="google">
                      <i class="fa fa-google-plus"></i>
                    </a>
                    <a href="#" class="pinterest">
                      <i class="fa fa-pinterest"></i>
                    </a>
                  </div>
                </div>
              <div class="col-md-6">
                <div class="container-fluid search-box">
                  <input type="text" class="search-input" placeholder="Search....">
                  <div class="search-title">
                    <i class="fa fa-search b-search"></i>
                  </div>
                  <script type="text/javascript">
                    jQuery('.search-title').click(function () {
                      jQuery('.search-input').toggleClass('active');
                    });
                  </script>
                </div>
              </div>
              <div class="col-md-6">
                <div class="top-menu">
                  <ul class="t-menu">
                    <li><a href=""><i class="fa fa-plus"></i> Register</a></li>
                    <li><a href=""><i class="fa fa-user"></i> Login</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

    <!-- End top header -->
