<!DOCTYPE html>
<html lang="<?php print $language->language; ?>">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?><?php print $scripts; ?><?php print $head; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
<div id="page">
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</div>
</body>
</html>
