<?php
// Plugin definition
$plugin = array(
  'title' => t('Homepage'),
  'category' => t('ski'),
  'theme' => 'ski_homepage',
  'admin theme' => 'ski_homepage_admin',
  'admin css' => 'homepage_admin.css',
  'regions' => array(
    'top' => t('top'),
    'content' => t('content'),
  )
);
