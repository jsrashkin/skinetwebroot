<?php
/**
 * @file
 * Template for a site template panel layout.
 *
 */
?>
<div class="panel-master clearfix panel-display" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="row-header panel-panel">
    <div class="inside"><?php print $content['top']; ?></div>
  </div>
  <div class="row-wrapper panel-panel">
      <?php print $content['content']; ?>
  </div>
</div>
