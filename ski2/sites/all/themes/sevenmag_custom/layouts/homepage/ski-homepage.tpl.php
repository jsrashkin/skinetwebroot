<?php
/**
 * @file
 * Site template for a front-end.
 */
?>
<?php if ($content['top']): ?>
  <?php print $content['top']; ?>
<?php endif; ?>
<?php $path = drupal_get_path('theme', 'sevenmag_custom');?>
<!-- Columns Home -->
<div class="row"><div class="wrap home-content-inner">	  
    <!-- First Column -->
    <div class="cols block-p">
      <?php if ($content['content']): ?>
        <?php  print $content['content']; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
