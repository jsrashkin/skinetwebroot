<?php
// Plugin definition
$plugin = array(
  'title' => t('Site template'),
  'category' => t('ski'),
  'theme' => 'ski_master',
  'admin theme' => 'ski_master_admin',
  'admin css' => 'master_admin.css',
  'regions' => array(
    'header' => t('Header'),
    'content' => t('Content'),
    'quicklinks' => t('Quicklinks'),
    'sidebar' => t('Sidebar right'),
    'footer' => t('Footer'),
  )
);
