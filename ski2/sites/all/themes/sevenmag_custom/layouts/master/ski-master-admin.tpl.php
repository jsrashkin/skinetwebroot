<?php
/**
 * @file
 * Template for a site template panel layout.
 *
 */
?>
<div class="panel-master clearfix panel-display" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="row-header panel-panel">
    <div class="inside"><?php print $content['header']; ?></div>
  </div>
  <div class="row-header panel-panel">
    <div class="inside"><?php print $content['quicklinks']; ?></div>
  </div>
  <div class="row-wrapper panel-panel">
    <div class="inside panel-col-left">
      <?php print $content['content']; ?>
    </div>
    <div class="inside panel-col-right">
      <?php print $content['sidebar']; ?>
    </div>
  </div>
  <div class="row-wrapper panel-panel">
    <div class="inside"><?php print $content['footer']; ?></div>
  </div>
</div>
