<?php
/**
 * @file
 * Site template for a front-end.
 */
global $base_path;
?>

<div id="page">
  <?php if ($content['header']): ?>
    <?php print $content['header']; ?>
  <?php endif; ?>

  <!-- Content Of Home page -->
  
  <div class="tabs-custom">
	 <?php if (!empty($tabs)): ?>
          <?php print render($tabs); ?>
        <?php endif; ?>

        <?php if (!empty($page['help'])): ?>
          <?php print render($page['help']); ?>
        <?php endif; ?>

        <?php if (!empty($action_links)): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>  

  </div>
  
    <div class="tabs-custom2">
  <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
  
   </div>
  <div class="container container-main">

    <?php if ($content['content']): ?>
      <?php print $content['content']; ?>
    <?php endif; ?>
  </div>
  <!-- End content home -->
<?php
$path = drupal_get_path('theme', 'sevenmag_custom');
?>
  <?php if ($content['footer']): ?>
  <!-- Footer -->
  <div id="footer">
    <div class="wrap">
     <!-- <div class="container-fluid">

      </div>-->
	<div class="container-fluid">
              <div class="row">
                <div class="col-md-3 col-sm-6">

                  <!-- Post List-->
  <?php/*
$PopularQ = new EntityFieldQuery();
$PopularQ->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'popular')
  ->propertyOrderBy('nid','desc')
  ->propertyCondition('status', NODE_PUBLISHED)
  ->range(0,4); // Run the query as user 1.
$Popular = $PopularQ->execute();

$RecentQ = new EntityFieldQuery();
$RecentQ->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'recent_posts')
  ->propertyOrderBy('nid','desc')
  ->propertyCondition('status', NODE_PUBLISHED)
  ->range(0,4); // Run the query as user 1.

$Recent = $RecentQ->execute();
*/
?>
	<div class="post-list">
		<h4>Recent</h4>
		<div class="container-list">
		<?php /*if(!empty($Recent)):?>
			<?php foreach($Recent['node'] as $r1):?>
				<?php $rd1=node_load($r1->nid);?>
				<div class="list-item">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-4">
								<img src="<?php echo $GLOBALS['base_url'].'/files/'.$rd1->field_recent_image['und'][0]['filename'];?>">
							</div>
							<div class="col-md-8">
								<h5><a href="<?php echo empty($rd1->page_title) ? $GLOBALS['base_url'].'/node/'.$r1->nid : $rd1->page_title;?>"><?php echo $rd1->title?> ...</a></h5>
								<p><?php echo date('m/d/Y',$rd1->created);?></p>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach;?>
		<?php endif;*/ ?>

		   <?php

		   $view = views_get_view('recent_post_at_footer');
		   print $view->execute_display('default', NULL);

           ?>

	</div>
</div>
                  <!-- End Post List-->

                </div>
                <div class="col-md-3 col-sm-6">

                  <!-- Post List-->

                  <div class="post-list">
                    <h4>Popular</h4>
                    <div class="container-list">
                      <?php /*if(!empty($PopularQ)):?>
							<?php foreach($PopularQ->ordered_results as $r):?>
								<?php $rd=node_load($r->entity_id);?>
								<div class="list-item">
									<div class="container-fluid">
										<div class="row">
											<div class="col-md-4">
												<img src="<?php echo $GLOBALS['base_url'].'/files/'.$rd->field_popular_img['und'][0]['filename'];?>">
											</div>
											<div class="col-md-8">
												<h5><a href="<?php echo empty($rd->page_title) ? $GLOBALS['base_url'].'/node/'.$r->entity_id : $rd->page_title;?>"><?php echo $rd->title?> ...</a></h5>
												<p><?php echo date('m/d/Y',$rd->created);?></p>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach;?>
						<?php endif;*/?>

						    <?php

						       $view = views_get_view('popular_at_homepage_footer');
                               print $view->execute_display('default', NULL);


                            ?>


                     </div>
                  </div>
                  <!-- End Post List-->

                </div>
                


                <div class="col-md-3 col-sm-6 slideshow_footer">

                  <!-- Slideshow Footer -->

                    <div class="slideshow-container">

                     <h4>Slideshow</h4>
                	<?php print views_embed_view('flex_slideshow_footer'); ?>


                        </div>

                  <!-- End Slideshow Footer -->
                </div>
                <div class="col-md-3 about col-sm-6 ">
                  <img src="<?php echo $base_path.$path . '/';?>images/logo-footer.png">
                  <p>King of the mountains. SKI is the go-to-guide for passionate skiers and their families. SKI embraces skiers of all attitudes and abilities, delivering the action of the world’s most exciting sport without any of the arrogance. Hot trends. The best gear. Expert instruction. Insider advice on mountain destinations. We’ve got you covered.</p><br>
                  <div class="socials-footer">
          <!--          <a href="" class="dribbble-footer"><i class="fa fa-dribbble"></i></a> 
                    <a href="" class="google-footer"><i class="fa fa-google-plus"></i></a> -->
                    <a href="http://www.twitter.com/SkiMagOnline" class="twitter-footer"><i class="fa fa-twitter"></i></a>
                    <a href="http://www.facebook.com/SkiMag" class="facebook-footer"><i class="fa fa-facebook"></i></a>
            <!--        <a href="" class="rss-footer"><i class="fa fa-rss"></i></a>
                    <a href="" class="github-footer"><i class="fa fa-github"></i></a>
                    <a href="" class="linkedin-footer"><i class="fa fa-linkedin-square"></i></a> -->
                    <a href="https://www.pinterest.com/skimagazine" class="pinterest-footer"><i class="fa fa-pinterest-square"></i></a>
                    <a href="https://instagram.com/SkiMagazine" class="instagram-footer"><i class="fa fa-instagram"></i></a>
                  </div>
                </div>
              </div>
	         <?php print $content['footer']; ?>
            </div>
    </div>
  </div>
  
  
  

  
  <script>
	
	jQuery(document).ready(function() {
	  
	jQuery('.dropdown .dropdown-toggle').hover(
	function(){jQuery(this).parent().addClass('open');}
	);
	
	jQuery( ".dropdown" ).mouseleave(function() {
      jQuery(this).removeClass('open');
    });
	
	});	  
	  
</script>
  
  
    <!-- End Footer -->
  <?php endif; ?>
</div>
