
<h4><?php echo $fields['title']->raw; ?></h4>
<p><strong><?php echo date('d/m/Y', $fields['created']->raw)?></strong></p>
<p class="excerpt"><?php print $fields['field_dek']->content; ?></p><br>
<?php
print l(t('Read more'), 'node/' . $fields['nid']->raw, array('attributes' => array('class' => 'read-more')));
?>