<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>


<?php if ($rows): ?>
 <!-- <div class="color-title pink isotope-element photos-home">
    <h3><span><?php echo t('Recent Posts'); ?></span></h3>
    <div class="border-color-container">
      <div class="description"> -->
            <?php print $rows; ?>
            
           
  <!--    </div>
    </div>
  </div> -->
<?php 
/*
$path = drupal_get_path('theme', 'sevenmag_custom');
?>
<div class="color-title pink isotope-element" style="position: absolute; left: 891px; top: 0px;">
	<h3><span>VIDEOS</span></h3>
	<div class="border-color-container">
		<div class="row">
	    	<div class="small-slider">
                	<div class="col-md-12">
                        	<div class="bx-wrapper" style="max-width: 100%;">
					<div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 173px;">
						<ul class="video-bxslider" style="width: 515%; position: relative; -webkit-transition: 0s; transition: 0s; -webkit-transform: translate3d(-280px, 0px, 0px);"><li style="float: left; list-style: none; position: relative; width: 240px;" class="bx-clone">
                            <img src="<?php echo $path . '/';?>images/slider/video-slider-3.jpg">
                            <div class="slide-caption">
                              <h4>Pro Skier’s: Summer Training...</h4>
                              <p>10/12/2015</p>
                            </div>
                          </li>
                          <li style="float: left; list-style: none; position: relative; width: 240px;">
                            <img src="<?php echo $path . '/';?>images/slider/video-slider-1.jpg">
                            <div class="slide-caption">
                              <h4>Winter Lab: Scott Alpride...</h4>
                              <p>08/12/2015</p>
                            </div>
                          </li>
                          <li style="float: left; list-style: none; position: relative; width: 240px;">
                            <img src="<?php echo $path . '/';?>images/slider/video-slider-2.jpg">
                            <div class="slide-caption">
                              <h4>Lorem Ipsum</h4>
                              <p>09/12/2015</p>
                            </div>
                          </li>
                          <li style="float: left; list-style: none; position: relative; width: 240px;">
                            <img src="<?php echo $path . '/';?>images/slider/video-slider-3.jpg">
                            <div class="slide-caption">
                              <h4>Pro Skier’s: Summer Training...</h4>
                              <p>10/12/2015</p>
                            </div>
                          </li>
                        <li style="float: left; list-style: none; position: relative; width: 240px;" class="bx-clone">
                            <img src="<?php echo $path . '/';?>images/slider/video-slider-1.jpg">
                            <div class="slide-caption">
                              <h4>Winter Lab: Scott Alpride...</h4>
                              <p>08/12/2015</p>
                            </div>
                          </li></ul></div><div class="bx-controls"></div></div>
                      </div>

                      <div class="col-md-12">
                        <div id="video-bx-pager">
                          <a data-slide-index="0" href="" class="control-top-slider active">
                            <div class="col-md-3">
                              <div class="row">
                              <img src="<?php echo $path . '/';?>images/slider/min-v-slider-1.jpg">
                              </div>
                            </div>
                            <div class="col-md-9">
                              <h4>How Skiing and Mountain Biking...</h4>
                              <p>08/12/2015</p>
                            </div>
                          </a>
                          <a data-slide-index="1" href="" class="control-top-slider">
                            <div class="col-md-3">
                              <div class="row">
                              <img src="<?php echo $path . '/';?>images/slider/min-v-slider-2.jpg">
                              </div>
                            </div>
                            <div class="col-md-9">
                              <h4>Part 5: Icelantic Skis...</h4>
                              <p>09/11/2015</p>
                            </div>
                          </a>
                          <a data-slide-index="2" href="" class="control-top-slider">
                            <div class="col-md-3">
                              <div class="row">
                              <img src="<?php echo $path . '/';?>images/slider/min-v-slider-3.jpg">
                              </div>
                            </div>
                            <div class="col-md-9">
                              <h4>Bobsledding at the Utah ...</h4>
                              <p>10/12/2015</p>
                            </div>
                          </a>
                        </div>
                      </div>
                      
                    </div>
                    </div>

                    <script type="text/javascript">
                    jQuery('.video-bxslider').bxSlider({
                      pagerCustom: '#video-bx-pager',                  
                      controls: false
                    });
                    </script>

                  </div>
                </div>
<img src="<?php echo $path . '/';?>images/b300.jpg" class="img-block isotope-element photos-home" style="position: absolute; left: 594px; top: 233px;">
<div class="color-title blue isotope-element big" style="position: absolute; left: 0px; top: 527px;">
                  <h3><span>GEAR REVIEWS</span></h3>
                  <div class="border-color-container">
                    <div class="row">
                      <div class="col-md-12 hide">
                        <div class="bx-wrapper" style="max-width: 100%;"><div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 0px;"><ul class="gear-bxslider" style="width: 515%; position: relative; -webkit-transition: 0s; transition: 0s; -webkit-transform: translate3d(0px, 0px, 0px);"><li style="float: left; list-style: none; position: relative; width: 100px;" class="bx-clone"><img src="<?php echo $path . '/';?>images/slider/s.jpg"></li>
                          <li style="float: left; list-style: none; position: relative; width: 100px;"><img src="<?php echo $path . '/';?>images/slider/s.jpg"></li>
                          <li style="float: left; list-style: none; position: relative; width: 100px;"><img src="<?php echo $path . '/';?>images/slider/s.jpg"></li>
                          <li style="float: left; list-style: none; position: relative; width: 100px;"><img src="<?php echo $path . '/';?>images/slider/s.jpg"></li>
                        <li style="float: left; list-style: none; position: relative; width: 100px;" class="bx-clone"><img src="<?php echo $path . '/';?>images/slider/s.jpg"></li></ul></div><div class="bx-controls"></div></div>
                      </div>
                      <div class="col-md-12">
                        <div class="row container-gear">
                          <div class="col-md-4">
                            <a data-slide-index="0" href="" class="control-top-slider active">
                            
                              <img src="<?php echo $path . '/';?>images/gear/1.jpg">
                              </a><div class="item-ski nomargin"><a data-slide-index="0" href="" class="control-top-slider">
                                <h5>2015 Gear Trend<br>Report</h5>
                                <p>08/11/2015</p>
                                <br>
                                </a><a href="#" class="read-more">Read More</a>
                              </div>
                            
                            
                          </div>
                          <div class="col-md-4">
                            <a data-slide-index="1" href="" class="control-top-slider">
                            
                              <img src="<?php echo $path . '/';?>images/gear/2.jpg">
                              </a><div class="item-ski nomargin"><a data-slide-index="1" href="" class="control-top-slider">
                                <h5>Best Men’s Powder<br>Skis of 2015</h5>
                                <p>07/25/2015</p>
                                <br>
                                </a><a href="#" class="read-more">Read More</a>
                              </div>
                            
                            
                          </div>
                          <div class="col-md-4">
                            <a data-slide-index="2" href="" class="control-top-slider">
                            
                              <img src="<?php echo $path . '/';?>images/gear/3.jpg">
                              </a><div class="item-ski nomargin"><a data-slide-index="2" href="" class="control-top-slider">
                                <h5>Buckle Up: It’s Time<br>to Test Skis</h5>
                                <p>07/13/2015</p>
                                <br>
                                </a><a href="#" class="read-more">Read More</a>
                              </div>
                            
                            
                          </div>
                        </div> 
                      </div>
                    </div>                   
                  </div>
                </div>
<div class="color-title black isotope-element popular-home" style="position: absolute; left: 891px; top: 497px;">
                  <h3><span>Popular</span></h3>
                  <div class="border-color-container">
                  <div class="block">
                    <div class="item-ski">
                      <a href="#">
                        <div class="col-md-4 little-padding">
                          <img src="<?php echo $path . '/';?>images/popular/popular-1.jpg">
                        </div>
                        <div class="col-md-8 little-padding">
                          <h5>Diner Dossier: Best int the East</h5>
                          <p>08/15/2015</p> 
                        </div>
                      </a>
                    </div>
                    <div class="item-ski">
                      <a href="#">
                        <div class="col-md-4 little-padding">
                          <img src="<?php echo $path . '/';?>images/popular/popular-2.jpg">
                        </div>
                        <div class="col-md-8 little-padding">
                          <h5>17 Reasons to Ski France ...</h5>
                          <p>08/14/2015</p>
                        </div>
                      </a>
                    </div>
                  </div>
                  </div>
                </div>
<div class="color-title black isotope-element like-home" style="position: absolute; left: 594px; top: 538px;">
                  <h3><span>Like Us</span></h3>
                  <div class="border-color-container">
                    <div class="row little-padding">
                      <img src="<?php echo $path . '/';?>images/facebook.jpg">
                    </div>
                  </div>
                </div>
<div class="color-title black isotope-element recent-home" style="position: absolute; left: 891px; top: 739px;">
                  <h3><span>Recent</span></h3>
                  <div class="border-color-container">
                    <div class="block">
                    <div class="item-ski">
                      <a href="#">
                        <div class="col-md-4 little-padding">
                          <img src="<?php echo $path . '/';?>images/recent/recent-0.jpg">
                        </div>
                        <div class="col-md-8 little-padding">
                          <h5>Used and Abused: Weekly Gear ...</h5>
                          <p>08/15/2015</p>
                        </div>
                      </a>
                    </div>
                    <div class="item-ski">
                      <a href="#">
                        <div class="col-md-4 little-padding">
                          <img src="<?php echo $path . '/';?>images/recent/recent-1.jpg">
                        </div>
                        <div class="col-md-8 little-padding">
                          <h5>Top Ski Resorts ...</h5>
                          <p>08/14/2015</p>
                        </div>
                      </a>
                    </div>
                    </div>
                  </div>
                </div>
<img src="<?php echo $path . '/';?>images/b300.jpg" class="img-block isotope-element" style="position: absolute; left: 594px; top: 825px;">
<img src="<?php echo $path . '/';?>images/b300.jpg" class="img-block isotope-element" style="position: absolute; left: 0px; top: 848px;">
<div class="color-title black isotope-element photos-home" style="position: absolute; left: 297px; top: 848px;">
                  <h3><span>FEATURED GALLERY</span></h3>
                  <div class="border-color-container">
                  <div class="block">
                      <img src="<?php echo $path . '/';?>images/featured.png">
                  </div>
                  </div>
                </div>
<div class="color-title black isotope-element" style="position: absolute; left: 891px; top: 981px;">
                  <h3><span>Instagram</span></h3>
                  <div class="border-color-container">
                    <div class="block">
                      <div class="one-half-social-info">
                        <div class="item-social">
                          <div>
                            <i class="fa fa-facebook"></i>
                          </div>
                          <div>
                            <p>2,597</p>
                            <span>Fans</span>
                          </div>
                        </div>
                        <div class="item-social">
                          <div>
                            <i class="fa fa-twitter"></i>
                          </div>
                          <div>
                            <p>1,568</p>
                            <span>Followers</span>
                          </div>
                        </div>
                        <div class="item-social">
                          <div>
                            <i class="fa fa-pinterest"></i>
                          </div>
                          <div>
                            <p>390k</p>
                            <span>Followers</span>
                          </div>
                        </div>
                        <div class="item-social">
                          <div>
                            <i class="fa fa-linkedin"></i>
                          </div>
                          <div>
                            <p>540</p>
                            <span>Subscribers</span>
                          </div>
                        </div>
                      </div>
                      <div class="one-half">
                        <div class="col-md-6 little-padding">
                          <img src="<?php echo $path . '/';?>images/instagram/instagram-1.jpg"> 
                        </div>
                        <div class="col-md-6 little-padding">
                          <img src="<?php echo $path . '/';?>images/instagram/instagram-2.jpg"> 
                        </div>

                        <div class="col-md-6 little-padding">
                          <img src="<?php echo $path . '/';?>images/instagram/instagram-3.jpg"> 
                        </div>
                        <div class="col-md-6 little-padding">
                          <img src="<?php echo $path . '/';?>images/instagram/instagram-4.jpg"> 
                        </div>

                        <div class="col-md-6 little-padding">
                          <img src="<?php echo $path . '/';?>images/instagram/instagram-5.jpg"> 
                        </div>
                        <div class="col-md-6 little-padding">
                          <img src="<?php echo $path . '/';?>images/instagram/instagram-6.jpg"> 
                        </div>

                        <div class="col-md-6 little-padding">
                          <img src="<?php echo $path . '/';?>images/instagram/instagram-7.jpg"> 
                        </div>
                        <div class="col-md-6 little-padding">
                          <img src="<?php echo $path . '/';?>images/instagram/instagram-8.jpg"> 
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
<div class="color-title light-orange isotope-element" style="position: absolute; left: 594px; top: 1130px;">
                  <h3><span>MAGAZINE</span></h3>
                  <div class="border-color-container">
                    <div class="row little-padding">
                      <img src="<?php echo $path . '/';?>images/magazine.jpg">
                      <a href="#" class="read-more orange">Read More ...</a><br>
                      <a href="#" class="subscribe orange">Subscribe</a>
                    </div>
                  </div>
                </div>
<div class="color-title black isotope-element home-form" style="position: absolute; left: 0px; top: 1153px;">
                  <h3><span>Subscribe</span></h3>
                  <div class="border-color-container">
                    <form action="" method="post" class="subscribe-form">
                      <input type="email" placeholder="Email">
                      <p style="text-align:right;">
                        <input type="submit" value="Submit">
                      </p>
                      
                    </form>
                  </div>
                </div>
<div class="color-title yellow isotope-element" style="position: absolute; left: 0px; top: 1153px;">
                  <h3><span>SKI INSTRUCTION</span></h3>
                  <div class="border-color-container">
                    <div class="row">
                      <div class="col-md-12">
                      <div class="bx-wrapper" style="max-width: 100%;"><div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 268px;"><ul class="ski-bxslider" style="width: 415%; position: relative; -webkit-transition: 0s; transition: 0s; -webkit-transform: translate3d(-280px, 0px, 0px);"><li style="float: left; list-style: none; position: relative; width: 240px;" class="bx-clone">
                          <img src="<?php echo $path . '/';?>images/instruction/instruction-1.jpg">
                        <h4>How to Start Your Kids Skiing...</h4>
                        <p><span class="black">08/14/2015</span></p>
                        </li>
                        <li style="float: left; list-style: none; position: relative; width: 240px;">
                          <img src="<?php echo $path . '/';?>images/instruction/instruction-1.jpg">
                        <h4>How to Start Your Kids Skiing...</h4>
                        <p><span class="black">08/14/2015</span></p>
                        </li>
                        <li style="float: left; list-style: none; position: relative; width: 240px;">
                          <img src="<?php echo $path . '/';?>images/instruction/instruction-1.jpg">
                        <h4>How to Start Your Kids Skiing...</h4>
                        <p><span class="black">08/14/2015</span></p>
                        </li>
                      <li style="float: left; list-style: none; position: relative; width: 240px;" class="bx-clone">
                          <img src="<?php echo $path . '/';?>images/instruction/instruction-1.jpg">
                        <h4>How to Start Your Kids Skiing...</h4>
                        <p><span class="black">08/14/2015</span></p>
                        </li></ul></div><div class="bx-controls"></div></div>
                    </div>
                    <div class="col-md-12">
                      <div class="item-ski" id="ski-bx-pager">
                        <a data-slide-index="0" href="" class="control-top-slider active">
                          <div class="col-md-4 little-padding">
                            <img src="<?php echo $path . '/';?>images/instruction/instruction-2.jpg">
                          </div>
                          <div class="col-md-8 little-padding">
                            <h5>Get Fit: Suspension Workout</h5>
                            <p>08/12/2015</p>
                          </div>
                        </a>
                        <a data-slide-index="1" href="" class="control-top-slider">
                          <div class="col-md-4 little-padding">
                            <img src="<?php echo $path . '/';?>images/instruction/instruction-2.jpg">
                          </div>
                          <div class="col-md-8 little-padding">
                            <h5>Secrets from a Ski Team Tech</h5>
                            <p>08/11/2015</p>
                          </div>
                        </a>
                      </div>
                    </div>
                    </div>

                    <script type="text/javascript">
                      
                    </script>

                  </div>
                </div>
<div class="color-title olive isotope-element photos-home" style="position: absolute; left: 297px; top: 1180px;">
                  <h3><span>NEWSLETTER SIGNUP</span></h3>
                  <div class="border-color-container">
                  <div class="block">
                      <h4>Sign-up to receive daily updates, news, tips and more!</h4>
                      <input type="text" value=""><br>
                      <div class="news-signup">
                          <a href="#" class="subscribe olive">Sign-up</a>
                      </div>
                  </div>
                  </div>
                </div>
<div class="bottom_add" style="position: absolute; left: 297px; top: 1537px;">
                <img src="<?php echo $path . '/';?>images/advertise.png">
                </div>
                
     */?>
                
<?php endif; ?>
