<?php /*<div class="channel-item">
  <div class="channel-image">
  <?php if ($image): ?>
    <?php print $image; ?>
  <?php endif; ?>
  </div>
  <div class="channel-title">
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  </div>
  <div class="channel-dek">
  <?php if ($dek): ?>
    <?php print $dek; ?>
  <?php endif; ?>
  </div>
</div>*/
$path = drupal_get_path('theme', 'sevenmag_custom');
?>
<div class="color-title orange full-t">
	<div class="border-color-container">
		<div class="row little-padding">
			<div class="thumb-cat">
				<img src="<?php echo $path . '/';?>images/category/1.jpg">
                <div class="custom-title">
					<?php if ($title): ?>
						<h2><?php print $title; ?></h2>
					<?php endif; ?>	
					<p><strong>08/14/2015</strong></p>
				</div>
			</div>
            <div class="content-article">
				<p>We’re bringing back the best pow, gear, and mountain shots from days of yore.</p>
                <a href="" class="read-more">Read More</a>
			</div>
		</div>
	</div>
</div>
