<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<!-- Title Home -->
<?php //drupal_add_js('http://code.jquery.com/jquery-1.11.3.min.js', 'external'); ?>
<?php $trending_news = views_embed_view('trending_news'); 
 ?>
<div class="row">
  <div class="wrap">
    <h5 class="hide" style="text-align:center"><?php print t('Swipe images from side-to-side to view more stories.'); ?> </h5>
    <div class="container-full-title">
      <div class="col-md-2">
        <div class="left-title">
          <?php print t('News Trending'); ?>
        </div>
      </div>    
      <div class="news-two-items col-md-8 marquee">
        <?php if ($rows): ?>
        <marquee> <span class="newstrend"><?php print $trending_news;?></span><?php print $rows; ?></marquee>
        <?php endif; ?>
      </div>
      <div class="col-md-2">
        <div class="right-title">
        </div>
      </div>
    </div>
  </div>
</div>
<script>
var get_html=jQuery('.newstrend .view-content').html();
	if(get_html == 'undefined'){
		get_html='';
	}else{
		jQuery('.newstrend').replaceWith('<span class="news_trending">' +get_html+'</span>');
	}
//remove undefined 	
var get_news = jQuery('.news_trending').html();
//alert(aa);
if(get_news =='undefined'){
jQuery('.news_trending').hide();
}
// remove last divider 
jQuery(".divider:last").hide();	
</script> 
<!-- End Title Home -->
