<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>


<?php /* foreach ($fields as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>

  <?php print $field->wrapper_prefix; ?>
    <?php print $field->label_html; ?>
    <?php print $field->content; ?>
  <?php print $field->wrapper_suffix; ?>
<?php endforeach; */ ?>

<?php 

 //echo "<pre>";
// print print_r(array_keys(get_defined_vars()), 1);
  
?>

    <div class="col-sm-4">
	
		 <a data-slide-index="0" href="" class="control-top-slider active">
			 
			<?php  
			 
			print $fields['field_review_image']->content;
		 
		    ?> 
		</a>	
	
        <div class="item-ski nomargin">
			<a data-slide-index="0" href="" class="control-top-slider">
				<h5><?php print $fields['title']->content; ?></h5>
				<!--<p><strong><?php //echo date('d/m/Y', $fields['created']->raw)?></strong></p>-->
			</a>
			<div class="content-article"><p><?php print $fields['field_dek']->content; ?><p></div>

		<!--<a href="<?php  print $fields['nid']->content; ?>" class="read-more">Read More</a> -->
		
		<?php  print l(t('Read more'), 'node/' . $fields['nid']->raw, array('attributes' => array('class' => 'read-more'))); ?>
		
		</div> 

    </div>
