<?php
/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<?php //print views_embed_view('top_row_news'); ?>
<?php print views_embed_view('snow_feeds');  ?>

<div class="left-box col-md-9 col-sm-8 col-xs-6">



<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>
  
  <div class="breadcrumb-box">
<div class="breadcrumb-inner">
<?php   
$bcarr = drupal_get_breadcrumb();
$bcarr [] = drupal_get_title();
print  theme('breadcrumb', array('breadcrumb'=>$bcarr));
?>
</div>
</div>
  
<div class="tab-frame">
	<ul class="nav nav-tabs">
		    <li><a href="./gear-search">All</a></li>
            <li><a href="./gear-ski">Skis</a></li>
            <li class="active"><a href="./gear-boots">Boots</a></li>
            <li><a href="#sectionC">How We Test</a></li>
            <li><a href="./gear-apperel-accessories">Apparel</a></li>
        </ul>
</div>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div>
</div>
<div class="right-box col-md-3 col-sm-4 col-xs-6 no-r-padding">
	
	<div class="nav-sidevar innr-box">
    	<div class="innr-list">
		<?php $block = module_invoke('block', 'block_view', '4');
			print render($block['content']); ?> 
            </div>
		</div>
	
	
		<div class="innr-box">		
			<h3><span>Popular</span></h3>
			<div class="innr-list space-bottom-mob">
				 <div class="block">
					<?php
			//		$view = views_get_view('popular_at_homepage');
			//		print $view->execute_display('default', NULL);
			//		print views_embed_view('popular_at_homepage','block_popular');
					print views_embed_view('sidebar_content', 'block'); ?>


					
				</div>
			</div>
		</div>

	<div class="innr-box">
			<h3><span>Recent</span></h3>
			<div class="innr-list space-bottom-mob">
				<div class="block">
					<?php
			/*		$view = views_get_view('recent_posts');
					print $view->execute_display('default', NULL); */
					
					print views_embed_view('sidebar_content', 'recent_post'); 
					?>
				</div> 
			</div>
 	</div>

	<div class="google-ad-custom-block-right_1 advert-block">
		<div class="innr-box"> 
        	<div class="space-bottom-mob">
	        <?php $block = module_invoke('ad_manager', 'block_view', 'right1');
					print render($block['content']); ?> 
                    </div>
		</div>
	</div>

	<div class="color-title pink isotope-element video-home">
	<h3><span>Most Recent SKI Videos</span></h3>
	<div class="video-container space-bottom-mob">
		
		
		<div class="pane-content">

<!--
By use of this code snippet, I agree to the Brightcove Publisher T and C
found at <a href="https://accounts.brightcove.com/en/terms-and-conditions/" title="https://accounts.brightcove.com/en/terms-and-conditions/">https://accounts.brightcove.com/en/terms-and-conditions/</a>.
--><!--
By use of this code snippet, I agree to the Brightcove Publisher T and C
found at <a href="https://accounts.brightcove.com/en/terms-and-conditions/" title="https://accounts.brightcove.com/en/terms-and-conditions/">https://accounts.brightcove.com/en/terms-and-conditions/</a>.
-->
<script src="http://admin.brightcove.com/js/BrightcoveExperiences.js" type="text/javascript" language="JavaScript"></script>
<object width="300" height="592" type="application/x-shockwave-flash" data="http://c.brightcove.com/services/viewer/federated_f9?&amp;width=300&amp;height=592&amp;flashID=myExperience&amp;bgcolor=%23FFFFFF&amp;playerID=1964542304001&amp;playerKey=AQ~~%2CAAAAAEwRm8I~%2COusBxQT0iGh1UQjskO9PU6xQBk91cKuu&amp;isVid=true&amp;isUI=true&amp;dynamicStreaming=true&amp;autoStart=&amp;debuggerID=&amp;startTime=1447152433812" id="myExperience" class="BrightcoveExperience" seamlesstabbing="undefined"><param name="allowScriptAccess" value="always"><param name="allowFullScreen" value="true"><param name="seamlessTabbing" value="false"><param name="swliveconnect" value="true"><param name="wmode" value="window"><param name="quality" value="high"><param name="bgcolor" value="#FFFFFF"></object><!--
This script tag will cause the Brightcove Players defined above it to be created as soon
as the line is read by the browser. If you wish to have the player instantiated only after
the rest of the HTML is processed and the page load is complete, remove the line.
--><!--
This script tag will cause the Brightcove Players defined above it to be created as soon
as the line is read by the browser. If you wish to have the player instantiated only after
the rest of the HTML is processed and the page load is complete, remove the line.--><script type="text/javascript">brightcove.createExperiences();</script><!-- End of Brightcove Player --><!-- End of Brightcove Player -->
  </div>
		
		
		          <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

                    asd2('.video-bxslider').bxSlider({
                      //pagerCustom: '#video-bx-pager',
                      //controls: false
                    });*/
                    
                    
					/*var slider = $('.bxslider').bxSlider({
						pagerCustom: '#bx-pager'
					});	*/	
									-->			
	</div>
</div>

	<div class="google-ad-custom-block-right_3 advert-block">
		<div class="innr-box"> 
        <div class="space-bottom-mob">
	        <?php $block = module_invoke('ad_manager', 'block_view', 'right3');
					print render($block['content']); ?> </div>
		</div> 
	</div>
		<div class="innr-box green magazine-block ">	
			<h3><span>MAGAZINE</span></h3>
            <div class="space-bottom-mob">
			<ul class="innr-list">
				<li>
					<span class="img"><img src="http://skinet-construction.com/ski//sites/all/themes/sevenmag_custom/images/magazine.jpg"></span>
                    <div class="magazine_btn">
					<a href="http://media.aimmedia.com/skiing/skimag/newslettersignup.html" class="read-more">Read More ...</a><br>
					<a href="http://media.aimmedia.com/skiing/skimag/newslettersignup.html" class="subscribe green">Subscribe</a>
                    </div>
     			</li>
			</ul>
            </div>
		</div>
	             
<!--	<img src="<?php //echo $path . '/';?>images/b300.jpg" class="img-block"> -->
	
	<div class="google-ad-custom-block-home_3 advert-block">
		<div class="innr-box">       	
        <div class="space-bottom-mob">
	        <?php $block_b = module_invoke('ad_manager', 'block_view', 'home3');
					print render($block_b['content']); ?> 
		</div>
        </div>
	</div>
      
	
</div>

<script>
jQuery(function(){
		test();
	})	



function test()
{
	//alert('ok');
	jQuery('.holder').each(function() {
	
    var text = jQuery(this).text();
    jQuery(this).text(text.replace('<Any>', 'Any')); 
});


	}
</script>

<?php /* class view */ ?>
