<?php
// $Id: views-view-fields.tpl.php,v 1.6 2008/09/24 22:48:21 merlinofchaos Exp $
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->separator: an optional separator that may appear before a field.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

?>
<?php foreach ($fields as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>

	<?php if ($id == 'common_image'): // we need a wrapper around the details ?>
		<div class="gearfinder-item-wrapper">
	<?php endif; ?>

  <<?php print $field->inline_html;?> class="views-field-<?php print $field->class; ?>">
    <?php if ($field->label): ?>
      <label class="views-label-<?php print $field->class; ?>">
        <?php print $field->label; ?>:
      </label>
    <?php endif; ?>
		
		<?php
			// $field->element_type is either SPAN or DIV depending upon whether or not
			// the field is a 'block' element type or 'inline' element type.
			//ghetto rigged change this later - later being never.
			if($id == 'field_product_awards_value') {
				$node = node_load($row->nid);
				$awards = $node->field_product_awards;
				foreach ($awards as $award) {
					if ($award['value'] == 6) {
						$gold = '<div id="gold-medal-gear" class="ski-labeled">Gold Medal Gear</div>';
					}
					if ($award['value'] == 7) {
						$best = '<div id="best-value" class="ski-labeled">Best Value</div>';
					}
					if ($award['value'] == 8) {
						$rock = '<div id="rocker" class="ski-labeled">Rocker</div>';
					}
				}
				/*
				dpm($field->raw);
				// $awards[] = $field->raw;
				
				// $stuff = explode(',',$field->raw);
				$gold = null;
				$best = null;
				$rock = null;
				// dpm($awards, '$awards');
				// foreach($awards as $a) {
				  if($field->raw == 6) {
				    $gold = '<div id="gold-medal-gear" class="ski-labeled">Gold Medal Gear</div>';
				  }
					if($field->raw == 7) {
				    $best = '<div id="best-value" class="ski-labeled">Best Value</div>';
				  }
					if($field->raw == 8) {
				    $rock = '<div id="rocker" class="ski-labeled">Rocker</div>';
				  }
				// }
				*/
				if($gold): print $gold; endif; 
				if($best): print $best; endif; 
				if($rock): print $rock; endif;
			}
		?>

      <<?php print $field->element_type; ?> class="field-content"><?php print $field->content; ?></<?php print $field->element_type; ?>>
  </<?php print $field->inline_html;?>>

	<?php if ($id == 'nothing'): // close the wrapper ?>
		</div><!-- gearfinder-item-wrapper -->
	<?php endif; ?>
	
<?php endforeach; ?>
