<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="block-first-row">
	<div class="resort-desktop"><?php $view = views_get_view('resort_and_travel'); print $view->execute_display('default', NULL); ?></div>	
	<div class="resort-mobile space-bottom-mob">
		<div class="color-title orange"><h3><span><?php print t('RESORT & TRAVEL'); ?></span></h3>
			<?php $view = views_get_view('resort_and_travels_home'); print $view->execute_display('default', NULL);?>
		</div>
	</div>	
	<div class="box native-content">                
		<?php if ($rows): ?>
		  <div class="color-title san-juan space-bottom-mob hide-mobile"><h3><span><?php echo t('NATIVE CONTENT'); ?></span></h3>
			<div class="border-color-container">
			  <div class="description"><?php print $rows; ?></div>
			</div>
		  </div>
		  
		  <div class="google-ad-custom-block-home_1 advert-block space-bottom-mob">
			<div class="innr-box"><?php $block_a = module_invoke('ad_manager', 'block_view', 'home1'); print render($block_a['content']); ?></div>
		</div>
	</div>
                            
<?php
$path = drupal_get_path('theme', 'sevenmag_custom');
$nodes = node_load_multiple(array(), array('type' => 'videos'));
$PopularQ = new EntityFieldQuery();
$PopularQ->entityCondition('entity_type', 'node')
   //->entityCondition('bundle', 'article')
   ->entityCondition('bundle', 'Popular')
  ->propertyOrderBy('nid','desc')
  ->range(0,2)
  ->propertyCondition('status', NODE_PUBLISHED); // Run the query as user 1.
$Popular = $PopularQ->execute();

$RecentQ = new EntityFieldQuery();
$RecentQ->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'recent_posts')
  ->propertyOrderBy('nid','desc')
  ->range(0,2)
  ->propertyCondition('status', NODE_PUBLISHED); // Run the query as user 1.
$RecentQ = $RecentQ->execute();

$GearReviews = new EntityFieldQuery();
$GearReviews->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'gear_reviews')
  ->propertyOrderBy('nid','desc')
  ->range(0,3)
  ->propertyCondition('status', NODE_PUBLISHED); // Run the query as user 1.
$GearReviewsQ = $GearReviews->execute();

$SkiIns = new EntityFieldQuery();
$SkiIns->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'ski_instruction')
  ->propertyOrderBy('nid','desc')
  ->range(0,4)
  ->propertyCondition('status', NODE_PUBLISHED); // Run the query as user 1.
$SkiInsQ=$SkiIns->execute();

$SkiIns2 = new EntityFieldQuery();
$SkiIns2->entityCondition('entity_type', 'node')
  ->entityCondition('bundle', 'ski_instruction')
  ->propertyOrderBy('nid','desc')
  ->range(0,2)
  ->propertyCondition('status', NODE_PUBLISHED); // Run the query as user 1.
$SkiInsQ2=$SkiIns2->execute();

?>

<!-- Video Content start-->

<div class="box">                
<div class="color-title dark-tangerine video-home space-bottom-mob"><h3><span>Most Recent SKI Videos</span></h3>
	<div class="border-color-container">		
		<div class="pane-content">
<!--By use of this code snippet, I agree to the Brightcove Publisher T and C found at <a href="https://accounts.brightcove.com/en/terms-and-conditions/" title="https://accounts.brightcove.com/en/terms-and-conditions/">https://accounts.brightcove.com/en/terms-and-conditions/</a>.-->
<!--By use of this code snippet, I agree to the Brightcove Publisher T and C found at <a href="https://accounts.brightcove.com/en/terms-and-conditions/" title="https://accounts.brightcove.com/en/terms-and-conditions/">https://accounts.brightcove.com/en/terms-and-conditions/</a>.-->
<script src="http://admin.brightcove.com/js/BrightcoveExperiences.js" type="text/javascript" language="JavaScript"></script>
<object width="300" height="500" type="application/x-shockwave-flash" data="http://c.brightcove.com/services/viewer/federated_f9?&amp;width=300&amp;height=592&amp;flashID=myExperience&amp;bgcolor=%23FFFFFF&amp;playerID=1964542304001&amp;playerKey=AQ~~%2CAAAAAEwRm8I~%2COusBxQT0iGh1UQjskO9PU6xQBk91cKuu&amp;isVid=true&amp;isUI=true&amp;dynamicStreaming=true&amp;autoStart=&amp;debuggerID=&amp;startTime=1447152433812" id="myExperience" class="BrightcoveExperience" seamlesstabbing="undefined"><param name="allowScriptAccess" value="always"><param name="allowFullScreen" value="true"><param name="seamlessTabbing" value="false"><param name="swliveconnect" value="true"><param name="wmode" value="window"><param name="quality" value="high"><param name="bgcolor" value="#FFFFFF"></object><!--
This script tag will cause the Brightcove Players defined above it to be created as soon as the line is read by the browser. If you wish to have the player instantiated only afterthe rest of the HTML is processed and the page load is complete, remove the line.-->
<!--This script tag will cause the Brightcove Players defined above it to be created as soonas the line is read by the browser. If you wish to have the player instantiated only after the rest of the HTML is processed and the page load is complete, remove the line.-->
<script type="text/javascript">brightcove.createExperiences();</script><!-- End of Brightcove Player --><!-- End of Brightcove Player --><p></p>
		</div>
    </div>
</div>
</div>
<!-- Video Content End-->
</div>
<!-- Block First Row  ---  End-->
<div class="block-second-row">
<!-- gear review Start --->
<div class="big-block gear_review-desktop">
		<div class="color-title blue gear-review "><h3><span>GEAR REVIEWS</span></h3>
                  <div class="border-color-container">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="row container-gear">							 
							<?php $view = views_get_view('gear_reviews_at_homepage'); print $view->execute_display('default', NULL);?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>	          
</div>

	<div class="gear_review-mobile">
		<div class="color-title blue gear-review "> <h3><span>GEAR REVIEWS</span></h3>
                  <div class="border-color-container space-bottom-mob">
                    <div class="row">
						<?php $view = views_get_view('gear_reviews'); print $view->execute_display('default', NULL); ?>
					</div>
				 </div>
		</div>			
	</div>
<!-- gear review End --->
<!-- Like Us Start --->
<div class="box">
	<div class="color-title black like-home"><h3><span class="small">Like Us</span></h3>
                  <div class="border-color-container">
                    <div class="row little-padding" style="min-height:225px;">
						<div id="fb-root"></div>	
						<div class="fb-page" data-href="https://www.facebook.com/facebook" databig-width="260" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div></div>
                    </div>
                  </div>
                </div>    
</div>                  
<!-- Like Us End --->
<!-- Popular Start --->
<div class="box">
	<div class="color-title black popular-home"><h3><span class="small">Popular</span></h3>
		<div class="border-color-container">
			<div class="block">
				<?php $view = views_get_view('popular_at_homepage'); print $view->execute_display('default', NULL); ?>
			</div>
		</div>
    </div> 
</div>                
<!-- Popular End --->            
</div>

<!-- Block Second Row  ---  End-->
<!-- Block Third Row  ---  Start-->

<div class="block-third-row">
<!-- Advert Block Start --->
<div class="box">
	<div class="black" >
		<div class="google-ad-custom-block-right_1 advert-block space-bottom-mob">
			<div class="innr-box">       	
				<?php $block_b = module_invoke('ad_manager', 'block_view', 'right1'); print render($block_b['content']); ?> 
			</div>
		</div>
	</div> 
</div>
<!-- Advert Block  End --->
<!-- featured Gallery Start --->
<div class="box">
	<div class="color-title black-bg photos-home featured-gallery" >
		  <h3><span>FEATURED GALLERY</span></h3>
			  <div class="border-color-container">
				  <div class="block">
					 <?php  $view = views_get_view('home_featured_gallery', 'block'); print $view->execute_display('default', NULL); ?>					 
				  </div>
			  </div>
		</div>
</div> 
<!-- featured Gallery End --->
<!-- Advert Block Start --->
<div class="box">
	 <div class="black" >
			<div class="google-ad-custom-block-right_3 advert-block space-bottom-mob">
				<div class="innr-box">       	
					<?php $block_a = module_invoke('ad_manager', 'block_view', 'right3'); print render($block_a['content']); ?> 
				</div>
		  </div>
	</div>   
</div>	                
<!-- Advert Block End --->
<!-- Recent Start --->
<div class="box">
	<div class="color-title black recent-home"><h3><span class="small">Recent</span></h3>
		  <div class="border-color-container space-bottom-mob">
			<div class="block">
				<?php  $view = views_get_view('recent_posts'); print $view->execute_display('default', NULL); ?>									   
			</div>      
		  </div>
	</div> 
</div>       
<!-- Recent End --->
</div> <!-- Block Third Row  ---  End-->
<div class="block-fourth-row">  <!-- Block Fourth Row  ---  Start-->
<!-- SKI INSTRUCTION Start --->
<div class="box">
	<div class="ski_instruction-desktop">
		<div class="color-title yellow ski-instruction">
				<div class="intruction-ski clearfix"> <h3><span>SKI INSTRUCTION</span></h3>
                  <div class="border-color-container">                    
                     <div class="row">
						<div class="col-md-12">
				            <ul class="skiinstruction">
								<?php 
							if(!empty($SkiInsQ['node']))
								{
									foreach($SkiInsQ['node'] as $r1)
									{
										$Sk=node_load($r1->nid);
								?>
								<!--<li style="float: left; list-style: none; position: relative; width: 240px; height:315px" class="bx-clone">-->
								<li class="bx-clone">
								<a href="<?php echo empty($Sk->page_title) ? $GLOBALS['base_url'].'/node/'.$Sk->nid : $Sk->page_title;?>">
								<img src="<?php echo $GLOBALS['base_url'].'/files/'.$Sk->field_ski_image['und'][0]['filename'];?>">
								</a>
									<h4><a href="<?php echo empty($Sk->page_title) ? $GLOBALS['base_url'].'/node/'.$Sk->nid : $Sk->page_title;?>"><?php echo $Sk->title?>...</a></h4>
									<p><span class="black"><?php //echo date('m/d/Y',$Sk->created);?></span></p>
									<p><?php $ins_body = $Sk->body['und'][0]['value'];		
										$inss_body = implode(' ', array_slice(explode(' ', $ins_body), 0, 10));
										print $inss_body; 
									?></p>
								</li>
								<?php 
								}
								}  
								?>                                                         
                            </ul>                        
                        </div>
                        </div>                                         
                    <div class="col-md-12">
                        <div class="item-ski" id="ski-bx-pager">
													<?php
										if(!empty($SkiInsQ2['node']))
										{
											$i=0;
											foreach($SkiInsQ2['node'] as $r12)
											{
												$Sk2=node_load($r12->nid);
											?>
											<a data-slide-index="<?php echo $i?>" href="<?php echo empty($Sk2->page_title) ? $GLOBALS['base_url'].'/node/'.$Sk2->nid : $Sk2->page_title;?>" class="control-top-slider active">
											    <div class="col-md-4 little-padding">
												   <img src="<?php echo $GLOBALS['base_url'].'/files/'.$Sk->field_ski_image['und'][0]['filename'];?>">
											    </div>
											    <div class="col-md-8 little-padding">
													<h5><?php echo $Sk2->title?></h5>
													   <p><?php //echo date('m/d/Y',$Sk2->created);?></p>
													</div>
													</a>
												<p><?php $in_body = $Sk2->body['und'][0]['value'];		
										$in_body = implode(' ', array_slice(explode(' ', $in_body), 0, 10));
										//print $in_body; 
									?></p>	

													<?php 	$i++;
														}

													}
													?>

                                                </div>
                        </div>
                    </div>
					</div>
                    
                  
<?php endif; ?>
		</div>
	</div>	
		<div class="ski_instruction-mobile">
			<div class="color-title yellow ski-instruction">
				<div class="intruction-ski clearfix"><h3><span>SKI INSTRUCTION</span></h3>
					 <div class="border-color-container space-bottom-mob">						
						 <div class="slider-first">
							 <?php  $view = views_get_view('ski_instruction_mobile'); print $view->execute_display('default', NULL); ?> 
						 </div>	 
						 <div class="slider-second">
							 <?php  $view = views_get_view('ski_instruction_mobile','block'); print $view->execute_display('default', NULL); ?>											   
						 </div>	 
					</div>
				</div>
			 </div>
		</div>
</div>
<!-- SKI INSTRUCTION End --->

<!-- NEWSLETTER SIGNUP Start --->
<div class="box">
    <div class="color-title dark-blue newsletter newsletter-block"> <h3><span>NEWSLETTER SIGNUP</span></h3>
                        <div class="border-color-container space-bottom-mob">
                            <div class="block">
	     	                    <h5 class="newsletter">Sign-up to receive daily updates, news, tips and more!</h5>
                                 <a class="subscribe" href="http://media.aimmedia.com/skiing/skimag/newslettersignup.html">Subscribe</a>
                            </div>
                        </div>
     </div>                  
    <div class="color-title burgundy newsletter newsletter-block"><h3><span>POLLS</span></h3>
			<div class="border-color-container space-bottom-mob">				
				<?php  $view = views_get_view('poll_homepage'); print $view->execute_display('default', NULL); ?>						
			</div>
    </div>    
</div>                    
<!-- NEWSLETTER SIGNUP End --->     
<!-- MAGAZINE Start --->
<div class="box">
	<div class="color-title light-orange magazine-block"><h3><span>MAGAZINE</span></h3>
                  <div class="border-color-container space-bottom-mob">
                    <div class="little-padding">
                      <span class="img" style="padding-left:0;"><a href="http://media.aimmedia.com/skiing/skimag/newslettersignup.html" class="read-more orange">
						  <img src="<?php echo $path . '/';?>images/magazine.jpg"></a></span>
                      <div class="magazine_btn">
                          <a href="http://media.aimmedia.com/skiing/skimag/newslettersignup.html" class="read-more orange">Read More ...</a></br>
                          <a href="http://media.aimmedia.com/skiing/skimag/newslettersignup.html" class="subscribe orange">Subscribe</a>
                      </div>
                    </div>
                  </div>
                </div>               
     <div class="color-title black home-form">   <!-- Subscribe Start --->
                  <h3><span class="small">Subscribe</span></h3>
                  <div class="border-color-container space-bottom-mob">
                    <form action="" method="post" class="subscribe-form">
                      <input type="email" placeholder="Email">
                      <p style="text-align:right;">
                        <input type="submit" value="Submit">
                      </p>

                    </form>
                  </div>
                </div>          <!-- Subscribe End --->                
</div>                
<!-- MAGAZINE End --->
<!-- Instagram Start --->
<div class="box">
	<div class="color-title black instagram">
                  <h3><span class="small">Instagram</span></h3>
                  <div class="border-color-container space-bottom-mob">
                    <div class="block">
                      <div class="one-half-social-info">
                        <div class="item-social">
                          <div><i class="fa fa-facebook"></i></div>
                          <div><p>2,597</p><span>Fans</span></div>
                        </div>
                        <div class="item-social">
                          <div><i class="fa fa-twitter"></i></div>
                          <div><p>1,568</p><span>Followers</span></div>
                        </div>
                        <div class="item-social">
                          <div><i class="fa fa-pinterest"></i></div>
                          <div><p>390k</p><span>Followers</span></div>
                        </div>
                        <div class="item-social">
                          <div><i class="fa fa-linkedin"></i></div>
                          <div><p>540</p><span>Subscribers</span></div>
                        </div>
                      </div>                      
                      <div class="one-half">
						<?php $block = module_invoke('instagram_block', 'block_view', 'instagram_block'); print render($block['content']); ?>
					  </div>
                    </div>
                  </div>
                </div>
                
</div>                
<!-- Instagram End --->      
</div> <!-- Block Fourth Row  ---  End-->
<!-- Bottom Banner Advert Start --->
<div class="box bottm-ad">
	<div class="full-ad-bnr">       
		<?php 	
			$Banners2 = new EntityFieldQuery();
			$Banners2->entityCondition('entity_type', 'node')
				  ->entityCondition('bundle', 'bottom_banner')
				  ->propertyOrderBy('nid','desc')
				  ->range(1,1)
				  ->propertyCondition('status', NODE_PUBLISHED); // Run the query as user 1.
			$BannersQ=$Banners2->execute();
			if(!empty($BannersQ))
			{
				$Bi=array_keys($BannersQ['node']);
				$BannerImage=node_load($Bi[0]);
			?>
			
	   <div class="bottom_add clearfix">               
               <?php  $block = module_invoke('ad_manager', 'block_view', 'bottom'); print render($block['content']); ?>                              
        </div>        
		<?php 	}?>
	</div>
</div>		
