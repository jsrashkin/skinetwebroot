<div class="col-md-6">
  <div class="item-ski">
    <div class="col-md-12 little-padding">
      <h5>
        <?php 
        $alter = array(
          'max_length' => 21, //Integer
          'ellipsis' => TRUE, //Boolean
          'word_boundary' => FALSE, //Boolean
          'html' => TRUE, //Boolean
        );
        
        $trimmed_title = views_trim_text($alter, $fields['title']->raw);
        echo l($trimmed_title, 'node/' . $fields['nid']->raw);
        ?>
     </h5>
		<?php print $fields['field_dek']->content; ?>
      <p><?php //echo date('d/m/Y', $fields['created']->raw)?></p>
      
    </div>
  </div>
</div>
