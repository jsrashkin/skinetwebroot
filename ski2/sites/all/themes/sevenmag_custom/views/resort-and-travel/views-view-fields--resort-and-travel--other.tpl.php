<h4><?php echo l($fields['title']->raw, 'node/' . $fields['nid']->raw);?></h4>
<p><strong><?php //echo date('d/m/Y', $fields['created']->raw)?></strong></p>
<p class="excerpt"><?php print $fields['field_dek']->content; ?></p>
<?php
print l(t('Read more'), 'node/' . $fields['nid']->raw, array('attributes' => array('class' => 'read-more')));
?>
