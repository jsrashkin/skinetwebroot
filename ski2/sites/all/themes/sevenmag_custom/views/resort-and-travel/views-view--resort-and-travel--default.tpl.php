<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="big-block ski_resort_home">
<div class="color-title orange">
  <h3><span><?php print t('RESORT & TRAVEL'); ?></span></h3>

  <div class="border-color-container">
    <div class="carousel-1-container">

      <div id="hide-carousel">
        <div id="resort-demo" class="owl-carousel owl-theme">

          <?php if ($rows): ?>
            <?php print $rows; ?>
          <?php endif; ?>


        </div>

        <script type="text/javascript">

          jQuery(document).ready(function () {

            jQuery("#resort-demo").owlCarousel({

              navigation: true, // Show next and prev buttons
              slideSpeed: 300,
              paginationSpeed: 400,
              singleItem: true

            });

          });

        </script>
      </div>

    <?php print render(views_embed_view('resort_and_travel', 'image')); ?>
    <div class="resorts-travels-home">
      <div class="row description ert">
        <div class="col-md-12">
          <?php print render(views_embed_view('resort_and_travel', 'other')); ?>
        </div>
        <div class="col-md-12 ski_featured">
          <?php print render(views_embed_view('resort_and_travel', 'items')); ?>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>
</div>

