<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>


<?php if ($rows): ?>
  <div class="row home-slider">
    <div class="col-md-12">
      <!-- Banner Home -->
      <div class="wrap">
        <div class="big-slider ">
          <div class="col-md-9">
            <?php print $rows; ?>
          </div>
          <div class="col-md-3">
            <?php print views_embed_view('featured_stories', 'panel_pane_1', $view->args[0])?>
          </div>
        </div>
        <script type="text/javascript">
          jQuery('.bxslider').bxSlider({
            pagerCustom: '#bx-pager',
            controls: false,
            auto: true
          });
        </script>
      </div>
    </div>
  </div>
<?php endif; ?>
