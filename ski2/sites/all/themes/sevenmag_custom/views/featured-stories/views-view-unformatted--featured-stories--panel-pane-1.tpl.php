<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
  <div id="bx-pager" class="control-slider">
  <?php foreach ($rows as $id => $row): ?>
    <a data-slide-index="<?php print $id; ?>" href="" class="control-top-slider">
      <div class="col-md-12"><?php print $row; ?></div>
    </a>
  <?php endforeach; ?>
  </div>