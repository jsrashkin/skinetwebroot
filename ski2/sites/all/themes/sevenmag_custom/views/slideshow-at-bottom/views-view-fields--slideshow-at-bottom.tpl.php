<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<?php /*foreach ($fields as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>

  <?php print $field->wrapper_prefix; ?>
    <?php print $field->label_html; ?>
    <?php print $field->content; ?>
  <?php print $field->wrapper_suffix; ?>
<?php endforeach; */?>
 
 
         

            <div class="owl-item" style="width: 235px;">
				<div class="item">
				 
					<?php
					
					
					/* if ( is_null($fields['field_banner_image']->content)) {
						print $fields['field_banner_image']->content;
					  } else {
						print 'No Image Found!';
					  }
				   */
				 
					  print $fields['field_banner_image']->content;
					  print $fields['field_popular_img']->content;
					  print $fields['field_image']->content;
					  print $fields['field_recent_image']->content;
					  print $fields['field_performance_image']->content;
					  print $fields['field_recent_images']->content;
					  print $fields['field_review_image']->content;
					  print $fields['field_revsort_life_image']->content;
					  print $fields['field_ski_image']->content;
					  print $fields['field_boots_snow_image']->content;
					  print $fields['field_popular_image']->content;
					  print $fields['field_guide_sponsor_image']->content;
					  print $fields['field_sponsor_image']->content; 
					?>
					
					<div class="col-md-8 little-padding slide-1">
						<p><?php print $fields['title']->content; ?></p>
							<!--<a href="">
							  <i class="fa fa-comment-o"></i>
							</a>-->
					</div>
				</div>
			</div>
