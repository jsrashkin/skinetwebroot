<?php
/**
 * @file
 * ski_content_types_part1.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ski_content_types_part1_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ski_content_types_part1_node_info() {
  $items = array(
    'best_of_region' => array(
      'name' => t('Best of Region'),
      'base' => 'node_content',
      'description' => t('Best of Region being viewed.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'best_of_resort' => array(
      'name' => t('Best of Resort'),
      'base' => 'node_content',
      'description' => t('Best of Resort'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'bottom_banner' => array(
      'name' => t('Banners'),
      'base' => 'node_content',
      'description' => t('This CT will manage the bottom banner.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'brand' => array(
      'name' => t('Brand'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'channel' => array(
      'name' => t('Channel'),
      'base' => 'node_content',
      'description' => t('Channels'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'chompon_deals' => array(
      'name' => t('Chompon Deals'),
      'base' => 'node_content',
      'description' => t('Place chompon embed widgets here'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'deal_of_the_day' => array(
      'name' => t('Deal of the Day'),
      'base' => 'node_content',
      'description' => t('Deal of the Day'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'deal_of_the_day_feed' => array(
      'name' => t('Deal of the Day Feed'),
      'base' => 'node_content',
      'description' => t('This content type provides an interface for multiple feeds for the Deal of the Day. Deal of the Day \'providers\' should be placed here.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'easy_guides' => array(
      'name' => t('Easy Guides'),
      'base' => 'node_content',
      'description' => t('Easy Guides Tout Object'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'event_channel' => array(
      'name' => t('Event channel'),
      'base' => 'node_content',
      'description' => t('Event channel'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'feature_tout' => array(
      'name' => t('Feature Tout'),
      'base' => 'node_content',
      'description' => t('Homepage carousel feature tout'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'feed' => array(
      'name' => t('Feed'),
      'base' => 'node_content',
      'description' => t('Items from these feeds will be turned into nodes.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'feed_sister_sites_item' => array(
      'name' => t('Feed - Sister Sites - Item'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'flash' => array(
      'name' => t('Flash'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'flowers' => array(
      'name' => t('Flowers'),
      'base' => 'node_content',
      'description' => t('Flowers'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'gear_reviews' => array(
      'name' => t('GEAR REVIEWS'),
      'base' => 'node_content',
      'description' => t('GEAR REVIEWS Content Type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
