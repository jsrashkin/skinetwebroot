<?php
/**
 * @file
 * ski_other.js_injector_rules.inc
 */

/**
 * Implements hook_js_injector_rule().
 */
function ski_other_js_injector_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 1;
  $rule->name = 'Gear Search Expposed Filter ';
  $rule->admin_description = 'Gear Search Expposed Filter ';
  $rule->js = 'jQuery( document ).ready(function() {
jQuery(".gearsearch .views-exposed-form #edit-field-product-gender-value-wrapper, .gearsearch .views-exposed-form #edit-field-product-category-value-wrapper, .gearsearch .views-exposed-form #edit-field-product-year-value-wrapper, .gearsearch .views-exposed-form #edit-field-product-awards-value-wrapper, .gearsearch .views-exposed-form #edit-field-product-brand-target-id-wrapper,  .gearsearch .views-exposed-form #edit-tid-wrapper").wrapAll(\'<div class="primary-filters-form custom-filter"></div>\');

jQuery(".gearsearch .views-exposed-form .primary-filters-form").prepend("<h3>Primary Filters</h3>");

jQuery(".gearsearch .views-exposed-form #edit-field-range-waistwidth-value-wrapper, .gearsearch .views-exposed-form #edit-field-product-level-value-wrapper, .gearsearch .views-exposed-form #edit-field-product-price-value-wrapper, .gearsearch .views-exposed-form #edit-field-ski-rating-stabilityspeed-value-wrapper, .gearsearch .views-exposed-form #edit-field-ski-rating-flotation-value-wrapper, .gearsearch .views-exposed-form #edit-field-ski-rating-forgiveness-value-wrapper, .gearsearch .views-exposed-form #edit-field-ski-rating-quickness-value-wrapper, .gearsearch .views-exposed-form #edit-field-rank-hard-snow-value-wrapper, .gearsearch .views-exposed-form #edit-field-ski-rating-crudperform-value-wrapper").wrapAll(\'<div class="more-filters-form custom-filter"><div class="more-filters-form-inner"></div></div>\');

jQuery(\'.gearsearch .views-exposed-form .more-filters-form\').prepend(\'<h3>More Filters</h3><span class="accordian pull-right">Minimize <span class="minimize">-</span></span>\');

jQuery(".gearsearch .views-exposed-form .more-filters-form .accordian").click(function(){
		  jQuery(".gearsearch .views-exposed-form .more-filters-form .more-filters-form-inner").toggle();	
});

        jQuery(".gearsearch .views-exposed-form .form-select").each(function(){
            jQuery(this).wrap("<span class=\'select-wrapper\'></span>");
           jQuery(this).after("<span class=\'holder\'></span>");
        });
       jQuery(".gearsearch .views-exposed-form .form-select").change(function(){
            var selectedOption = jQuery(this).find(":selected").text();
           jQuery(this).next(".holder").text(selectedOption);
        }).trigger(\'change\');



jQuery(\'.more-filters-form-inner\').hide();

});';
  $rule->position = 'header';
  $rule->preprocess = 0;
  $rule->inline = 0;
  $rule->page_visibility = 0;
  $rule->page_visibility_pages = '';
  $export['Gear Search Expposed Filter '] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 1;
  $rule->name = 'Sub Mneu Tabs';
  $rule->admin_description = 'active class for submenu';
  $rule->js = 'jQuery( document ).ready(function() {
var activeurl = window.location;
jQuery(\'a[href="\'+activeurl+\'"]\').parent(\'.video-menus li\').addClass(\'active\');
});';
  $rule->position = 'footer';
  $rule->preprocess = 0;
  $rule->inline = 0;
  $rule->page_visibility = 0;
  $rule->page_visibility_pages = '';
  $export['Sub Mneu Tabs'] = $rule;

  return $export;
}
