<?php
/**
 * @file
 * ski_other.brightcove.inc
 */

/**
 * Implements hook_brightcove_player().
 */
function ski_other_brightcove_player() {
  $export = array();

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'default';
  $preset->display_name = 'Default';
  $preset->player_id = '4649891404001';
  $preset->player_key = 'AQ~~,AAAAAEwRm8I~,OusBxQT0iGjuujel0yiHvhh5YII0yom_';
  $preset->responsive = TRUE;
  $export['default'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'related_videos_player';
  $preset->display_name = 'Related Videos Player';
  $preset->player_id = '4661112846001';
  $preset->player_key = 'AQ~~,AAAAAEwRm8I~,OusBxQT0iGjhrkYeeHCoRhdMv91qcP_v';
  $preset->responsive = TRUE;
  $export['related_videos_player'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'responsive_player';
  $preset->display_name = 'Responsive Player';
  $preset->player_id = '1964542304001';
  $preset->player_key = 'AQ~~,AAAAAEwRm8I~,OusBxQT0iGh1UQjskO9PU6xQBk91cKuu';
  $preset->responsive = TRUE;
  $export['responsive_player'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'test';
  $preset->display_name = 'Desktop';
  $preset->player_id = '4649891406001';
  $preset->player_key = 'AQ~~,AAAAAEwRm8I~,OusBxQT0iGh5ivXIBqfTleGWyLCgB4JO';
  $preset->responsive = TRUE;
  $export['test'] = $preset;

  return $export;
}
