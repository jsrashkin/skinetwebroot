<?php
/**
 * @file
 * ski_other.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function ski_other_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'header_top';
  $mini->category = '';
  $mini->admin_title = '';
  $mini->admin_description = '';
  $mini->requiredcontexts = FALSE;
  $mini->contexts = FALSE;
  $mini->relationships = FALSE;
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array();
  $display->cache = array();
  $display->title = '';
  $display->uuid = '6a61c318-c923-43fc-ba51-e2313c7b8aab';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-4959e98f-6aab-48cb-8cec-2b6d7d86868d';
    $pane->panel = 'middle';
    $pane->type = 'ads';
    $pane->subtype = 'Top';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = '4959e98f-6aab-48cb-8cec-2b6d7d86868d';
    $display->content['new-4959e98f-6aab-48cb-8cec-2b6d7d86868d'] = $pane;
    $display->panels['middle'][0] = 'new-4959e98f-6aab-48cb-8cec-2b6d7d86868d';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-4959e98f-6aab-48cb-8cec-2b6d7d86868d';
  $mini->display = $display;
  $export['header_top'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'sister_site_feeds_panel';
  $mini->category = '';
  $mini->admin_title = 'From Our Sister Sites';
  $mini->admin_description = '';
  $mini->requiredcontexts = FALSE;
  $mini->contexts = FALSE;
  $mini->relationships = FALSE;
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 56.285660392666998,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'left',
          1 => 'center',
          2 => 'right',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Middle',
        'width' => '33.658119576532904',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'top',
        ),
        'parent' => 'main',
      ),
      'top' => array(
        'type' => 'region',
        'title' => 'Top',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
      ),
      'left' => array(
        'type' => 'region',
        'title' => 'Left',
        'width' => '33.262639046478874',
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      'right' => array(
        'type' => 'region',
        'title' => 'Right',
        'width' => '33.07924137698822',
        'width_type' => '%',
        'parent' => 'main-row',
      ),
    ),
  );
  $display->panel_settings = array(
    'style' => 'template',
    'style_settings' => array(
      'default' => array(
        'display_theme' => '',
        'display_css_id' => '',
        'template_theme' => '',
        'simple_theme' => 1,
      ),
    ),
    'top' => array(
      'style' => '-1',
    ),
    'left' => array(
      'style' => '-1',
    ),
    'middle' => array(
      'style' => '-1',
    ),
    'right' => array(
      'style' => '-1',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'afda77bf-bcca-4b68-b25d-2a37093cde91';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-51a630ca-8231-457f-9128-a120b3035bdf';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'sister_site_feeds';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '8',
      'pager_id' => '',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_4',
      'override_title' => 1,
      'override_title_text' => '<none>',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array(
      'css_id' => 'sister_site_feeds',
      'css_class' => 'panels-flexible-column-inside',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = '51a630ca-8231-457f-9128-a120b3035bdf';
    $display->content['new-51a630ca-8231-457f-9128-a120b3035bdf'] = $pane;
    $display->panels['center'][0] = 'new-51a630ca-8231-457f-9128-a120b3035bdf';
    $pane = new stdClass();
    $pane->pid = 'new-0208ea23-a7bf-4192-a7e1-7b97f34a72e5';
    $pane->panel = 'left';
    $pane->type = 'views';
    $pane->subtype = 'sister_site_feeds';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '8',
      'pager_id' => '',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_2',
      'override_title' => 1,
      'override_title_text' => '<none>',
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'default',
    );
    $pane->css = array(
      'css_id' => 'sister_site_feeds',
      'css_class' => 'panels-flexible-column-inside',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = '0208ea23-a7bf-4192-a7e1-7b97f34a72e5';
    $display->content['new-0208ea23-a7bf-4192-a7e1-7b97f34a72e5'] = $pane;
    $display->panels['left'][0] = 'new-0208ea23-a7bf-4192-a7e1-7b97f34a72e5';
    $pane = new stdClass();
    $pane->pid = 'new-9e53a3b0-df8b-4924-8325-f8a895f6ff0e';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'sister_site_feeds';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '8',
      'pager_id' => '',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_5',
      'override_title' => 1,
      'override_title_text' => '<none>',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array(
      'css_id' => 'sister_site_feeds',
      'css_class' => 'panels-flexible-column-inside',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = '9e53a3b0-df8b-4924-8325-f8a895f6ff0e';
    $display->content['new-9e53a3b0-df8b-4924-8325-f8a895f6ff0e'] = $pane;
    $display->panels['right'][0] = 'new-9e53a3b0-df8b-4924-8325-f8a895f6ff0e';
    $pane = new stdClass();
    $pane->pid = 'new-536de3ba-aabf-4038-9a77-f7fcdb32ee31';
    $pane->panel = 'top';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Title',
      'title' => '<none>',
      'body' => '<h2 class="pane-title">From our sister sites</div>',
      'format' => '2',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array(
      'css_id' => 'sister_site_feeds',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = '';
    $pane->uuid = '536de3ba-aabf-4038-9a77-f7fcdb32ee31';
    $display->content['new-536de3ba-aabf-4038-9a77-f7fcdb32ee31'] = $pane;
    $display->panels['top'][0] = 'new-536de3ba-aabf-4038-9a77-f7fcdb32ee31';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['sister_site_feeds_panel'] = $mini;

  return $export;
}
