<?php
/**
 * @file
 * ski_other.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ski_other_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "brightcove" && $api == "brightcove") {
    return array("version" => "1");
  }
  if ($module == "file_entity" && $api == "file_default_displays") {
    return array("version" => "1");
  }
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
  if ($module == "js_injector" && $api == "js_injector_rules") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
  if ($module == "video_embed_field" && $api == "default_video_embed_styles") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function ski_other_flag_default_flags() {
  $flags = array();
  // Exported flag: "Best Answer".
  $flags['answers_best_answer'] = array(
    'entity_type' => 'laserfist_node',
    'title' => 'Best Answer',
    'global' => 1,
    'types' => array(
      0 => 'user_answer',
    ),
    'flag_short' => 'mark best answer',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'unmark best answer',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'api_version' => 3,
    'module' => 'ski_other',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Editor Approved".
  $flags['answers_editor_approved'] = array(
    'entity_type' => 'laserfist_node',
    'title' => 'Editor Approved',
    'global' => 1,
    'types' => array(
      0 => 'user_answer',
    ),
    'flag_short' => 'mark editor approved',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'unmark editor approved',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'api_version' => 3,
    'module' => 'ski_other',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Must Read Answers".
  $flags['answers_must_read'] = array(
    'entity_type' => 'laserfist_node',
    'title' => 'Must Read Answers',
    'global' => 1,
    'types' => array(
      0 => 'user_question',
    ),
    'flag_short' => 'mark must read',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'unmark must read',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'api_version' => 3,
    'module' => 'ski_other',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Resvoled".
  $flags['answers_resolved'] = array(
    'entity_type' => 'laserfist_node',
    'title' => 'Resvoled',
    'global' => 1,
    'types' => array(
      0 => 'user_question',
    ),
    'flag_short' => 'mark resolved',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'unmark resolved',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'api_version' => 3,
    'module' => 'ski_other',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Sponsored Answer".
  $flags['answers_sponsored_answer'] = array(
    'entity_type' => 'laserfist_node',
    'title' => 'Sponsored Answer',
    'global' => 1,
    'types' => array(
      0 => 'user_answer',
    ),
    'flag_short' => 'sponsored answer',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'not sponsored answer',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'api_version' => 3,
    'module' => 'ski_other',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Subscribe to Answer Updates".
  $flags['answers_subscribe'] = array(
    'entity_type' => 'laserfist_node',
    'title' => 'Subscribe to Answer Updates',
    'global' => 0,
    'types' => array(
      0 => 'user_question',
    ),
    'flag_short' => 'subscribe',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'unsubscribe',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'api_version' => 3,
    'module' => 'ski_other',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Bookmarks".
  $flags['bookmarks'] = array(
    'entity_type' => 'node',
    'title' => 'Bookmarks',
    'global' => 0,
    'types' => array(
      0 => 'story',
      1 => 'forum',
      2 => 'blog',
    ),
    'flag_short' => 'Bookmark this',
    'flag_long' => 'Add this post to your bookmarks',
    'flag_message' => 'This post has been added to your bookmarks',
    'unflag_short' => 'Unbookmark this',
    'unflag_long' => 'Remove this post from your bookmarks',
    'unflag_message' => 'This post has been removed from your bookmarks',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'teaser' => TRUE,
      'full' => TRUE,
    ),
    'show_as_field' => FALSE,
    'show_on_form' => 1,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'api_version' => 3,
    'module' => 'ski_other',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Friend".
  $flags['friend'] = array(
    'entity_type' => 'user',
    'title' => 'Friend',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Add friend',
    'flag_long' => 'Add this user to your list of friends.',
    'flag_message' => '',
    'unflag_short' => 'Remove friend',
    'unflag_long' => 'Remove this user from your list of friends.',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(),
    'show_as_field' => FALSE,
    'show_on_form' => FALSE,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'show_on_profile' => 1,
    'access_uid' => 'others',
    'flag_confirmation' => 'Are you sure you want to add [user:name] to your list of friends?',
    'unflag_confirmation' => 'Are you sure you want to remove [user:name] from your list of friends?',
    'module' => 'ski_other',
    'api_version' => 3,
    'status' => FALSE,
    'import_roles' => array(
      'flag' => array(
        0 => 2,
      ),
      'unflag' => array(
        0 => 2,
      ),
    ),
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Inappropiate Comment ".
  $flags['inappropriate_comment'] = array(
    'entity_type' => 'comment',
    'title' => 'Inappropiate Comment ',
    'global' => 0,
    'types' => array(
      0 => 'article',
    ),
    'flag_short' => 'report',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'un-report',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(),
    'show_as_field' => FALSE,
    'show_on_form' => FALSE,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'api_version' => 3,
    'module' => 'ski_other',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Inappropriate User Generated Content".
  $flags['inappropriate_ugc'] = array(
    'entity_type' => 'node',
    'title' => 'Inappropriate User Generated Content',
    'global' => 0,
    'types' => array(
      0 => 'forum',
      1 => 'ugc_photo',
    ),
    'flag_short' => 'Report',
    'flag_long' => 'Report this content as inappropriate.',
    'flag_message' => 'An editor will evaluate this content.',
    'unflag_short' => 'Unflag',
    'unflag_long' => 'Flag this content as appropriate.',
    'unflag_message' => 'The content has been unflagged.',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => TRUE,
    ),
    'show_as_field' => FALSE,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'api_version' => 3,
    'module' => 'ski_other',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Report Comment".
  $flags['report_comment'] = array(
    'entity_type' => 'comment',
    'title' => 'Report Comment',
    'global' => 0,
    'types' => array(
      0 => 'product',
      1 => 'product_boot',
      2 => 'product_ski',
      3 => 'resort',
    ),
    'flag_short' => 'report',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => '&nbsp;',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => TRUE,
    ),
    'show_as_field' => FALSE,
    'show_on_form' => FALSE,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'api_version' => 3,
    'module' => 'ski_other',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}
