<?php
/**
 * @file
 * ski_other.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function ski_other_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'resort_region';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Resort region';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'resorts_popular',
      'display' => 'block_1',
      'args' => '',
      'title' => 'Most Popular',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'resorts_region',
      'display' => 'block_1',
      'args' => '',
      'title' => 'East Coast',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'resorts_region',
      'display' => 'block_2',
      'args' => '',
      'title' => 'Midwest',
      'weight' => '-98',
      'type' => 'view',
    ),
    3 => array(
      'vid' => 'resorts_region',
      'display' => 'block_3',
      'args' => '',
      'title' => 'Rockies',
      'weight' => '-97',
      'type' => 'view',
    ),
    4 => array(
      'vid' => 'resorts_region',
      'display' => 'block_4',
      'args' => '',
      'title' => 'Western US',
      'weight' => '-96',
      'type' => 'view',
    ),
    5 => array(
      'vid' => 'resorts_region',
      'display' => 'block_5',
      'args' => '',
      'title' => 'Canada',
      'weight' => '-95',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'accordion';
  $quicktabs->style = 'default';
  $quicktabs->options = array(
    'history' => 0,
    'jquery_ui' => array(
      'autoHeight' => 0,
      'collapsible' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Canada');
  t('East Coast');
  t('Midwest');
  t('Most Popular');
  t('Resort region');
  t('Rockies');
  t('Western US');

  $export['resort_region'] = $quicktabs;

  return $export;
}
