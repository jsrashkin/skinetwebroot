<?php
/**
 * @file
 * ski_content_types_part3.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ski_content_types_part3_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ski_content_types_part3_node_info() {
  $items = array(
    'product_apparel' => array(
      'name' => t('Product Apparel'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product_binding' => array(
      'name' => t('Product Binding'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product_boot' => array(
      'name' => t('Product Boot'),
      'base' => 'node_content',
      'description' => t('Gear - Boot'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product_gadget' => array(
      'name' => t('Product Gadget'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product_ski' => array(
      'name' => t('Product Ski'),
      'base' => 'node_content',
      'description' => t('Gear - Ski'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'profile' => array(
      'name' => t('Profile'),
      'base' => 'node_content',
      'description' => t('A user profile built as content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'reader_resort_awards' => array(
      'name' => t('Reader Resort Awards'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'recent_posts' => array(
      'name' => t('Recent Posts'),
      'base' => 'node_content',
      'description' => t('Recent Posts'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'recirc' => array(
      'name' => t('Re-Circ'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'region' => array(
      'name' => t('Region'),
      'base' => 'node_content',
      'description' => t('Geographical region for resorts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'resort' => array(
      'name' => t('Resort'),
      'base' => 'node_content',
      'description' => t('A ski resort'),
      'has_title' => '1',
      'title_label' => t('Resort Name'),
      'help' => '',
    ),
    'resort_life' => array(
      'name' => t('Resort Life'),
      'base' => 'node_content',
      'description' => t('Resort Life'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'season_pass_deals' => array(
      'name' => t('Season Pass Deals'),
      'base' => 'node_content',
      'description' => t('Season pass deals and promotions content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'sister_site_feed' => array(
      'name' => t('Sister Site Feeds'),
      'base' => 'node_content',
      'description' => t('Sister Site Feeds API'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ski_contest_entries' => array(
      'name' => t('Ski Contest Entries'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ski_instruction' => array(
      'name' => t('SKI INSTRUCTION'),
      'base' => 'node_content',
      'description' => t('SKI INSTRUCTION'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'syndication_feed' => array(
      'name' => t('Syndication Feed'),
      'base' => 'node_content',
      'description' => t('Feeds for syndication partners'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'test_' => array(
      'name' => t('Test'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'top_ten' => array(
      'name' => t('Top Ten'),
      'base' => 'node_content',
      'description' => t('Top Ten Tout Object'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'travel_advisory_board' => array(
      'name' => t('Travel Advisory Board Member'),
      'base' => 'node_content',
      'description' => t('Travel Advisory Board Members listing'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'travel_advisory_board_node' => array(
      'name' => t('Travel Advisory Board'),
      'base' => 'node_content',
      'description' => t('Page containing board members'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'user_answer' => array(
      'name' => t('Q&A - Answer'),
      'base' => 'node_content',
      'description' => t('User posts an Answer'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'user_question' => array(
      'name' => t('Q&A - Question'),
      'base' => 'node_content',
      'description' => t('User posts a question. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'videos' => array(
      'name' => t('Videos'),
      'base' => 'node_content',
      'description' => t('This content type will display video for right side panel.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'voting_contest' => array(
      'name' => t('Voting Contest'),
      'base' => 'node_content',
      'description' => t('Allows creation of custom voting contest.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'whats_hot' => array(
      'name' => t('What\'s Hot'),
      'base' => 'node_content',
      'description' => t('Homepage featured what\'s hot at ski.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
