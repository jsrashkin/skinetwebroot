<?php
/**
 * @file
 * ski_content_types_part2.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ski_content_types_part2_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ski_content_types_part2_node_info() {
  $items = array(
    'gear_deals' => array(
      'name' => t('Gear Deals'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'guide' => array(
      'name' => t('Guide'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'guide_type' => array(
      'name' => t('Guide Type'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'homepage' => array(
      'name' => t('Homepage'),
      'base' => 'node_content',
      'description' => t('Homepage Node'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'menu_item' => array(
      'name' => t('Menu Item'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'most_popular_ski_topics' => array(
      'name' => t('Most Popular Ski Topics'),
      'base' => 'node_content',
      'description' => t('Homepage Most Popular Ski Topics'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'news' => array(
      'name' => t('News'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'our_sponsors_logo' => array(
      'name' => t('Our Sponsors Logo'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Our Sponsor Logo'),
      'help' => '',
    ),
    'pcd' => array(
      'name' => t('PCD'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'performance' => array(
      'name' => t('Performance'),
      'base' => 'node_content',
      'description' => t('performance'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'popular' => array(
      'name' => t('Popular'),
      'base' => 'node_content',
      'description' => t('Popular posts goes here'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => t('Gear'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product_accessory' => array(
      'name' => t('Product Accessory'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
