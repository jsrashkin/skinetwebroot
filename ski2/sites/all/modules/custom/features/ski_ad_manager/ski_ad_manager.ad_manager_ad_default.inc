<?php
/**
 * @file
 * ski_ad_manager.ad_manager_ad_default.inc
 */

/**
 * Implements hook_ad_manager_ad_default().
 */
function ski_ad_manager_ad_manager_ad_default() {
  $export = array();

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 728,
    1 => 90,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'bottom',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'bottom';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['bottom'] = $ad;

$size = new stdClass();
$size->{0} = NULL;
$targeting = new stdClass();
$targeting->firstnew = array(
  0 => array(
    'value' => 'firstnew',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'firstnew';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['firstnew'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 88,
    1 => 31,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'frame1',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'frame1';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['frame1'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 300,
    1 => 250,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'gallery',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'gallery';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['gallery'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 300,
    1 => 250,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'home1',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'home1';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['home1'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 300,
    1 => 250,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'home2',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'home2';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['home2'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 200,
    1 => 250,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'hom',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'home250';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['home250'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 300,
    1 => 250,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'home3',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'home3';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['home3'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 1,
    1 => 1,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'interstitial',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'interstitial';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['interstitial'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 160,
    1 => 600,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'left',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'left';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['left'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 160,
    1 => 600,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'right',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'right';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['right'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 300,
    1 => 250,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'right1',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'right1';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['right1'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 300,
    1 => 250,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'right2',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'right2';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['right2'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 300,
    1 => 250,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'right3',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'right3';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['right3'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 728,
    1 => 90,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'top',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'top';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['top'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 728,
    1 => 90,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'top_new',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'top_new';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['top_new'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 300,
    1 => 100,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'x01',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'x01';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['x01'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 300,
    1 => 100,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'x02',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'x02';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['x02'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 990,
    1 => 50,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'x90',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'x90';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['x90'] = $ad;

$size = new stdClass();
$size->{0} = array(
  0 => array(
    0 => 1500,
    1 => 889,
  ),
);
$targeting = new stdClass();
$targeting->pos = array(
  0 => array(
    'value' => 'x96',
    'eval' => 0,
  ),
);
$ad = new stdClass();
$ad->disabled = FALSE; 
$ad->api_version = 1;
$ad->name = 'x96';
$ad->provider = 'gpt_ads';
$ad->settings = array(
  'gpt_ads' => array(
    'outofpage' => 0,
    'refresh' => 1,
    'size' => $size,
    'targeting' => $targeting,
  ),
);
  $export['x96'] = $ad;

  return $export;
}
