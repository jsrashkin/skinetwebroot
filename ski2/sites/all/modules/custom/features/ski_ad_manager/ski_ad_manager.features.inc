<?php
/**
 * @file
 * ski_ad_manager.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ski_ad_manager_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ad_manager" && $api == "ad_manager_ad_default") {
    return array("version" => "1");
  }
}
