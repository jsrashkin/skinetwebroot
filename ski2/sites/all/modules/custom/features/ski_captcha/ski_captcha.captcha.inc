<?php
/**
 * @file
 * ski_captcha.captcha.inc
 */

/**
 * Implements hook_captcha_default_points().
 */
function ski_captcha_captcha_default_points() {
  $export = array();

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_agents_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_agents_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_article_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_article_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_best_of_region_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_best_of_region_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_best_of_resort_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_best_of_resort_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_blog_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_blog_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_blog_post_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_blog_post_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_bottom_banner_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_bottom_banner_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_brand_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_brand_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_channel_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_channel_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_chompon_deals_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_chompon_deals_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_deal_of_the_day_feed_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_deal_of_the_day_feed_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_deal_of_the_day_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_deal_of_the_day_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_easy_guides_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_easy_guides_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_event_channel_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_event_channel_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_event_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_event_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_feature_tout_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_feature_tout_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_feed_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_feed_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_feed_sister_sites_item_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_feed_sister_sites_item_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_flash_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_flash_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_flowers_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_flowers_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_forum_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_forum_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_gallery_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_gallery_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_gear_deals_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_gear_deals_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_gear_reviews_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_gear_reviews_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_guide_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_guide_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_guide_type_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_guide_type_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_homepage_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_homepage_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_image_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_image_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_link_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_link_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_logbook_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_logbook_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_marketplace_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_marketplace_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_menu_item_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_menu_item_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_most_popular_ski_topics_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_most_popular_ski_topics_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_news_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_news_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_our_sponsors_logo_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_our_sponsors_logo_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_page_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_page_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_panel_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_panel_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_pcd_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_pcd_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_performance_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_performance_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_person_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_person_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_poll_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_poll_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_popular_form';
  $captcha->module = '';
  $captcha->captcha_type = 'default';
  $export['comment_node_popular_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_product_accessory_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_product_accessory_form'] = $captcha;

  $captcha = new stdClass();
  $captcha->disabled = FALSE; /* Edit this to true to make a default captcha disabled initially */
  $captcha->api_version = 1;
  $captcha->form_id = 'comment_node_product_apparel_form';
  $captcha->module = '';
  $captcha->captcha_type = '';
  $export['comment_node_product_apparel_form'] = $captcha;

  return $export;
}
