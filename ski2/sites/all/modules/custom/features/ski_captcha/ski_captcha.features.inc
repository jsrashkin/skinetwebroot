<?php
/**
 * @file
 * ski_captcha.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ski_captcha_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "captcha" && $api == "captcha") {
    return array("version" => "1");
  }
}
