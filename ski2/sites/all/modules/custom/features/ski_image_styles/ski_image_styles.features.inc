<?php
/**
 * @file
 * ski_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function ski_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: 137x138.
  $styles['137x138'] = array(
    'label' => '137x138',
    'effects' => array(
      5 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 137,
          'height' => 138,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 245x245.
  $styles['245x245'] = array(
    'label' => '245X245',
    'effects' => array(
      10 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 236,
          'height' => 245,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: fetured_gallery.
  $styles['fetured_gallery'] = array(
    'label' => 'Featured gallery(275x280)',
    'effects' => array(
      24 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 275,
          'height' => 280,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: footer_images_45x45_.
  $styles['footer_images_45x45_'] = array(
    'label' => 'Footer Images(65x65)',
    'effects' => array(
      12 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 65,
          'height' => 65,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: front_thumbnail_270x115.
  $styles['front_thumbnail_270x115'] = array(
    'label' => 'front thumbnail 320x127',
    'effects' => array(
      19 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 318,
          'height' => 127,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: gallery_page.
  $styles['gallery_page'] = array(
    'label' => 'gallery page',
    'effects' => array(
      18 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 970,
          'height' => 570,
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: gear_review.
  $styles['gear_review'] = array(
    'label' => 'Gear review',
    'effects' => array(
      13 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 170,
          'height' => 118,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: gear_search_product.
  $styles['gear_search_product'] = array(
    'label' => 'Gear Search Product',
    'effects' => array(
      25 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 137,
          'height' => 138,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: homepage_slider.
  $styles['homepage_slider'] = array(
    'label' => 'homepage_slider',
    'effects' => array(
      20 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 760,
          'height' => 370,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: homepage_slider_small.
  $styles['homepage_slider_small'] = array(
    'label' => 'homepage_slider_small',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 280,
          'height' => 115,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: related_article.
  $styles['related_article'] = array(
    'label' => 'related article',
    'effects' => array(
      7 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 365,
          'height' => 240,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: resort_and_travel.
  $styles['resort_and_travel'] = array(
    'label' => 'resort_and_travel',
    'effects' => array(
      3 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 530,
          'height' => 175,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: right-original.
  $styles['right-original'] = array(
    'label' => 'right-original',
    'effects' => array(
      11 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 483,
          'height' => 243,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: right-thumb.
  $styles['right-thumb'] = array(
    'label' => 'right-thumb',
    'effects' => array(
      8 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 65,
          'height' => 65,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: test_insert.
  $styles['test_insert'] = array(
    'label' => 'test_insert',
    'effects' => array(
      22 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 350,
          'height' => 300,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: video_sidebar.
  $styles['video_sidebar'] = array(
    'label' => 'video sidebar',
    'effects' => array(
      6 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 65,
          'height' => 65,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
