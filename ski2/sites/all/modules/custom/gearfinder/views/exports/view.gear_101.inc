<?php
$view = new view;
$view->name = 'gear_101';
$view->description = 'Gear channel sidebar block';
$view->tag = '';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = 2;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->override_option('relationships', array(
  'nodequeue_rel' => array(
    'label' => 'queue',
    'required' => 1,
    'limit' => 1,
    'qids' => array(
      '36' => 36,
      '1' => 0,
      '2' => 0,
      '3' => 0,
      '5' => 0,
      '6' => 0,
      '9' => 0,
      '10' => 0,
      '11' => 0,
      '12' => 0,
      '13' => 0,
      '14' => 0,
      '15' => 0,
      '16' => 0,
      '17' => 0,
      '18' => 0,
      '19' => 0,
      '20' => 0,
      '21' => 0,
      '22' => 0,
      '23' => 0,
      '24' => 0,
      '25' => 0,
      '26' => 0,
      '27' => 0,
      '28' => 0,
      '29' => 0,
      '30' => 0,
      '31' => 0,
      '32' => 0,
      '33' => 0,
      '34' => 0,
      '35' => 0,
    ),
    'id' => 'nodequeue_rel',
    'table' => 'node',
    'field' => 'nodequeue_rel',
    'relationship' => 'none',
  ),
));
$handler->override_option('fields', array(
  'common_teaser_image' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'absolute' => 0,
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'exclude' => 0,
    'id' => 'common_teaser_image',
    'table' => 'bonnier_common',
    'field' => 'common_teaser_image',
    'relationship' => 'none',
    'format' => 'thumb_50_linked',
  ),
  'common_title' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'absolute' => 0,
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'link_to_node' => 1,
    'exclude' => 0,
    'id' => 'common_title',
    'table' => 'bonnier_common',
    'field' => 'common_title',
    'relationship' => 'none',
  ),
  'common_dek' => array(
    'label' => '',
    'alter' => array(
      'alter_text' => 0,
      'text' => '',
      'make_link' => 0,
      'path' => '',
      'absolute' => 0,
      'link_class' => '',
      'alt' => '',
      'prefix' => '',
      'suffix' => '',
      'target' => '',
      'help' => '',
      'trim' => 0,
      'max_length' => '',
      'word_boundary' => 1,
      'ellipsis' => 1,
      'html' => 0,
      'strip_tags' => 0,
    ),
    'empty' => '',
    'hide_empty' => 0,
    'empty_zero' => 0,
    'link_to_node' => 1,
    'exclude' => 0,
    'id' => 'common_dek',
    'table' => 'bonnier_common',
    'field' => 'common_dek',
    'relationship' => 'none',
  ),
));
$handler->override_option('sorts', array(
  'position' => array(
    'order' => 'ASC',
    'id' => 'position',
    'table' => 'nodequeue_nodes',
    'field' => 'position',
    'relationship' => 'nodequeue_rel',
  ),
));
$handler->override_option('filters', array(
  'status_extra' => array(
    'operator' => '=',
    'value' => '',
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'id' => 'status_extra',
    'table' => 'node',
    'field' => 'status_extra',
    'relationship' => 'none',
  ),
));
$handler->override_option('access', array(
  'type' => 'none',
));
$handler->override_option('cache', array(
  'type' => 'none',
));
$handler->override_option('items_per_page', 3);
$handler->override_option('style_plugin', 'list');
$handler->override_option('style_options', array(
  'grouping' => '',
  'type' => 'ul',
));
