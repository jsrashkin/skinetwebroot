<?php
// $Id$
/**
 * @file
 * Views default exports for Popphoto Buying Guide
 */



/**
 * Implementation of hook_views_default_views().
 */
function gearfinder_views_default_views() {
$path = drupal_get_path('module', 'gearfinder') . '/views/exports';
  $files = drupal_system_listing('\.inc$', $path, 'name', 0);
  foreach ($files as $file) {
    require($file->filename);
    if (!empty($view->name)) {
      $views[$view->name] = $view;
    }
  }
  return $views;
}

