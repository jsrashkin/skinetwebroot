<?php

// $Id$
/**
 * @file
 * Views hooks for Skinet Gearfinder module.
 */

/**
 * Implementation of hook_views_data().
 */
function gearfinder_views_data() {
  $tables['gearfinder_images']['table']['group'] = t('Gearfinder');

  // Join gearfinder_images to term_data
  $tables['gearfinder_images']['table']['join']['term_data'] = array(
    'left_field' => 'tid',
    'field' => 'tid',
  );

  // Description of the table columns
  $tables['gearfinder_images']['filepath'] = array(
    'title' => t('Term image path'),
    'help' => t('Provides path to taxonomy term image'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
  );

  return $tables;
}

/**
 * Implementation of hook_views_pre_render().
 */
function gearfinder_views_pre_render(&$view) {
  switch ($view->name) {
		case 'gearfinder_brand':
    case 'gearfinder_types':
		case 'gearfinder_filters':
    case 'gearfinder_latest_gear':
      // Include our JS file to redirect the user upon changing order by.
      drupal_add_js(drupal_get_path('module', 'gearfinder') . '/js/gearfinder.forms_helper.js', 'module');
      break;
    default:
      break;
  }
}



/**
 * Implementation of hook_views_pre_build().
 */
function gearfinder_views_query_alter(&$view, &$query) {
  // Support sorting
	if (($view->name == 'gearfinder_filters' && !isset($view->gearfinder_filter_limit)) || $view->name == 'gearfinder_brand') {
		if (isset($_GET['sort-by']) && isset($query->orderby[$_GET['sort-by']])) {
			$query->orderby = array($query->orderby[$_GET['sort-by']]);
		}
	}
}
