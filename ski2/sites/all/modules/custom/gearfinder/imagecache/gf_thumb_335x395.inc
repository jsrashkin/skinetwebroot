<?php
$presets['gf_thumb_335x395'] = array (
  'presetname' => 'gf_thumb_335x395',
  'actions' =>
  array (
    0 =>
    array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' =>
      array (
        'width' => '335',
        'height' => '395',
        'upscale' => 0,
      ),
    ),
    1 =>
    array (
      'weight' => '0',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_definecanvas',
      'data' =>
      array (
        'RGB' =>
        array (
          'HEX' => 'FFFFFF',
        ),
        'under' => 1,
        'exact' =>
        array (
          'width' => '335',
          'height' => '395',
          'xpos' => 'center',
          'ypos' => 'center',
        ),
        'relative' =>
        array (
          'leftdiff' => '',
          'rightdiff' => '',
          'topdiff' => '',
          'bottomdiff' => '',
        ),
      ),
    ),
  ),
);