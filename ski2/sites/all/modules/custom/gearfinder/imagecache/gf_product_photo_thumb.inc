<?php
// Product photo thumbnails from associated gallery
$presets['gf_product_photo_thumb'] = array (
  'presetname' => 'gf_product_photo_thumb',
  'actions' =>
  array (
    0 =>
    array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' =>
      array (
        'width' => '50',
        'height' => '33',
      ),
    ),
  ),
);