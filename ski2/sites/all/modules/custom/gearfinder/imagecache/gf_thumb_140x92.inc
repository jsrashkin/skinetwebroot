<?php
// Used for More Cameras and Lenses on products
$presets['gf_thumb_140x92'] = array (
  'presetname' => 'gf_thumb_140x92',
  'actions' =>
  array (
    0 =>
    array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' =>
      array (
        'width' => '140',
        'height' => '92',
        'upscale' => 0,
      ),
    ),
    1 =>
    array (
      'weight' => '0',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_definecanvas',
      'data' =>
      array (
        'RGB' =>
        array (
          'HEX' => 'FFFFFF',
        ),
        'under' => 1,
        'exact' =>
        array (
          'width' => '140',
          'height' => '92',
          'xpos' => 'center',
          'ypos' => 'center',
        ),
        'relative' =>
        array (
          'leftdiff' => '',
          'rightdiff' => '',
          'topdiff' => '',
          'bottomdiff' => '',
        ),
      ),
    ),
  ),
);