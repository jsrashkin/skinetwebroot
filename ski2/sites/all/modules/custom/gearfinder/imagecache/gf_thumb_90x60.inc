<?php
// Thumbnails for latest gear on Buying Guide home
$presets['gf_thumb_90x60'] = array (
  'presetname' => 'gf_thumb_90x60',
  'actions' =>
  array (
    0 =>
    array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' =>
      array (
        'width' => '90',
        'height' => '60',
        'upscale' => 0,
      ),
    ),
    1 =>
    array (
      'weight' => '0',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_definecanvas',
      'data' =>
      array (
        'RGB' =>
        array (
          'HEX' => 'FFFFFF',
        ),
        'under' => 1,
        'exact' =>
        array (
          'width' => '90',
          'height' => '60',
          'xpos' => 'center',
          'ypos' => 'center',
        ),
        'relative' =>
        array (
          'leftdiff' => '',
          'rightdiff' => '',
          'topdiff' => '',
          'bottomdiff' => '',
        ),
      ),
    ),
  ),
);