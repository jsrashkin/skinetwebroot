<?php
// Used for Brands logos on All Brands page
$presets['gf_thumb_130x130'] = array (
  'presetname' => 'gf_thumb_130x130',
  'actions' =>
  array (
    0 =>
    array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' =>
      array (
        'width' => '130',
        'height' => '130',
        'upscale' => 0,
      ),
    ),
    1 =>
    array (
      'weight' => '0',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_definecanvas',
      'data' =>
      array (
        'RGB' =>
        array (
          'HEX' => 'FFFFFF',
        ),
        'under' => 1,
        'exact' =>
        array (
          'width' => '130',
          'height' => '130',
          'xpos' => 'center',
          'ypos' => 'center',
        ),
        'relative' =>
        array (
          'leftdiff' => '',
          'rightdiff' => '',
          'topdiff' => '',
          'bottomdiff' => '',
        ),
      ),
    ),
  ),
);