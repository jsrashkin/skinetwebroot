<?php
// Sample photo thumbnails from associated gallery
$presets['gf_sample_photo_thumb'] = array (
  'presetname' => 'gf_sample_photo_thumb',
  'actions' =>
  array (
    0 =>
    array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' =>
      array (
        'width' => '80',
        'height' => '53',
      ),
    ),
  ),
);
