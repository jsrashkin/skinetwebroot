<?php

/**
 * @file
 * Include for all site contexts. Naming convention for files "namespace.value.inc"
 */



/**
 * Implementation of hook_context_default_contexts().
 */

function gearfinder_context_default_contexts() {
	$items = array();
	$path = drupal_get_path('module', 'gearfinder') . '/contexts';
	$files = drupal_system_listing('.inc$', $path, 'name', 0);
	foreach($files as $file) {
		include_once $file->filename;
	 }
	return $items;
}