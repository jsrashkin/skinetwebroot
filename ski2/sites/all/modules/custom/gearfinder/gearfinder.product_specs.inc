<?php
// $Id$
/**
 * @file
 * Product specification listing definitions.
 *
 * @see gearfinder_product_specs()
 */


/**
 * Helper function; ski specifications.
 */
function gearfinder_product_specs_product_ski() {
  return array(
    'display after' => 'field_gf_review',
    'fields' => array(
			'field_product_year',
			'field_product_level',
			'field_product_price',
			'field_product_map',
			'field_product_gender',
			'field_product_category',
			// 'field_product_awards',
			'field_rank_hard_snow',
			'field_rank_mixed_snow',
			'field_rank_deep_snow',
			'field_rank_mixed_snow_value',
			'field_rank_hard_snow_value',
			'field_ski_binding',
			'field_ski_lengths',
			'field_ski_tiptailwaist',
			'field_ski_rating_powderperform',
			'field_ski_rating_stabilityspeed',
			'field_ski_rating_longturns',
			'field_ski_rating_mediumturns',
			'field_ski_rating_shortturns',
			'field_ski_rating_maneuver',
			'field_ski_rating_hardsnowgrip',
			'field_ski_rating_crudperform',
			'field_ski_rating_corduroyperform',
			'field_ski_rating_mogulsperform',
			'field_ski_rating_forgiveness',
			'field_ski_rating_quickness',
			'field_ski_rating_reboundenergy',
			'field_ski_rating_flotation',
			'field_ski_rating_averagescore',
			'field_ski_rating_balanceofskill',
			'field_ski_rating_overall',		
		),
  );
}

/**
 * Helper function; boot specifications.
 */
function gearfinder_product_specs_product_boot() {
  return array(
    'display after' => 'field_gf_review',
    'fields' => array(
			'field_product_year',
			'field_product_level',
			'field_product_price',
			'field_product_map',
			'field_product_gender',
			'field_product_category',
			//'field_gold_medal_gear',
			'field_last_width',
			'field_boot_weight',
			'field_boot_flex',
			'field_boot_sizes',
			'field_boot_rating_toeboxfit', 
			'field_boot_rating_forefootfit',
			'field_boot_rating_anklefit',
			'field_boot_rating_instepfit',
			'field_boot_rating_adjustments',
			'field_boot_rating_closure',
			'field_boot_rating_response',
			'field_boot_rating_support',
			'field_boot_rating_flex', 
			'field_boot_rating_steering',
			'field_boot_rating_comfort', 
			'field_boot_rating_overalimp',
			'field_boot_rating_averagescore',
		),
  );
}
