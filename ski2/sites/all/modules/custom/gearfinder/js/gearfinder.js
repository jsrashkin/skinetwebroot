$(document).ready(function(){
    
	$('#creating').hide();
	$('#creating-popup').hide();
        $('#edit-submit-gearfinder-skis-thickbox').hide();
  $('.gearfinder-filters .views-exposed-widget select').change(function(){
		$('.gearfinder-filters .views-exposed-widget select').attr('disabled', true); //disable all of the select fields until page reloads
		$('#creating').show();
    $('.gearfinder-filters').addClass("waiting");
	});    
	
	var limitCount = 200;
	var elements = $('.node-type-product-detail p.the-body');
	var charCount = 0;
	var wrapElement = $(document.createElement("div"));
	wrapElement.attr("class", "full-block");
	
	elements.each(function(s){
		var elementText = $(this).text();
		charCount += elementText.length;
		if(charCount >= limitCount){
			$(this).nextAll().not('.clear').wrapAll(wrapElement);
			return false;
		}
	});
	
	// hide the wrapped element
	var hidden_block = $('.full-block');
	
	if(hidden_block != null){
		// hide the wrapped element
		hidden_block.hide();
	
		var readMoreElement = $(document.createElement("a"));
		readMoreElement.attr("href", "#");
		readMoreElement.attr("onclick", "return false;");
		readMoreElement.attr("class", "read-more-link");
		readMoreElement.text("Read full review");
	
		$(readMoreElement).insertAfter(hidden_block);
	
		readMoreElement.click(function(){
			hidden_block.slideDown("slow");
			readMoreElement.remove();
		});			
	}
});
