$(function() {
	$('#views-exposed-form-gearfinder-skis-thickbox-page-1 .form-item').hover(function(){
  // $("table tr").hover(function(){
    $(this).addClass("highlight");
   },
   function(){
    $(this).removeClass("highlight");
   })
 })


$(document).ready(function() {
	
  $("a[rel=external]").click(function() {
    this.target = "_blank";
  });

  $('.thickbox').click(function() {
    $("iframe").each(function() {
    if (this['id'] != 'TB_iframeContent') {
      $(this).hide();
    }
    });
  });

    $('.content-in a[tooltip]').each(function() {
      $(this).qtip({
        content: $(this).attr('tooltip'),
        style: {
          name: 'cream',
          padding: '7px 13px',
      tip: true,
          width: {
            max: 300,
            min: 0
          }
    },
    position: {
      corner: {
        target: 'topRight',
        tooltip: 'bottomLeft'
      }
    }
      });
   });
  
});

/* overwrite tb_remove() ?? */
function tb_remove() {
  $('#TB_imageOff').unbind('click');
  $('#TB_overlay').unbind('click');
  $('#TB_closeWindowButton').unbind('click');
  $('#TB_window').fadeOut(400,function() {$('#TB_window,#TB_overlay,#TB_HideSelect').trigger('unload').unbind().remove();});
  $('#TB_load').remove();
  $("iframe").each(function() {
    if (this['id'] != 'TB_iframeContent') {
      $(this).show();
    }
  });
  if (typeof document.body.style.maxHeight == 'undefined') { //if IE 6
    $('body','html').css({height: 'auto', width: 'auto'});
    $('html').css('overflow','');
  }
  document.onkeydown = '';
  document.onkeyup = '';
  return false;
}


function gearfinder_get_parent_url() {
	 alert('blah');
}


function brand_form_to_post(filter_type) {
	// get the parent's url:
	var url = top.location.href;
	var getParam = '';
	var counter = 0;
	// ghetto but works:
	var first = true;
	$("input[name='" + filter_type + "[]']:checked").each( function () {
		if (first) {
			getParam += filter_type + '[' + counter + ']=' + $(this).val();
			first = false;
		}
		else {
			getParam += '&' + filter_type + '[' + counter + ']=' + $(this).val();
		}
		counter++;
	});
	if (getParam == '') {
	  alert('Please choose variants.');
	  return false;
	}
	
	// show the creating and disable all buttons:
	$('#creating-popup').show();
	$('#brand-search-container').hide();
	// $('.view-gearfinder-brand-thickbox input[type=checkbox]').attr('disabled','true');
	
	// redirect the parent window to the new URL
	if (url.indexOf('?') != -1) {
	  parent.window.location = url + '&' + getParam;
	}
	else {
	  parent.window.location = url + '?' + getParam;
	}
}

/*function iframe_src_edit_image(nid) {
  $('#right-column-add-edit-image fieldset').removeClass('collapsed');  
  $('#right-column-add-edit-image .fieldset-wrapper').attr('style', 'display:block');  
  $('#right-column-add-edit-image iframe').attr("src",'/node/'+nid+'/edit/column');
}*/

function change_iframe(id, url) {
  $(id).attr("src", url);
}

function refresh_iframe(name) {
  window.frames[name].location.reload();
}

function gearfinder_search_select_all(field) {
	// [@name="+field+"]
  $("INPUT[type='checkbox']").attr('checked', $('#select-all').is(':checked'));
  $('#select-all').toggleClass('checked');
  if ($('#select-all').hasClass('checked')) {
	  $('#brand-results-actions span').text('Deselect All')
  } else {
	  $('#brand-results-actions span').text('Select All')
  }
}


function tb_init_copy() {
  $('a,area,input').filter('.thickbox:not(.initThickbox-processed)').addClass('initThickbox-processed').click(function() {
    tb_setBrowserExtra();
    var t = this.title || this.name || null;
    var a = this.href || this.alt;
    var g = this.rel || false;
    tb_show(t,a,g);
    this.blur();
    return false;
  });
}

