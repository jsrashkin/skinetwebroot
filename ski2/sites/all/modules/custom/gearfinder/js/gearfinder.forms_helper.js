Drupal.behaviors.gearfinderBgFormsHelper = function (context) {
  var url = window.location.toString();
  if (url.match(/page=[0-9]+&?/)) {
    url = url.replace(/page=[0-9]+&?/, '');
  }

$('form[id^=views-exposed-form-gearfinder-skis-thickbox-page-]:not(.gearfinder-form-processed)').find('input[type=submit]').hide();

  $('form[id^=views-exposed-form-gearfinder-filters-page-]:not(.gearfinder-form-processed)')
    .find('input[type=submit]').hide().end()
    .find('select').change(function () {
      if ($(this).val() != 'All') {
        if($(this).attr('name')== 'brand' || $(this).attr('name')== 'price' || $(this).attr('name')== 'level' || $(this).attr('name')== 'width' || $(this).attr('name')== 'awards' ||
            $(this).attr('name')== 'category' || $(this).attr('name')== 'year' || $(this).attr('name')== 'flex' || $(this).attr('name')== 'lastwidth'){
        //if multiselect
        var getParam = encodeURI($(this).attr('name') + '[]=' + $(this).val());             
        } else {
           var getParam = encodeURI($(this).attr('name') + '=' + $(this).val());
        }  
        if (url.indexOf('?') != -1) {
          window.location = url + '&' + getParam;
        }
        else {
          window.location = url + '?' + getParam;
        }
      }
    }).end()
    .addClass('gearfinder-form-processed');

		$('form[id^=views-exposed-form-gearfinder-filters-block-]:not(.gearfinder-form-processed)')
		  .find('input[type=submit]').hide().end()
		  .find('select').change(function () {
		    if ($(this).val() != 'All') {
		      var getParam = encodeURI($(this).attr('name') + '=' + $(this).val());
		      if (url.indexOf('?') != -1) {
		        window.location = url + '&' + getParam;
		      }
		      else {
		        window.location = url + '?' + getParam;
		      }
		    }
		  }).end()
		  .addClass('gearfinder-form-processed');

  $('#gearfinder-sort-by:not(.gearfinder-form-processed)')
    .find('select').change(function () {
      var getParam = encodeURI('sort-by=' + $(this).val());
      if (url.match(/sort-by=[0-9]+/)) {
        window.location = url.replace(/sort-by=[0-9]+/, getParam);
      }
      else if (url.indexOf('?') != -1) {
        window.location = url + '&' + getParam;
      }
      else {
        window.location = url + '?' + getParam;
      }
    }).end()
    .addClass('gearfinder-form-processed');

  $('#gearfinder-latest-gear-category:not(.gearfinder-form-processed)')
    .find('select').val('').change(function () {
      var appendURI = '/' + encodeURI($(this).val());
      if (url.indexOf('?') != -1) {
        window.location = url.replace(/\?.*/, appendURI);
      }
      else {
        window.location = url + appendURI;
      }
    }).end()
    .addClass('gearfinder-form-processed');

  function getCompareCount(context) {
    return context.find('input[id^=product-comparison-select-]:checked').length;
  }
  function uncheckCompare(context) {
    if (getCompareCount(context) == 2) {
      context.find('input[id^=product-comparison-select-]:disabled')
        .removeAttr('disabled');
    }
  }
  function checkCompare(context) {
    if (getCompareCount(context) >= 3) {
      context.find('input[id^=product-comparison-select-]:enabled').not(':checked')
        .attr('disabled', 'disabled');
    }
  }
  $('form.gearfinder-compare-products:not(.gearfinder-form-processed)')
    .each(function () {
      checkCompare($(this));
    })
    .find('input[id^=product-comparison-select-]')
      .change(function () {
        if ($(this).attr('checked')) {
          checkCompare($(this).parents('form'));
        }
        else {
          uncheckCompare($(this).parents('form'));
        }
      })
      .click(function () {
        this.blur();
        this.focus();
      })
    .end()
    .addClass('gearfinder-form-processed');
}
