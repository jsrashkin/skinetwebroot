<?php
$handler = new stdClass;
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'term_view_browse_skis';
$handler->task = 'term_view';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = -29;
$handler->conf = array(
    'autogenerate_title' => FALSE,
    'title' => 'Gearfinder - Skis',
    'no_blocks' => 0,
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'name' => 'vocabulary',
        'id' => 1,
        'identifier' => 'Taxonomy vocabulary',
        'keyword' => 'vocabulary',
        'context_settings' => array(
          'vid' => '12',
          ),
        ),
      ),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'term',
          'settings' => array(
            'vid' => '12',
            1 => array(),
            4 => array(),
            14 => array(),
            2 => array(),
            18 => array(),
            3 => '',
            19 => array(),
            20 => array(),
            9 => array(),
            15 => '',
            25 => array(),
            24 => array(),
            23 => array(),
            22 => array(),
            5 => array(),
            26 => array(),
            12 => array(
              3 => '3',
              ),
            13 => array(),
            21 => array(),
            17 => array(),
            10 => array(),
            6 => array(),
            7 => array(),
            8 => array(),
            16 => array(),
            ),
            'context' => 'argument_terms_1',
            'not' => FALSE,
            ),
            ),
            'logic' => 'and',
            ),
            );
$display = new panels_display;
$display->layout = 'twocol_stacked';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = '%term:name';
$display->content = array();
$display->panels = array();
$pane = new stdClass;
$pane->pid = 'new-1';
$pane->panel = 'left';
$pane->type = 'block';
$pane->subtype = 'views--exp-gearfinder_filters-page_1';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array(
    'inherit_path' => 1,
    'override_title' => 1,
    'override_title_text' => 'Refine Results',
    );
$pane->cache = array();
$pane->style = array();
$pane->css = array(
    'css_id' => '',
    'css_class' => 'gearfinder-filters',
    );
$pane->extras = array();
$pane->position = 0;
$display->content['new-1'] = $pane;
$display->panels['left'][0] = 'new-1';
$pane = new stdClass;
$pane->pid = 'new-2';
$pane->panel = 'left';
$pane->type = 'block';
$pane->subtype = 'bonnier_ads-left';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    );
$pane->cache = array();
$pane->style = array();
$pane->css = array();
$pane->extras = array();
$pane->position = 1;
$display->content['new-2'] = $pane;
$display->panels['left'][1] = 'new-2';
$pane = new stdClass;
$pane->pid = 'new-3';
$pane->panel = 'right';
$pane->type = 'views';
$pane->subtype = 'gearfinder_filters';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '18',
    'pager_id' => '',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 1,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'page_1',
    'override_title' => 1,
    'override_title_text' => '<none>',
    'context' => array(
      0 => '',
      ),
    );
$pane->cache = array();
$pane->style = array();
$pane->css = array();
$pane->extras = array();
$pane->position = 0;
$display->content['new-3'] = $pane;
$display->panels['right'][0] = 'new-3';
$pane = new stdClass;
$pane->pid = 'new-4';
$pane->panel = 'top';
$pane->type = 'gear_header';
$pane->subtype = 'gear_header';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array();
$pane->cache = array();
$pane->style = array();
$pane->css = array();
$pane->extras = array();
$pane->position = 0;
$display->content['new-4'] = $pane;
$display->panels['top'][0] = 'new-4';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;

