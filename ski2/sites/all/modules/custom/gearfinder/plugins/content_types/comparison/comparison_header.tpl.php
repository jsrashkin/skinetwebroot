<div id="product-comparison-header">
	<div class="header-left">
		<?php print $headerleft; ?>
	</div>
	<div class="header-right">
		<div class="utility-links">
		  <div class="email"><?php print $email; ?></div>
		  <div class="print"><?php print $print; ?></div>
		  <div class="share"><?php print $share; ?></div>
		  <?php if ($comment): ?>
		    <div class="comment"><?php print $comment; ?></div>
		  <?php endif; ?>
		</div>
		<?php print $headerright; ?>
	</div>
</div><!-- product-comparison-header -->