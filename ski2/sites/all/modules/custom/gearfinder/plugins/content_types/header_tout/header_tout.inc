<?php

function gearfinder_header_tout_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Gearfinder Header Tout'),
    'description' => t('Topmost tout for gearfinder'),
    'category' => 'Gearfinder v2',
    'hook theme' => 'gearfinder_header_tout_theme',
  );
}

function gearfinder_header_tout_theme(&$theme) {
  $theme['header_tout'] = array(
    'template' => 'header-tout',
    'path' => drupal_get_path('module','gearfinder').'/plugins/content_types/header_tout',
  );
}

function gearfinder_header_tout_content_type_render($subtype, $conf, $panel_args, $context) {
  $content = theme('header_tout');
  $block->content = $content;
  $block->title = '';
  $block->delta = 'header_tout';
  $block->css_class = 'header-tout';
  return $block;
}




function template_preprocess_header_tout(&$vars) {
	// $vars['skis_link'] = l('', 'gear/skis');
	// $vars['boots_link'] = l('', 'gear/boots');
	if ($link_text = variable_get('gearfinder_how_we_test_link_text', 'Read the Story')) {
		$vars['test_text'] = $link_text;
	}
	if ($text = variable_get('gearfinder_how_we_test_text', '')) {
		$vars['video_text'] = $text;
	}
	if ($nid = variable_get('gearfinder_how_we_test_nid', '')) {
		$vars['video_link'] = 'node/'.$nid;
	}
}


function gearfinder_header_tout_content_type_edit_form(&$form, &$form_state) {}