<div class="above-header-content">
	
	<div class="browse-nav">
		<?php print $browse_links; ?>
	</div><!-- browse-nav -->
	
	<div class="social-buttons">
		<?php print $social_buttons; ?>
	</div><!-- social-buttons -->
	
</div><!-- above-header-content -->