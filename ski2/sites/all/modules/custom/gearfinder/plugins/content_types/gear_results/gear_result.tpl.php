<?php //dpm(get_defined_vars()); ?>
<div class="views-row">
  <?php if ($image) : ?>
    <div class="left"><div class="image"><?php print $image ?></div></div>
  <?php endif; ?>
    <div class="right<?php if (!$image) { print ' no-image'; } ?>">
      <div class="title"><?php print $title; ?></div>
      <div class="dek"><?php print $description; ?></div>
      <?php if($tags){ ?>
        <div class="tags"><span class="title">Related Tags:</span><?php print $tags; ?></div>
      <?php } ?>
    </div>
</div>