<div class="photo-box template-<?php print $class; ?>">

  <div class="image">
    <?php print $image; ?>
  </div>

  <?php if($credit): ?>
  <div class="credit">
    <?php print $credit; ?>
  </div>
  <?php endif; ?>

</div>
