<?php
// $Id$

/**
 * Implementation of hook_views_default_views().
 */
function bonnier_type_marketplace_views_default_views() {
  $marketplace_vid = db_result(db_query("SELECT vid FROM {vocabulary} WHERE module = 'bonnier_type_marketplace'"));

  $view = new view;
  $view->name = 'marketplace_listing';
  $view->description = t('Display a listing of marketplace articles.');
  $view->tag = t('Bonnier');
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', t('Defaults'), 'default');
  $handler->override_option('fields', array(
    'common_teaser_image' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => FALSE,
        'max_length' => '',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'strip_tags' => FALSE,
        'html' => FALSE,
      ),
      'exclude' => FALSE,
      'id' => 'common_teaser_image',
      'table' => 'bonnier_common',
      'field' => 'common_teaser_image',
      'relationship' => 'none',
      'format' => 'article_thumbnail_linked',
    ),
    'common_title' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => FALSE,
        'max_length' => '',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'strip_tags' => FALSE,
        'html' => FALSE,
      ),
      'link_to_node' => TRUE,
      'exclude' => FALSE,
      'id' => 'common_title',
      'table' => 'bonnier_common',
      'field' => 'common_title',
      'relationship' => 'none',
    ),
    'common_dek' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => FALSE,
        'text' => '',
        'make_link' => FALSE,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => FALSE,
        'max_length' => '',
        'word_boundary' => TRUE,
        'ellipsis' => TRUE,
        'strip_tags' => FALSE,
        'html' => FALSE,
      ),
      'link_to_node' => FALSE,
      'exclude' => FALSE,
      'id' => 'common_dek',
      'table' => 'bonnier_common',
      'field' => 'common_dek',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'created' => array(
      'order' => 'DESC',
      'granularity' => 'second',
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'term_node_tid_depth' => array(
      'default_action' => 'not found',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => t('All'),
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'taxonomy_term',
      'validate_fail' => 'not found',
      'depth' => 2,
      'break_phrase' => TRUE,
      'set_breadcrumb' => FALSE,
      'id' => 'term_node_tid_depth',
      'table' => 'node',
      'field' => 'term_node_tid_depth',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => FALSE,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(),
      'validate_argument_node_access' => FALSE,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        $marketplace_vid => $marketplace_vid,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => FALSE,
      'validate_user_restrict_roles' => FALSE,
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => 1,
      'group' => 0,
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'marketplace' => 'marketplace',
      ),
      'group' => 0,
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('use_pager', TRUE);
  $handler->override_option('style_plugin', 'list');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'type' => 'ul',
  ));
  foreach (taxonomy_get_tree(db_result(db_query("SELECT vid FROM {vocabulary} WHERE module = 'bonnier_type_marketplace'")), 0, -1, 1) as $channel) {
    foreach (taxonomy_get_tree($channel->vid, $channel->tid, -1, 1) as $term) {
      $handler = $view->new_display('panel_pane', t('@channel/@term content pane', array('@channel' => $channel->name, '@term' => $term->name)), 'panel_pane_'. $term->tid);
      $handler->override_option('sorts', array());
      $handler->override_option('arguments', array(
        'tid' => array(
          'default_action' => 'summary asc',
          'style_plugin' => 'default_summary',
          'style_options' => array(
            'count' => TRUE,
            'override' => FALSE,
            'items_per_page' => 0,
          ),
          'wildcard' => 'all',
          'wildcard_substitution' => t('All'),
          'title' => '%1',
          'breadcrumb' => '',
          'default_argument_type' => 'fixed',
          'default_argument' => '',
          'validate_type' => 'taxonomy_term',
          'validate_fail' => 'not found',
          'break_phrase' => FALSE,
          'add_table' => FALSE,
          'require_value' => TRUE,
          'reduce_duplicates' => FALSE,
          'set_breadcrumb' => FALSE,
          'id' => 'tid',
          'table' => 'term_node',
          'field' => 'tid',
          'validate_user_argument_type' => 'uid',
          'validate_user_roles' => array(
          ),
          'override' => array(
            'button' => t('Use default'),
          ),
          'relationship' => 'none',
          'default_options_div_prefix' => '',
          'default_argument_user' => FALSE,
          'default_argument_fixed' => '',
          'default_argument_php' => '',
          'validate_argument_node_type' => array(
          ),
          'validate_argument_node_access' => FALSE,
          'validate_argument_nid_type' => 'nid',
          'validate_argument_vocabulary' => array(
            $marketplace_vid => $marketplace_vid,
          ),
          'validate_argument_type' => 'tid',
          'validate_argument_transform' => FALSE,
          'validate_user_restrict_roles' => FALSE,
          'validate_argument_php' => '',
        ),
      ));
      $handler->override_option('filters', array(
        'status' => array(
          'operator' => '=',
          'value' => 1,
          'group' => 0,
          'exposed' => FALSE,
          'expose' => array(
            'operator' => FALSE,
            'label' => '',
          ),
          'id' => 'status',
          'table' => 'node',
          'field' => 'status',
          'relationship' => 'none',
        ),
        'type' => array(
          'operator' => 'in',
          'value' => array(
            'marketplace' => 'marketplace',
          ),
          'group' => 0,
          'exposed' => FALSE,
          'expose' => array(
            'operator' => FALSE,
            'label' => '',
          ),
          'id' => 'type',
          'table' => 'node',
          'field' => 'type',
          'relationship' => 'none',
        ),
        'term_node_tid_depth' => array(
          'operator' => 'or',
          'value' => array(
            $term->tid => $term->tid,
          ),
          'group' => 0,
          'exposed' => FALSE,
          'expose' => array(
            'operator' => FALSE,
            'label' => '',
          ),
          'type' => 'select',
          'limit' => TRUE,
          'vid' => $marketplace_vid,
          'depth' => 1,
          'id' => 'term_node_tid_depth',
          'table' => 'node',
          'field' => 'term_node_tid_depth',
          'hierarchy' => 1,
          'override' => array(
            'button' => t('Use default'),
          ),
          'relationship' => 'none',
          'reduce_duplicates' => FALSE,
        ),
      ));
      $handler->override_option('title', $term->name);
      $handler->override_option('items_per_page', 0);
      $handler->override_option('use_pager', FALSE);
      $handler->override_option('row_plugin', 'node');
      $handler->override_option('row_options', array(
        'relationship' => 'none',
        'build_mode' => 'teaser',
        'links' => FALSE,
        'comments' => FALSE,
      ));
      $handler->override_option('pane_title', t('Marketplace categories: @channel/@term', array('@channel' => $channel->name, '@term' => $term->name)));
      $handler->override_option('pane_description', '');
      $handler->override_option('pane_category', array(
        'name' => 'View panes',
        'weight' => 0,
      ));
      $handler->override_option('allow', array(
        'use_pager' => FALSE,
        'items_per_page' => 0,
        'offset' => 0,
        'link_to_view' => FALSE,
        'more_link' => FALSE,
        'path_override' => 'path_override',
        'title_override' => FALSE,
        'exposed_form' => FALSE,
      ));
      $handler->override_option('argument_input', array());
      $handler->override_option('link_to_view', 0);
      $handler->override_option('inherit_panels_path', 0);
    }
  }
  $views[$view->name] = $view;

  return $views;
}
