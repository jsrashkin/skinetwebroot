<?php
// $Id: $

/**
 * @file
 * This is not a bulk export. These are dynamically generated page handlers.
 */

/**
 * Implementation of hook_default_page_manager_handlers()
 */
function bonnier_type_marketplace_default_page_manager_handlers() {
  $marketplace_vid = db_result(db_query("SELECT vid FROM {vocabulary} WHERE module = 'bonnier_type_marketplace'"));

  $handler = new stdClass;
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_marketplace';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => t('Marketplace'),
    'no_blocks' => FALSE,
    'css_id' => 'marketplace',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(
      array(
        'context' => 'argument_nid_1',
        'name' => 'term_from_node',
        'id' => 1,
        'identifier' => t('Marketplace category'),
        'keyword' => 'marketplace_category',
        'relationship_settings' => array('vid' => $marketplace_vid),
      ),
    ),
    'access' => array(
      'plugins' => array(
        array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'marketplace' => 'marketplace',
            ),
          ),
          'context' => 'argument_nid_1',
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display;
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array();
  $display->cache = array();
  $display->title = '';
  $display->hide_title = FALSE;
  $display->title_pane = 97;
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass;
  $pane->pid = 'marketplace_header';
  $pane->panel = 'middle';
  $pane->type = 'marketplace_header';
  $pane->subtype = 'marketplace_header';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'relationship_term_from_node_1',
    'override_title' => FALSE,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content[$pane->pid] = $pane;
  $display->panels[$pane->panel][$pane->position] = $pane->pid;
  $pane = new stdClass;
  $pane->pid = 'node-content';
  $pane->panel = 'middle';
  $pane->type = 'node_content';
  $pane->subtype = 'node_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'links' => FALSE,
    'page' => TRUE,
    'no_extras' => FALSE,
    'override_title' => TRUE,
    'override_title_text' => '<none>',
    'teaser' => FALSE,
    'identifier' => '',
    'link' => FALSE,
    'leave_node_title' => TRUE,
    'context' => 'argument_nid_1',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'article-content',
  );
  $pane->extras = array();
  $pane->position = 1;
  $display->content[$pane->pid] = $pane;
  $display->panels[$pane->panel][$pane->position] = $pane->pid;
  $handler->conf['display'] = $display;
  $handlers[$handler->name] = $handler;

  foreach (taxonomy_get_tree(db_result(db_query("SELECT vid FROM {vocabulary} WHERE module = 'bonnier_type_marketplace'")), 0, -1, 1) as $channel) {
    $handler = new stdClass;
    $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
    $handler->api_version = 1;
    $handler->name = 'term_view_panel_context_marketplace_'. $channel->tid;
    $handler->task = 'term_view';
    $handler->subtask = '';
    $handler->handler = 'panel_context';
    $handler->weight = 0;
    $handler->conf = array(
      'title' => $channel->name,
      'no_blocks' => FALSE,
      'css_id' => 'marketplace-listing',
      'css' => '',
      'contexts' => array(),
      'relationships' => array(
        array(
          'context' => 'argument_terms_1',
          'name' => 'term_parent',
          'id' => 1,
          'identifier' => t('Term parent'),
          'keyword' => 'parent_term',
          'relationship_settings' => array('type' => 'top'),
        ),
      ),
      'access' => array(
        'plugins' => array(
          array(
            'name' => 'term',
            'settings' => array(
              'vid' => $marketplace_vid,
              $marketplace_vid => array($channel->tid => $channel->tid),
            ),
            'context' => 'relationship_term_parent_1',
          ),
        ),
        'logic' => 'and',
      ),
    );
    $display = new panels_display;
    $display->layout = 'twocol_stacked';
    $display->layout_settings = array();
    $display->panel_settings = array(
      'left' => array(
        'style' => 'template',
      ),
      'style_settings' => array(
        'left' => array(
          'template_theme' => 'marketplace-leftbar',
          'simple_theme' => 1,
        ),
      ),
    );
    $display->cache = array();
    $display->title = '%term:names';
    $display->hide_title = FALSE;
    $display->title_pane = 58;
    $display->content = array();
    $display->panels = array();
    $pane = new stdClass;
    $pane->pid = 'marketplace_header';
    $pane->panel = 'top';
    $pane->type = 'marketplace_header';
    $pane->subtype = 'marketplace_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_terms_1',
      'override_title' => FALSE,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $display->content[$pane->pid] = $pane;
    $display->panels[$pane->panel][$pane->position] = $pane->pid;
    $position = 0;
    foreach (taxonomy_get_tree($channel->vid, $channel->tid, -1, 1) as $term) {
      $pane = new stdClass;
      $pane->pid = 'marketplace-categories-'. $term->tid;
      $pane->panel = 'left';
      $pane->type = 'views_panes';
      $pane->subtype = 'marketplace_listing-panel_pane_'. $term->tid;
      $pane->shown = TRUE;
      $pane->access = array();
      $pane->configuration = array(
        'path' => 'taxonomy/term',
      );
      $pane->cache = array();
      $pane->style = array();
      $pane->css = array(
        'css_id' => '',
        'css_class' => 'marketplace-categories',
      );
      $pane->extras = array();
      $pane->position = $position++;
      $display->content[$pane->pid] = $pane;
      $display->panels[$pane->panel][$pane->position] = $pane->pid;
    }
    $pane = new stdClass;
    $pane->pid = 'channel-listing';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'marketplace_listing';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nodes_per_page' => 10,
      'pager_id' => 0,
      'use_pager' => TRUE,
      'offset' => 0,
      'more_link' => FALSE,
      'feed_icons' => FALSE,
      'panel_args' => FALSE,
      'link_to_view' => FALSE,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'context' => array('argument_terms_1.tids'),
      'override_title' => FALSE,
      'override_title_text' => '',
      'override_pager_settings' => FALSE,
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'channel-listing',
    );
    $pane->extras = array();
    $pane->position = 0;
    $display->content[$pane->pid] = $pane;
    $display->panels[$pane->panel][$pane->position] = $pane->pid;
    $handler->conf['display'] = $display;
    $handlers[$handler->name] = $handler;
  }

  return $handlers;
}
