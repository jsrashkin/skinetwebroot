<?php
// $Id$

function bonnier_type_marketplace_preprocess_node_marketplace(&$vars) {
  $node = $vars['node'];

  // Add the manufacturer's website
  if ($node->field_manufacturer_website[0]['view']) {
    $vars['website'] = $node->field_manufacturer_website[0]['view'];
  }

  // Fill the photo box
  $photo_node = node_load($node->field_main_photo[0]['nid']);

  if($photo_node) {
    $vars['photo_box'] = theme('marketplace_photo', $photo_node, $node);
  }

  // Paging
  $vars['pager'] = $node->paging;

  if (!empty($node->field_manufacturer_email[0]['email'])) {
    $vars['request'] = 'hai';
    $vars['below_node_content'] .= drupal_get_form('bonnier_type_marketplace_request_form');
  }

}
