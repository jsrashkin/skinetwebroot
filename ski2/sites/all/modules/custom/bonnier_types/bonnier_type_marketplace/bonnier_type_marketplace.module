<?php
// $Id$

/**
 * Implementation of hook_block().
 */
function bonnier_type_marketplace_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      return array(
        'marketplace' => array(
          'info' => t('Marketplace'),
          'cache' => BLOCK_CACHE_GLOBAL,
        ),
      );
    case 'view':
      switch ($delta) {
        case 'marketplace':
          return array(
            'subject' => t('Marketplace'),
            'content' => theme('marketplace_block_marketplace'),
          );
      }
      break;
  }
}

/**
 * Implementation of hook_node_info().
 */
function bonnier_type_marketplace_node_info() {
  return array(
    'marketplace' => array(
      'name' => t('Marketplace article'),
      'module' => 'bonnier_type_marketplace',
      'description' => t('A <em>marketplace article</em> is an article about a vendor.'),
      'title_label' => t('Vendor'),
      'body_label' => t('Description'),
    ),
  );
}

/**
 * Implementation of hook_perm().
 */
function bonnier_type_marketplace_perm() {
  return array(
    'create marketplace content',
    'delete own marketplace content',
    'delete any marketplace content',
    'edit own marketplace content',
    'edit any marketplace content',
  );
}

/**
 * Implementation of hook_access().
 */
function bonnier_type_marketplace_access($op, $node, $account) {
  switch ($op) {
    case 'create':
      $access = user_access('create marketplace content', $account);
      break;
    case 'delete':
      $access = user_access('delete any marketplace content', $account) || user_access('delete own marketplace content', $account) && $account->uid == $node->uid;
      break;
    case 'update':
      $access = user_access('edit any marketplace content', $account) || user_access('edit own marketplace content', $account) && $account->uid == $node->uid;
      break;
  }

  drupal_alter('bonnier_access', $access, $op, $node, $account);

  return $access;
}

/**
 * Implementation of hook_form().
 */
function bonnier_type_marketplace_form($node, $form_state) {
  $form = node_content_form($node, $form_state);
  $form['main_photo_placement'] = array(
    '#type' => 'select',
    '#title' => t('Main photo placement'),
    '#default_value' => isset($node->main_photo_placement) ? $node->main_photo_placement : 'left',
    '#options' => array(
      'right' => t('Right'),
      'left' => t('Left'),
      'center' => t('Center'),
    ),
    '#description' => t('Choose the placement for the main photo. Right and left photos will be displayed within the article body. Center photos will be displayed as a header across the top of the article.'),
  );
  return $form;
}

/**
 * Implementation of hook_load().
 */
function bonnier_type_marketplace_load($node) {
  return db_fetch_object(db_query("SELECT * FROM {bonnier_type_marketplace} WHERE vid = %d", $node->vid));
}

/**
 * Implementation of hook_insert().
 */
function bonnier_type_marketplace_insert($node) {
  drupal_write_record('bonnier_type_marketplace', $node);
}

/**
 * Implementation of hook_update().
 */
function bonnier_type_marketplace_update($node) {
  drupal_write_record('bonnier_type_marketplace', $node, array('vid'));
  if (!db_affected_rows()) {
    bonnier_type_marketplace_insert($node);
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function bonnier_type_marketplace_nodeapi($node, $op, $teaser, $page) {
  switch ($op) {
    case 'delete revision':
      db_query("DELETE FROM {bonnier_type_marketplace} WHERE vid = %d", $node->vid);
      break;
  }
}

/**
 * Implementation of hook_delete().
 */
function bonnier_type_marketplace_delete($node) {
  db_query("DELETE FROM {bonnier_type_marketplace} WHERE nid = %d", $node->nid);
}

/**
 * Implementation of hook_content_extra_fields().
 */
function bonnier_type_marketplace_content_extra_fields($type_name) {
  if ($type_name == 'marketplace') {
    return array(
      'main_photo_placement' => array(
        'label' => t('Main photo placement'),
        'description' => t('Main photo placement setting.'),
      ),
    );
  }
}

/**
 * Implementation of hook_views_api().
 */
//function bonnier_type_marketplace_views_api() {
//  return array(
//    'api' => 2.0,
//    'path' => drupal_get_path('module', 'bonnier_type_marketplace') .'/includes',
//  );
//}

/**
 * Implementation of hook_ctools_plugin_api().
 */
//function bonnier_type_marketplace_ctools_plugin_api($module, $api) {
//  $info = array();
//  switch ($module) {
//    case 'page_manager':
//      switch ($api) {
//        case 'pages_default':
//          $info = array(
//            'version' => 1,
//            'path' => drupal_get_path('module', 'bonnier_type_marketplace') .'/includes',
//          );
//          break;
//      }
//      break;
//  }
//  return $info;
//}

/**
 * Implementation of hook_ctools_plugin_directory().
 */
function bonnier_type_marketplace_ctools_plugin_directory($module, $plugin) {
  switch ($module) {
    case 'ctools':
      switch ($plugin) {
        case 'content_types':
          return 'plugins/content_types';
      }
      break;
  }
}

/**
 * Implementation of hook_theme()
 */
function bonnier_type_marketplace_theme() {
  return array(
    'marketplace_photo' => array(
      'template' => 'theme/marketplace-photo',
      'arguments' => array('photo_node' => NULL, 'node' => NULL),
    ),
    'marketplace_block_marketplace' => array(
      'template' => 'theme/marketplace-block-marketplace',
      'arguments' => array(),
    ),
  );
}

/**
 * Implementation of hook_theme_registry_alter().
 */
function bonnier_type_marketplace_theme_registry_alter(&$theme_registry) {
  $path = drupal_get_path('module', 'bonnier_type_marketplace') .'/theme';
  $theme_registry['node']['theme paths'][] = $path;
  $theme_registry['location']['theme paths'][] = $path;
  $theme_registry['locations']['theme paths'][] = $path;
  $theme_registry['views_view_summary']['theme paths'][] = $path;
}

/**
 * Provide variables for the Marketplace photo template.
 */
function template_preprocess_marketplace_photo(&$vars) {
  $node = $vars['node'];
  // Fill the photo box
  $photo_node = $vars['photo_node'];
  if($photo_node) {
    $template = $node->main_photo_placement;
    $template = $template?$template:'left';
    if($photo_node->field_image[0]['data']['alt'] != '') {
    	$alt = $photo_node->field_image[0]['data']['alt'];
    } else {
    	$alt = $node->title;
    }
    if($template == 'center') {
      $image = theme('imagecache','article_image_center',$photo_node->field_image[0]['filepath'],$alt);
    } else {
      $image = theme('imagecache','article_image',$photo_node->field_image[0]['filepath'],$alt);
    }
    $vars['image'] = $image;
    $class = $template;
    $vars['class'] = $class;

    $caption = $photo_node->body;
    $vars['caption'] = $caption;

    $title = $photo_node->title;
    $vars['title'] = $title;

		$imagepath = bonnier_common_image($photo_node);
	  $enlarged_imagepath = imagecache_create_url('enlarged_image', $imagepath);

		$enlarge = l('Enlarge Photo', $enlarged_imagepath, array('attributes' => array('class' => 'thickbox enlarge', 'title' => $photo_node->title)));
		$vars['enlarge'] = $enlarge;
  }
}

/**
 * Provide variables for the Marketplace block template.
 */
function template_preprocess_marketplace_block_marketplace(&$variables) {
  $term = reset(taxonomy_get_tree(db_result(db_query("SELECT vid FROM {vocabulary} WHERE module = 'bonnier_type_marketplace'")), 0, -1, 1));
  $channels = taxonomy_get_tree($term->vid, $term->tid, -1, 1);
  if (!empty($channels)) {
    if (count($channels) == 1) {
      $term = reset($channels);
      $channels = taxonomy_get_tree($term->vid, $term->tid, -1, 1);
    }
    foreach (array_slice($channels, 0, 3) as $channel) {
      $items[] = l($channel->name, 'taxonomy/term/'. $channel->tid);
    }
    if (count($channels) > 3) {
      $items[] = l('And More', 'taxonomy/term/'. $term->tid) .'&hellip;';
    }
    $variables['channels'] = theme('item_list', $items, t('Find great Products & Services!'));
  }
}

/**
 * Register the node type preprocess functions
 */
function bonnier_type_marketplace_preprocess_node(&$variables) {
  if ($variables['node']->type == 'marketplace') {
    module_load_include('inc', 'bonnier_type_marketplace', 'includes/bonnier_type_marketplace.theme');
    bonnier_type_marketplace_preprocess_node_marketplace($variables);
  }
}

function bonnier_type_marketplace_preprocess_location(&$variables) {
  $location = $variables['location'];
  if ($node = node_load(db_result(db_query_range("SELECT nid FROM {location_instance} WHERE lid = %d", $location['lid'], 0, 1)))) {
    if ($node->type == 'marketplace') {
      $variables['template_files'][] = 'location-marketplace';
      if (!empty($location['name'])) {
        $variables['name'] = check_plain($location['name']);
      }
      $city = $variables['city'];
      $province = $variables['province'];
      $postal_code = $variables['postal_code'];
      if (!empty($city) || !empty($province) || !empty($postal_code)) {
        $city_province_postal = array();
        if (!empty($city)) {
          $city_province_postal[] = '<span class="locality">'. $city .'</span>';
        }
        if (!empty($province)) {
          $city_province_postal[] = '<span class="region">'. $province .'</span>';
        }
        $city_province_postal = implode(', ', $city_province_postal);
        if (!empty($postal_code)) {
          if (empty($city_province_postal)) {
            $city_province_postal = '<span class="postal-code">'. $postal_code .'</span>';
          }
          else {
            $city_province_postal .= ' <span class="postal-code">'. $postal_code .'</span>';
          }
        }
        $variables['city_province_postal'] = $city_province_postal;
      }
    }
  }
}

function bonnier_type_marketplace_preprocess_locations(&$variables) {
  $locations = $variables['rawlocs'];
  $location = reset($locations);
  if ($node = node_load(db_result(db_query_range("SELECT nid FROM {location_instance} WHERE lid = %d", $location['lid'], 0, 1)))) {
    if ($node->type == 'marketplace') {
      $variables['template_files'][] = 'locations-marketplace';
    }
  }
}

function bonnier_type_marketplace_preprocess_views_view_summary(&$variables) {
  $view = $variables['view'];
  if ($view->name == 'marketplace_listing' && $view->display[$view->current_display]->display_plugin == 'panel_pane') {
    $variables['template_files'][] = 'views-view-summary--marketplace-listing--panel-pane';
  }
}

/**
 * Request more information form
 */
function bonnier_type_marketplace_request_form($form_state) {
  $form['request'] = array(
    '#type' => 'fieldset',
    '#title' => t('Request More Information'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['request']['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['request']['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['request']['address_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['request']['address_2'] = array(
    '#type' => 'textfield',
    '#title' => '',
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => FALSE,
  );
  $form['request']['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );

  $states = regions_api_iso2_get_options_array('US');
  $provinces = regions_api_iso2_get_options_array('CA');
  $default = array('' => 'Select a State / Province');
  $options = array_merge($default, $states, $provinces);

  $form['request']['state'] = array(
    '#type' => 'select',
    '#title' => t('State / Province'),
    '#default_value' => '',
    '#options' => $options,
    '#required' => TRUE,
  );
  $form['request']['postal_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip / Postal Code'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['request']['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $countries =
  $form['request']['country'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#default_value' => 'US',
    '#options' => countries_api_get_options_array(),
    '#required' => TRUE,
  );
  $form['request']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['request']['email_confirm'] = array(
    '#type' => 'textfield',
    '#title' => t('Confirm Email'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['nid'] = array('#type' => 'hidden', '#value' => arg(1));
  $form['request']['signup'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sign Up for Great Offers by this Advertiser'),
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));

  return $form;
}

/**
 * Validation handler for Request more information form
 */
function bonnier_type_marketplace_request_form_validate($form, &$form_state) {
  // If email address is not a valid format
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t('Please verify your email address.'));
  }
  // Else if email confirmation does not match email
  elseif ($form_state['values']['email'] != $form_state['values']['email_confirm']) {
    form_set_error('email_confirm', t('Please confirm your email address.'));
  }
}

/**
 * Submit handler for Request more information form
 */
function bonnier_type_marketplace_request_form_submit($form, &$form_state) {
  global $language;
  $params = $form_state['values'];
  $node = node_load($form_state['values']['nid']);
  $params['node'] = $node;
  $to = $node->field_manufacturer_email[0]['email'];
  $from = $form_state['values']['email'];
  drupal_mail('bonnier_type_marketplace', 'request', $to, $language, $params, $from);
  drupal_set_message('Your request for more information is being processed.');
}

/**
 * Implementation of hook_mail()
 */
function bonnier_type_marketplace_mail($key, &$message, $params) {
  switch($key) {
    case 'request':
    $message['body'][] = "There has been a new request for information generated by ". variable_get('site_name', 'Bonnier');
    $message['body'][] = "This request was generated from ". url('node/'. $params['node']->nid, array('absolute' => TRUE));
    $message['body'][] .= "First Name: ". $params['first_name'];
    $message['body'][] .= "Last Name: ". $params['last_name'];
    $message['body'][] .= "Address: ". $params['address_1'];
    $message['body'][] .= $params['address_2'];
    $message['body'][] .= "City: ". $params['city'];
    $message['body'][] .= "State: ". $params['state'];
    $message['body'][] .= "Postal Code: ". $params['postal_code'];
    $message['body'][] .= "Phone: ". $params['phone'];
    $message['body'][] .= "Country: ". $params['country'];
    $message['body'][] .= "Email: ". $params['email'];
    if ($params['signup']) {
      $message['body'][] .= "This user would like to be signed up for additional offers";
    }
    else {
      $message['body'][] .= "This user would like to be exculded from additional offers";
    }
    $message['subject'] = variable_get('site_name', 'Bonnier') .': Request for more information';
    break;
  }
}
