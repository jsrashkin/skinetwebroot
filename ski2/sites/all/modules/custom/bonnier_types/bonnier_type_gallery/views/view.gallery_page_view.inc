<?php

$view = new view;
$view->name = 'gallery_page_view';
$view->description = '';
$view->tag = t('Bonnier');
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = 2;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
$handler = $view->new_display('default', t('Defaults'), 'default');
$handler->override_option('relationships', array(
  'field_pages_nid' => array(
    'label' => t('Pages'),
    'required' => TRUE,
    'delta' => -1,
    'id' => 'field_pages_nid',
    'table' => 'node_data_field_pages',
    'field' => 'field_pages_nid',
    'relationship' => 'none',
  ),
));
$handler->override_option('arguments', array(
  'nid' => array(
    'default_action' => 'ignore',
    'style_plugin' => 'default_summary',
    'style_options' => array(),
    'wildcard' => 'all',
    'wildcard_substitution' => t('All'),
    'title' => '',
    'breadcrumb' => '',
    'default_argument_type' => 'fixed',
    'default_argument' => '',
    'validate_type' => 'none',
    'validate_fail' => 'not found',
    'break_phrase' => FALSE,
    'not' => FALSE,
    'id' => 'nid',
    'table' => 'node',
    'field' => 'nid',
    'validate_user_argument_type' => 'uid',
    'validate_user_roles' => array(
    ),
    'relationship' => 'none',
    'default_options_div_prefix' => '',
    'default_argument_user' => FALSE,
    'default_argument_fixed' => '',
    'default_argument_php' => '',
    'validate_argument_node_type' => array(
    ),
    'validate_argument_node_access' => FALSE,
    'validate_argument_nid_type' => 'nid',
    'validate_argument_vocabulary' => array(
    ),
    'validate_argument_type' => 'tid',
    'validate_argument_transform' => FALSE,
    'validate_user_restrict_roles' => FALSE,
    'validate_argument_node_flag_name' => '*relationship*',
    'validate_argument_node_flag_test' => 'flaggable',
    'validate_argument_node_flag_id_type' => 'id',
    'validate_argument_user_flag_name' => '*relationship*',
    'validate_argument_user_flag_test' => 'flaggable',
    'validate_argument_user_flag_id_type' => 'id',
    'validate_argument_php' => '',
  ),
));
$handler->override_option('access', array(
  'type' => 'none',
));
$handler->override_option('cache', array(
  'type' => 'none',
));
$handler->override_option('items_per_page', 1);
$handler->override_option('use_pager', 'mini');
$handler->override_option('row_plugin', 'node');
$handler->override_option('row_options', array(
  'relationship' => 'field_pages_nid',
  'build_mode' => 'full',
  'links' => FALSE,
  'comments' => FALSE,
));
