<?php
// $Id: node.tpl.php,v 1.4 2008/09/15 08:11:49 johnalbin Exp $

/**
 * @file node.tpl.php
 *
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: Node body or teaser depending on $teaser flag.
 * - $picture: The authors picture of the node output from
 *   theme_user_picture().
 * - $date: Formatted creation date (use $created to reformat with
 *   format_date()).
 * - $links: Themed links like "Read more", "Add new comment", etc. output
 *   from theme_links().
 * - $name: Themed username of node author output from theme_user().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $submitted: themed submission information output from
 *   theme_node_submitted().
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $teaser: Flag for the teaser state.
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"><div class="node-inner">

  <?php if ($unpublished): ?>
    <div class="unpublished"><?php print t('Unpublished'); ?></div>
  <?php endif; ?>
  <div class="content">
    <?php if($above_node_content): ?>
      <div class="above-node-content">
        <?php print $above_node_content; ?>
      </div>
    <?php endif; ?>

    <?php if ($photo_box): ?>
        <?php print $photo_box; ?>
    <?php endif; ?>
		
		
<?php if($page){ ?>
<div class="agent-info">
	<div class="left">	
	  <?php print $picture; ?>	
	</div>
	<div class="right">
	    <h2 class="title">
	      <?php /*print $title;*/ ?>
        <?php $node_tmp = node_load($node->nid); print $node_tmp->title; ?>
	    </h2>
		
	    <?php if($locations):?>
				<div class="locations"><strong>Destination Expertise:</strong> <?php print $locations; ?> </div>
			<?php endif; ?>
			<?php if($specialties): ?>
				<div class="specialties"><strong>Specialties:</strong> <?php print $specialties; ?></div>
			<?php endif; ?>
			<div class="office-contact"><strong>Office Contact:</strong>
				<?php print $field_company[0]['value'];?>
				<p><?php print $field_address[0]['value'];?> <?php print $field_address2[0]['value'];?></p>
				<p><?php print $field_city[0]['value'] .' '.$field_state[0]['value'].' '.$field_postal_code[0]['value'];?></p>
			</div>
			<?php if($field_phone[0]['value'] != ''):?>
				<div class="phone"><strong>Phone:</strong> <?php print $field_phone[0]['value'];?></div>
			<?php endif; ?>
			<?php if($field_website[0]['url'] != ''): ?>
				<div class="website"><strong>Website:</strong> <?php print $field_website[0]['view']; ?></div>
			<?php endif; ?>
	</div>		
	<div class="clear"></div>
</div>	

		<div class="agent-bio">
			<?php print $body; ?>
		</div>
		<div class="clear-block"></div>
		<div class="contact-agent">
			<h2>Email Contact</h2>
			<p>	Complete and submit this form below to contact this specialist directly.</p>
			<?php print $form; ?>
		</div>
		<div class="clear-block"></div>

    <?php if($below_node_content): ?>
      <div class="below-node-content">
        <?php print $below_node_content; ?>
      </div>
      <div class="clear-block"></div>
    <?php endif; ?>
    
    <?php } else { ?>
    
     <?php print $picture; ?>	
    
    <h2 class="title">
      <a href="<?php print $node_url; ?>" title="<?php print $title ?>"><?php print $title; ?></a>
    </h2>
    
			<div class="list-content">
				<?php if($locations):?>
					<div class="locations"><strong>Destination Expertise:</strong> <?php print $locations; ?> </div>
				<?php endif;?>
      <?php if($specialties): ?>
        <div class="specialties"><strong>Specialties:</strong> <?php print $specialties; ?></div>
      <?php endif; ?>
				<div class="bio"><strong>Bio:</strong> <?php print $dek; ?></div>
				<div class="more-link"><?php print l('More &#x25B6;','node/'.$nid,array('html'=>TRUE));?></div>
			</div>
		<?php } ?>
  </div>
<div class="clear"></div>
  <div class="related-info">
    <?php print $related_info; ?>   
  </div>

  <?php print $links; ?>

  <?php print $pager; ?> 

</div></div> <!-- /node-inner, /node -->
