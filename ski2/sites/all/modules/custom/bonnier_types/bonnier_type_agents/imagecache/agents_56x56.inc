<?php
$presets['agents_56x56'] = array (
  'presetname' => 'agents_56x56',
  'actions' =>
  array (
    0 =>
    array (
      'weight' => '0',
      'module' => 'imagecache_icon',
      'action' => 'imagecache_icon',
      'data' =>
      array (
        'width' => '56',
        'pad_width' => 1,
        'height' => '56',
        'pad_height' => 1,
        'upscale' => 0,
        'canvas_x_offset' => 'center',
        'canvas_y_offset' => 'center',
        'bg_hex' => 'FFFFFF',
        'border_hex' => '',
        'border_size' => '0',
        'ratio_landscape' => '',
        'ratio_portrait' => '',
      ),
    ),
  ),
);
