<?php
$presets['agents_150x150'] = array (
  'presetname' => 'agents_150x150',
  'actions' =>
  array (
    0 =>
    array (
      'weight' => '0',
      'module' => 'imagecache_icon',
      'action' => 'imagecache_icon',
      'data' =>
      array (
        'width' => '150',
        'pad_width' => 1,
        'height' => '150',
        'pad_height' => 1,
        'upscale' => 0,
        'canvas_x_offset' => 'center',
        'canvas_y_offset' => 'center',
        'bg_hex' => 'FFFFFF',
        'border_hex' => '',
        'border_size' => '0',
        'ratio_landscape' => '',
        'ratio_portrait' => '',
      ),
    ),
  ),
);
