<?php if ( $editor_form ) { print $editor_form; } else { ?>
<?php if($form_description): ?>
<div class="form-description">
  <?php print $form_description; ?>
</div>
<?php endif; ?>

<div id="step-1" class="file-upload form-step">
  <div class="header">
    <h2><span class="number">1<span>.</span></span> <?php print $label_step1; ?></h2>
  </div>
  <div class="content">
    <?php print $upload; ?>
    <script type="text/javascript">$('#edit-field-image-0-filefield-upload').hide();</script>
  </div>
</div>

<div id="step-2" class="describe-your-photo form-step">
  <div class="header">
    <h2><span class="number">2<span>.</span></span> <?php print $label_step2; ?></h2>
  </div>
  <div class="content">
    <div class="title">
      <div class="description"><?php print $label_title; ?> <span class="form-required" title="This field is required.">*</span></div>
      <div class="form-item">
        <?php print $title; ?>
      </div>
    </div>
    <div class="caption">
      <div class="description"><?php print $label_body; ?>
        <?php if ($body_limit): ?>
          <em>(<?php print $body_limit; ?> characters or less)</em>
        <?php endif; ?>
      </div>
      <div class="form-item">
        <?php print $body; ?>
      </div>
    </div>
  </div>
</div>

<?php if ($categories): ?>
  <div id="step-3" class="tag-your-photo form-step">
    <div class="header">
      <h2><span class="number">3<span>.</span></span> <?php print $label_step3; ?></h2>
    </div>
    <div class="content">
      <?php print $categories; ?>
    </div>
  </div>
<?php endif; ?>

<div id="step-4" class="form-step">
  <div class="header">
    <h2><span class="number">4<span>.</span></span> <?php print $label_step4; ?></h2>
  </div>
  <div class="content">
    <?php if ($preview_message): ?>
      <div class="message"><?php print $preview_message; ?></div>
    <?php endif; ?>
    <?php print $preview_button; ?>
    <?php print $submit_button; ?>
    <?php print $delete_button; ?>
  </div>
</div>

<div class="hidden" style="display:none;">
  <?php print $other; ?>
</div>
<?php } ?>
