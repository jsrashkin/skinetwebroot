<?php

function bonnier_type_ugc_photo_upload_page() {
  global $user;
  $type = 'ugc_photo';

  if (node_access('create', $type)) {
    // Include page handler for node_add()
    module_load_include('inc', 'node', 'node.pages');
    // Note title before rendering of form.

    // Initialize settings:
    $node = array('uid' => $user->uid, 'name' => (isset($user->name) ? $user->name : ''), 'type' => $type, 'language' => '');

    $output = drupal_get_form($type .'_node_form', $node);
  }
  return $output;
}

function bonnier_type_ugc_photo_post_form_ugc_photo_node_form_alter(&$form, &$form_state) {
  ugc_forms_process($form, $form_state, 'bonnier_type_ugc_photo_form');
}

function template_preprocess_bonnier_type_ugc_photo_form(&$vars) {
  $form = $vars['form'];
  $is_editor = bonnier_editor_is_editor();
  if ( $is_editor ){
    $form['buttons']['#weight'] = 9999999;
    $vars['editor_form'] = drupal_render($form);
  }
  else {
    // Clean up form
    $form['title']['#title'] = ''; 
    $form['body_field']['#title'] = ''; 
    $form['body_field']['body']['#title'] = ''; 
    $form['group_find_your_photo']['field_image']['#title'] = '';
    $form['group_find_your_photo']['field_image'][0]['#title'] = '';
    $form['#field_info']['field_image']['widget']['label'] = '';

    $vars['upload'] = drupal_render($form['group_find_your_photo']['field_image']);
    $vars['title'] = drupal_render($form['title']);
    $vars['body'] = drupal_render($form['body_field']['body']);
    $vars['categories'] = drupal_render($form['taxonomy']);
    $vars['preview_button'] = drupal_render($form['buttons']['preview']);
    if($form['field_ugc_photo_tags']) {
      unset($form['field_ugc_photo_tags']);
    }
    $vars['other'] = drupal_render($form);
    
    $vars['body_limit'] = variable_get('bonnier_type_ugc_photo_bodylength', 700);
    
    // Field labels.
    $vars['label_step1'] = variable_get('bonnier_type_ugc_photo_label_step1', 'Find Your Photo');
    $vars['label_step2'] = variable_get('bonnier_type_ugc_photo_label_step2', 'Describe Your Photo');
    $vars['label_step3'] = variable_get('bonnier_type_ugc_photo_label_step3', 'Categorize Your Photo');
    $vars['label_step4'] = variable_get('bonnier_type_ugc_photo_label_step4', 'Finish Uploading Your Photo');
    $vars['label_title'] = variable_get('bonnier_type_ugc_photo_label_title', 'Title');
    $vars['label_body'] = variable_get('bonnier_type_ugc_photo_label_body', 'Description');
    $vars['preview_message'] = variable_get('bonnier_type_ugc_photo_previewmessage', 'Thanks for taking the time to upload your photo. Please preview it before we put it on our site.');
  }
}

function template_preprocess_bonnier_type_ugc_photo_form_preview(&$vars) {
  drupal_add_css(drupal_get_path('module', 'bonnier_type_ugc_photo') .'/bonnier_type_ugc_photo.css', 'module', 'all', FALSE);
  $form = $vars['form'];
  
  // Allow the Submit button to be renamed.
  $form['buttons']['submit']['#value'] = variable_get('bonnier_type_ugc_photo_label_save', 'Save');

  $vars['submit_button'] = drupal_render($form['buttons']['submit']);
  $vars['edit_button'] = drupal_render($form['buttons']['ugc_forms_edit']);
  $vars['node_preview'] = $form['#node_preview'];

  $vars['other'] = drupal_render($form);

  // Add a variable that can be keyed off to tell this is a preview page.
  $vars['preview'] = TRUE;
}
