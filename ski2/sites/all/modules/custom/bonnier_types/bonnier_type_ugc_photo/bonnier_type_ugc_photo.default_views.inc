<?php

/**
 * Relevant variables that should be used in any views export:
 *
 *  variable_get('laserfist_photo_channel_tout_qid', NULL);
 *    replaces the first qids array element
 */

/**
 * Implementation of hook_views_default_views().
 */
function bonnier_type_ugc_photo_views_default_views() {
  $views = array();
  $path = dirname(__FILE__) .'/views';

  // Views related to the UGC Photo content type.
  include($path . '/view.node_ugc_photo.inc');
  $views[$view->name] = $view;

  return $views;
}
