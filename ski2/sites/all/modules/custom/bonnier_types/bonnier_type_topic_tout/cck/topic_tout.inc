<?php
$content['type']  = array(
);
$content['fields'] = array(
  0 => array(
    'label' => 'Topic Tout Tags',
    'field_name' => 'field_topic_tout_tags',
    'type' => 'content_taxonomy',
    'widget_type' => 'content_taxonomy_options',
    'change' => 'Change basic information',
    'weight' => '-4',
    'show_depth' => 1,
    'group_parent' => '0',
    'description' => '',
    'default_value' => array(
      0 => array(
        'value' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => false,
    'required' => 1,
    'multiple' => '1',
    'save_term_node' => 1,
    'vid' => '2',
    'parent' => '0',
    'parent_php_code' => '',
    'depth' => '',
    'op' => 'Save field settings',
    'module' => 'content_taxonomy',
    'widget_module' => 'content_taxonomy_options',
    'columns' => array(
      'value' => array(
        'type' => 'int',
        'not null' => false,
        'sortable' => false,
      ),
    ),
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      4 => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content['extra']  = array(
  'title' => '-5',
  'body_field' => '-3',
  'revision_information' => '1',
  'comment_settings' => '4',
  'menu' => '-2',
  'path' => '3',
  'nodewords' => '-1',
  'print' => '2',
  'scheduler_settings' => '0',
);
