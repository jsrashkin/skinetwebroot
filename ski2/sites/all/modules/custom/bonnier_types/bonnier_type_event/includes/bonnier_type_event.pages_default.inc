<?php
// $Id: $

/**
 * Implementation of hook_default_page_manager_handlers()
 */
function bonnier_type_event_default_page_manager_handlers() {
  $handler = new stdClass;
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_event';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => t('Event'),
    'no_blocks' => FALSE,
    'css_id' => 'event',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'event' => 'event',
            ),
          ),
          'context' => 'argument_nid_1',
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display;
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array();
  $display->cache = array();
  $display->title = t('Calendar');
  $display->hide_title = FALSE;
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass;
  $pane->pid = 'sharing-links';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'sharing_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => TRUE,
    'override_title_text' => '<none>',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'sharing-links',
  );
  $pane->extras = array();
  $pane->position = 0;
  $display->content[$pane->pid] = $pane;
  $display->panels['middle'][$pane->position] = $pane->pid;
  $pane = new stdClass;
  $pane->pid = 'event-pager';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'custom_pagers-'. db_result(db_query_range("SELECT pid FROM {custom_pager} WHERE position = 'block' AND view = 'event_navigation' AND (node_type = 'event' OR node_type LIKE '%,event,%' OR node_type LIKE 'event,%' OR node_type LIKE '%,event')", 0, 1));
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => TRUE,
    'override_title_text' => '<none>',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_id' => 'event-pager',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 1;
  $display->content[$pane->pid] = $pane;
  $display->panels['middle'][$pane->position] = $pane->pid;
  $pane = new stdClass;
  $pane->pid = 'content';
  $pane->panel = 'middle';
  $pane->type = 'node_content';
  $pane->subtype = 'node_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'links' => FALSE,
    'page' => TRUE,
    'no_extras' => FALSE,
    'override_title' => FALSE,
    'override_title_text' => '',
    'teaser' => FALSE,
    'identifier' => '',
    'link' => FALSE,
    'leave_node_title' => FALSE,
    'context' => 'argument_nid_1',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'event-content',
  );
  $pane->extras = array();
  $pane->position = 2;
  $display->content[$pane->pid] = $pane;
  $display->panels['middle'][$pane->position] = $pane->pid;
  $handler->conf['display'] = $display;
  $handlers[$handler->name] = $handler;

  return $handlers;
}
