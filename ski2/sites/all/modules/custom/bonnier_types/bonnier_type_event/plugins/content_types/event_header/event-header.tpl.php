<div id="channel-sub-nav">
  <div class="right">
    <div class="utility-links">
      <div class="email"><?php print $email; ?></div>
      <div class="print"><?php print $print; ?></div>
      <div class="share"><?php print $share; ?></div>
      <?php if ($comment): ?>
        <div class="comment"><?php print $comment; ?></div>
      <?php endif; ?>
    </div>
  </div>
  <div id="parent"><?php print $date; ?></div>
  <div id="title"><h1><?php print $title; ?></h1></div>
</div>
