<?php
/**
 * @file
 * 
 */

/**
 * Implementation of hook_default_page_manager_pages()
 */
function bonnier_type_travel_special_default_page_manager_pages() {
  $pages = array();
  $path = dirname(__FILE__) .'/panels';

  // The main list page.
  include($path .'/page.travel_specials.inc');
  $pages[$page->name] = $page;
  
  return $pages;
}

/**
 * Implementation of hook_default_page_manager_handlers()
 */
function bonnier_type_travel_special_default_page_manager_handlers() {
  $handlers = array();
  $path = dirname(__FILE__) .'/panels';

  // The main list page.
  include($path .'/panel.node_view_travel_special.inc');
  $handlers[$handler->name] = $handler;
  
  return $handlers;
}
