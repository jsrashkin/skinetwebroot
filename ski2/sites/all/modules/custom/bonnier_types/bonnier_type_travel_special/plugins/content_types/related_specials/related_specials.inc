<?php

function bonnier_type_travel_special_related_specials_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Travel Specials - Related Specials'),
    'category' => 'Bonnier',
		'hook theme' => 'bonnier_type_travel_special_related_specials_theme',
  );
}

function bonnier_type_travel_special_related_specials_theme(&$theme) {
  // The main output.
	$theme['travel_special_related_specials'] = array(
    'template' => 'travel_special_related_specials',
		'arguments' => array('specials' => NULL),
		'path' => drupal_get_path('module', 'bonnier_type_travel_special') .'/plugins/content_types/related_specials',
	);
	// Each individual item.
  $theme['travel_special_related_specials_item'] = array(
    'template' => 'travel_special_related_specials_item',
  	'arguments' => array('node' => NULL),
  	'path' => drupal_get_path('module', 'bonnier_type_travel_special') .'/plugins/content_types/related_specials',
  );
}

function bonnier_type_travel_special_related_specials_content_type_render($subtype, $conf, $panel_args, $context) {
  $nid = arg(1);
  $nodes = bonnier_type_travel_special_related_specials_list($nid);
  // Make sure related nodes were found.
  if (!empty($nodes)) {
    $specials = array();
    foreach ($nodes as $node) {
      $specials[] = theme('travel_special_related_specials_item', $node);
    }
    $output = theme('travel_special_related_specials', $specials);
  }
  else {
    $output = '';//t('No related travel specials found.');
  }

  $block = new StdClass();
  $block->content = $output;
  $block->title = '';
  $block->delta = 'related_specials';
	$block->css_id = 'travel-special-node-header';
  return $block;
}

function bonnier_type_travel_special_related_specials_list($nid) {
  // This is the primary vocab used to decide the structure.
  $vid = variable_get('travel_specials_primary_vocab', 13);

  // The minimum number of nodes required before loading items from the parent
  // term.
  $min = variable_get('travel_specials_related_min', 3);

  // The node we're working from.
  $node = node_load($nid);

  $nids = array();
  $tid = 0;

  // Get the term ID we're focused on.
  foreach ($node->taxonomy as $term) {
    if ($term->vid == $vid) {
      $tid = $term->tid;
    }
  }

  // Only proceed if a term ID was found.
  if ($tid > 0) {
    $tids = array($tid);
    // Find a list of all other nodes that match this term ID.
    $nids = bonnier_type_travel_special_related_specials_query($tids, $nid);

    // Make sure there are enough.
    if (count($nodes) < $min) {
      // Get the parent term.
      $parent = array_shift(taxonomy_get_parents($tid));

      // Get all child terms.
      $tids = array();
      foreach(taxonomy_get_children($parent->tid) as $x => $sibling) {
        if ($sibling->tid != $tid) {
          $tids[] = $sibling->tid;
        }
      }
      // Find nodes that match the parent.
      $nids += bonnier_type_travel_special_related_specials_query($tids, $nid);
    }
    $nids = array_unique($nids);
  }
  
  // Compile the nodes.
  $nodes = array();
  foreach ($nids as $related_nid) {
    $nodes[] = node_load($related_nid);
  }
  
  return $nodes;
}

function bonnier_type_travel_special_related_specials_query($tids, $nid) {
  // The maximum number of items shown.
  $max = variable_get('travel_specials_related_max', 5);

  $nids = array();
  $placeholders = db_placeholders($tids, 'int');
  $results = db_query_range(db_rewrite_sql("SELECT
      n.nid
    FROM
      node n
      INNER JOIN term_node tn ON tn.nid=n.nid AND tn.vid=n.vid
    WHERE
      n.status=1
      AND n.type = 'travel_special'
      AND tn.tid IN ($placeholders)"), $tids, 0, $max);

  // Get all of the node IDs.
  while ($record = db_fetch_array($results)) {
    // Don't include the one for the page we're looking at.
    if ($record['nid'] != $nid) {
      $nids[] = $record['nid'];
    }
  }

  return $nids;
}

function template_preprocess_travel_special_related_specials(&$vars) {
  $path = variable_get('travel_specials_path', 'travel-specials');

  $vars['title'] = t(variable_get('travel_specials_label_related', 'More Travel Specials'));
  $vars['all'] = l(variable_get('travel_specials_label_all', 'All Travel Specials'), $path);
}

function template_preprocess_travel_special_related_specials_item(&$vars) {
  $node = $vars['node'];
  $thumb = theme('imagecache', 'thumb_50', bonnier_common_teaser_image($node));
  $vars['thumb'] = l($thumb, 'node/'. $node->nid, array('html' => TRUE));
  $vars['title'] = l($node->title, 'node/'. $node->nid);
  $vars['dek'] = check_plain($node->field_dek[0]['value']);
}
