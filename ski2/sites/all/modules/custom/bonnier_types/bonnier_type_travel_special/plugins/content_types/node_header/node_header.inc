<?php

function bonnier_type_travel_special_node_header_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Travel Specials - Node Header'),
    'category' => 'Bonnier',
		'hook theme' => 'bonnier_type_travel_special_node_header_theme',
  );
}

function bonnier_type_travel_special_node_header_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new StdClass();
  $block->content = theme('travel_special_node_header');
  $block->title = '';
  $block->delta = 'node_header';
	$block->css_id = 'travel-special-node-header';
  return $block;
}

function template_preprocess_travel_special_node_header(&$vars) {
  $path = variable_get('travel_specials_path', 'travel-specials');
  $path_rss = variable_get('travel_specials_path_rss', $path .'.rss');
  $path_icon = variable_get('travel_specials_path_icon', base_path() .'/sites/all/modules/platform/site_map/feed-small.png');
  $title = t('Travel Specials');

  $vars['title'] = l($title, $path);
  $vars['rss'] = l("<img src=\"{$path_icon}\" />", $path_rss, array('html' => TRUE));
}

function bonnier_type_travel_special_node_header_theme(&$theme) {
	$theme['travel_special_node_header'] = array(
    'template' => 'travel_special_node_header',
		'arguments' => array(),
		'path' => drupal_get_path('module', 'bonnier_type_travel_special') .'/plugins/content_types/node_header',
	);
}
