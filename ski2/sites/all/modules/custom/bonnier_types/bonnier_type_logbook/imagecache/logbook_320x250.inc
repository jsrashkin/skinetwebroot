<?php
$presets['image_320x250'] = array (
  'presetname' => 'logbook_320x250',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '320',
        'height' => '250',
        'upscale' => 0,
      ),
    ),
	),
);
