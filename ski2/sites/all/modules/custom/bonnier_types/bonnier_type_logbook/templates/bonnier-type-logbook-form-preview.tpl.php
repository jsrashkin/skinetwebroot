<?php if($form_description): ?>
<div class="form-description">
  <?php print $form_description; ?>
</div>
<?php endif; ?>

<div id="buttons" class="form-step">
	<?php print $edit_button; ?>
	<?php print $submit_button; ?>
</div>

<div class="node-preview">
	<div class="date">
		<?php print $date; ?>
	</div>
	<h2><?php print $title; ?></h2>
  <div class="byline">
    By <?php print $author; ?>
  </div>
  <div class="image">
    <?php print theme('imagecache','logbook_320x250',$filepath);?>
  </div>
  <div class="content">
    <?php print $body; ?>
  </div>
</div>


<div class="hidden" style="display:none;">
	<?php print $other; ?>
</div>
