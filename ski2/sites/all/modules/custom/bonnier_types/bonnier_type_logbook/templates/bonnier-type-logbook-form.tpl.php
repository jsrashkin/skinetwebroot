<?php if(in_array('admin',$user->roles) && arg(2) == 'edit') {?>
	<?php print drupal_render($form); ?>
<?php } else { ?>
<?php if($form_description): ?>
<div class="form-description">
  <?php print $form_description; ?>
</div>
<?php endif; ?>
<div id="step-1" class="form-step title">
  <div class="header">
    <h2>
      <span class="number">1.</span>
      Title
    </h2>
  </div>
  <div class="content">
    <?php print $title; ?>
  </div>
</div>
<div id="step-2" class="form-step log-entry">
  <div class="header">
    <h2>
      <span class="number">2.</span>
      Log Entry
    </h2>
  </div>
  <div class="form-item">
	  <?php print $body; ?>
   </div>
</div>
<div id="step-3" class="form-step photo">
	<div class="header">
		<h2><span class="number">3.</span>Photo (optional)</h2>
	</div>
	<div class="form-item">
		<?php print $upload; ?>
	</div>
</div>



<div id="step-4" class="form-step buttons">
	<?php print $preview_button; ?>
</div>

<div class="hidden" style="display:none;">
	<?php print $other; ?>
</div>
<?php } ?>
