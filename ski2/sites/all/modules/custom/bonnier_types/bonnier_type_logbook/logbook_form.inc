<?php

function bonnier_type_logbook_upload_page() {
  global $user;
  $type = 'logbook';

  if (node_access('create', $type)) {
    // Include page handler for node_add()
    module_load_include('inc', 'node', 'node.pages');
    // Note title before rendering of form.

    // Initialize settings:
		if(arg(1) == $user->uid) {
	    $node = array('uid' => $user->uid, 'name' => (isset($user->name) ? $user->name : ''), 'type' => $type, 'language' => '');
			$output = drupal_get_form($type .'_node_form', $node);

		} else {
			//redirect user to appropriate url
			drupal_goto('logbook/'. $user->uid. '/create', NULL, NULL, 301);
		}

  }
  return $output;
}

function bonnier_type_logbook_edit_page() {
  global $user;
  $type = 'logbook';
	
    if($user->uid == arg(1)) {
      $node = node_load(arg(3));
			if($node->uid == $user->uid) {
        module_load_include('inc', 'node', 'node.pages');

				$output = '<div id="profile-header"><h1>Edit entry</h1></div>';
  	    $output .= drupal_get_form($type.'_node_form',$node);
        //$output .= bonnier_type_logbook_form($node);
			}
    }
  return $output;
}
/*
If you wanted to customize the remove page beyond drupal standards
Make sure you cahnge the buttons in the content type render
*/
function bonnier_type_logbook_remove_page() {
  global $user;
  $type = 'logbook';

    if($user->uid == arg(1)) {
			node_delete(arg(3));
   	}
}


function bonnier_type_logbook_post_form_logbook_node_form_alter(&$form, &$form_state) {
  ugc_forms_process($form, $form_state, 'bonnier_type_logbook_form');
}

function template_preprocess_bonnier_type_logbook_form(&$vars) {
  $form = $vars['form'];

  // Clean up form
  $form['title']['#title'] = ''; 
  $form['body_field']['#title'] = ''; 
  $form['body_field']['body']['#title'] = ''; 
  $form['field_image']['#title'] = '';
  $form['field_image'][0]['#title'] = '';
  $form['#field_info']['field_image']['widget']['label'] = '';

  $vars['upload'] = drupal_render($form['field_image']);
  $vars['title'] = drupal_render($form['title']);
  $vars['body'] = drupal_render($form['body_field']['body']);

  $vars['preview_button'] = drupal_render($form['buttons']['preview']);
  $vars['other'] = drupal_render($form);

}

function template_preprocess_bonnier_type_logbook_form_preview(&$vars) {
  drupal_add_css(drupal_get_path('module', 'bonnier_type_logbook') .'/bonnier_type_logbook.css', 'module', 'all', FALSE);
  $form = $vars['form'];
  $form['buttons']['submit']['#value'] = 'Publish';
  $vars['submit_button'] = drupal_render($form['buttons']['submit']);
  $vars['edit_button'] = drupal_render($form['buttons']['ugc_forms_edit']);
  
	$node = $vars['form']['#node'];
	$vars['date'] = date('F j, Y',$node->created);
  $vars['title'] = $node->title;
  $user = $vars['user'];
  $vars['author'] = l($user->name,'users/'.$user->name);
  //build tag links
  $vars['body'] = $node->body;
  $vars['filepath'] = $node->field_image[0]['filepath'];

  $vars['other'] = drupal_render($form);
}
