<?php
$presets['wallpaper_1200x800'] = array (
  'presetname' => 'wallpaper_1200x800',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '1200',
        'height' => '800',
        'upscale' => 0,
      ),
    ),
 ),
);
