<?php
$presets['wallpaper_800x600'] = array (
  'presetname' => 'wallpaper_800x600',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '800',
        'height' => '600',
        'upscale' => 0,
      ),
    ),
 ),
);
