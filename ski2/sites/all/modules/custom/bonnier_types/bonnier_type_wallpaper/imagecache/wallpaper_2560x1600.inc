<?php
$presets['wallpaper_2560x1600'] = array (
  'presetname' => 'wallpaper_2560x1600',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '2560',
        'height' => '1600',
        'upscale' => 0,
      ),
    ),
 ),
);
