<?php
$presets['wallpaper_1920x1200'] = array (
  'presetname' => 'wallpaper_1920x1200',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '1920',
        'height' => '1200',
        'upscale' => 0,
      ),
    ),
 ),
);
