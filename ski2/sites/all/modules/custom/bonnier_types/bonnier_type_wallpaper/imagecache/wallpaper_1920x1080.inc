<?php
$presets['wallpaper_1920x1080'] = array (
  'presetname' => 'wallpaper_1920x1080',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '1920',
        'height' => '1080',
        'upscale' => 0,
      ),
    ),
 ),
);
