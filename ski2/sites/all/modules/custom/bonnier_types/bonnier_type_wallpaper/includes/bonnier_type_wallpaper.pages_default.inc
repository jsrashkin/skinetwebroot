<?php
// $Id$

/**
 * Implementation of hook_default_page_manager_pages()
 */
function bonnier_type_wallpaper_default_page_manager_pages() {
  $page = new stdClass;
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'wallpapers';
  $page->task = 'page';
  $page->admin_title = t('Wallpapers');
  $page->admin_description = t('wallpapers, figure it out');
  $page->path = 'wallpapers';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => t('Wallpapers'),
    'name' => 'menu-content-by-type',
    'weight' => 0,
  );
  $page->arguments = array();
  $page->conf = array();
  $page->default_handlers = array();
  $handler = new stdClass;
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_wallpapers_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'wallpapers';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => t('Panel'),
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'logic' => 'and',
    ),
  );
  $display = new panels_display;
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array();
  $display->cache = array();
  $display->title = t('Wallpapers');
  $display->hide_title = FALSE;
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass;
  $pane->pid = 'header';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'title' => '<none>',
    'body' => '<p>'. t('Optimize your desktop with our wallpapers! The photography of contributors and staff features a terrific mix of action and scenery, and is sure to keep you inspired while you\'re stuck at work. Check back frequently as we are always adding more...') .'</p>',
    'format' => 2,
    'substitute' => NULL,
    'admin_title' => t('Header'),
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content[$pane->pid] = $pane;
  $display->panels[$pane->panel][$pane->position] = $pane->pid;
  $pane = new stdClass;
  $pane->pid = 'listing';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'wallpaper';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'nodes_per_page' => 24,
    'pager_id' => 0,
    'use_pager' => TRUE,
    'offset' => 0,
    'more_link' => FALSE,
    'feed_icons' => FALSE,
    'panel_args' => FALSE,
    'link_to_view' => FALSE,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'override_title' => TRUE,
    'override_title_text' => '<none>',
    'override_pager_settings' => FALSE,
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $display->content[$pane->pid] = $pane;
  $display->panels[$pane->panel][$pane->position] = $pane->pid;
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages[$page->name] = $page;

  return $pages;
}
