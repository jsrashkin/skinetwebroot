<div class="channel-header-events channel-header"></div>
<div id="content-area-header" class="<?php print $classes ?>">
  <div class="left">
    <?php if ($title): ?>
      <h1><?php print $title; ?></h1>
    <?php endif; ?>
    <?php if($dek): ?>
      <div class="dek"><?php print $dek; ?></div>
    <?php endif; ?>
    <?php if($author): ?>
      <div class="by-line">Photo by <?php print $author; ?></div>
    <?php endif; ?>
  </div>
  <div class="right">
    <div class="utility-links">
      <span class="email"><?php print $email; ?></span>
      <span class="print"><?php print $print; ?></span>
      <span class="share"><?php print $share; ?></span>
      <?php if($comment_count): ?>
        <div class="comments-count">
          <a href="#comment-box"> comments <?php print $comment_count; ?> </a>
        </div>
      <?php endif; ?>
    </div>
  </div>
  <div class="clear"></div>
</div>
