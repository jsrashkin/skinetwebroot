<?php

$handler = new stdClass;
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'node_view_page';
$handler->task = 'node_view';
$handler->subtask = '';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => t('Page'),
  'no_blocks' => FALSE,
  'css_id' => 'static-page',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
  'access' => array(
    'plugins' => array(
      array(
        'name' => 'node_type',
        'settings' => array(
          'type' => array(
            'page' => 'page',
          ),
        ),
        'context' => 'argument_nid_1',
      ),
    ),
    'logic' => 'and',
  ),
);
$display = new panels_display;
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = '';
$display->hide_title = FALSE;
$display->content = array();
$display->panels = array();
$pane = new stdClass;
$pane->pid = 'page-header';
$pane->panel = 'middle';
$pane->type = 'core_header';
$pane->subtype = 'core_header';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array();
$pane->cache = array();
$pane->style = array();
$pane->css = array();
$pane->extras = array();
$pane->position = 0;
$display->content[$pane->pid] = $pane;
$display->panels['middle'][$pane->position] = $pane->pid;
$pane = new stdClass;
$pane->pid = 'page-content';
$pane->panel = 'middle';
$pane->type = 'node_content';
$pane->subtype = 'node_content';
$pane->shown = TRUE;
$pane->access = array();
$pane->configuration = array(
  'links' => 0,
  'page' => 1,
  'no_extras' => 0,
  'override_title' => 1,
  'override_title_text' => '<none>',
  'teaser' => 0,
  'identifier' => '',
  'link' => 0,
  'leave_node_title' => 0,
  'context' => 'argument_nid_1',
);
$pane->cache = array();
$pane->style = array();
$pane->css = array(
  'css_id' => '',
  'css_class' => 'page-content',
);
$pane->extras = array();
$pane->position = 1;
$display->content[$pane->pid] = $pane;
$display->panels['middle'][$pane->position] = $pane->pid;
$pane = new stdClass;
$pane->pid = 'related-content';
$pane->panel = 'middle';
$pane->type = 'views';
$pane->subtype = 'related_content';
$pane->shown = FALSE;
$pane->access = array();
$pane->configuration = array(
  'nodes_per_page' => 3,
  'pager_id' => 1,
  'use_pager' => FALSE,
  'offset' => 0,
  'more_link' => FALSE,
  'feed_icons' => FALSE,
  'panel_args' => FALSE,
  'link_to_view' => FALSE,
  'args' => '',
  'url' => '',
  'display' => 'default',
  'context' => array(
    '0' => 'argument_nid_1.tid_any',
    '1' => 'argument_nid_1.nid',
  ),
  'override_title' => TRUE,
  'override_title_text' => 'Related Pages',
);
$pane->cache = array();
$pane->style = array();
$pane->css = array(
  'css_id' => '',
  'css_class' => 'related-content',
);
$pane->extras = array();
$pane->position = 2;
$display->content[$pane->pid] = $pane;
$display->panels['middle'][$pane->position] = $pane->pid;
$pane = new stdClass;
$pane->pid = 'comments';
$pane->panel = 'middle';
$pane->type = 'block';
$pane->subtype = 'laserfist_comments-comments';
$pane->shown = FALSE;
$pane->access = array();
$pane->configuration = array(
  'override_title' => FALSE,
  'override_title_text' => '',
);
$pane->cache = array();
$pane->style = array();
$pane->css = array(
  'css_id' => 'comments',
  'css_class' => '',
);
$pane->extras = array();
$pane->position = 3;
$display->content[$pane->pid] = $pane;
$display->panels['middle'][$pane->position] = $pane->pid;
$handler->conf['display'] = $display;
