<?php

function bonnier_type_blog_categories_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Blogs Categories'),
    'description' => t('Blogs Categories'),
    'required context' => new ctools_context_optional(t('Blog'), 'node'),
    'category' => 'Blogs',
    'hook theme' => 'bonnier_type_blog_categories_theme',
  );
}

function bonnier_type_blog_categories_theme(&$theme) {
  $path = drupal_get_path('module', 'bonnier_type_blog') . '/plugins/content_types/categories';
  $theme['blogs_categories_listing'] = array(
    'template' => 'blogs-categories-listing',
    'path' => $path,
    'arguments' => array('blog_node' => NULL),
  );
  $theme['blogs_category'] = array(
    'template' => 'blogs-category',
    'path' => $path,
    'arguments' => array('term' => NULL, 'blog_node' => NULL),
  );
}

function bonnier_type_blog_categories_content_type_render($subtype, $conf, $panel_args, $context) {
  $blog_node = $context->data;
  $content = theme('blogs_categories_listing', $blog_node); 

  $block->content = $content;
  $block->title = 'Blog Categories';
  $block->delta = 'categories';
  $block->css_id = 'blog-categories';
  return $block;
}

function template_preprocess_blogs_categories_listing(&$vars) {
  $blog_node = $vars['blog_node'];
  // If there is no blog node then show ALL blog comments
  $where = '';
  if ($blog_node->nid) {
    $where = 'WHERE bp.field_blog_nid = ' . $blog_node->nid;
  }

  $sql = "
  SELECT 
    td.tid as tid,
    count(*) AS post_count
  FROM {term_data} td 
    INNER JOIN {term_node} tn 
    ON tn.tid = td.tid
    INNER JOIN {content_type_blog_post} bp
    ON bp.nid = tn.nid
    $where
  GROUP BY
  tid
  ";

  $results = db_query($sql,$blog_node->nid);

  while ($row = db_fetch_object($results)) {
    $term = taxonomy_get_term($row->tid);
    $term->post_count = $row->post_count;
    $terms[] = $term;
  }
  $vars['terms'] = $terms;

  if (!$terms) {
    return;
  }

  foreach($terms as $term) {
    $links[] = theme('blogs_category', $term, $blog_node);    
  }
	$vars['links'] = $links;
  $vars['categories'] = theme('item_list', $links);
}

function template_preprocess_blogs_category(&$vars) {
  $blog_node = $vars['blog_node'];
  $term = $vars['term'];

  $title = $term->name . ' (' . $term->post_count . ')';
  if ($blog_node && $term) {
    $vars['link'] = l($title, 'node/' . $blog_node->nid . '/' . $term->tid);
  }
  else {
    $vars['link'] = l($title, 'blogs/all/' . $term->tid);
  }
}


function bonnier_type_blog_categories_content_type_edit_form(&$form, &$form_state) {
    // provide a blank form so we have a place to have context setting.

}
