<?php

function bonnier_type_blog_blog_info_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Blog Info'),
    'description' => t('Blog Info'),
    'category' => 'Blogs',
    'required context' => new ctools_context_optional(t('Blog'), 'node'),
    'hook theme' => 'bonnier_type_blog_blog_info_theme',
  );
}

function bonnier_type_blog_blog_info_theme(&$theme) {
  $path = drupal_get_path('module', 'bonnier_type_blog') . '/plugins/content_types/blog_info';

  $theme['blog_info'] = array(
    'template' => 'blog-info',
    'path' => $path,
    'arguments' => array('blog_node'=> NULL),
  );
}


function bonnier_type_blog_blog_info_content_type_render($subtype, $conf, $panel_args, $context) {
  $blog_node = $context->data;

  $content = theme('blog_info', $blog_node); 

  $block->content = $content;
  $block->title = '';
  $block->delta = 'blog_info';
  return $block;
}

function template_preprocess_blog_info(&$vars) {
  $node = $vars['blog_node'];
  // Default to jsut a nothing blog info 
  $vars['blog_name'] = $node->title;
  if ($author_nid = $node->field_default_blog_author[0]['nid']) {
    $author = node_load($author_nid);
    $vars['blog_author_name'] = $author->title;
  }
  if ($info_nid = $node->field_blog_info_article[0]['nid']) {
    $info = node_load($info_nid);
    $vars['blog_info_dek'] = bonnier_common_dek($info);
    $teaser_image_path = bonnier_common_teaser_image($info);
    if ($teaser_image_path) {
      $vars['blog_info_image_src'] = $teaser_image_path;
      $vars['blog_info_image'] = theme('imagecache', 'blog_info_image', $teaser_image_path);
    }

    $read_more_href = url(bonnier_common_link($info));
    $vars['blog_info_read_more_href'] = $read_more_href;
  }
}


function bonnier_type_blog_blog_info_content_type_edit_form(&$form, &$form_state) {
}

function bonnier_type_blog_blog_info_content_type_edit_form_submit(&$form, &$form_state) {
}
