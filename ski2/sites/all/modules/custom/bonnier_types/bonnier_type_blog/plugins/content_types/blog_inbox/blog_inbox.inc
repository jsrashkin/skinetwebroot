<?php

function bonnier_type_blog_blog_inbox_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Blog Inbox'),
    'description' => t('Blog Inbox'),
    'category' => 'Blogs',
    'required context' => new ctools_context_optional(t('Blog'), 'node'),
    'hook theme' => 'bonnier_type_blog_blog_inbox_theme',
  );
}

function bonnier_type_blog_blog_inbox_theme(&$theme) {
  $path = drupal_get_path('module', 'bonnier_type_blog') . '/plugins/content_types/blog_inbox';

  $theme['blog_inbox'] = array(
    'template' => 'blog-inbox',
    'path' => $path,
    'arguments' => array('blog_node' => NULL),
  );
}


function bonnier_type_blog_blog_inbox_content_type_render($subtype, $conf, $panel_args, $context) {
  $blog_node = $context->data;

  $content = theme('blog_inbox',$blog_node); 

  $block = new stdClass();
  $block->content = $content;
  $block->title = '';
  $block->delta = 'blog_inbox';
  $block->css_class = 'blog-inbox';
  return $block;
}

function template_preprocess_blog_inbox(&$vars) {
  $node = $vars['blog_node'];
  $settings = bonnier_type_blog_get_blog_settings($node);
  // Default to jsut a nothing blog inbox 
  if ($settings->feedburner_name) {
    $vars['blog_inbox_content'] = 'Click here to sign up!';
    $vars['feedburner_name'] = $settings->feedburner_name;
    $vars['empty_inbox'] = TRUE;
    $vars['classes'] = 'default-inbox';
  }
}


function bonnier_type_blog_blog_inbox_content_type_edit_form(&$form, &$form_state) {
}

function bonnier_type_blog_blog_inbox_content_type_edit_form_submit(&$form, &$form_state) {
}
