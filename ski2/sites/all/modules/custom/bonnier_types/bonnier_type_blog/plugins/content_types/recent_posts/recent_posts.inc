<?php

function bonnier_type_blog_recent_posts_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Blogs Recent Posts'),
    'description' => t('Blogs Recent Posts'),
    'category' => 'Blogs',
    'required context' => new ctools_context_optional(t('Blog'),'node'),
    'hook theme' => 'bonnier_type_blog_recent_posts_theme',
  );
}

function bonnier_type_blog_recent_posts_theme(&$theme) {
  $path = drupal_get_path('module','bonnier_type_blog').'/plugins/content_types/recent_posts';
  $theme['blogs_recent_post'] = array(
    'template' => 'blogs-recent-post',
    'arguments' => array('node' => NULL),
    'path' => $path,
  );
  $theme['blogs_recent_posts_listing'] = array(
    'template' => 'blogs-recent-posts-listing',
    'arguments' => array('blog_node' => NULL, 'max' => 5),
    'path' => $path,
  );
}


function bonnier_type_blog_recent_posts_content_type_render($subtype, $conf, $panel_args, $context) {
  $blog_node = $context->data;
  $max = is_numeric($conf['max_recent_posts']) ? $conf['max_recent_posts'] : 5;
  $content = theme('blogs_recent_posts_listing', $blog_node, $max); 

  $block->content = $content;
  $block->title = 'Recent Posts';
  $block->delta = 'recent_posts';
  $block->css_class = 'blog-recent_posts';
  return $block;
}

function template_preprocess_blogs_recent_posts_listing(&$vars) {
  $blog_node = $vars['blog_node'];
  $max = $vars['max'];

  $view = views_get_view('blog_posts');
  if ($blog_node) {
    $view->set_arguments(array($blog_node->nid));
  }
  $view->pager['use_pager'] = TRUE; 
  $view->pager['items_per_page'] = $max; 
  
  $view->execute();
  $results = $view->result;

  foreach ($results as $result) {
    $recent_posts[] = node_load($result->nid);
  }

  foreach ((array)$recent_posts as $post) {
    $items[] = theme('blogs_recent_post', $post);
  }

  if (!empty($items)) {
    $vars['recent_posts'] = theme('item_list', $items);
  }
}

function template_preprocess_blogs_recent_post(&$vars) {
  $node = $vars['node'];
  $vars['link'] = l($node->title, 'node/' . $node->nid);
  $comment_count = db_result(db_query('SELECT comment_count FROM {node_comment_statistics} WHERE nid = %d', $node->nid));
  if ($comment_count) {
    $vars['comment_count'] = $comment_count;
  } else {
    $vars['comment_count'] = 0;
  }
}

function bonnier_type_blog_recent_posts_content_type_edit_form(&$form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  $form['max_recent_posts'] = array(
    '#title' => t('Max # of Recent Posts'),
    '#type' => 'textfield',
    '#default_value' => 5,
  );
}

function bonnier_type_blog_recent_posts_content_type_edit_form_submit(&$form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  $form_state['conf']['max_recent_posts'] = $form_state['values']['max_recent_posts'];
}
