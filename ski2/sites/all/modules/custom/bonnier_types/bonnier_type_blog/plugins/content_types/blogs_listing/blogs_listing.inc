<?php

function bonnier_type_blog_blogs_listing_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Blogs Listing'),
    'description' => t('Blogs Listing'),
    'category' => 'Blogs',
    'required context' => new ctools_context_optional(t('Blog'), 'node'),
    'hook theme' => 'bonnier_type_blog_blogs_listing_theme',
  );
}

function bonnier_type_blog_blogs_listing_theme(&$theme) {
  $path = drupal_get_path('module', 'bonnier_type_blog') . '/plugins/content_types/blogs_listing';
  $theme['blogs_blogs_listing'] = array(
    'template' => 'blogs-blogs-listing',
    'path' => $path,
    'arguments' => array('blog_node' => NULL),
  );

  $theme['blogs_blog_detail'] = array(
    'template' => 'blogs-blog-detail',
    'path' => $path,
    'arguments' => array('blog_node' => NULL),
  );
}


function bonnier_type_blog_blogs_listing_content_type_render($subtype, $conf, $panel_args, $context) {
  $blog_node = $context->data;
  $content = theme('blogs_blogs_listing', $blog_node); 
  if (!$content) {
    return;
  }

  $block->content = $content;
  $block->title = 'Blog Listing';
  $block->delta = 'blogs_listing';
  $block->css_class = 'blogs_listing';
  return $block;
}

function template_preprocess_blogs_blogs_listing(&$vars) {
  $limit = variable_get('blogs_listing_limit',FALSE);
  if ($limit) {
    //add a limit clause
    $limit_clause = ' LIMIT '.$limit;
  }
  $blog_node = $vars['blog_node'];
  //check to make sure field_blog is not in join table
  $check_sql = 'select type_name from {content_node_field_instance} where field_name = "field_blog"';
  $check_query = db_query($check_sql);
  $count = mysql_num_rows($check_query);
  if($count > 1) {
    $sql = "
    SELECT
      bp.nid as nid,
      count(n.status) AS post_count
    FROM {node} bp
      LEFT OUTER JOIN {content_field_blog} post
        ON bp.nid = post.field_blog_nid
      LEFT OUTER JOIN {node} n
        ON n.vid = post.vid AND n.status = 1
    WHERE
      bp.status = 1
      AND bp.type = 'blog'
    GROUP BY
    nid
    ".$limit_clause;
  } else {
    $sql = "
    SELECT 
      bp.nid as nid,
      count(n.status) AS post_count
    FROM {node} bp 
      LEFT OUTER JOIN {content_type_blog_post} post
        ON bp.nid = post.field_blog_nid
      LEFT OUTER JOIN {node} n
        ON n.vid = post.vid AND n.status = 1 
    WHERE 
      bp.status = 1
      AND bp.type = 'blog'
    GROUP BY
    nid
    ".$limit_clause;
  }
  $data = db_query($sql);

  $results = array();
  while ($row = db_fetch_object($data)) {
    $results[] = $row;
  }
  $nids = array();
  foreach ($results as $result) {
    $blog = node_load($result->nid);
    $blog->post_count = $result->post_count; 
    if ($blog->nid == $blog_node->nid) {
      $blog->active_blog = TRUE;
    }
    $links[] = theme('blogs_blog_detail', $blog);
    $nids[] = $blog->nid;
  }
  $vars['blog_links'] = $links;
  $vars['nids'] = $nids;
  if ($links) {
    $vars['blogs'] = theme('item_list', $links);
  }
}

function template_preprocess_blogs_blog_detail(&$vars) {
  $blog_node = $vars['blog_node'];
  $vars['link'] = l($blog_node->title, bonnier_common_link($blog_node));
  $vars['post_count'] = $blog_node->post_count;
  if ($blog_node->active_blog) {
    $vars['classes'] = 'active';
  }
}

function bonnier_type_blog_blogs_listing_content_type_edit_form(&$form, &$form_state) {
}

function bonnier_type_blog_blogs_listing_content_type_edit_form_submit(&$form, &$form_state) {
}
