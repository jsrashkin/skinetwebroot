<?php

$page = new stdClass;
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'blogs_home';
$page->task = 'page';
$page->admin_title = 'Blogs';
$page->admin_description = 'Blog Landing Page. Normally lists all blogs.';
$page->path = 'blogs';
$page->access = array();
$page->menu = array(
  'type' => 'normal',
  'title' => t('Blogs'),
  'name' => 'menu-content-by-type',
  'weight' => 0,
);
$page->arguments = array();
$page->conf = array();
$page->default_handlers = array();
$handler = new stdClass;
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_blogs_home_panel_template';
$handler->task = 'page';
$handler->subtask = 'blogs_home';
$handler->handler = 'panel_template';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Panel Node Template',
  'no_blocks' => 0,
  'css_id' => 'blogs-template',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display;
$display->layout = 'threecol_25_50_25_stacked';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = 'Blogs';
$display->hide_title = FALSE;
$display->content = array();
$display->panels = array();
  $pane = new stdClass;
  $pane->pid = 'new-1';
  $pane->panel = 'left';
  $pane->type = 'comments';
  $pane->subtype = 'comments';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'max_comments' => '5',
    'context' => 'empty',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-1'] = $pane;
  $display->panels['left'][0] = 'new-1';
  $pane = new stdClass;
  $pane->pid = 'new-2';
  $pane->panel = 'left';
  $pane->type = 'blogs_listing';
  $pane->subtype = 'blogs_listing';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'empty',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $display->content['new-2'] = $pane;
  $display->panels['left'][1] = 'new-2';
  $pane = new stdClass;
  $pane->pid = 'new-3';
  $pane->panel = 'left';
  $pane->type = 'blog_share_links';
  $pane->subtype = 'blog_share_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'empty',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $display->content['new-3'] = $pane;
  $display->panels['left'][2] = 'new-3';
  $pane = new stdClass;
  $pane->pid = 'new-4';
  $pane->panel = 'left';
  $pane->type = 'ads';
  $pane->subtype = 'Left';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $display->content['new-4'] = $pane;
  $display->panels['left'][3] = 'new-4';
  $pane = new stdClass;
  $pane->pid = 'new-5';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'blog_posts';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'nodes_per_page' => '10',
    'pager_id' => '1',
    'use_pager' => 1,
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-5'] = $pane;
  $display->panels['middle'][0] = 'new-5';
  $pane = new stdClass;
  $pane->pid = 'new-6';
  $pane->panel = 'right';
  $pane->type = 'ads';
  $pane->subtype = 'Right';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-6'] = $pane;
  $display->panels['right'][0] = 'new-6';
  $pane = new stdClass;
  $pane->pid = 'new-7';
  $pane->panel = 'right';
  $pane->type = 'blog_inbox';
  $pane->subtype = 'blog_inbox';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'empty',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $display->content['new-7'] = $pane;
  $display->panels['right'][1] = 'new-7';
  $pane = new stdClass;
  $pane->pid = 'new-8';
  $pane->panel = 'top';
  $pane->type = 'blog_header';
  $pane->subtype = 'blog_header';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'empty',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-8'] = $pane;
  $display->panels['top'][0] = 'new-8';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
