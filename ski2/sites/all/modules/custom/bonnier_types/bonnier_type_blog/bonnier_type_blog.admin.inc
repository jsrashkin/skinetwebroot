<?php

function bonnier_type_blog_admin_page() {
  return drupal_get_form('bonnier_type_blog_admin_settings');
}

function bonnier_type_blog_admin_settings() {

  $form['default_blog'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Blog Settings'),
    '#collapsible' => TRUE,
  );
  $form['default_blog']['bonnier_type_blog_single_blog_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Single Blog Mode'),
    '#default_value' => variable_get('bonnier_type_blog_single_blog_mode',''),
  );
  $form['default_blog']['bonnier_type_blog_feedburner_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Feedburner Name'),
    '#default_value' => variable_get('bonnier_type_blog_feedburner_name',''),
  );
  $form['default_blog']['bonnier_type_blog_syndicate_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Syndication Section Name'),
    '#default_value' => variable_get('bonnier_type_blog_syndicate_title',''),
  );

  return system_settings_form($form);
}
