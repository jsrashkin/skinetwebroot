<?php
  //dpm(get_defined_vars());
?>

<div id="blogs">
  <div class="outer-left">
    <div class="top">
      <?php print $content['top']; ?>
    </div>
    <div class="inner-left">
      <?php print $content['left']; ?>
    </div>
    <div class="middle">
      <?php print $content['middle']; ?>
    </div>
    <div class="clear"></div>
  </div>

  <div class="outer-right">
    <?php print $content['right']; ?>
  </div>
  <div class="clear"></div>
</div>
