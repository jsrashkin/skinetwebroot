<?php

function bonnier_type_ugc_videos_upload_page() {
  global $user;
  $type = 'ugc_videos';

  if (node_access('create', $type)) {
    // Include page handler for node_add()
    module_load_include('inc', 'node', 'node.pages');
    $node = array('uid' => $user->uid, 'name' => (isset($user->name) ? $user->name : ''), 'type' => $type, 'language' => '');
		$output = drupal_get_form($type .'_node_form', $node);
  }
  
  return $output;
}

function bonnier_type_ugc_videos_edit_page() {
  global $user;
  module_load_include('inc', 'node', 'node.pages');

  $type = 'ugc_videos';
	$node = node_load(arg(1));
	if($node->uid == $user->uid) {
		$output = '<div id="profile-header"><h1>Edit video</h1></div>';
  	$output .= drupal_get_form($type.'_node_form',$node);
  }
  return $output;
}

/**
 * If you wanted to customize the remove page beyond drupal standards
 * Make sure you change the buttons in the content type render
 */
function bonnier_type_ugc_videos_remove_page() {
  global $user;
  $type = 'ugc_videos';

  if($user->uid == arg(1)) {
		node_delete(arg(3));
 	}
}


function bonnier_type_ugc_videos_post_form_ugc_videos_node_form_alter(&$form, &$form_state) {
  ugc_forms_process($form, $form_state, 'bonnier_type_ugc_videos_form',TRUE);
	$form['buttons']['submit']['#submit'][] = 'bonnier_types_ugc_videos_submit';
  $form['#validate'][] = 'ugc_video_validate';
	unset($form['buttons']['preview']);
}

function ugc_video_validate(&$form,$form_state) {
  $values = $form_state['values'];
  if($values['field_ugc_video'][0]['filepath'] == '') {
    form_set_error('field_ugc_video', t('Please select a video to upload.'));
  }
}

function template_preprocess_bonnier_type_ugc_videos_form(&$vars) {
  $form = $vars['form'];

  $form['title']['#title'] = 'Title'; 
  $form['body_field']['body']['#title'] = 'Description'; 
  $form['field_image']['#title'] = '';
  $form['field_image'][0]['#title'] = '';
  $form['#field_info']['field_ugc_video']['widget']['label'] = '';
	$form['#field_info']['field_ugc_video_tags']['widget']['label'] = '';
  $form['field_ugc_video']['#title'] = '';
  $form['field_ugc_video'][0]['#title'] = '';

  $vars['upload'] = drupal_render($form['field_ugc_video']);
  $vars['title'] = drupal_render($form['title']);
  $vars['body'] = drupal_render($form['body_field']['body']);
	$vars['submit_button'] = drupal_render($form['buttons']['submit']);
  $vars['other'] = drupal_render($form);
}

function template_preprocess_bonnier_type_ugc_videos_form_preview(&$vars) {
  drupal_add_css(drupal_get_path('module', 'bonnier_type_ugc_videos') .'/bonnier_type_ugc_videos.css', 'module', 'all', FALSE);
  $form = $vars['form'];
  $node = $vars['form']['#node'];
	$user = $vars['user'];
	
  $vars['submit_button'] = drupal_render($form['buttons']['submit']);
  $vars['edit_button'] = drupal_render($form['buttons']['ugc_forms_edit']);
	$vars['date'] = date('F j, Y',$node->created);
  $vars['title'] = $node->title;
  $vars['author'] = l($user->name,'users/'.$user->name);  
  $vars['body'] = $node->body;
  $vars['filepath'] = $node->field_image[0]['filepath'];
  $vars['other'] = drupal_render($form);
}

function bonnier_types_ugc_videos_submit(&$form, &$form_state) {
  module_load_include('admin.inc', 'video_upload');

  $fields = _video_upload_relevant_fields();
  foreach ($fields as $field => $config) {
    $result = _video_upload_query_videos($config, VIDEO_UPLOAD_STATUS_UPLOAD_PENDING);
    while ($video = db_fetch_object($result)) {
      $video->field = $config;
      if (video_upload_upload($config, $video)) {
        #drupal_set_message("Your videos is processing");
      }
    }
  }

	$form_state['redirect'] = 'user/'.$form_state['values']['uid'].'/profile/videos';
	drupal_set_message("Thanks for Uploading Your Video!  It could take up to 24 hours for your video to be processed.");
}
