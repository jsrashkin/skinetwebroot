<?php
$presets['image_150x110'] = array (
  'presetname' => 'ugc_videos_150x110',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '-10',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '150',
        'height' => '110',
        'upscale' => 0,
      ),
    ),
	),
);
