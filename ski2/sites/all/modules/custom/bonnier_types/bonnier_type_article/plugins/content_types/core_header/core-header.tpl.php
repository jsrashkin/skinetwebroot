<div id="content-area-header" class="<?php print $classes ?>">
  <div class="left">
    <?php if ($title): ?>
      <h2 class="main-title"><?php print $title; ?></h2>
    <?php endif; ?>
  
    <?php if ($dek): ?> 
    <div class="dek"><?php print $dek; ?></div>
    <?php endif; ?>
  
    <?php if ($credit): ?> 
      <div class="author">By <?php print $credit; ?></div>
    <?php endif; ?>
  </div>
  <div class="right">
    <div class="utility-links">
      <div class="email"><?php print $email; ?></div>
      <div class="print"><?php print $print; ?></div>
      <div class="share"><?php print $share; ?></div>
      <?php if ($comment): ?>
        <div class="comment"><?php print $comment; ?></div>
      <?php endif; ?>
    </div>
  </div>  
  <div class="clear"></div>
  <?php if ($related_tags): ?> 
    <div class="related-tags">
      <span class="label">related tags:</span>
      <?php print $related_tags; ?>
    </div>
  <?php endif; ?>

  <?php if ($bottom): ?>
    <div class="bottom">
      <?php print $bottom; ?>
    </div>
  <?php endif; ?>

  <?php if ($comment_count): ?>
    <div class="comments-count">
      <a href="#comment-box">
      comments <?php print $comment_count; ?>
      </a>
    </div>
  <?php endif; ?>
  <div class="clear"></div>
</div>
