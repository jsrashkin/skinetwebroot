<?php
// $Id$

/**
 * Preprocessor for the photo box template
 */
function template_preprocess_photo_box(&$vars) {
  $node = $vars['node'];
  $photo_node = $vars['photo_node'];

  // Fill the photo box
  if ($photo_node) {
    $template = $node->main_photo_placement;
    $template = $template ? $template : 'left';
    if ($photo_node->field_image[0]['data']['alt'] != '') {
      $alt = $photo_node->field_image[0]['data']['alt'];
    }
    else {
      $alt = $node->title;
    }

    $vars['image_alt'] = $alt;
    $vars['main_photo_template'] = $template;

    $image_path = bonnier_common_image($photo_node);
    $vars['image_path'] = $image_path;
    
    // Work out which imagecache preset to use.
    $preset = 'article_image';
    if ($template == 'center') {
      $preset = 'article_image_center';
    }

    // Create the imagecache themed output.
    $image = theme('imagecache', $preset, $image_path, $alt);

    // Get the metadata for the imagecache preset.
    if ($dimensions = image_get_info(imagecache_create_path($preset, $image_path))) {
      $vars['image_width'] = $dimensions['width'];
      $vars['image_height'] = $dimensions['height'];
    }
    
    $vars['image'] = $image;
    $class = $template;
    $vars['class'] = $class;

    // Use title if if caption doesn't exist.
    $vars['caption'] = $photo_node->body;
    $vars['title'] = $photo_node->title;
    if (empty($vars['caption'])) {
      $vars['caption'] = $vars['title'];
    }

    $photo_credit = theme('laserfist_credit', $photo_node);
    if ($photo_credit) {
      $vars['photo_credit'] = $photo_credit;
    }

    $imagepath = bonnier_common_image($photo_node);
    
    // Work out the path to the enlarged image.
    // Use a variable 'bonnier_photo_popup_enlarged' to set an imagecache preset
    // to use, set the variable to an empty string or FALSE to just link
    // straight to the original image.
    $enlarged_cache = variable_get('bonnier_photo_popup_enlarged', 'enlarged_image');
    if ($enlarged_cache) {
      $enlarged_imagepath = imagecache_create_url($enlarged_cache, $imagepath);
    }
    else {
      $enlarged_imagepath = $imagepath;
    }

    $enlarge = l('Enlarge Photo', $enlarged_imagepath, array('attributes' => array('class' => 'thickbox enlarge', 'title' => $photo_node->title)));
    $vars['enlarge'] = $enlarge;

    $related_galleries = array();
    foreach ((array)$node->field_related_galleries as $link) {
      if ($link['view']) {
        $related_galleries[] = $link['view'];
      }
    }
    if ($related_galleries) {
      $related_galleries_content = theme('item_list', $related_galleries);
      $related_galleries_content = '<div class="related-galleries"><h4>Related Galleries</h4><div class="content">' . $related_galleries_content . '</div></div>';
    }

    $vars['related_box'] = $related_galleries_content;
  }
}

/**
  * This function runs on all node types not just article.
  */ 
function bonnier_type_article_preprocess_node_article(&$vars) {
  $node = $vars['node'];
  // Fill the photo box
  $photo_node = node_load($node->field_main_photo[0]['nid']);

  // Multiple photo support
  if(variable_get('bonnier_type_article_multiple_photo', FALSE)) {
    $page = bonnier_type_article_get_page();
    $photo_override_nid = $node->field_main_photo[$page]['nid'];
    if($photo_override_nid) {
      $photo_node = node_load($photo_override_nid);
    } else {
      unset($photo_node);
    }
  }

  if ($photo_node) {
    $vars['photo_box'] = theme('photo_box', $photo_node, $node);
  }

  $vars['related_tags'] = theme('laserfist_related_tags', $node);  

  // Related Info
  $related_links = array();
  if (!empty($node->field_related_content)) {
    foreach ($node->field_related_content as $link) {
      if ($link['view']) {
        $related_links[] = $link['view'];
      }
    }
  }
  if (!empty($node->field_related_links)) {
    foreach ($node->field_related_links as $link) {
      if ($link['view']) {
        $related_links[] = $link['view'];
      }
    }
  }


  if ($related_links) {
    $related_links_content = theme('item_list', $related_links);
    $vars['below_node_content'] .= theme('article_related_box', 'Related Content', $related_links_content);
    $vars['related_links'] = $related_links;
  }

  // Paging
  $vars['pager'] = $node->paging;
}
