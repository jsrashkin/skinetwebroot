<div class="photo-box template-<?php print $class; ?>"<?php if (is_numeric($image_width)):?> style="width:<?php print $image_width; ?>px;"<?php endif; ?>>

  <div class="image"><?php print $image; ?></div>

  <?php if ($caption): ?>
    <div class="caption"><?php print $caption; ?></div>
  <?php endif; ?>

  <?php if ($enlarge): ?>
    <div class="enlarge"><?php print $enlarge; ?></div>
  <?php endif; ?>

  <?php if ($related_gallery): ?>
    <div class="related-gallery"><?php print $related_gallery; ?></div>
  <?php endif; ?>

  <?php if ($photo_credit): ?>
    <div class="photo_credit">Photo by: <span><?php print $photo_credit; ?></span></div>
  <?php endif; ?>

  <?php if ($related_box): ?>
    <div class="related-box"><?php print $related_box; ?></div>
  <?php endif; ?>

</div>
