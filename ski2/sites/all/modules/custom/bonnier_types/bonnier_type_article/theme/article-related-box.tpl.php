<div class="related-content">
  <?php if ($title): ?>
    <h4 class="title"><?php print $title; ?></h4>
  <?php endif; ?>

  <?php if ($content): ?>
    <div class="content">
      <?php print $content; ?>
    </div>
  <?php endif; ?>
</div>
