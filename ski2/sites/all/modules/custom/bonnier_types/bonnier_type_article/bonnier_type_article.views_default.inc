<?php
// $Id$

function bonnier_type_article_views_default_views() {
  $views = array();
  $path = dirname(__FILE__) .'/views';

  // A list of related content.
  include($path .'/view.related_content.inc');
  $views[$view->name] = $view;

  // A list of related tags.
  include($path .'/view.related_tags.inc');
  $views[$view->name] = $view;

  return $views;
}
