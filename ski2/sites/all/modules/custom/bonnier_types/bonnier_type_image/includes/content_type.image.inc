<?php
$content['type']  = array (
  'name' => 'Image',
  'type' => 'image',
  'description' => 'Image Type',
  'title_label' => 'Title',
  'body_label' => 'Descrption',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => false,
    'sticky' => false,
    'revision' => false,
  ),
  'addthis_nodetype' => 1,
  'forward_display' => false,
  'scheduler' => 0,
  'scheduler_touch' => 0,
  'old_type' => 'image',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'nodewords_edit_metatags' => true,
  'content_profile_use' => false,
  'comment' => 0,
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => 0,
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
  'flatcomments_remove_reply_link' => 
  array (
    'reply' => false,
  ),
  'fivestar' => 0,
  'fivestar_stars' => 5,
  'fivestar_labels_enable' => 1,
  'fivestar_label_0' => 'Cancel rating',
  'fivestar_label_1' => 'Poor',
  'fivestar_label_2' => 'Okay',
  'fivestar_label_3' => 'Good',
  'fivestar_label_4' => 'Great',
  'fivestar_label_5' => 'Awesome',
  'fivestar_label_6' => 'Give it @star/@count',
  'fivestar_label_7' => 'Give it @star/@count',
  'fivestar_label_8' => 'Give it @star/@count',
  'fivestar_label_9' => 'Give it @star/@count',
  'fivestar_label_10' => 'Give it @star/@count',
  'fivestar_style' => 'average',
  'fivestar_text' => 'dual',
  'fivestar_title' => 1,
  'fivestar_feedback' => 1,
  'fivestar_unvote' => 0,
  'fivestar_position_teaser' => 'hidden',
  'fivestar_position' => 'below',
  'fivestar_comment' => 0,
  'page_title' => 
  array (
    'show_field' => 
    array (
      'show_field' => false,
    ),
    'pattern' => '',
  ),
  'print_display' => 1,
  'print_display_comment' => 0,
  'print_display_urllist' => 1,
  'better_formats_allowed' => 
  array (
    1 => false,
    2 => false,
    3 => false,
    4 => false,
  ),
  'better_formats_defaults' => 
  array (
    'node-4' => 
    array (
      'format' => '2',
      'weight' => '-25',
    ),
    'node-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'node-2' => 
    array (
      'format' => '0',
      'weight' => '-23',
    ),
    'node-1' => 
    array (
      'format' => '0',
      'weight' => '-22',
    ),
    'comment-4' => 
    array (
      'format' => '4',
      'weight' => '-25',
    ),
    'comment-3' => 
    array (
      'format' => '2',
      'weight' => '-24',
    ),
    'comment-2' => 
    array (
      'format' => '4',
      'weight' => '-23',
    ),
    'comment-1' => 
    array (
      'format' => '4',
      'weight' => '-22',
    ),
  ),
);
$content['fields']  = array (
  0 => 
  array (
    'label' => 'Display Title',
    'field_name' => 'field_display_title',
    'type' => 'text',
    'widget_type' => 'text_textarea',
    'change' => 'Change basic information',
    'weight' => '-4',
    'rows' => '5',
    'size' => 60,
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'value' => '',
        '_error_element' => 'default_value_widget][field_display_title][0][value',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
        'views' => true,
      ),
    ),
    'display_settings' => 
    array (
      'label' => 
      array (
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  1 => 
  array (
    'label' => 'image',
    'field_name' => 'field_image',
    'type' => 'filefield',
    'widget_type' => 'imagefield_widget',
    'change' => 'Change basic information',
    'weight' => '-2',
    'file_extensions' => 'jpg jpeg png gif',
    'progress_indicator' => 'bar',
    'file_path' => '_images/[site-date-yyyy][site-date-mm]',
    'max_filesize_per_file' => '',
    'max_filesize_per_node' => '',
    'max_resolution' => 0,
    'min_resolution' => 0,
    'custom_alt' => 1,
    'alt' => '[site-name]',
    'custom_title' => 0,
    'title_type' => 'textfield',
    'title' => '',
    'use_default_image' => 0,
    'default_image_upload' => '',
    'default_image' => NULL,
    'description' => '',
    'group' => false,
    'required' => 1,
    'multiple' => '0',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'op' => 'Save field settings',
    'module' => 'filefield',
    'widget_module' => 'imagefield',
    'columns' => 
    array (
      'fid' => 
      array (
        'type' => 'int',
        'not null' => false,
        'views' => true,
      ),
      'list' => 
      array (
        'type' => 'int',
        'size' => 'tiny',
        'not null' => false,
        'views' => true,
      ),
      'data' => 
      array (
        'type' => 'text',
        'serialize' => true,
        'views' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '-3',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'image_plain',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'image_plain',
        'exclude' => 1,
      ),
      2 => 
      array (
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'image_plain',
        'exclude' => 0,
      ),
    ),
  ),
  2 => 
  array (
    'label' => 'Galleries',
    'field_name' => 'field_galleries',
    'type' => 'nodereferrer',
    'widget_type' => 'nodereferrer_list',
    'change' => 'Change basic information',
    'weight' => '-1',
    'description' => '',
    'group' => false,
    'required' => false,
    'multiple' => 0,
    'referrer_types' => 
    array (
      'gallery' => 'gallery',
      'article' => 0,
      'blog' => 0,
      'blog_post' => 0,
      'channel' => 0,
      'channel_guide' => 0,
      'channel_guide_section' => 0,
      'person' => 0,
      'forum' => 0,
      'og_group' => 0,
      'homepage' => 0,
      'image' => 0,
      'link' => 0,
      'multichoice' => 0,
      'page' => 0,
      'panel' => 0,
      'poll' => 0,
      'user_answer' => 0,
      'user_question' => 0,
      'quiz' => 0,
      'story' => 0,
      'ugc_photo' => 0,
      'webform' => 0,
      'menu_item' => false,
      'brand' => false,
      'deal_of_the_day' => false,
      'deal_of_the_day_feed' => false,
      'feature_tout' => false,
      'feed' => false,
      'feed_sister_sites_item' => false,
      'long_answer' => false,
      'matching' => false,
      'our_sponsors_logo' => false,
      'pcd' => false,
      'product' => false,
      'product_boot' => false,
      'product_ski' => false,
      'profile' => false,
      'quiz_directions' => false,
      'resort' => false,
      'short_answer' => false,
      'true_false' => false,
      'video' => false,
      'laserfist_gallery' => false,
      'family_member' => false,
      'directory_section' => false,
      'pregnancy' => false,
      'timeline' => false,
      'tool' => false,
      'tag' => false,
    ),
    'referrer_fields' => 
    array (
      'field_gallery_page' => 'field_items',
      'field_main_photo' => 0,
      'field_thumbnail' => 0,
      'field_related_content' => 0,
      'field_blog' => 0,
      'field_author' => 0,
      'field_directory' => 0,
      'field_channel_sections' => 0,
      'field_guide_links' => 0,
      'field_question' => 0,
      'field_best_answer' => 0,
      'field_photo_credit' => false,
      'field_related_gallery' => false,
      'field_tout_target' => false,
      'field_items' => false,
      'field_related_galleries' => false,
      'field_product_brand' => false,
      'field_resort_trailmap_image' => false,
      'field_credit' => false,
      'field_panel_template' => false,
      'field_right_column_panel' => false,
      'field_pages' => false,
      'field_feature_content' => false,
      'field_feature_slideshow' => false,
      'field_week_article' => false,
      'field_target' => false,
    ),
    'referrer_nodes_per_page' => '0',
    'referrer_pager_element' => '0',
    'referrer_order' => 'TITLE_ASC',
    'op' => 'Save field settings',
    'module' => 'nodereferrer',
    'widget_module' => 'nodereferrer',
    'columns' => 
    array (
    ),
    'display_settings' => 
    array (
      'weight' => '-2',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
  3 => 
  array (
    'label' => 'Photo Credit',
    'field_name' => 'field_photo_credit',
    'type' => 'nodereference',
    'widget_type' => 'noderefcreate_autocomplete',
    'change' => 'Change basic information',
    'weight' => 0,
    'autocomplete_match' => 'contains',
    'description' => '',
    'default_value' => 
    array (
      0 => 
      array (
        'nid' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => 
    array (
      'field_photo_credit' => 
      array (
        0 => 
        array (
          'nid' => 
          array (
            'nid' => '',
            '_error_element' => 'default_value_widget][field_photo_credit][0][nid][nid',
          ),
          '_error_element' => 'default_value_widget][field_photo_credit][0][nid][nid',
        ),
      ),
    ),
    'group' => false,
    'required' => 0,
    'multiple' => '0',
    'referenceable_types' => 
    array (
      'person' => 'person',
      'article' => 0,
      'blog' => 0,
      'blog_post' => 0,
      'channel' => 0,
      'channel_guide' => 0,
      'channel_guide_section' => 0,
      'forum' => 0,
      'gallery' => 0,
      'og_group' => 0,
      'homepage' => 0,
      'image' => 0,
      'link' => 0,
      'menu_item' => 0,
      'multichoice' => 0,
      'page' => 0,
      'panel' => 0,
      'poll' => 0,
      'user_answer' => 0,
      'user_question' => 0,
      'quiz' => 0,
      'story' => 0,
      'ugc_photo' => 0,
      'webform' => 0,
      'brand' => false,
      'deal_of_the_day' => false,
      'deal_of_the_day_feed' => false,
      'feature_tout' => false,
      'feed' => false,
      'feed_sister_sites_item' => false,
      'long_answer' => false,
      'matching' => false,
      'our_sponsors_logo' => false,
      'pcd' => false,
      'product' => false,
      'product_boot' => false,
      'product_ski' => false,
      'profile' => false,
      'quiz_directions' => false,
      'resort' => false,
      'short_answer' => false,
      'true_false' => false,
      'video' => false,
      'laserfist_gallery' => false,
      'family_member' => false,
      'directory_section' => false,
      'pregnancy' => false,
      'timeline' => false,
      'tool' => false,
      'tag' => false,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'noderefcreate',
    'columns' => 
    array (
      'nid' => 
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' => 
    array (
      'weight' => '-1',
      'parent' => '',
      'label' => 
      array (
        'format' => 'above',
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 1,
      ),
      2 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content['extra']  = array (
  'title' => '-5',
  'body_field' => '-3',
  'revision_information' => '2',
  'comment_settings' => '3',
  'menu' => '1',
  'path' => '4',
  'nodewords' => '6',
  'print' => '7',
  'scheduler_settings' => '5',
);

