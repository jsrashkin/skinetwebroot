<?php
/**
 * @file
 * Plugin to provide access control based upon a taxonomy values.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Taxonomy: vocabulary vid"),
  'description' => t('Control access by taxonomy vocabulary.'),
  'callback' => 'taxonomy_vocabulary_ctools_access_check',
  'settings form' => 'taxonomy_vocabulary_ctools_access_settings',
  'settings form submit' => 'taxonomy_vocabulary_ctools_access_settings_submit',
  'summary' => 'taxonomy_vocabulary_ctools_access_summary',
);

/**
 * Settings form for the 'Taxonomy' access plugin.
 */
function taxonomy_vocabulary_ctools_access_settings($form, &$form_state, $conf) {
  $vocabularies = taxonomy_get_vocabularies();
  $options = array();
  foreach ($vocabularies as $vid => $vocabulary) {
    $options[$vid] = $vocabulary->name;
  }
  $form['settings']['vocabulary_vid'] = array(
    '#title' => t('Taxonomy vocabulary name'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $conf['vocabulary_vid'],
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Submit function for the access plugins settings.
 *
 * We cast all settings to numbers to ensure they can be safely handled.
 */
function taxonomy_vocabulary_ctools_access_settings_submit($form, $form_state) {
  $form_state['conf']['vocabulary_vid'] = $form_state['values']['settings']['vocabulary_vid'];
}

/**
 * Check for access.
 */
function taxonomy_vocabulary_ctools_access_check($conf, $context) {
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $term = taxonomy_term_load(arg(2));
    if ($term && $term->vid == $conf['vocabulary_vid']) {
      return true;
    }
  }
  return false;
}

/**
 * Provide a summary description based upon the checked terms.
 */
function taxonomy_vocabulary_ctools_access_summary($conf, $context) {
  return t('If taxonomy vocabulary vid is "@val"', array(
    '@val' => $conf['vocabulary_vid'],
  ));
}