<?php
/**
 * @file
 * Plugin to provide access control based upon a varialbe value.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Varialbe: value"),
  'description' => t('Control access by varialbe value.'),
  'callback' => 'varialbe_value_ctools_access_check',
  'settings form' => 'varialbe_value_ctools_access_settings',
  'settings form submit' => 'varialbe_value_ctools_access_settings_submit',
  'summary' => 'varialbe_value_ctools_access_summary',
);

/**
 * Settings form for the 'varialbe' access plugin.
 */
function varialbe_value_ctools_access_settings($form, &$form_state, $conf) {

  $form['settings']['variable_name'] = array(
    '#title' => t('Variable name'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );

  $form['settings']['variable_value'] = array(
    '#title' => t('Variable value'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Submit function for the access plugins settings.
 *
 * We cast all settings to numbers to ensure they can be safely handled.
 */
function varialbe_value_ctools_access_settings_submit($form, $form_state) {
  foreach (array('variable_name', 'variable_value') as $key) {
    $form_state['conf'][$key] = $form_state['values']['settings'][$key];
  }
}

/**
 * Check for access.
 */
function varialbe_value_ctools_access_check($conf, $context) {
  return variable_get($conf['variable_name'], NULL) == $conf['variable_value'];
}

/**
 * Provide a summary description based upon the checked terms.
 */
function varialbe_value_ctools_access_summary($conf, $context) {
  return t('If variable "@var" has value "@val"', array(
    '@var' => $conf['variable_name'],
    '@val' => $conf['variable_value'],
  ));
}