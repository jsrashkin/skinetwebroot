<?php

/**
 * @file
 * Plugin to handle the 'Header' block to be embedded into a panel.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Header HTML'),
  'content_types' => 'ski_header_html',
  'single' => TRUE,
  'render callback' => 'ski_header_html_render',
  'description' => t('Header HTML block.'),
  'category' => t('ski'),
);

/**
 * Output function for the 'ski_header_html' content type.
 */
function ski_header_html_render($subtype, $conf, $args, $context) {
  global $user;
  $block = new stdClass();
  $block->title = t('Header HTML');

  $logo = theme_get_setting('logo');

  $main_menu = '';
  $menu_exists = (menu_load('main-menu') !== FALSE);
  if ($menu_exists && count($menu_tree = menu_tree_output(menu_tree_all_data('main-menu')))) {
    $main_menu = $menu_tree;
  }

  $block->content = array(
    '#theme' => 'ski_panels_header',
    '#logo' => $logo,
    '#main_menu' => render($main_menu),
  );
  return $block;
}
