<?php

/**
 * @file
 * Plugin to handle the 'footer' block to be embedded into a panel.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Footer HTML'),
  'content_types' => 'ski_footer',
  'single' => TRUE,
  'render callback' => 'ski_footer_html_render',
  'description' => t('footer HTML block.'),
  'category' => t('ski'),
);

/**
 * Output function for the 'ski_footer_html' content type.
 */
function ski_footer_html_render($subtype, $conf, $args, $context) {
  global $user;
  $block = new stdClass();
  $block->title = t('footer HTML');

  $logo = theme_get_setting('logo');

  $main_menu = '';
  $menu_exists = (menu_load('menu-footer-nav') !== FALSE);
  if ($menu_exists && count($menu_tree = menu_tree_output(menu_tree_all_data('menu-footer-nav')))) {
    $menu = $menu_tree;
  }

  $block->content = '
    <div class="copyright-footer">
      <div class="col-md-4"><p>' . t('© Copyright © @year Cruz Bay Publishing, Inc. an Active Interest Media Company.', array('@year' => date('Y'))) . '</p></div>
      <div class="col-md-8">
        ' . render($menu) . '
      </div>
    </div>
  ';
  return $block;
}
