<?php
$path_to_theme = drupal_get_path('theme', $GLOBALS['theme']);
?>
<!-- Header -->
<div id="header">
  <div class="wrap">
    <!-- Top header elements -->
    <div class="top-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-2 logo">
            <?php if ($logo) { ?>
              <a href="<?php print url('<front>'); ?>"><img src="<?php print $logo; ?>"/></a>
            <?php } ?>
          </div>
          <div class="col-md-7 ad">
		<?php
			$Banners2 = new EntityFieldQuery();
			$Banners2->entityCondition('entity_type', 'node')
				  ->entityCondition('bundle', 'bottom_banner')
				  ->propertyOrderBy('nid','desc')
				  ->range(0,1)
				  ->propertyCondition('status', NODE_PUBLISHED); // Run the query as user 1.
			$BannersQ=$Banners2->execute();
			if(!empty($BannersQ))
			{
				$Bi=array_keys($BannersQ['node']);
				$BannerImage=node_load($Bi[0]);
		?>
<!--        	<div class="col-md-7 ad">-->
  	<!--	<img src="<?php //echo $GLOBALS['base_url'].'/files/'.$BannerImage->field_banner['und'][0]['filename'];?>" lass="advertise"> -->

              <div class="hide"> <?php  $block = module_invoke('ad_manager', 'block_view', 'top');
					print render($block['content']); ?>
					</div>
								<?php  $block = module_invoke('ad_manager', 'block_view', 'top');
					print render($block['content']); ?>
<!--			</div>-->
		<?php 	}?>
          </div>
          <div class="col-md-3 register">
            <div class="container-fluid p-socials">
              <div class="social-wrap">
                <div class="top-socials">
                  <a href="https://www.facebook.com/skimag/" target="_blank" class="facebook">
                    <i class="fa fa-facebook"></i>
                  </a>
                  <a href="https://twitter.com/SkiMagOnline" class="twitter">
                    <i class="fa fa-twitter"></i>
                  </a>
                 <!-- <a href="#" class="dribbble">
                    <i class="fa fa-dribbble"></i>
                  </a>-->
                <!--  <a href="https://instagram.com/SkiMagazine/" class="instagram">
                    <i class="fa fa-instagram"></i>
                  </a> -->

					<a href="https://instagram.com/SkiMagazine/" class="instagram" >
						<img src="http://skinet-construction.com/ski/sites/all/themes/sevenmag_custom/images/ins.png">
				   </a>

                  <a href="https://www.pinterest.com/skimagazine/" class="pinterest">
                    <i class="fa fa-pinterest"></i>
                  </a>
                </div>
              </div>
              <div class="header-search">
                <div class="container-fluid search-box">
                
                <?php
//$block = module_invoke('search', 'block_view', 'search');
//print render($block['content']);
//print drupal_render(drupal_get_form('search_block_form'));
?>

   <div class="header-form-block">
	     <form accept-charset="UTF-8" id="search-block-form" method="post" action="/ski/search/node" class="search-form">
		  <div>
			<div class="container-inline">
			  <div class="form-item form-type-textfield form-item-search-block-form">
				<input type="text" class="search-input form-text search-input" maxlength="27" size="15" placeholder="Search..." name="keys" id="edit-keys" title="Enter the terms you wish to search for.">
			  </div>
			  <div class="search-title search-icon">
                   <i class="fa fa-search b-search"></i>
             </div>

			  <div id="edit-actions" class="search-title form-actions form-wrapper">
				<input type="submit" class="form-submit" value="Go" name="op" id="edit-submit">
			  </div>
			  <input type="hidden" value="form-UD5c2KYhm-8feOb_jLU7Mch7DaNaZIUqI07R261MsWo" name="form_build_id">
			  <input type="hidden" value="search_block_form" name="form_id">
			</div>
			
					  </div>
		</form>
   
   </div>
                 <!-- <input type="text" class="search-input active" placeholder="<?php print t('Search...'); ?>"> -->

                  <div class="search-title">
                  </div>
                  <script type="text/javascript">


							jQuery('.search-title').click(function () {
							  jQuery('.search-input').toggleClass('active');
							  jQuery('.search-icon').hide();

							});

                  </script>
                </div>
              </div>
              <!--div class="col-md-6">
                <div class="top-menu">
                  <ul class="t-menu">
                    <li><a href=""><i class="fa fa-plus"></i> Register</a></li>
                    <li><a href=""><i class="fa fa-user"></i> Login</a></li>
                  </ul>
                </div>
              </div-->
            </div>
          </div>
        </div>
      </div>
    </div>
    
					

    <!-- End top header -->
    <!-- Nav Primary -->

    <div class="nav-primary">
      <nav class="navbar navbar-default">
        <div class="container-fluid"> 
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only"><?php print t('Toggle navigation'); ?></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php print $main_menu; ?>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </div>
    <!-- End Nav Primary -->
  </div>
</div>
<!-- End header -->
