<?php
/**
* Get results from views - really helpful when trying to just 
* use Views for data capture
*/
function _get_results_from_view($name, $display = 'default'){
  $my_view = new stdClass();
  $my_view = views_get_view($name, $display);
  $my_view->execute($display);
  $my_view->render();
  $results = array();
  foreach($my_view->result as $result){
    $results[] = $result;
  }
	// dpm($results);
  return $results;
}