<?php
$presets['blog_post_image'] = array (
  'presetname' => 'blog_post_image',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => 
      array (
        'width' => '450',
        'height' => '270',
        'upscale' => 0,
      ),
    ),
  ),
);

