<?php
$presets['small_square_landscape_thumbnail'] = array (
  'presetname' => 'small_square_landscape_thumbnail',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => array (
        'width' => '',
        'height' => '125',
        'upscale' => 0,
      ),
    ),
    1 => array (
      'weight' => '0',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_definecanvas',
      'data' => array (
        'RGB' => array (
          'HEX' => 'ffffff',
        ),
        'under' => 1,
        'exact' => array (
          'width' => '125',
          'height' => '125',
          'xpos' => 'center',
          'ypos' => 'center',
        ),
        'relative' => array (
          'leftdiff' => '',
          'rightdiff' => '',
          'topdiff' => '',
          'bottomdiff' => '',
        ),
      ),
    ),
    2 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_crop',
      'data' => array (
        'width' => '125',
        'height' => '125',
        'xoffset' => 'center',
        'yoffset' => 'top',
      ),
    ),
  ),
);

