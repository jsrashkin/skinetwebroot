<?php
$presets['from_our_sponsor'] = array (
  'presetname' => 'from_our_sponsor',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => array (
        'width' => '85',
        'height' => '75',
        'upscale' => 1,
      ),
    ),
  ),
);
