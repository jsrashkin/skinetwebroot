<?php
$presets['small_square_thumbnail'] = array (
  'presetname' => 'small_square_thumbnail',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache_canvasactions',
      'action' => 'canvasactions_aspect',
      'data' => array (
        'portrait' => '27',
        'landscape' => '28',
      ),
    ),
  ),
);

