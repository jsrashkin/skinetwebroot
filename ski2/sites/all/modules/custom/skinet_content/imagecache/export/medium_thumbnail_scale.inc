<?php
$presets['medium_thumbnail_scale'] = array (
  'presetname' => 'medium_thumbnail_scale',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale',
      'data' => array (
        'width' => '150',
        'height' => '150',
        'upscale' => 0,
      ),
    ),
  ),
);

