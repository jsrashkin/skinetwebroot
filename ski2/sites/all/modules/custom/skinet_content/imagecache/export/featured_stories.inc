<?php
$presets['featured_stories'] = array(
  'presetname' => 'featured_stories',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' => array (
        'width' => '1000',
        'height' => '400',
      ),
    ),
  ),
);
