<?php
$presets['med_175x150'] = array (
  'presetname' => 'med_175x150',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' => array (
        'width' => '175',
        'height' => '150',
      ),
    ),
  ),
);

