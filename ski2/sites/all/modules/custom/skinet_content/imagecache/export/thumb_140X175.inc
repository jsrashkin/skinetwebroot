<?php
$presets['thumb_140X75'] = array (
  'presetname' => 'thumb_140X75',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' => array (
        'width' => '140',
        'height' => '75',
      ),
    ),
  ),
);
