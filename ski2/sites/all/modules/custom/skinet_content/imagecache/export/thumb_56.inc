<?php
$presets['thumb_56'] = array (
  'presetname' => 'thumb_56',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' => array (
        'width' => '56',
        'height' => '56',
      ),
    ),
  ),
);

