<?php

$presets['hp_feature_slideshow_515x360'] = array (
  'presetname' => 'hp_feature_slideshow_515x360',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' => 
      array (
        'width' => '515',
        'height' => '360',
      ),
    ),
  ),
);