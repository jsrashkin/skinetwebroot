<?php
$presets['new_gear118x118'] = array (
  'presetname' => 'new_gear118x118',
  'actions' => array (
    0 => array (
      'weight' => '0',
      'module' => 'imagecache',
      'action' => 'imagecache_scale_and_crop',
      'data' => array (
        'width' => '118',
        'height' => '118',
      ),
    ),
  ),
);
