<?php
/**
* Declare a new articles content type
*/
function skinet_content_sister_site_feeds_ctools_content_types(){
  return array(
    'single' => TRUE,
    'title' => t('Sister Site Feeds block'),
    'description' => t('A block showing the sister site feeds'),
    'category' => 'Sister Site Feeds',
    'hook theme' => 'skinet_content_sister_site_feeds_block_theme',
  );
}

/**
* Implementation of hook_block
*/
function skinet_content_sister_site_feeds_block_theme(&$theme) {
  $plugin_path = drupal_get_path('module', 'skinet_content').'/plugins/content_types/sister_site_feeds';

  $theme['sister_site_feeds_block'] = array(
    'template' => 'sister_site_feeds_block',
    'path' => $plugin_path,
		'arguments' => array(
			'feeds' => NULL,
    ),
  );
}

function template_preprocess_sister_site_feeds_block(&$vars){
	$feeds = &$vars['feeds'];
	foreach($feeds as $feed => $feed_val){
		foreach($feed_val['results'] as $f_obj){
			$node = node_load($f_obj->nid); 
			$external_url = $node->feedapi_node->url;
			$f_obj->node_title = truncate_utf8($f_obj->node_title, 26, false, true);
			$f_obj->link = l($f_obj->node_title, $external_url, array('attributes' => array('target' => '_blank'))); 
		}
	}
}


/**
* Output sister site feeds
*/
function skinet_content_sister_site_feeds_content_type_render($subtype, $conf, $panel_args, $context){

	if(!function_exists('_get_results_from_view')){
	  module_load_include('inc', 'skinet_content');
	}
	$block = new stdClass();
	$block->title = '';
	$feed_arr = array();

  $current_site = array_shift(explode('/', trim(request_uri(), '/')));
  
  // get feeds - 2, 4, 5, 6, 7
	if($current_site != 'ski') {
		$feed_arr['ski_feed']['header_text'] = 'Ski Header';
		$feed_arr['ski_feed']['rss_link'] = _skinet_content_feed_icon("http://www.skinet.com/ski/feed/all", '');
		$feed_arr['ski_feed']['id'] = 'ski-site';
		$feed_arr['ski_feed']['results'] = _get_results_from_view('sister_site_feeds', 'block_6');
		$feed_arr['ski_feed']['feed_footer'] = '<p><a href="http://www.skinet.com/ski/">Go to skimag.com</a></p>';
	}	
	if($current_site != 'skiing') {
		$feed_arr['skiing_feed']['header_text'] = 'Skiing Header';
		$feed_arr['skiing_feed']['rss_link'] = _skinet_content_feed_icon("http://www.skinet.com/skiing/feed/all", '');
		$feed_arr['skiing_feed']['id'] = 'skiing-site';
		$feed_arr['skiing_feed']['results'] = _get_results_from_view('sister_site_feeds', 'block_2');		
		$feed_arr['skiing_feed']['feed_footer'] = '<p><a href="http://www.skinet.com/skiing/">Go to skiingmag.com</a></p>';
	}
	if($current_site != 'nastar') {
		$feed_arr['nastar_feed']['header_text'] = 'Nastar Header';
		$feed_arr['nastar_feed']['rss_link'] = _skinet_content_feed_icon("http://www.nastar.com/feed/all", '');
		$feed_arr['nastar_feed']['id'] = 'nastar-site';
		$feed_arr['nastar_feed']['results'] = _get_results_from_view('sister_site_feeds', 'block_4');
		$feed_arr['nastar_feed']['feed_footer'] = '<p><a href="http://www.nastar.com/">Go to nastar.com</a></p>';
	}	
	if($current_site != 'warrenmiller') {
		$feed_arr['wm_feed']['header_text'] = 'Warren Miller Header';
		$feed_arr['wm_feed']['rss_link'] = _skinet_content_feed_icon("http://www.skinet.com/warrenmiller/feed/all", '');
		$feed_arr['wm_feed']['id'] = 'warren-miller-site';
		$feed_arr['wm_feed']['results'] = _get_results_from_view('sister_site_feeds', 'block_5');
		$feed_arr['wm_feed']['feed_footer'] = '<p><a href="http://www.skinet.com/warrenmiller/">Go to warrenmiller.com</a></p>';
	}	
	if(variable_get('skinet_content_sissite_snow_en', 1) && $current_site != 'snow') {
		$feed_arr['snowmag_feed']['header_text']	= 'SnowMag Header';
		$feed_arr['snowmag_feed']['rss_link'] = _skinet_content_feed_icon("http://www.thesnowmag.com/rss.xml", '');                
		$feed_arr['snowmag_feed']['id'] = 'snow-mag-site';
		$feed_arr['snowmag_feed']['results']	= _get_results_from_view('sister_site_feeds', 'block_3'); //block_7
		$feed_arr['snowmag_feed']['feed_footer'] = '<p><a href="http://www.thesnowmag.com">Go to thesnowmag.com</a></p>';
	}
	$block->content = theme('sister_site_feeds_block', $feed_arr);
	
	return $block;
}
