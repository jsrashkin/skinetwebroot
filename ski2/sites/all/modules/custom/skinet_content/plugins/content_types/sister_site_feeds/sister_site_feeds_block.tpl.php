<?php
/*  Template file for sister site feeds
 *    $wedding_of_the_week[$wedding]
 *      - nid
 *      - node_title
 *      - image_thumb 
 *      - node_data_field_dek_field_dek_value 
*/
?>
<div id="sister_site_feeds">
	<h2 class="pane-title"><span class="full-title">From Our <span class="sister-sites">Sister Sites</span></span></h2>

	<?php foreach($feeds as $feed_obj){ ?>	
		<div class="inside panels-flexible-column-inside panels-flexible-column-8-1-inside panels-flexible-column-inside-first">
			
			<div class="sister-site-block-header">
				<div class="sister-site-block" id="<?php print $feed_obj['id']; ?>"><?php print $feed_obj['header_text']; ?></div>
				<div class="rss-icon" id="<?php print $feed_obj['id']; ?>-sister-feed-rss"><?php print $feed_obj['rss_link']; ?></div>
			</div><!-- sister-site-block-header -->
			
				<div class="item-list">
					<ul>
						<?php
							foreach($feed_obj['results'] as $feed_result){
								print '<li>' . $feed_result->link . '</li>';
							}
						?>
					</ul><!-- item-list -->
				</div>
                        
			<div class="view-footer">
				<?php print $feed_obj['feed_footer']; ?>
			</div>
		</div>
	<?php } ?>
</div><!-- sister_site_feeds -->
