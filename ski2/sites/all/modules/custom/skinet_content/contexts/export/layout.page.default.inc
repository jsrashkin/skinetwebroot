<?php

$contexts[] = array(
  'namespace' => 'layout',
  'attribute' => 'page',
  'value' => 'z_default',
  'default' => '1',
  'block' => array(
    'bonnier_ads-x90' => array(
      'module' => 'bonnier_ads',
      'delta' => 'x90',
      'weight' => 137,
      'region' => 'below_header',
      'status' => '0',
      'label' => 'DART tag: x90',
      'type' => 'context_ui',
    ),
  ),
);
