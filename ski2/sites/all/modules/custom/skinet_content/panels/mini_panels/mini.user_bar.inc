<?php

$mini = new stdClass;
$mini->disabled = TRUE; /* Edit this to true to make a default mini disabled initially */
$mini->api_version = 1;
$mini->name = 'user_bar';
$mini->category = 'header';
$mini->title = 'User Bar';
$mini->requiredcontexts = array();
$mini->contexts = FALSE;
$mini->relationships = FALSE;
$display = new panels_display;
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = '';
$display->hide_title = FALSE;
$display->title_pane = 0;
$display->content = array();
$display->panels = array();
  $pane = new stdClass;
  $pane->pid = 'new-1';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(),
  );
  $pane->configuration = array(
    'title' => 'User Bar',
    'body' => '<?php
      global $user;
      if ($user->uid > 0) {
        $items[] .= \'<span class="welcome">Welcome,</span> \'.l($user->name,\'user/\'.$user->uid,array(\'attributes\'=>array(\'class\'=>\'username\')));
        $items[] = l(\'LOGOUT\',\'logout\');
      } else {
        $items[] = l(\'LOG IN\',\'user\');
        $items[] = l(\'REGISTER\',\'user/register\');
      }
      $attr = array();
      $attr[\'class\'] = \'menu\';
      print theme(\'item_list\',$items,null,\'ul\',$attr);
    ?>',
    'format' => '3',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array(
    'css_id' => 'user-bar',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-1'] = $pane;
  $display->panels['middle'][0] = 'new-1';
$mini->display = $display;
