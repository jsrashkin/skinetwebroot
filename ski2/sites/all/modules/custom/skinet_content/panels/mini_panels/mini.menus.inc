<?php

$mini = new stdClass;
$mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
$mini->api_version = 1;
$mini->name = 'menus';
$mini->category = 'menus';
$mini->title = 'Mega Menu';
$mini->requiredcontexts = FALSE;
$mini->contexts = FALSE;
$mini->relationships = FALSE;
$display = new panels_display;
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = '';
$display->hide_title = FALSE;
$display->title_pane = 0;
$display->content = array();
$display->panels = array();
  $pane = new stdClass;
  $pane->pid = 'new-1';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'mega_menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'nodes_per_page' => '10',
    'pager_id' => '1',
    'use_pager' => 0,
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'override_title' => 1,
    'override_title_text' => '<none>',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-1'] = $pane;
  $display->panels['middle'][0] = 'new-1';
$mini->display = $display;
