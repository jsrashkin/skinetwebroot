<?php
$page = new stdClass;
$page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
$page->api_version = 1;
$page->name = 'skinet_media_photos';
$page->task = 'page';
$page->admin_title = 'Skinet Media - Photos';
$page->admin_description = '';
$page->path = 'photos/!term';
$page->access = array();
$page->menu = array();
$page->arguments = array(
  'term' => array(
    'id' => '',
    'identifier' => '',
    'argument' => '',
    'settings' => array(),
  ),
);
$page->conf = array();
$page->default_handlers = array();
$handler = new stdClass;
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'page_skinet_media_photos_panel_context';
$handler->task = 'page';
$handler->subtask = 'skinet_media_photos';
$handler->handler = 'panel_context';
$handler->weight = 0;
$handler->conf = array(
  'title' => 'Photos',
  'no_blocks' => 0,
  'css_id' => '',
  'css' => '',
  'contexts' => array(),
  'relationships' => array(),
);
$display = new panels_display;
$display->layout = 'twocol_stacked';
$display->layout_settings = array(
  'items' => array(
    'canvas' => array(
      'type' => 'row',
      'contains' => 'column',
      'children' => array(
        '0' => 'main',
      ),
      'parent' => NULL,
    ),
    'main' => array(
      'type' => 'column',
      'width' => 100,
      'width_type' => '%',
      'children' => array(
        '0' => 1,
        '1' => 'main-row',
      ),
      'parent' => 'canvas',
    ),
    'main-row' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        '0' => 'center',
      ),
      'parent' => 'main',
    ),
    'center' => array(
      'type' => 'region',
      'title' => 'Center',
      'width' => 100,
      'width_type' => '%',
      'parent' => 'main-row',
    ),
    '1' => array(
      'type' => 'row',
      'contains' => 'region',
      'children' => array(
        '0' => 'heading',
      ),
      'parent' => 'main',
    ),
    'heading' => array(
      'type' => 'region',
      'title' => 'Heading',
      'width' => 100,
      'width_type' => '%',
      'parent' => '1',
    ),
  ),
);
$display->panel_settings = array(
  'center' => array(
    'style' => 'tabs',
  ),
  'style_settings' => array(
    'center' => array(
      'title' => 'Browse Photo Galleries',
      'filling_tabs' => 'disabled',
    ),
  ),
);
$display->cache = array();
$display->title = 'Photos';
$display->content = array();
$display->panels = array();
  $pane = new stdClass;
  $pane->pid = 'new-1';
  $pane->panel = 'left';
  $pane->type = 'photo_cats';
  $pane->subtype = 'photo_cats';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-1'] = $pane;
  $display->panels['left'][0] = 'new-1';
  $pane = new stdClass;
  $pane->pid = 'new-2';
  $pane->panel = 'right';
  $pane->type = 'views';
  $pane->subtype = 'media_grid';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '15',
    'pager_id' => '',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => 'gallery,ugc_photo/%1',
    'url' => '',
    'display' => 'default',
    'override_title' => 1,
    'override_title_text' => '<none>',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-2'] = $pane;
  $display->panels['right'][0] = 'new-2';
  $pane = new stdClass;
  $pane->pid = 'new-3';
  $pane->panel = 'top';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Header',
    'title' => '',
    'body' => '<h3>Browse Photo Galleries</h3>',
    'format' => '2',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-3'] = $pane;
  $display->panels['top'][0] = 'new-3';
$display->hide_title = PANELS_TITLE_FIXED;
$display->title_pane = '0';
$handler->conf['display'] = $display;
$page->default_handlers[$handler->name] = $handler;
