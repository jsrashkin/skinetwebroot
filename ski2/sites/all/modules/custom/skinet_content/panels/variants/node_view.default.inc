<?php
$handler = new stdClass;
$handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
$handler->api_version = 1;
$handler->name = 'node_view_default';
$handler->task = 'node_view';
$handler->subtask = '';
$handler->handler = 'panel_template';
$handler->weight = 999;
$handler->conf = array(
    'title' => 'Default Panel Node Template',
    'no_blocks' => FALSE,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'logic' => 'and',
      ),
    );
$display = new panels_display;
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();

$display->title = '';

$display->content = array();

$display->panels = array();

$pane = new stdClass;

$pane->pid = 'new-1';

$pane->panel = 'middle';

$pane->type = 'core_header';

$pane->subtype = 'core_header';

$pane->shown = TRUE;

$pane->access = array(

    'plugins' => array(

      0 => array(

        'name' => 'node_type',

        'settings' => array(

          'type' => array(

            'webform' => 'webform',

            'article' => 'article',

            'blog_post' => 'blog_post',

            'gallery' => 'gallery',

            'travel_special' => 'travel_special',

            'ugc_photo' => 'ugc_photo',

            'brand' => 'brand',

            'deal_of_the_day' => 'deal_of_the_day',

            'feature_tout' => 'feature_tout',

            'gear_deals' => 'gear_deals',

            'page' => 'page',

            'resort' => 'resort',

            'season_pass_deals' => 'season_pass_deals',

            'video' => 'video',

            ),

            ),

            'context' => 'argument_nid_1',

            ),

            ),

            );

            $pane->configuration = array();

            $pane->cache = array();

            $pane->style = array();

            $pane->css = array();

            $pane->extras = array();

            $pane->position = 0;

            $display->content['new-1'] = $pane;

            $display->panels['middle'][0] = 'new-1';

            $pane = new stdClass;

            $pane->pid = 'new-2';

            $pane->panel = 'middle';

            $pane->type = 'node_content';

            $pane->subtype = 'node_content';

            $pane->shown = TRUE;

            $pane->access = array();

            $pane->configuration = array(

                'links' => 1,

                'page' => 1,

                'no_extras' => 0,

                'override_title' => 1,

                'override_title_text' => '<none>',

                'teaser' => 0,

                'identifier' => '',

                'link' => 0,

                'leave_node_title' => 0,

                'context' => 'argument_nid_1',

                );

                $pane->cache = array();

                $pane->style = array();

                $pane->css = array();

                $pane->extras = array();

                $pane->position = 1;

                $display->content['new-2'] = $pane;

                $display->panels['middle'][1] = 'new-2';

                $pane = new stdClass;

                $pane->pid = 'new-3';

                $pane->panel = 'middle';

                $pane->type = 'block';

                $pane->subtype = 'laserfist_comments-comments';

                $pane->shown = TRUE;

                $pane->access = array(

                    'plugins' => array(

                      0 => array(

                        'name' => 'node_type',

                        'settings' => array(

                          'type' => array(

                            'webform' => 'webform',

                            'article' => 'article',

                            'blog_post' => 'blog_post',

                            'gallery' => 'gallery',

                            'travel_special' => 'travel_special',

                            'brand' => 'brand',

                            'gear_deals' => 'gear_deals',

                            'product' => 'product',

                            'product_accessory' => 'product_accessory',

                            'product_apparel' => 'product_apparel',

                            'product_binding' => 'product_binding',

                            'product_boot' => 'product_boot',

                            'product_gallery' => 'product_gallery',

                            'product_ski' => 'product_ski',

                            'resort' => 'resort',

                            'season_pass_deals' => 'season_pass_deals',

                            'video' => 'video',

                            ),

                            ),

                            'context' => 'argument_nid_1',

                            ),

                            ),

                            );

                            $pane->configuration = array(

                                'override_title' => 1,

                                'override_title_text' => '<none>',

                                );

                            $pane->cache = array();

                            $pane->style = array();

                            $pane->css = array();

                            $pane->extras = array();

                            $pane->position = 2;

                            $display->content['new-3'] = $pane;

                            $display->panels['middle'][2] = 'new-3';

                            $display->hide_title = PANELS_TITLE_FIXED;

                            $display->title_pane = '0';

                            $handler->conf['display'] = $display;


