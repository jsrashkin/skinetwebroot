<div class="snowreports-ticker">
  <?php if ($more_link): ?>
  <span class="snowreports-title">
    <?php print $more_link; ?>
  </span>
  <?php endif; ?>
  
  <div class="tickers">
      <div class="tickers-wrapper">
		  <?php foreach($resorts as $key => $resort): ?>
            <?php print $resort; ?>
          <?php endforeach; ?>
      </div>
  </div>

  <?php if ($ad): ?>
    <div class="ticker-ad">
      <?php print $ad; ?>
    </div>
  <?php endif; ?>
</div>
