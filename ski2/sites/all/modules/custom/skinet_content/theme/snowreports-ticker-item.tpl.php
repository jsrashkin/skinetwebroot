<div class="snowreports-ticker-item">
  <?php if ($title): ?>
    <span class="snowreports-ticker-title"><?php print $title; ?></span>
  <?php endif; ?>

  <?php if ($status): ?>
    <span class="snowreports-ticker-status <?php print $status; ?>"><?php print $status; ?></span>
  <?php endif; ?>

  <?php if ($hi_temp): ?>
    <span class="snowreports-ticker-hi-temp"><?php print $hi_temp; ?></span>
  <?php endif; ?>

  <?php if ($low_temp): ?>
    <span class="snowreports-ticker-lowtemp"><?php print $low_temp; ?></span>
  <?php endif; ?>

  <span class="snowreports-ticker-extras">
  <?php if ($snowfall_24): ?>
    <span class="snowreports-ticker-snowfall-24"><?php print $snowfall_24; ?></span>
  <?php endif; ?>

  <?php if ($snowfall_48): ?>
    <span class="snowreports-ticker-snowfall-48"><?php print $snowfall_48; ?></span>
  <?php endif; ?>

  <?php if ($lifts_open): ?>
    <span class="snowreports-ticker-lifts-open"><?php print $lifts_open; ?></span>
  <?php endif; ?>

  <?php if ($base_depth): ?>
    <span class="snowreports-ticker-base-depth"><?php print $base_depth; ?></span>
  <?php endif; ?>
  </span>
</div>
