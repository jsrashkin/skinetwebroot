function rboxReady(evnt) {
  TRC.loadRBox(
      {"mode": "horizontalx3", "container": "taboola-main-column"},
      {"mode": "horizontalx2", "container": "taboola-side-column"});
}
var taboolaLoader = document.createElement('script');
document.getElementsByTagName('head')[0].appendChild(taboolaLoader);
if (taboolaLoader.attachEvent) {
  taboolaLoader.attachEvent('onactivate', rboxReady);
} else {
  taboolaLoader.addEventListener('activate', rboxReady, false);
}
taboolaLoader.type = "text/javascript";
taboolaLoader.src = "http://cdn.taboolasyndication.com/libtrc/bonnier-skinet/rbox.js?home";
