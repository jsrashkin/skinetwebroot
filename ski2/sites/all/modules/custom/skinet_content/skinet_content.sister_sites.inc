<?php
/**
 * Sister Sites functionality
 */
function skinet_content_sites_resorts() {
  $feed_titles = array('skiing-resorts');
  return skinet_content_feed_items($feed_titles);
}

function skinet_content_feed_items($feed_titles, $limit = 3) {
  $where_titles = "'". implode("','", $feed_titles) ."'";
  
  $sql = <<<SQL
    SELECT
      fnif.feed_item_nid
    FROM
      {node} n
        INNER JOIN {feedapi_node_item_feed} fnif ON n.nid = fnif.feed_nid
    WHERE
      n.status = 1
      AND n.type = 'feed'
      AND n.title IN ($where_titles)
    LIMIT $limit
SQL;

  $results = db_query($sql);
  $output = '';
  while ($result = db_fetch_object($results)) {
    $node = node_load($result->feed_item_nid);
    $output .= $node->body;
  }
  return $output;
}

