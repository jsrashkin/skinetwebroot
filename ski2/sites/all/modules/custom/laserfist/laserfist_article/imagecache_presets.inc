<?php
function laserfist_article_imagecache_default_presets() {
  $presets = array();

  $presets['article_image'] = array (
    'presetname' => 'article_image',
    'actions' => 
    array (
      0 => 
      array (
        'weight' => '0',
        'module' => 'imagecache',
        'action' => 'imagecache_scale',
        'data' => 
        array (
          'width' => '300',
          'height' => '',
          'upscale' => 0,
        ),
      ),
    ),
  );

  $presets['article_image_center'] = array (
    'presetname' => 'article_image_center',
    'actions' => 
    array (
      0 => 
      array (
        'weight' => '0',
        'module' => 'imagecache',
        'action' => 'imagecache_scale_and_crop',
        'data' => 
        array (
          'width' => '934',
          'height' => '400',
        ),
      ),
    ),
  );

  return $presets;
}
