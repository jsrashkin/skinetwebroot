<div id="content-header" class="<?php print $classes; ?>">
	<?php if ($title): ?> 
	<h2 class="main-title">
		<?php print $title; ?> 
	</h2>		
	<?php endif; ?>

	<?php if ($dek): ?> 
	<div class="dek">
		<?php print $dek; ?>
	</div>
	<?php endif; ?>

	<?php if ($credit): ?> 
	<div class="author">
		By <?php print $credit; ?>
	</div>
	<?php endif; ?>

	<?php if ($related_tags): ?> 
	<div class="related-tags">
		<span class="label">related tags:</span>
		<?php print $related_tags; ?>
	</div>
	<?php endif; ?>

	<div class="right-rail">
		<div class="utility-links">
			<?php print $email; ?>
			<?php print $print; ?>
			<?php print $share; ?>
		</div>
		<?php if($comment_count): ?>
		<div class="comments-count">
			<a href="#comment-box">
			comments <?php print $comment_count; ?>
			</a>
		</div>
		<?php endif; ?>

	</div>
</div>
