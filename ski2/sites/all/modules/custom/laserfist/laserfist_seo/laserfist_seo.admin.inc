<?php

function _laserfist_seo_path_redirect_admin_page() {
  $search = array();
  $fields = array('source', 'redirect', 'query', 'fragment');
  foreach ($fields as $field) {
    if ($_GET[$field]) {
      $search[$field] = $_GET[$field];
    }
  }

  $content = drupal_get_form('laserfist_seo_path_redirect_search_form', $search);
  $content .= _laserfist_seo_path_redirect_search_results($search);
  return $content;
}


// based on path_redirect_admin()
function _laserfist_seo_path_redirect_search_results($search) {
  $multilanguage = (module_exists('locale') || db_result(db_query("SELECT rid FROM {path_redirect} WHERE language <> ''")));
  $languages = language_list();

  $header = array(
    array('data' => t('From'), 'field' => 'source', 'sort' => 'asc'),
    array('data' => t('To'), 'field' => 'redirect'),
    array('data' => t('Type'), 'field' => 'type'),
    array('data' => t('Operations'), 'colspan' => '2'),
  );
  if ($multilanguage) {
    array_splice($header, 3, 0, array(array('data' => t('Language'), 'field' => 'language')));
  }
  $rows = array();
  $limit = 50;

  // Build out where
  foreach ($search as $key => $val) {
    $where[] = $key . ' like '. "'%$val%'";
  }
  if ($where) {
    $WHERE_SQL = 'WHERE '. implode($where, ' AND ') .' ';
  }

  $redirects = pager_query('SELECT rid, source as path, redirect, query, fragment, language, type FROM {path_redirect} '. $WHERE_SQL . tablesort_sql($header), $limit);

  while ($r = db_fetch_object($redirects)) {
    $redirect = url($r->redirect, array('query' => $r->query, 'fragment' => $r->fragment, 'absolute' => TRUE, 'alias' => TRUE));
    $row = array(
      'data' => array(
        // @todo: Revise the following messy, confusing line.
        l($r->path, preg_replace('/[\?\&].*/', '', $r->path), array('query' => strstr($r->path, '?') ? preg_replace('/.*\?/', '', $r->path) : NULL, 'language' => $r->language ? $languages[$r->language] : NULL)),
        // @todo: Fix sorting on the redirect field
        l($redirect, $redirect, array('external' => TRUE)),
        $r->type,
        l(t('Edit'), 'admin/build/path-redirect/edit/'. $r->rid, array('query' => drupal_get_destination())),
        l(t('Delete'), 'admin/build/path-redirect/delete/'. $r->rid, array('query' => drupal_get_destination())),
      ),
    );
    if ($multilanguage) {
      array_splice($row['data'], 3, 0, module_invoke('locale', 'language_name', $r->language));
    }
    $rows[] = $row;
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No redirects available.'), 'colspan' => $multilanguage ? 5 : 4));
  }

  $output = theme('table', $header, $rows, array('class' => 'path-redirects'));
  $output .= theme('pager', NULL, $limit);

  return $output;
}

function laserfist_seo_path_redirect_search_form(&$form_state) {
  $edit = $_GET;  

  $form = array();
  $form['#method'] = 'GET';
  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => 'Search',
  );
  $form['search']['source'] = array(
    '#type' => 'textfield',
    '#title' => 'From',
    '#default_value' => $edit['source'],
  );
  $form['search']['redirect'] = array(
    '#type' => 'item',
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
    '#title' => t('To'),
  );
  $form['search']['redirect']['redirect'] = array(
    '#type' => 'textfield',
    '#size' => 30,
    '#maxlength' => 255,
    '#default_value' => $edit['redirect'],
  );

  $form['search']['redirect'][] = array(
    '#value' => '?',
  );

  $form['search']['redirect']['query'] = array(
    '#type' => 'textfield',
    '#size' => 12,
    '#maxlength' => 255,
    '#default_value' => $edit['query'],
  );

  $form['search']['redirect'][] = array(
    '#value' => '#',
  );

  $form['search']['redirect']['fragment'] = array(
    '#type' => 'textfield',
    '#size' => 12,
    '#maxlength' => 50,
    '#default_value' => $edit['fragment'],
  );
  $form['submit'] = array(
    '#type'=> 'submit',
    '#value' => 'Search',
  );
  return $form;
}

function laserfist_seo_path_redirect_search_form_submit(&$form, &$form_state) {
}
