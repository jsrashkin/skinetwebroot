<?php

function laserfist_search_search_info_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Search Info'),
    'description' => t('Search Info'),
    'category' => 'Apache SOLR Search',
    'render last' => TRUE,
    'hook theme' => 'laserfist_search_search_info_theme',	
  );
}

function laserfist_search_search_info_content_type_render($subtype, $conf, $panel_args, $context) {
  $response = apachesolr_static_response_cache();
  if(empty($response)) {
    return;
  }


  $content = theme('laserfist_search_info',$response);	
  $block->content = $content;
  $block->title = '';
  $block->delta = 'laserfist_search_info';
  return $block;
}

function laserfist_search_search_info_theme(&$theme) {
  $theme['laserfist_search_info'] = array(
    'template' => 'laserfist-search-info',
    'arguments' => array('response' => null),
    'path' => drupal_get_path('module','laserfist_search').'/plugins/content_types',
  );	
}

function template_preprocess_laserfist_search_info(&$vars) {
  $response = $vars['response'];
  if(empty($response)) {
    return;
  }

  $menu = menu_get_item();
  $search_path = $menu['path'];
  $keys = filter_xss(trim(laserfist_search_get_keys()));

  $count = $response->response->numFound;	

  $vars['result_count'] = $count;
  $vars['search_terms'] = $keys;

  $vars['start'] = $response->response->start + 1;
  
  $vars['end'] = $response->response->start + count($response->response->docs);
  
}
