<?php

function laserfist_search_search_form_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Search Form'),
    'description' => t('Search Form'),
    'category' => 'Apache SOLR Search',
		'render last' => TRUE,
  );
}

function laserfist_search_search_form_content_type_render($subtype, $conf, $panel_args, $context) {

	$type = 'laserfist_search';
	$menu = menu_get_item();
	$search_path = $menu['path'];

	$keys = trim(laserfist_search_get_keys());
	$content = drupal_get_form('search_form', $search_path, $keys, $type);

  $block->content = $content;
  $block->title = '';
  $block->delta = 'laserfist_search_form';
  return $block;
}
