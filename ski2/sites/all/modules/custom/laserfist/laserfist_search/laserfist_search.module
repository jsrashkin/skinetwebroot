<?php

module_load_include('inc','search','search.pages');

function laserfist_search_ctools_plugin_directory($module,$plugin) {
  if($module == 'ctools') {
    return 'plugins/'.$plugin;
  }
}

function laserfist_apachesolr_search_view($search_path) {
	static $results;
	if($results) {
		return $results;
	}
	// set the search path
	laserfist_search_get_search_path($search_path);
  $results = '';
	$type = 'laserfist_search';
	$keys = filter_xss(trim(laserfist_search_get_keys()));
	$filters = '';
	if (isset($_GET['filters'])) {
		$filters = trim($_GET['filters']);
	}
	// Only perform search if there is non-whitespace search term or filters:
	if ($keys || $filters) {
		// Log the search keys:
		$log = $keys;
		if ($filters) {
			$log .= 'filters='. $filters;
		}      watchdog('search', '%keys (@type).', array('%keys' => $log, '@type' => t('Search')), WATCHDOG_NOTICE, l(t('results'), 'search/'. $type .'/'. $keys));

		// Collect the search results:
		$results = search_data($keys, $type);

		if ($results) {
			$results = theme('box', t('Search results'), $results);
		}
		else {        
			$results = theme('box', t('Your search yielded no results'), search_help('search#noresults', drupal_help_arg()));
		}
	}
	return $results;
}

function laserfist_search_search($op = 'search', $keys = NULL) {
  switch ($op) {
    case 'name':
      return t('Search');

    case 'reset':
      apachesolr_clear_last_index('apachesolr_search');
      return;

    case 'status':
      return apachesolr_index_status('apachesolr_search');

    case 'search':
      $filters = isset($_GET['filters']) ? $_GET['filters'] : '';
      $solrsort = isset($_GET['solrsort']) ? $_GET['solrsort'] : '';
      $page = isset($_GET['page']) ? $_GET['page'] : 0;
      $search_path = laserfist_search_get_search_path();
      // clean up the keys from url/post encoding
      $keys = htmlspecialchars_decode($keys, ENT_QUOTES);
      try {
        $results = apachesolr_search_execute($keys, $filters, $solrsort, $search_path, $page);
        return $results;
      }
      catch (Exception $e) {
        watchdog('Apache Solr', nl2br(check_plain($e->getMessage())), NULL, WATCHDOG_ERROR);
        apachesolr_failure(t('Solr search'), $keys);
      }
      break;
  } // switch
}

/**
 * Implementation of hook_apachesolr_modify_query().
 */
function laserfist_search_apachesolr_modify_query(&$query, &$params) {
  $keys = arg(1);
  //check for OR caps in the search
  preg_match('/OR/',$keys,$matches);
  if($matches) {
    //set mm to 1 to allow OR
    $params['mm'] = 1;
  }
}



function laserfist_search_get_keys() {
	static $return;
	$max_bit = 2;
	if(!isset($return)) {
		$path = explode('/',$_GET['q'],$max_bit);
		$return = count($path) == $max_bit ? $path[$max_bit-1] : null;
	}
	return check_plain(strip_tags($return));
}

function laserfist_search_get_search_path($path=null) {
	static $search_path;
	if($path) {
		$search_path = $path;
	}
	return $search_path;
}

function laserfist_search_form_search_form_alter(&$form, $form_state) {
  if ($form['module']['#value'] == 'laserfist_search') {
    $form['#submit'] = array('laserfist_search_form_search_submit');
    // No other modification make sense unless a query is active.
    // Note - this means that the query must always be run before
    // calling drupal_get_form('search_form').
    $apachesolr_has_searched = apachesolr_has_searched();

    $queryvalues = array();
    if ($apachesolr_has_searched) {
      $query = apachesolr_current_query();
      $queryvalues = $query->get_url_queryvalues();
    }

    $form['basic']['apachesolr_search']['#tree'] = TRUE;
    $form['basic']['apachesolr_search']['queryvalues'] = array(
      '#type' => 'hidden',
      '#default_value' => serialize($queryvalues),
    );
    $form['basic']['apachesolr_search']['get'] = array(
      '#type' => 'hidden',
      '#default_value' => serialize(array_diff_key($_GET, array('q' => 1, 'page' => 1, 'filters' => 1, 'solrsort' => 1, 'retain-filters' => 1))),
    );
    if ($queryvalues || isset($form_state['post']['apachesolr_search']['retain-filters'])) {
      $form['basic']['apachesolr_search']['retain-filters'] = array(
        '#type' => 'checkbox',
        '#title' => t('Retain current filters'),
        '#default_value' => (int) isset($_GET['retain-filters']),
      );
    }

    if (variable_get('apachesolr_search_spellcheck', FALSE) && $apachesolr_has_searched && ($response = apachesolr_static_response_cache())) {
      //Get spellchecker suggestions into an array.
      $suggestions = get_object_vars($response->spellcheck->suggestions);

      if ($suggestions) {
        //Get the original query and replace words.
        $query = apachesolr_current_query();

        foreach($suggestions as $word => $value) {
          $replacements[$word] = $value->suggestion[0];
        }
        $new_keywords = strtr($query->get_query_basic(), $replacements);

        $form['basic']['suggestion'] = array(
          '#prefix' => '<div class="spelling-suggestions">',
          '#suffix' => '</div>',
          '#type' => 'item',
          '#title' => t('Did you mean'),
          '#value' => l($new_keywords, $query->get_path($new_keywords)),
        );
      }
    }
  }
}

/**
 * Added form submit function to account for Apache mode_rewrite quirks.
 *
 * @see apachesolr_search_form_search_form_alter()
 */
function laserfist_search_form_search_submit($form, &$form_state) {
  $fv = $form_state['values'];
  $keys = $fv['processed_keys'];
	$base = $form['#action'].'/';
  if (variable_get('clean_url', '0')) {
    $keys = str_replace('+', '%2B', $keys);
  }
  $get = unserialize($fv['apachesolr_search']['get']);
  $queryvalues = unserialize($fv['apachesolr_search']['queryvalues']);
  if (!empty($fv['apachesolr_search']['retain-filters']) && $queryvalues) {
    $get = $queryvalues + $get;
    $get['retain-filters'] = '1';
  }
  $form_state['redirect'] = array($base . $keys, $get);
  if ($keys == '' && !$queryvalues) {
    form_set_error('keys', t('Please enter some keywords.'));
  }
}

/**
 * Setup the Solr search results for theming
 */ 
function laserfist_search_preprocess_search_result(&$vars, $hook) {
  $node = node_load($vars['result']['node']->__get("nid"));
  $filepath = bonnier_common_teaser_image($node);
  $img_title = bonnier_common_common_image_alt($node);
  if ($filepath) {
    $vars['search_thumbnail'] = l(theme('imagecache', variable_get('laserfist_search_vars_search_result_imagecache', 'thumb_175x125'), $filepath, $img_title, $img_title), 'node/'. $node->nid, array('html' => TRUE));
  } 
  // Extract terms
  $terms = array();
  if (is_array($node->taxonomy)) {
    foreach ($node->taxonomy as $term) {
      $terms[] = l($term->name, 'taxonomy/term/'. $term->tid, array('alt' => $term->name));
    }
  }
  $vars['terms'] = theme('item_list', $terms);
  $vars['author'] = l($node->name, 'user/'. $node->uid);
  $vars['date'] = format_date($node->created, 'short');
  $vars['comment_count'] = $node->comment_count;
  
  $vars['title'] = l(bonnier_common_title($node), 'node/'. $node->nid);
  
  $vars['type'] = $node->type;
  $vars['node'] = $node;
}

function laserfist_search_imagecache_default_presets() {  
	$presets = array();
	require('imagecache/thumb_175x125.inc');
	return $presets;
}
