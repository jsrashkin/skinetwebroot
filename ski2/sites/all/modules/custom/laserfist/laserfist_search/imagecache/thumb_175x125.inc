<?php
$presets['thumb_175x125'] = array (
  'presetname' => 'thumb_175x125',
  'actions' => 
  array (
    0 => 
    array (
      'weight' => '0',
      'module' => 'imagecache_icon',
      'action' => 'imagecache_icon',
      'data' => 
      array (
        'width' => '175',
        'height' => '125',
        'upscale' => 0,
        'canvas_x_offset' => 'center',
        'canvas_y_offset' => 'center',
        'bg_hex' => 'ffffff',
        'border_hex' => '',
        'border_size' => '',
        'ratio_landscape' => '',
        'ratio_portrait' => '',
      ),
    ),
  ),
);