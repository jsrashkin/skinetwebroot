<?php
/*
 * Provides a set of common image, dek, tout functions
 */


/**
 * Returns a common image filepath
 */
function bonnier_common_image(&$node) {
  $hook = 'common_image';
  return bonnier_common_process('bonnier_common', $hook, $node);
}
function bonnier_common_common_image(&$node) {
  switch ($node->type) {
    default:
      // Required Image
      if ($image = $node->field_image[0]['filepath']) {
        return $image;
      }
      // Optional image
      if ($image = $node->field_tout_image[0]['filepath']) {
        return $image;
      }

      // Image is via a noderef
      $image_nid = $node->field_main_photo[0]['nid'];
      if ($image_nid) {
        $image = bonnier_common_image($image_nid);
      }
      break;
      
  }
  return $image;
}

/**
 * Returns a common image filepath
 */
function bonnier_common_image_node(&$node) {
  $hook = 'common_image_node';
  return bonnier_common_process('bonnier_common', $hook, $node);
}

function bonnier_common_common_image_node(&$node) {
  switch ($node->type) {
    default:
      $image_nid = $node->field_main_photo[0]['nid'];
      $image = node_load($image_nid);

      if (!$image && bonnier_common_image($node)) {
        $image = $node;
      }    
      break;
  }
  return $image;
}

/**
 * Returns a common thumbnail image filepath
 */
function bonnier_common_teaser_image(&$node) {
  $hook = 'common_teaser_image';
  return bonnier_common_process('bonnier_common', $hook, $node);
}
function bonnier_common_common_teaser_image(&$node) {
  switch ($node->type) {
    case 'image':
      $image = bonnier_common_image($node);
      break;
    default:
      $thumbnail_nid = $node->field_thumbnail[0]['nid'];
      if ($thumbnail_nid) {
        $image = bonnier_common_teaser_image($thumbnail_nid);
      }
      // We have no thumbnail. Grab the main photo
      if (!$thumbnail_nid) {
        $image = bonnier_common_image($node);
      }
      break;
  }
  return $image;
}

/**
 * Returns a common thumbnail image filepath
 */
function bonnier_common_teaser_image_node(&$node) {
  $hook = 'common_teaser_image_node';
  return bonnier_common_process('bonnier_common', $hook, $node);
}

function bonnier_common_common_teaser_image_node(&$node) {
  switch ($node->type) {
    default:
      $thumbnail_nid = $node->field_thumbnail[0]['nid'];
      if (!$thumbnail_nid) {
        $thumbnail_nid = $node->field_main_photo[0]['nid'];
      }
      $image = node_load($thumbnail_nid);

      if(!$image && bonnier_common_teaser_image($node)) {
        $image = $node;
      }    
      break;
  }
  return $image;
}

/**
 * Returns a common body
 */
function bonnier_common_body(&$node) {
  $hook = 'common_body';
  return bonnier_common_process('bonnier_common', $hook, $node);
}
function bonnier_common_common_body(&$node) {
  switch ($node->type) {
    default:
      $body = $node->body;
      break;
  }
  return $body;
}

/**
 * Returns a common Dek
 */
function bonnier_common_dek(&$node) {
  $hook = 'common_dek';
  return bonnier_common_process('bonnier_common', $hook, $node);
}
function bonnier_common_common_dek(&$node) {
  switch ($node->type) {
    default:
      $dek = $node->field_dek[0]['value'];
      if (empty($dek) && variable_get('bonnier_common_dek_use_node_teaser', TRUE) ) {
        $dek = $node->teaser;
      }
      break;
  }
  return $dek;
}

/**
 * Returns a common credit link. Either to a node or user
 */
function bonnier_common_credit(&$node) {
  $hook = 'common_credit';
  return bonnier_common_process('bonnier_common', $hook, $node);
}
function bonnier_common_common_credit(&$node) {
  switch ($node->type) {
    case 'article':
      $credit = $node->field_author[0]['nid']; 
      break;
    case 'image':
      $credit = $node->field_photo_credit[0]['nid']; 
      break;
    default:
      $credit = $node->field_author[0]['nid']; 
      break;
  }
  if ($credit) {
    return node_load($credit);
  }
}

/**
 * Returns a common credit link. Either to a node or user
 */
function bonnier_common_title(&$node) {
  $hook = 'common_title';
  return bonnier_common_process('bonnier_common', $hook, $node);
}
function bonnier_common_common_title(&$node) {
  switch ($node->type) {
    default:
      $title = $node->title; 
      break;
  }
  return $title;
}

/**
 * Returns a common node link.
 */
function bonnier_common_link(&$node) {
  $hook = 'common_link';
  return bonnier_common_process('bonnier_common', $hook, $node);
}
function bonnier_common_common_link(&$node) {
  // If we have a target node ref.
  if (!empty($node->field_target[0]['nid']) && is_numeric($node->field_target[0]['nid'])) {
    $url = 'node/' . $node->field_target[0]['nid'];
  }
  // Redirect path takes precendence.
  if (!empty($node->field_redirect[0]['value'])) {
    $url = $node->field_redirect[0]['value'];
  }
  
  // If nothing found yet, just use the node path.
  $url = $url ? $url : 'node/' . $node->nid;

  return $url;
}

/**
 * Returns a common image filepath.
 */
function bonnier_common_image_alt(&$node) {
  $hook = 'common_image_alt';
  return bonnier_common_process('bonnier_common', $hook, $node);
}
function bonnier_common_common_image_alt(&$node) {
  switch ($node->type) {
    default:
      if ($node->field_image[0]['data']['alt']) {
        $alt = $node->field_image[0]['data']['alt'];
      } 
      if (!$alt) {
        $alt = $node->title;
      }
      break;
  }
  return $alt;
}

/**
 * Allow imagecache_presets to be overridden per content type.
 */
function bonnier_common_imagecache_preset(&$node, $preset) {
  $hook = 'common_imagecache_preset';
  return bonnier_common_process('bonnier_common', $hook, $node, $preset);
}
function bonnier_common_common_imagecache_preset(&$node, $preset) {
  // Get all of the active presets.
  $presets = imagecache_presets();
  // Work out what the new preset would be named.
  $new_preset = $preset . '_' . $node->type;
  
  // If the preset exists, use it.
  if (array_key_exists($new_preset, $presets)) {
    $preset = $new_preset;
  }

  // By default just return the preset.
  return $preset;
}
