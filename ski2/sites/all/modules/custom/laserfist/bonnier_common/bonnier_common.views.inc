<?php

function bonnier_common_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'bonnier_common') .'/includes',
    ),
    'handlers' => array(
      'bonnier_common_handler_field_image' => array(
        'parent' => 'views_handler_field',
      ),
      'bonnier_common_handler_field_node' => array(
        'parent' => 'views_handler_field_node',
      ),
      'bonnier_common_handler_field' => array(
        'parent' => 'views_handler_field',
      ),
      'bonnier_common_handler_field_credit' => array(
        'parent' => 'views_handler_field_node',
      ),
    ),
  );
}

function bonnier_common_views_data() {
  $data['bonnier_common']['table']['group'] = t('Bonnier Common');

  $data['bonnier_common']['table']['join']['node'] = array(
    '#global' => array(),
  );

//  $data['bonnier_common']['common_image'] = array(
//    'title' => t('Image'),
//    'help' => t('Bonnier Common Image'),
//    'field' => array(
//      'notafield' => TRUE,
//      'click sortable' => FALSE,
//      'handler' => 'bonnier_common_handler_field_image',
//      'handler function' => 'bonnier_common_image_node',
//    ),
//  );

  $data['bonnier_common']['common_teaser_image'] = array(
    'title' => t('Teaser Image'),
    'help' => t('Bonnier Common Teaser Image'),
    'field' => array(
      'notafield' => TRUE,
      'click sortable' => FALSE,
      'handler' => 'bonnier_common_handler_field_image',
      'handler function' => 'bonnier_common_teaser_image_node',
    ), 
  );

  $data['bonnier_common']['common_dek'] = array(
    'title' => t('Dek'),
    'help' => t('Bonnier Common Dek'),
    'field' => array(
      'notafield' => TRUE,
      'click sortable' => FALSE,
      'handler' => 'bonnier_common_handler_field_node',
      'handler function' => 'bonnier_common_dek',
    ), 
  );

  $data['bonnier_common']['common_link'] = array(
    'title' => t('Link'),
    'help' => t('Bonnier Common Link'),
    'field' => array(
      'notafield' => TRUE,
      'click sortable' => FALSE,
      'handler' => 'bonnier_common_handler_field_node',
      'handler function' => 'bonnier_common_link',
    ), 
  );

  $data['bonnier_common']['common_credit'] = array(
    'title' => t('Credit'),
    'help' => t('Bonnier Common Credit'),
    'field' => array(
      'notafield' => TRUE,
      'click sortable' => FALSE,
      'handler' => 'bonnier_common_handler_field_credit',
      'handler function' => 'bonnier_common_credit',
    ), 
  );

  $data['bonnier_common']['common_title'] = array(
    'title' => t('Title'),
    'help' => t('Bonnier Common Title'),
    'field' => array(
      'notafield' => TRUE,
      'click sortable' => FALSE,
      'handler' => 'bonnier_common_handler_field_node',
      'handler function' => 'bonnier_common_title',
    ), 
  );

  return $data;
}
