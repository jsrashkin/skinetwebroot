<?php

function laserfist_cache_signal_cache_pane_settings(&$display, &$content, $pane_key) {
  $cache_conf = array(); 
  global $user;

  // Don't cache 24x7 ads.
  if ($content->type == 'ads' || strpos($content->subtype, 'ad247realmedia') !== FALSE) {
    $cache_conf['disabled'] = TRUE;
  }

  // Support node based contexts
  $context = $content->configuration['context'];  
  switch($context) {
    case 'argument_nid_1':
    case 'relationship_taxonomy_node_from_node_1':
    case 'context_panels_template_node_1':
      $signal = 'node:'. $display->context[$context]->argument;
      $cache_conf['signals'][] = $signal;
  }

  // When you embed a node directly into a pane
  if ($content->type == 'node' && $content->subtype == 'node') {
    $cache_conf['signals'][] = 'node:'. $content->configuration['nid'];
    $node = node_load($content->configuration['nid']);
    if (user_access('view pane admin links')) {
      $cache_conf['granularity'][] = array('admin_links'=>'active');
    }
    // Do not cache panel nodes since they have displays of their own   
    if ($node->type == 'panel') {
      $cache_conf['disabled'] = TRUE;
    }
  }

  // Don't cache exposed Views filters.
  if (strpos($content->subtype, 'views--exp') === 0) {
    $cache_conf['disabled'] = TRUE;
  }

  // Don't cache the ams_newsletter_signup block.
  if (strpos($content->subtype, 'ams_newsletter_signup') === 0) {
    $cache_conf['disabled'] = TRUE;
  }

  // Don't cache comments.
  if ($content->subtype == 'laserfist_comments-comments') {
    $cache_conf['disabled'] = TRUE;
  }

  // Cache profile pages 
  if ($content->subtype == 'bonnier_profile-user_profile_content') {
    //$content->cache['settings']['caching_options']['granularity']['profile_arg'] = 'profile_arg:' . arg(3).arg(4);
    $cache_conf['disabled'] = TRUE;
  }

  // Cache this forever
  if ($pane_key == 'block__bonnier_publications-1') {
    $cache_conf['lifetime'] = 'permanent';
    $cache_conf['signals'][] = 'static_asset';
  }
}
