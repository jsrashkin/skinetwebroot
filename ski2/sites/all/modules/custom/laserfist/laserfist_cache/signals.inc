<?php

function laserfist_cache_nodeapi(&$node, $op, $a3, $a4 = NULL) {
  switch ($op) {
    case 'delete':
    case 'update':
    case 'insert':
      laserfist_cache_node_signal($node, $op, $a3, $a4);
      break;
  }
}

/*
  Taken from Nodereferrers
  Invalidate all nodes referencing this node 
  Also invlidate all nodes that this one points to
*/
function laserfist_cache_node_signal(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'insert':
    case 'update':
    case 'delete':
      // Clear content cache to help maintain proper display of nodes.
      $nids = array();
      $type = content_types($node->type);
      foreach ($type['fields'] as $field) {
        // Add referenced nodes to nids. This will clean up nodereferrer fields
        // when the referencing node is updated.
        if ($field['type'] == 'nodereference') {
          $node_field = isset($node->$field['field_name']) ? $node->$field['field_name'] : array();
          foreach ($node_field as $delta => $item) {
            $nids[$item['nid']] = $item['nid'];
          }
        }
      }

      // Clear Content cache for nodes that reference the node that is being updated.
      // This will keep nodereference fields up to date when referred nodes are
      // updated. @note this currenlty doesn't work all that well since nodereference
      // doesn't respect publishing states or access control.
      if (isset($node->nid)) {
        $referrers = nodereferrer_referrers($node->nid);
        $referrer_nids = array_keys($referrers);
        $nids = array_merge($nids, $referrer_nids);
      }

      foreach ($nids as $nid) {
        signal_caching_invalidate_node($nid);
      }
  }
}
