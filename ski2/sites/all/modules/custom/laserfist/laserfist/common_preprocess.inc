<?php
/*
  These are functions to common preprocess. You apply and use them
  in your own modules
*/

function laserfist_common_preprocess_node(&$vars, &$node) {
  $func = 'laserfist_common_preprocess_node_'. $node->type;
  if (function_exists($func)) {
    $func($vars,$node);
  } 
  else { // default
    laserfist_common_preprocess_node_common($vars, $node);    
  }
}

function laserfist_common_preprocess_node_common(&$vars, &$node) {
  laserfist_common_preprocess_utility_links($vars, $node);
  // Common attributes
  $vars['credit'] = theme('laserfist_credit', $node);
  $vars['publish_date'] = format_date($node->created);
}

function laserfist_common_preprocess_utility_links(&$vars, &$node) {
  // utility links
  $vars['share'] = theme('laserfist_share', $node);
  $vars['print'] = theme('laserfist_print', $node);
  $vars['email'] = theme('laserfist_email', $node);
  if ($node->comment == COMMENT_NODE_READ_WRITE) {
    $vars['comment'] = l('comment', "node/". $node->nid, array('fragment' => 'comments'));
  }
}

function laserfist_common_preprocess_node_image(&$vars, &$node) {
  // Don't replace credit
  $old_credit = $vars['credit'];
  laserfist_common_preprocess_node_common($vars, $node);
  $vars['credit'] = $old_credit;

  // Image Based
  $vars['photo_credit'] = theme('laserfist_credit', $node);
  $vars['enlarge'] = theme('laserfist_enlarge', $node);
  $vars['enlarge_link'] = theme('laserfist_enlarge_link', $node);
}
