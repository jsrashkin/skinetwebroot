<?php

/*
	This is ALMOSt a verbatim copy of the  _omniture_get_page_snippet function
	from the omniture module. Only real change is taht I don't output
	script and just return the settings
	
	Sorry I butcher this function - John
*/
function laserfist_omniture_old_omniture($custom_path = NULL) {

  // load the node corresponding with the current page
  // Or load the one provided by custom path
  $node = menu_get_object('node', 1, $custom_path);

  $current_theme = variable_get('theme_default', 'none');
  $pagename = variable_get('omniture_pagename', '');
  $ignore_paths = explode("\n", variable_get('omniture_ignore', ''));

  // Define path we're trying to get Omniture variables for
  $request_uri = !empty($custom_path) ? drupal_get_path_alias($custom_path) : request_uri(); 

  // first off, see if the page should be ignored
  foreach ($ignore_paths as $ignore) {
    // complete paths
    if ($request_uri === $ignore) {
      return;
    }
    // partial paths identified with the wildcard
    if (substr($ignore, -1) == '*') {
      $ignore = substr($ignore, 0, strlen($ignore) - 1);
      if (substr($request_uri, 0, strlen($ignore)) == $ignore) {
        return;
      }
    }
  }

  // see if there's a custom s_code.js file, if so load it instead
  global $theme_key;

  $default_file = base_path() . drupal_get_path('module', 'laserfist_omniture') .'/js/s_code.js';
  $custom_file_path = $_SERVER['DOCUMENT_ROOT'] . base_path() . drupal_get_path('theme', $theme_key) .'/scripts/s_code.js';
  $custom_file = base_path() . drupal_get_path('theme', $theme_key) .'/scripts/s_code.js';
  if ( file_exists($custom_file_path) ) {
    $script_path = $custom_file;
    $using_custom_file = true;
  }
  else {
    $script_path = $default_file;
    $using_custom_file = false;
  }
  $is404 = strpos(drupal_get_headers(), '404 Not Found') !== FALSE;
  
  $settings = array();

	$settings['script_path'] = $script_path;
  $settings['using_custom_scode'] = $using_custom_file;

  // load the optional s_account variable
  $settings['s_account'] = variable_get("omniture_account", '');
  $settings['s_dynamic_account_list'] = variable_get('omniture_dynamic_account_list', '');
  $settings['s_link_track_vars'] = variable_get('omniture_link_track_vars','');
  $settings['s_link_track_events'] = variable_get('omniture_link_track_events','');
  $settings['s_form_list'] = variable_get('omniture_form_list', '');
  $settings['s_var_used'] = variable_get('omniture_var_used','');
  $settings['s_event_list'] = variable_get('omniture_event_list','');
  // Find the correct tracking server, but fall back to default.
  $settings['s_tracking_server'] = variable_get('omniture_tracking_server','');
  if (empty($settings['s_tracking_server'])) {
    $settings['s_tracking_server'] = 'bonniercorp.122.2o7.net';
  }
  // Set migration variables for switching to 1st-party cookies.
  $settings['omniture_visitor_migration_key'] = variable_get('omniture_visitor_migration_key', '');
  $settings['omniture_visitor_migration_server'] = variable_get('omniture_visitor_migration_server', '');
  // Use custom link filters.
  $settings['s_link_internal_filters'] = variable_get('omniture_link_internal_filters','');


  // variable initialization
  $settings['base_pagename'] = $pagename;
//  $settings['pageType'] = '';
  $settings['pageName'] = array($settings['base_pagename']);
  $settings['channel'] = '';
  $settings['prop4'] = '';
  $settings['prop9'] = '';
  $settings['prop10'] = '';
  $settings['prop11'] = '';
  $settings['prop12'] = '';
  $settings['prop13'] = '';
  $settings['prop16'] = '';
  $settings['prop18'] = '';
  $settings['results'] = '';
  
  // Custom Variable to use for the token
  $settings['js_window_location_href'] = 'js|window.location.href';
//  $settings['primary_category'] = '';
//  $settings['secondary_category'] = '';

  // homepage
  if (drupal_is_front_page()) {
    $settings['channel'] = 'home';
    $settings['prop16'] = $settings['channel'];
    $settings['prop11'] = $settings['channel'];
    $settings['prop12'] = $settings['channel'];
    $settings['pageName'][] = $settings['channel'];
    $data = laserfist_omniture_prop_load(LASERFIST_OMNITURE_DEFAULT_OMID);
    if($data['system']) {
      // pull default values for homepage
      foreach ($data['system'] as $key => $value) {
        if ($key == 'pageName') {
          // removes the last array from pageName
          array_pop($settings['pageName']);
          // Adds the new pageName
          $settings['pageName'][] = $value;
        }
        else{
          $settings[$key] = $value;
        }
      }
    }
  }
  // taxonomy page, presuming using path_auto to make it look nicer
  elseif (arg(0, $custom_path) == 'taxonomy' && arg(1, $custom_path) == 'term' && is_numeric(arg(2, $custom_path))) {
    // remove the querystring
    $url = explode('?', $request_uri);
    // the string before the questionmark will be the actual page name
    // also remove any blank items
    $url = array_filter(explode('/', $url[0]));
    $settings['channel'] = $url[0];
    $settings['prop16'] = implode(':', $url);
    $settings['prop11'] = implode(':', $url);
    $settings['prop12'] = implode(':', $url);
    $settings['pageName'] = array_merge($settings['pageName'], $url);
  }
  // regular node
  elseif ($node || (arg(0, $custom_path) == 'node' && is_numeric(arg(1, $custom_path)))) {
    // if the node doesn't exist, e.g. we're on a webform return page, load it
    if (!$node) {
      $node = node_load(arg(1, $custom_path));
    }
    // basic node fields
    $settings['prop4'] = $node->type;
    $settings['prop9'] = trim(check_plain($node->title));
    $settings['title'] = trim(check_plain($node->title));
    $settings['prop10'] = $node->nid;
    $settings['author'] = '';
    // Need another variable for nodeid because prop10 can get overwritten via the custom pattern
    $settings['nid'] = $node->nid;
    if ($node->field_author) {
      foreach ($node->field_author as $key => $value) {
        $author_array[] = $node->field_author[$key]['safe']['title'];  
      }
      $settings['author'] = implode(",", $author_array); 
    } 
    if (!$settings['author']) {
      if ($credit_node = bonnier_common_credit($node)) {
        $settings['author'] = $credit_node->title;
      }
    } 
    if ($node->taxonomy) {
      $tags = array();
      foreach($node->taxonomy as $key => $value) {
        $tags[] = strtolower($node->taxonomy[$key]->name);
      }
      if ($tags) {
        $tags = array_unique($tags);
        $settings['tags'] = implode(',',$tags);
      }
    }

    // build a hierarchy of terms
    // by default use taxonomy_breadcrumbs to build it
    $terms = array();
    $term = _omniture_node_get_lightest_term(arg(1, $custom_path));
    $parents = array_reverse(taxonomy_get_parents_all($term->tid));
    foreach ($parents as $term) {
      $terms[] = $term->name;
    }
    
    /*if (module_exists('taxonomy_breadcrumb')) {
      if (!function_exists('_taxonomy_breadcrumb_node_get_lightest_term')) {
        include_once drupal_get_path('module', 'taxonomy_breadcrumb') .'/taxonomy_breadcrumb.inc';
      }
      $term = _taxonomy_breadcrumb_node_get_lightest_term(arg(1));
      $parents = array_reverse(taxonomy_get_parents_all($term->tid));
      foreach ($parents as $term) {
        $terms[] = $term->name;
      }
    }
    */
    // otherwise run the optional hook_omniture_node() functions
    //else {
      foreach (module_implements('omniture_node') as $module) {
        $result = call_user_func($module .'_omniture_node', $settings);
        if (is_array($result)) {
          $terms = array_merge($terms, $result);
        }
      }
    //}
    // clean up all of the terms
    // Must load the include file before invoking the pathautho_cleanstring function
    if (module_exists('pathauto')) {
      require_once(drupal_get_path('module', 'pathauto').'/pathauto.inc');
    }
    foreach ($terms as $t => $term) {
      $terms[$t] = module_invoke('pathauto', 'cleanstring', $term);
    }
    // compile the section variables
    // one level of terms

    if (count($terms) >= 1) {
      $settings['channel'] = $terms[0]; // section
      $settings['prop16'] = $terms[0]; // subsection level 1
      $settings['prop11'] = $terms[0]; // subsection level 2
      $settings['prop12'] = $terms[0]; // subsection level 3
    }

    // two levels of terms
    if (count($terms) >= 2) {
      $settings['prop16'] .= ':' . $terms[1]; // subsection level 1
      $settings['prop11'] .= ':' . $terms[1]; // subsection level 2
      $settings['prop12'] .= ':' . $terms[1]; // subsection level 3
    }
    // three levels of terms
    if (count($terms) >= 3) {
      $settings['prop11'] .= ':' . $terms[2]; // subsection level 2
      $settings['prop12'] .= ':' . $terms[2]; // subsection level 3
    }

    // Grabbing all terms going by parent channel:subchannel:child
    $settings['terms'] = $settings['prop11'];
    
    // If the current node is a channel node populate the information for that term
    if (!$terms && $node->type == 'channel') {
      $settings['channel'] = check_plain(strip_tags($node->title));
      $settings['terms'] = str_replace(' ','-',strtolower($settings['channel']));
    }else if ($node->type == 'channel') {
      // If no terms but is a channel node, append the channel term to the beggining of $settings['terms']
      $settings['terms'] .= ':'.  check_plain(strip_tags(str_replace(' ','-',strtolower($node->title))));
    }
    
    // Grab the current terms
    $curent_term = array();
    $current_term = explode(":", $settings['terms']);
    // If any channel exist
   
    if (count($current_term) > 0 ) {
      // Loops backward to get the curent terms since it's the last term for the hierachy.
      for ($i = count($current_term); $i > 0; $i--) {
        // Make sure the term isn't blank
        if (trim($current_term[$i - 1]) != '') {
          $settings['current-term'] = $current_term[$i - 1];
          break;
        }
      }
    }
    

    // add the terms into the pagename
    $settings['pageName'] = array_merge($settings['pageName'], $terms);
    // add the page title to the pagename
    if ($settings['prop9'] != '') {
      $settings['pageName'][] = $settings['prop9'];
    }
    // add the webform Thank You page
    if (arg(2, $custom_path) == 'done') {
      $settings['prop11'] .= ":Thank You";
      if (is_numeric($_GET['sid']) && is_numeric($settings['nid'])) {
        $settings['thankyou'] = TRUE; 
         // check to see if the webform also has a newsletter subscription being checked by user
        if(_ominiture_check_enews_in_contest($_GET['sid'], $settings['nid'])){
          $settings['newsletter'] = TRUE;
        }
      }
    }

  }
  // the error pages gets special treatment
  elseif ($is404) {
    $settings['pageName'] = array();
    $settings['pageType'] = 'errorPage';
    $settings['channel'] = 'error page';
    $settings['prop16'] = 'error page';
    $settings['prop11'] = 'error page';
    $settings['prop12'] = 'error page';
    $data = laserfist_omniture_prop_load(LASERFIST_OMNITURE_ERROR_OMID);
    if($data['system']) {
      // pull default values for error page
      foreach ($data['system'] as $key => $value) {
        $settings[$key] = $value;
      }
    }
  }
  // find the search term if one was available
  elseif ($s_matches = array() && preg_match("/search\/(.*)\/(.*)/i", $_GET['q'], $s_matches) > 0 && count($s_matches) >= 2) {
    // strip all HTML tags and trim the search string, some simple precautions
    $settings['prop18'] = strip_tags(trim($s_matches[2]));
    if (module_exists('apachesolr')){
      $response = apachesolr_static_response_cache();
      if (!empty($response)) {
        $count_values = $response->response->numFound;
        $settings['prop20'] = (int)$count_values;
      }
    }
  }
  // GoogleCSE support
  elseif ($_GET['q'] == "search/google" && isset($_GET['query']) && drupal_strlen(trim($_GET['query'])) > 0) {
    // strip all HTML tags and trim the search string, some simple precautions
    $settings['prop18'] = strip_tags(trim($_GET['query']));
  }
  
  // Sometimes they have the facet filtering with tid after the search so we are going add a statement to check that
  if (isset($_GET['filters'])) {
    $filters = explode(' ', $_GET['filters']);
    foreach ($filters as $value) {
      if(preg_match('/tid:([\d]*)$/', $value, $digits)){
        $extra_term[] = taxonomy_get_term($digits[1]);
      }
    }
    if ($extra_term) {
      $extra_term_name = array();
      foreach($extra_term as $key => $value){
        if (isset($extra_term[$key]->name)) {
          $extra_term_name[] =  $extra_term[$key]->name;
        }
      }
      if ($extra_term_name) {
        $settings['facets'] = implode(":", $extra_term_name);
      }
    }
  }
  // Preserved the channel variable incase the db overides it.  If users call [channel] it should call channel1 variable
  $settings['channel1'] = $settings['channel'];  
  
  $continue = TRUE;
  
  if (!db_table_exists('omniture')) {
    $continue = FALSE;
  }
  
  if (drupal_is_front_page() && $continue) {
    $omid = LASERFIST_OMNITURE_HOMEPAGE_OMID; 
    $continue = FALSE;
  }

  if ($continue && $is404) {
    $omid = LASERFIST_OMNITURE_ERROR_OMID; 
    $continue = FALSE;
  }
  // Loading information from database if any
  // Must be check in this order nodeid, absolute path, aliases(wild card), nodetype
  if ($continue) {
    $line_break = "\r\n";
    $sql = "SELECT omid, path, type FROM {omniture} WHERE type NOT IN ('error', 'default') ORDER BY type, weight";
    $results = db_query($sql);
    while ($row = db_fetch_object($results)) {
      $omid_path = array();
      $split = explode($line_break, $row->path);
      foreach ($split as $value) {
        $omid_path[] = strtolower($value);  
      }
      $omniture[$row->type][$row->omid] = $omid_path;
    }
    $match['nid'] = $settings['prop10'];
    if (is_numeric($match['nid']) && $omniture['nodeid'] && $continue) {
      // Checks Nodeid
      foreach ($omniture['nodeid'] as $key => $value) {
        if (in_array($match['nid'], $omniture['nodeid'][$key])) {
          $continue = FALSE;
          $omid = $key;
          break;
        }
      }
    } 
  }
  
  if ($continue && $omniture['absolute']) {
    // Did not find any nodeid matches so its going to now look for absolute path

    // TODO Needs to break apart the query string too  Right now it will 
    // add the query string into the path so it will not work correctly.
    $match['absolute'] = substr($request_uri, 1);
    
    foreach ($omniture['absolute'] as $key => $value) {
      if (in_array($match['absolute'], $value)) {
        $continue = FALSE;
        $omid = $key;
        break;
      }
    }
  }
  
  if ($continue && $omniture['aliases']) {
    // Did not find any exact path matches so now it looks for aliases path with wild cards
    // Remove the front slash infront of aliases
    if ($match['nid']) {
      // If omniture is tie to a node id
      $match['url'] = substr(url('node/'.$match['nid']), 1);
    }
    else {
      // Not all aliases have a node id
      $match['url'] = substr($request_uri, 1);
    }
    $accept_values = "([A-Za-z0-9-_\/]+?)";
    foreach ($omniture['aliases'] as $key => $value) {
      for ($i = 0; $i < count($value); $i++) {
        // Regex Time!!!!
        $regex = str_replace('\*', $accept_values, preg_quote($value[$i],'/'));
        $regex = '/'.$regex.'/';
        if (preg_match($regex, $match['url'], $matches)) {
          $continue = FALSE;
          $omid = $key;
          break;
        }
      }
      if (!$continue) break;
    }
  }
  
  if ($continue  && $settings['prop4'] && $omniture['nodetype']) {
    // Catching the node type
    $match['nodetype'] = strtolower($settings['prop4']);
    // Check node type if matches
    foreach ($omniture['nodetype'] as $key => $value) {
      if (in_array($match['nodetype'], $omniture['nodetype'][$key])) {
        $continue = FALSE;
        $omid = $key;
        break;
      }
    }
  }
  // Default page ominture
  if ($continue) {
    $omid = LASERFIST_OMNITURE_DEFAULT_OMID; 
  }
  
  // Original default value sprop
  $default_sprop = array(
    'pageType',
    'pageName', 
    'channel', 
    'events', 
    'prop2', 
    'prop3',
    'prop4', 
    'prop5', 
    'prop8', 
    'prop9', 
    'prop10', 
    'prop11',
    'prop12',
    'prop13',
    'prop16',
    'prop17',
    'prop18',
    'prop20'
  );

  if ($omid) {
    // FINALLY loading omniture property values after one of those omniture type 
    // is catch into if statement with $continue
    $data = laserfist_omniture_prop_load($omid);
    if ($data['system']) {
      foreach ($data['system'] as $key => $value) {
        if ($key == 'pageName') {
          $settings['pageName'] = array('',$value);
        }
        else{
          $settings[$key] = $value;
        }
      }
    }
  
    $search_urls = variable_get('laserfist_omniture_search_url' , 'search,find');
    if ($search_urls) {
      $search_url = explode(',', $search_urls);
      if (is_array($search_url)) {
        $search_url = array_map('trim', $search_url);
      }
    }
    if (
      module_exists('apachesolr')
      && isset($settings['prop18']) 
      && $settings['prop18'] != '[empty]' 
      && isset($search_url) 
      && is_array($search_url) 
      && !isset($response) 
    ){
      // Not all url will be /search/whatever, some sites uses /find/whateever so 
      // we will make it rely on prop18 to determine if the site is a search result
      $query_string = explode('/', $_GET['q']);
      if (in_array(strtolower($query_string[0]), $search_url)) {
        $response = apachesolr_static_response_cache();
        $settings['searchcount'] = $response->response->numFound;
        // Adding prop20 with the result count
        $settings['prop20'] = $settings['searchcount'];
        // Lets the loop pick up the variable so it doesn't unset the variable via the str_replace
        $query_string = explode('/', $_GET['q']);
        // Remove the first array "search" or "find" usually is contain is this array
        array_shift($query_string);
        $query_string = implode('/', $query_string);
        $settings['prop18'] = check_plain(strip_tags($query_string));
      }
    }
  
    // clean up the pageName
    if (is_array($settings['pageName'])) {
      // Gonna comment this line out and have it override the pageName instead of appending to it
      //$settings['pageName'] = implode(':', $settings['pageName']);
      $settings['pageName'] = end($settings['pageName']);
      // remove the : from intial title if any
      if (substr($settings['pageName'], 0, 1) == ':') {
        $settings['pageName'] = substr($settings['pageName'],1);
      } 
    }
    elseif (drupal_strlen($settings['pageName']) > 0) {
      // Why is this being wrap in quotes?
      $settings['pageName'] = '"' . $settings['pageName'] . '"';
    }
    else {
      $settings['pageName'] = 'document.title';
    }
    
    // Clone PageName here.  All clone settings will be resign back to settings with new values
    $clone_settings = array();
    $clone_settings['pageName'] = $settings['pageName'];
    
    // Load omniture pattern
    $pattern = _laserfist_omniture_prop_pattern();
    
    if (!isset($settings['thankyou'])) {
      // reset $settings['events'] to '' because its not @ the thankyou page
      $settings['events'] = '';
    }
    
    // Replacement Pattern for all values with [];  
    foreach ($settings as $key => $value) {
      if ($value != ''){
        if (preg_match_all('/\[prop([1-9]?[0-9])+?\]/', $value, $matches)) {
          foreach($matches[0] as $key1 => $match) {
            // replacing [prop#] with $old_settings['prop#'] values if multiple values
            $replace = $clone_settings['prop'.$matches[1][$key1]];
            if (!isset($clone_settings['prop'.$matches[1][$key1]])){
              $replace = $settings['prop'.$matches[1][$key1]];
            }
            $clone_settings[$key] = str_replace($match, $replace, $value);
            // update the values1 data
            $value = $clone_settings[$key];
          }
        }
        // regluar match without the prop*
        if (preg_match_all('/\[([a-z-_]*)\]+?/', $value, $matches)) {
          $count = 0;
          foreach($matches[0] as $match) {
            // adds the replacement into an array and replace a string all at once per property value
            // eg $settings['pageName'], $settings['channel'] ...
            if (array_key_exists($match, $pattern)) {
              $match_this[$count] = $match;
              // Only run if the pattern value is not an array else it will be overwritten by alter.
              if (!is_array($pattern[$match]['value'])) {
                // Set the match value to the pre assign settings variable being loaded above this function.
                $match_with[$count] = $settings[$pattern[$match]['value']];
              }
              elseif ($pattern[$match]['value'][0] &&
                function_exists($pattern[$match]['value'][0])
              ){ // Checking to see if function exists
                $func = $pattern[$match]['value'][0];
                // Remove the function name out of the first array so that we only have the arguments
                array_shift($pattern[$match]['value']);
                // Call the function with the aguments pass into it.
                $match_with[$count] = call_user_func_array($func, $pattern[$match]['value']);
              }
              $count++;
            }
          }
          if ($count) {
            // if pattern inside of string than replace all pattern.
            $replace = $pattern[$match]['value'];
            $clone_settings[$key] = str_replace($match_this, $match_with, $value);
          }
        }
      }
    }
    
    // Add the searchcount into result.  This will automatically add the searchcount into prop20.
    $clone_settings['result'] = $settings['searchcount'];
    
    // Reassign back on the changes made only from the regex from pattern values
    
    if(count($clone_settings) > 0) {
      foreach ($clone_settings as $key => $value) {
        $settings[$key] = $value;
      }
    }
   
    // Remove initial : in page name if any
    // Remove trailing spaces and :: to :
    $preset = array();
    foreach ($settings as $key => $value) {
      $value = str_replace('::', ':', $value);
      $preset[$key] = trim($value);
    }
  
    // if override is 1 than the custom omniture gets fire before the database values else it would do it vice versa
    $override = variable_get('laserfist_omniture_override', 1);
    
    // hook_omniture(), allow other modules to override settings
    // the current fields get passed as an array
    foreach (module_implements('omniture') as $module) {
  	$func = $module.'_omniture';
      $result = $func($settings);
      if (is_array($result)) {
        $settings = array_merge($settings, $result);
      }
    }
    // Grabs the preset array to see if it gets override by the module or db first.
    if ($preset) {
      if ($override) {
        // Database overrides the custom omniture settings
        $settings = array_merge($settings, array_filter($preset));
      }
      else {
        // File Overwides the Database settings
        $settings = array_merge($preset, array_filter($settings));
      }
    }
    // Remove prop value thatis not enable by {omniture_type}
    $prop_type = _laserfist_omniture_prop_type_load_all();

    if ($prop_type) {
      $check_settings = array();
      foreach ($prop_type as $key => $value) {
        if ($prop_type[$key]['enabled'] == 1) {
          if (array_key_exists($key, $settings)) {
            $check_settings[$key] = $settings[$key];
          }
          else {
            // Variable does not exist in function but is enabled in the database
            // so it creates a variable with empty value
            $check_settings[$key] = '';  
          }
          $check_settings['omniture']['key'][] = $key;
        }
      }
      
      // Destroy the $settings variable and reapply the one enabled in the db only.
      // If we dont' do this the preprocess that loops thru will catch all those extra settings like $settings['tags']
      // and put them into the js.
      $load = $check_settings;
    }
    else{
      $load = $settings;
      $load['omniture']['key'] = $default_sprop;
    }
  }
  else {
    // This is the default sprop in the footer page if the extended omniture module is not enable
    
    foreach ($default_sprop as $value) {
      $sprop[$value] = $settings[$value];
    }
    $sprop['omniture']['key'] = $default_sprop;
    
    if (is_array($sprop['pageName'])) {
      // Gonna comment this line out and have it override the pageName instead of appending to it
      //$settings['pageName'] = implode(':', $settings['pageName']);
      $sprop['pageName'] = end($sprop['pageName']);
      // remove the : from intial title if any
      if (substr($sprop['pageName'], 0, 1) == ':') {
        $sprop['pageName'] = substr($sprop['pageName'],1);
      } 
    }
    elseif (drupal_strlen($sprop['pageName']) > 0) {
      // Why is this being wrap in quotes?
      $sprop['pageName'] = '"' . $sprop['pageName'] . '"';
    }
    else {
      $sprop['pageName'] = 'document.title';
    }
    $load = $sprop;
  }
  // Remove one of the static s.prop17.  Seems to always be s.prop17 = window.location.href
  if (isset($load['prop17'])) {
    unset($load['prop17']);
  }
  $load['script_path'] = $settings['script_path'];
  $load['s_account'] = $settings['s_account'];
  $load['s_dynamic_account_list'] = $settings['s_dynamic_account_list'];
  $load['s_link_track_vars'] = variable_get('omniture_link_track_vars','');
  $load['s_link_track_events'] = variable_get('omniture_link_track_events','');
  $load['s_form_list'] = variable_get('omniture_form_list', '');
  $load['s_var_used'] = variable_get('omniture_var_used','');
  $load['s_event_list'] = variable_get('omniture_event_list','');
  $load['using_custom_scode'] = $settings['using_custom_scode'];

	return $load;
}

/*
	VERBANTUM COPIES BELOW
*/

/**
 * This function searches the path to determine if a taxonomy (preferably for vocabulary 1) is available, and if so, returns it.
 *
 * @return int
 *   A taxonomy id if one is found, otherwise -1.
 */
function _omniture_get_taxonomy_from_path($need_type = FALSE) {
  static $node_terms;
  if (!isset($node_terms)) {
    $node_terms = array();
  }

  $tid = -1;

  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $tid = intval(arg(2));
  }
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    if ($node_terms[arg(1)] > 0) {
      $tid = $node_terms[arg(1)];
    }
    elseif ($node = menu_get_object()) {
      foreach ($node->taxonomy as $term) {
        $tid = $term->tid;
        if ($term->vid == 1) {
          break;
        }
      }
      if ($tid > 0) {
        $node_terms[arg(1)] = $tid;
      }
    }
  }
  if (arg(0) == 'bookmarks') {
    $tid = -1;
  }
  else if ($tid == -1 && arg(0) != 'node') {
    $tid = omniture_get_tid_from_taxonomy(arg(0));
  }
  if ($tid == -1 && (arg(0) == 'galleries' || arg(0) == 'videos' || arg(0) == 'photos')) {
    $tid = omniture_get_tid_from_taxonomy(arg(1));
  }
  if ($need_type == TRUE) {
    static $node_type;
    if (empty($node_type) && (arg(0) == 'node'  && is_numeric(arg(1)))) {
      $node_type = $node->type;
    }
    $node_tid_type = array(
      'tid' => $tid,
      'type' => $node_type,
    );
    return $node_tid_type;
  }
  else {
    return $tid;
  }
}

/**
 * This function gets all of the existing taxonomy terms for the given page.  If it's a node on the page, then we grab all of the taxonomy terms for that node.
 * If it's something else, then we call _omniture_get_taxonomy_from_path(), and get the taxonomy object to send back.
 *
 * @return array
 *   Array of taxonomy terms.
 */
function _omniture_get_all_taxonomies_from_path() {
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $terms = taxonomy_node_get_terms(arg(1));
  }
  else {
    $tid = _omniture_get_taxonomy_from_path();
    $terms = array();
    if ($tid > -1) {
      $terms[] = taxonomy_get_term($tid);
    }
  }
  return $terms;
}

function omniture_get_tid_from_taxonomy($term) {
  //drupal_set_message("Searching for $term");
  if (is_numeric($term) && $term == intval($term)) {
    return intval($term);
  }

  // Primary taxonomy hit?
  $query = "SELECT src FROM {url_alias} WHERE dst = '%s'";
  $result = db_query($query, $term);
  if ($url = db_fetch_object($result)) {
    //drupal_set_message("url: ". $url->src);
    $url_parts = explode('/', $url->src);
    //drupal_set_message('found '. $url_parts[2] .':'. intval($url_parts[2]));
    return intval($url_parts[2]);
  }

  $query = "SELECT src FROM {url_alias} WHERE dst = 'category/tags/%s'";
  $result = db_query($query, $term);
  if ($url = db_fetch_object($result)) {
    $url_parts = explode('/', $url->src);
    return intval($url_parts[2]);
  }
  $terms = taxonomy_get_term_by_name($term);
  if (count($terms)) {
    return $terms[0]->tid;
  }

  return -1;
}

function _omniture_get_page_name($node = NULL) {
  $name = array(variable_get("omniture_pagename", 'pagename'));
  if (function_exists('pathauto_cleanstring')) {
    if (isset($node)) {
      // Get primary category, if any...
      $tax_name = _omniture_get_primary_category($node);
      if ($tax_name != '') {
        $name[] = pathauto_cleanstring($tax_name);
      }
      $name[] = $node->type; // assumed clean
      $name[] = pathauto_cleanstring($node->title);
    }
    elseif (drupal_is_front()) {
      $name[] = 'front-page';
    }
    else {
      $tid = _omniture_get_taxonomy_from_path();
      if ($tid > 0) {
        $term = taxonomy_get_term($tid);
        $name[] = pathauto_cleanstring($term->name);
      }
      $title = _omniture_get_page_title();
      if ($title != '' && $name[count($name) -1] != $title) {
        $name[] = $title;
      }
    }
  }

  if ($_GET['page']) {
    $name[] = 'page-'. (intval($_GET['page']) + 1);
  }

  return implode('|', $name);
}

function _omniture_get_page_title() {
  $title = drupal_get_title();
  if (function_exists('pathauto_cleanstring')) {
    $title = pathauto_cleanstring($title);
  }
  return $title;
}

function _omniture_format_name($name) {
  $formatted_name = check_plain($name);
  // make lowercase, use tabs
  return $formatted_name;
}

function _omniture_node_get_lightest_term($nid) {
  // We only want the first row of the result--this is the lightest term of the
  // lightest vocab.  This query should be the same as the query found in
  // taxonomy_node_get_terms.
  $result = db_query(db_rewrite_sql('SELECT t.* FROM {term_node} r INNER JOIN {term_data} t ON r.tid = t.tid INNER JOIN {vocabulary} v ON t.vid = v.vid WHERE r.nid = %d ORDER BY v.weight, t.weight, t.name', 't', 'tid'), $nid);
  $term = db_fetch_object($result);  // extract first row of query
  return $term;
}

function omniture_match_page($page_regex) {
  static $path_alias;

  if (!isset($path_alias)) {
    $path_alias = drupal_get_path_alias($_GET['q']);
  }

  $page_match = preg_match($page_regex, $path_alias);
  if ($path_alias != $_GET['q']) {
    $page_match = $page_match || preg_match($page_regex, $_GET['q']);
  }

  if ($page_match) {
    return true;
  }
  return false;
}
function _ominiture_check_enews_in_contest($sid, $nid) {
  if (module_exists('webform')) {
    $sql = "SELECT a.data data
            FROM {webform_submitted_data} a, {webform_component} b
            WHERE a.cid=b.cid and a.nid=b.nid and b.form_key in ('ams_main_enewsletter','ams_sponsors_enewsletter')
              AND a.sid=%d and a.nid=%d";
    $result = db_query($sql, $sid, $nid);
    $enew_signup = FALSE;
    while ($content = db_fetch_object($result)) {
      if ($content->data == '1' || substr(strtolower($content->data),0,1) == 'y') {
        $enew_signup = TRUE;
      }
    }
    if ($enew_signup) {
      return TRUE;
    }
  }
  return FALSE;
}
