function laserfist_omniture_select (reset) {
	value =  $('#edit-easy-easy-option').val();
	append_to =  $('#edit-condition-path').val();
	// Seperator for textbox

	line_row = append_to.split("\n");
	
	count = 0;
	temp_value = '';
	
	if (jQuery.inArray( value, line_row) > 0) {
		alert('This value already exist and will be remove');
	}
	else{
		line_row.push(value);
		for (i = 0; i < line_row.length; i++) {
			if (line_row[i] != '') {
				// checks if already exists in array
				seperator = (count > 0) ? "\n" : '';
				temp_value +=  seperator + line_row[i];
				
				count++;
				
			}
		}
	
		
		$('#edit-condition-path').val(temp_value);
	}
	if (reset == 1) {
		// use this only for formfields
		$('#edit-easy-easy-option').val('');
	}
}