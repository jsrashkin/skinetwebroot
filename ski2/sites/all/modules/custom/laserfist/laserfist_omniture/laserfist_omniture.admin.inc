<?php

/**
 * Admin form for configuring the Omniture settings
 */
function omniture_admin_settings() {
  $vocabs = _omniture_get_vocabularies();
  $form = array();

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => TRUE,
    '#description' => t('General settings for Omniture module')
  );
  $form['general']["omniture_account"] = array(
    '#type' => 'textfield',
    '#title' => t("Affiliate account name"),
    '#default_value' => check_plain(variable_get("omniture_account", '')),
    '#description' => t("Affiliate account name - s_account"),
  );
  $form['general']["omniture_dynamic_account_list"] = array(
    '#type' => 'textfield',
    '#title' => t("Dynamic account list"),
    '#default_value' => check_plain(variable_get("omniture_dynamic_account_list", '')),
    '#description' => t("String of urls mapped to account names (Looks something like this: bonnsite=www.site.com;bonnasdf=www.asdf.com)."),
    '#maxlength' => 500,
  );
  $form['general']["omniture_link_track_vars"] = array(
    '#type' => 'textfield',
    '#title' => t("Link Track Vars"),
    '#default_value' => check_plain(variable_get("omniture_link_track_vars", '')),
    '#description' => t('String of links to track (Looks something like this: events,dc).'),
  );
  $form['general']["omniture_link_track_events"] = array(
    '#type' => 'textfield',
    '#title' => t("Link Track Events"),
    '#default_value' => check_plain(variable_get("omniture_link_track_events", '')),
    '#description' => t('String of links  mapped to events (Looks something like this: None).'),
  );
  $form['general']["omniture_form_list"] = array(
    '#type' => 'textfield',
    '#title' => t("Form List"),
    '#default_value' => check_plain(variable_get("omniture_form_list", '')),
    '#description' => t('String of links forms to track (Looks something like this: registerUserLoginForm).'),
  );
  $form['general']["omniture_var_used"] = array(
    '#type' => 'textfield',
    '#title' => t("Var Used"),
    '#default_value' => check_plain(variable_get("omniture_var_used", '')),
    '#description' => t('String of omniture prop to map forms to (Looks something like this: eVar27).'),
  );
  $form['general']["omniture_event_list"] = array(
    '#type' => 'textfield',
    '#title' => t("Event List"),
    '#default_value' => check_plain(variable_get("omniture_event_list", '')),
    '#description' => t('String of events to be mapped (Looks something like this: event11,event12,event13).'),
  );
  $form['general']["omniture_pagename"] = array(
    '#type' => 'textfield',
    '#title' => t("Page name"),
    '#default_value' => check_plain(variable_get("omniture_pagename", '')),
    '#description' => t("Short name of the site, used in the omniture tagging."),
  );
  $form['general']['omniture_tracking_server'] = array(
    '#type' => 'textfield',
    '#title' => t("Tracking Server"),
    '#default_value' => check_plain(variable_get('omniture_tracking_server', '')),
    '#description' => t('The address that replaces the default tracking server.
      Rendered as s.trackingServer'),
  );
  $form['general']['omniture_visitor_migration_key'] = array(
    '#type' => 'textfield',
    '#title' => t("Visitor Migration Key"),
    '#default_value' => check_plain(variable_get('omniture_visitor_migration_key', '')),
    '#description' => t('Rendered as s.visitorMigrationKey'),
  );
  $form['general']['omniture_visitor_migration_server'] = array(
    '#type' => 'textfield',
    '#title' => t("Visitor Migration Server"),
    '#default_value' => check_plain(variable_get('omniture_visitor_migration_server', '')),
    '#description' => t('Rendered as s.visitorMigrationServer'),
  );
  $form['general']['omniture_link_internal_filters'] = array(
    '#type' => 'textfield',
    '#title' => t('Link Internal Filters'),
    '#default_value' => variable_get('omniture_link_internal_filters', ''),
    '#description' => t('Tells omniture that these sites are the sites for collecting traffic data on and are not referrers.'),
  );
  $form['general']["omniture_ignore"] = array(
    '#type' => 'textarea',
    '#rows' => 5,
    '#title' => t("Pages to ignore"),
    '#default_value' => check_plain(variable_get("omniture_ignore", '')),
    '#description' => t("Page paths, starting with the initial forward-slash, of pages you do not wish to track. Please put each path on a separate line with no tabs, spaces or commas.<br />Additionally, ending the string with a * will allow for partial matches, e.g. '/gear*' will match all URLs that start with '/gear', or '/gear/bcs/*' will match all sub-pages off the BCs landing page but include the main BCs page itself."),
  );
  if (db_table_exists('omniture_type')) {
    $form['general']["omniture_override"] = array(
      '#type' => 'checkbox',
      '#title' => t("Override Custom Site Omniture"),
      '#default_value' => variable_get("laserfist_omniture_override", 1),
      '#description' => t("If checked, values set in database will override any variables returned by hook_omniture(). Note: This is required to be checked if you plan on setting per-node Omniture values in UI/database."),
    );
  }
  $form['general']["omniture_primary_vocabulary"] = array(
    '#type' => 'select',
    '#title' => t("Primary Vocabulary"),
    '#default_value' => variable_get("omniture_primary_vocabulary", '1'),
    '#options' => $vocabs,
    '#description' => t("The primary vocabulary you are using to categorize your content. Usually this is the 'Channel'."),
  );
  $form['general']["omniture_secondary_vocabulary"] = array(
    '#type' => 'select',
    '#title' => t("Secondary Vocabulary"),
    '#default_value' => variable_get("omniture_secondary_vocabulary", '1'),
    '#options' => $vocabs,
    '#description' => t("The secondary vocabulary you are using to categorize your content."),
  );
  $form['general']['laserfist_omniture_search_url'] = array(
    '#type' => 'textfield',
    '#title' => t('search url'),
    '#description' => t('Apachesolr search path following the initial
    slash. E.g. If the url is  http://www.example.com/search/hello-world, it would be search.  
      If there are multiple search pages, use comma delimiter'),
    '#default_value' => variable_get('laserfist_omniture_search_url', 'search,find')
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 10,
  );
  return $form;
}

/**
 * Compile an array of vocabularies suitable for use in a select form field
 */
function _omniture_get_vocabularies() {
  $vocabs = array();
  foreach (taxonomy_get_vocabularies() as $vid => $vocab) {
    $vocabs[$vid] = $vocab->name;
  }
  return $vocabs;
}

/**
 * Implementation of hook_validate().
 *
 * @param array $form
 *   Contains the id's for each form item.
 * @param array $form_state
 *   Contains the values of each form item.
 * @param array $form
 *   Contains all items in the form.
 */
function omniture_admin_settings_validate($form, &$form_state) {
  if (!true) {
    form_set_error('message_key', t('Error message.'));
  }
}

/**
 * Implementation of hook_submit().
 *
 * @param array $form
 *   Contains the id's for each form item.
 * @param array $form_state
 *   Contains the values of each form item.
 * @return string
 *   The URL to go to once the form has been submitted.
 */
function omniture_admin_settings_submit($form, &$form_state) {
  variable_set('omniture_account', $form_state['values']['omniture_account']);
  variable_set('omniture_dynamic_account_list', $form_state['values']['omniture_dynamic_account_list']);
  variable_set('omniture_link_track_vars', $form_state['values']['omniture_link_track_vars']);
  variable_set('omniture_link_track_events', $form_state['values']['omniture_link_track_events']);
  variable_set('omniture_form_list', $form_state['values']['omniture_form_list']);
  variable_set('omniture_var_used', $form_state['values']['omniture_var_used']);
  variable_set('omniture_event_list', $form_state['values']['omniture_event_list']);
  variable_set('omniture_pagename', $form_state['values']['omniture_pagename']);
  variable_set('omniture_tracking_server', $form_state['values']['omniture_tracking_server']);
  variable_set('omniture_visitor_migration_key', $form_state['values']['omniture_visitor_migration_key']);
  variable_set('omniture_visitor_migration_server', $form_state['values']['omniture_visitor_migration_server']);
  variable_set('omniture_primary_vocabulary', $form_state['values']['omniture_primary_vocabulary']);
  variable_set('omniture_secondary_vocabulary', $form_state['values']['omniture_secondary_vocabulary']);
  variable_set('omniture_link_internal_filters', $form_state['values']['omniture_link_internal_filters']);
  variable_set('omniture_ignore', $form_state['values']['omniture_ignore']);
  if (db_table_exists('omniture_type')) {
    variable_set('laserfist_omniture_override', $form_state['values']['omniture_override']);
  }
  drupal_set_message(t('Your settings have been successfully saved.'), 'status');
}
/**
 * Define the form for entering omniture values.
 */
function omniture_admin_custom_add_form() {
  drupal_add_js(drupal_get_path('module', 'laserfist_omniture'). '/js/laserfist_omniture.js');
  $form['#tree'] = TRUE;
  $form['condition'] = array(
    '#title' => t('Type'),
    '#type' => 'fieldset',
  );
  $form['condition']['name'] = array(
    '#title' => t('Name'),
    '#description' => 'Name of omniture settings',
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 100,
    '#required' => TRUE,
  );
  $condition_array = array(
    'absolute' => t('Absolute path'),
    'aliases' => t('Url path. Can include * as wild card example path/*, path/*/path2'), 
    'nodetype' => t('Node type'),
    'nodeid' => t('Node ID'),
  );
  $form['condition']['type'] = array(
    '#title' => t('Content type'),
    '#description' => t('Please check one.  '),
    '#type' => 'radios',
    '#required' => TRUE,
    '#options' => $condition_array,
    '#default_value' => 'absolute',
    '#ahah' => array(
      'event' => 'change',
      'method' => 'replace',
      'path' => 'admin/content/omniture/custom/js',
      'wrapper' => 'update-omniture-list',
    ),
  );
  
  $form['condition']['weight'] = array(
    '#title' => t('Weight'),
    '#description' => t('The order of when this omniture gets run from -1000 to 1000'),
    '#type' => 'weight',
    '#delta' => 200,
  );
  $form['condition']['ajax_wrapper'] = array(
    '#type' => 'item',
    '#value' => '',
    '#prefix' => '<div id="update-omniture-list">',
    '#suffix' => '</div>',
  );
  $form['condition']['path'] = array(
    '#title' => t('Values of path, aliases or node type'),
    '#description' => t('Values of content type.  If multiple values, uses linebreak'),
    '#type' => 'textarea',
    '#rows' => 5,
    '#cols' => 60,
    '#required' => TRUE,
  );
  // Create a hidden form element so that autocomplete gets created onto the form instead of after
  $form['easy']['option'] = array(
    '#prefix' => '<div class="poll-form" style="display:none;">',
    '#suffix' => '</div>',
    '#title' => t('Node ID Listing'),
    '#type' => 'textfield',
    '#autocomplete_path' => 'admin/content/omniture/custom/autocomplete_nodeid',
  );
  $form['system'] = array(
    '#title' => t('Omniture Value'),
    '#type' => 'fieldset',
  );
  // Load all property type into form
  $data = _laserfist_omniture_prop_type_load_all();
  if ($data) {
    foreach ($data as  $key => $value) {
      if ($data[$key]['enabled']) {
        $form['system'][$key] = array(
          '#title' => t($data[$key]['name']),
          '#description' => t($data[$key]['description']),
          '#type' => 'textfield',
          '#maxlength' => 100,
        );
      }
    }
  }
  // Add replacement patterm tip
  $form['pattern'] = array(
    '#title' => t('Replacement Pattern'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE, 
  );
  
  $display = '';
  $pattern_replace = _laserfist_omniture_prop_pattern();
  foreach($pattern_replace as $key => $value) {
    if ($pattern_replace[$key]['display']) {
      $display .= '<strong>' . $key . '</strong> = ' . $pattern_replace[$key]['description'] .'<br />';
    }
  } 
  $form['pattern']['tips'] = array(
    '#value' => '<p>' . $display . '</p>',
  );
  $form['#redirect'] = 'admin/content/omniture/custom';
  $form['submit'][] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  
  return $form; 
}
function _omniture_by_nodetype_list() {
  static $lists = array();
  if (!$lists) {
    $results = db_query("SELECT type, name FROM {node_type} WHERE type NOT IN ('pcd', 'menu_item') ORDER BY name");
    while ($row = db_fetch_array($results)){
      $lists[$row['type']] = $row['name'];
    }  
  }
  return $lists;
}
/**
 * Returns autocomplete nodeid from the title being typed in.
 */
function omniture_admin_custom_autocomplete_nodeid($data = '') {
  $output = array('status' => FALSE);

  // Must have at least 3 characters to populate the nodeid
 
  if (strlen(trim($data)) > 2) {
    $sql = "SELECT nid, title FROM {node} where title LIKE '%%%s%%' ORDER BY title";
    $results = db_query_range(db_rewrite_sql($sql), $data, 0, 10);
   
    while($obj = db_fetch_object($results)) {
      $items[$obj->nid] = check_plain(strip_tags($obj->title));
    }
    if ($items) {
      $output = $items;
    }
  }
  print drupal_json($output);
  exit;
 
}
/**
 * Returns autocomplete absolute path for people who might spell the path wrong!.
 */
function omniture_admin_custom_autocomplete_absolute($data = '') {
  $output = array('status' => FALSE);

  // Must have at least 3 characters to populate the nodeid
 
  if (strlen(trim($data)) > 2) {
    $sql = "SELECT dst FROM {url_alias} where dst LIKE '%s%%' ORDER BY dst";
    $results = db_query_range(db_rewrite_sql($sql), $data, 0, 10);
   
    while($obj = db_fetch_object($results)) {
      $items[$obj->dst] = check_plain(strip_tags($obj->dst));
    }
    if ($items) {
      $output = $items;
    }
  }
  print drupal_json($output);
  exit;
 
}
/**
 * Ajax callback for the conditional radio boxes.
 * This will make it easier for users not have to guess what node type or nodeid it is. It will have a drop down list they can just add the information.
 */
function omniture_admin_custom_js() {

  $data = $_POST['condition']['type'];  
  switch($data) {
    case 'absolute':
      $type = 'textfield';
      $form['easy']['option'] = array(
         '#title' => t('URL path without initial slash'),
         '#type' => 'textfield',
          '#size' => 110,
         '#autocomplete_path' => 'admin/content/omniture/custom/autocomplete_absolute',
        );
      break;
    case 'nodeid':
      $type = 'textfield';
      $form['easy']['option'] = array(
        '#title' => t('Node Title to retrieve the node ID'),
        '#type' => 'textfield',
         '#size' => 110,
        '#autocomplete_path' => 'admin/content/omniture/custom/autocomplete_nodeid',
      );
      break;
      
    case 'nodetype':
      $type = 'dropdown';
      $select = _omniture_by_nodetype_list();
      
      $form['easy']['option'] = array(
        '#title' => t('Node Type Listing'),
        '#type' => 'select',
        '#options' => $select,
      );
      break;
      
    case 'aliases':
      break;
      
    
  }
  if (isset($form['easy']['option'])){
    // reset input values after the add button
    $reset = ($type == 'dropdown') ? 0 : 1;
    $form['easy']['button'] = array(
      '#type' => 'button',
      '#value' => t('Add'),
      '#attributes' => array('onclick' => "laserfist_omniture_select($reset);return false;"),
    );
    $output = laserfist_omniture_ahah_render($form, 'easy');
  }
  else{
    $output = '';
  }

  return drupal_json(array('status' => TRUE, 'data' => $output));
}
/**
 * Grab from http://www.nicklewis.org/node/967
 * Just rerenders the dynamic form 
 */
function laserfist_omniture_ahah_render($fields, $name) {
  $form_state = array('submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  // Add the new element to the stored form. Without adding the element to the
  // form, Drupal is not aware of this new elements existence and will not
  // process it. We retreive the cached form, add the element, and resave.
  $form = form_get_cache($form_build_id, $form_state);
  $form[$name] = $fields;
  form_set_cache($form_build_id, $form, $form_state);
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
  );
  // Rebuild the form.
  $form = form_builder($_POST['form_id'], $form, $form_state);

  // Render the new output.
  $new_form = $form[$name];
  return drupal_render($new_form); 
}
/**
 * Handle validation before submission of adding new omniture values
 */
function omniture_admin_custom_add_form_validate(&$form, &$form_state) {
  // Check conditions path see if anything exist
  $line_break = array("\r\n");
  
  // TODO Remove trailing spaces for each line before checking pattern
  
  // Check name field
  
  if (trim(strip_tags($form_state['values']['condition']['name'])) == '') {
    // strip tags out and reset values of name
    $form['condition']['name']['#value'] = trim(strip_tags($$form['condition']['name']['#value']));
    form_set_error('name', 'Your omniture name cannot be empty');
  }
  
  // Accepted characters
  $accepted_pattern = '/^[A-Za-z0-9-_\?\=\/\*\r\n]*$/';
  if (!preg_match($accepted_pattern, $form_state['values']['condition']['path'])) {
    form_set_error('path', 'Your values contains an invalid characters');
  }
  else {
    // grab each line breaks and return the path into an array
    $path = str_replace($line_break, '|', $form_state['values']['condition']['path']);
    $path = explode('|', $path);
    
    // Check condition type arguments to see if the path and the condition type works together.
    switch($form_state['values']['condition']['type']) {
      case 'nodeid':
        // Node id must have only interger in path
        for ($i = 0; $i < count($path); $i++) {
          if (!is_numeric($path[$i])) {
            form_set_error('path', 'Node ID must contain only numeric values');
            break;
          }
        }
        // validation for node id
        break;
      case 'absolute':
        // Abosolute path can't contain asterisk
        $pattern = '/^[A-Za-z0-9-_\/]*$/';
        for ($i = 0; $i < count($path); $i++) {
          if (!preg_match($pattern, $path[$i]) || $path[$i][0] == '/') {
            form_set_error('path', 'One of the path cannot contain wildcards (*)');
            break;
          }
        }
        break;
      case 'aliases':
        // Accepts all values so it will not need to parse path
        // Maybe requires asterisk?
        $pattern = '/^[A-Za-z0-9-_\?\=\/\*]*$/';
        for ($i = 0; $i < count($path); $i++) {
          if (!preg_match($pattern, $path[$i]) || substr($path[$i], 0, 1) ==  '/') {
            form_set_error('path', 'Must have an asterisk (*)');
            break;
          }
        }
        break;
      case 'nodetype':
        // must contain only the node type.  Will check the db if node type exist or not.
        $pattern = '/^[A-Za-z0-9-_]*$/';
        $node_types = node_get_types('names');
        $node_types = array_map("strtolower", $node_types);

        for ($i = 0; $i < count($path); $i++) {
          if (!preg_match($pattern, $path[$i])) {
            form_set_error('path', 'One of the path cannot contain wildcards (*)');
            break;
          }
          else if(!array_key_exists($path[$i], $node_types)) {
            form_set_error('path', 'One of the node type does not exist');
            break;
          }
        }
        break; 
    }
  }
}
/**
 * Handle submission for omniture_admin_custom_form
 */
function omniture_admin_custom_add_form_submit($form, &$form_state) {
  
  // Add conditions data to the database table {omniture}
  $insert = new stdClass();
  $insert->name = trim(strip_tags($form_state['values']['condition']['name']));
  $insert->path = $form_state['values']['condition']['path'];
  $insert->type = $form_state['values']['condition']['type'];
  $insert->weight = $form_state['values']['condition']['weight'];
  $insert->date_modified = time();
  drupal_write_record('omniture', $insert);
  // Get last inserted id
  $last_id = $insert->omid;

  _omniture_add_property($last_id, $form_state);
  drupal_set_message('Form has been successfully added', 'status');
  
  $form['#redirect'] = 'admin/content/omniture/custom';
}
/**
 * Insert property values into {omniture_prop} 
 */
function _omniture_add_property($omid, $form_state) {
  // Load all {omniture_type} into arrays keys with id values
  $sql = 'SELECT spid, name FROM {omniture_type}';
  $results = db_query($sql);
  while ($row = db_fetch_array($results)) {
    $sprop = explode(".", $row['name']);
    $sprop = end($sprop);
    $data[strtolower($sprop)] = $row['spid'];
  }
 
  if ($data) {
    // Add property value data to the database table {omniture_prop}
    foreach ($form_state['values']['system'] as $key => $value) {
      $key_lowercase = strtolower($key);
      // Checks if the key exists in the database
      if (trim($form_state['values']['system'][$key]) != '' && array_key_exists($key_lowercase, $data)) {
        // Check if the data already exist.  If so just do an update instead of an insert
        $sql = "SELECT id FROM {omniture_prop} WHERE omid = %d AND spid = %d";
        $result = db_query($sql, $omid, $data[$key_lowercase]);
        $row = db_fetch_array($result);
        if ($row) {
          $sql = "UPDATE {omniture_prop} SET value = '%s' WHERE id = %d";
          db_query($sql, $value, $row['id']);
        }
        else {
          $sql = "INSERT INTO {omniture_prop} (omid, spid, value) VALUES (%d, %d, '%s')";
          db_query($sql, $omid, $data[$key_lowercase],$value);
        }
      }
      else if (trim($form_state['values']['system'][$key]) == ''){
        // If values are empty values it will delete the values from {omniture_prop}
        $sql = "DELETE FROM {omniture_prop} WHERE omid = %d AND spid = %d";
        db_query($sql, omid, $data[$key_lowercase]);
      }
    }
  } 
}
/**
 * Callback form setting
 */
function omniture_admin_default_form() {
  $form['omniture'] = array(
    '#title' => 'Field Set Title for custom pagination',
    '#description' => t('Field Set Desciption'),
    '#type' => 'fieldset',
  );
  return $form; 
}
/**
 * Callback form for editing omniture values  
 */
function omniture_admin_custom_edit($form_state, $omid = 0) {
  
  if (!is_numeric($omid)) {
    if ($omid == 'omniture') {
      // editing default values of omniture
      $omid = LASERFIST_OMNITURE_DEFAULT_OMID;
    } 
    elseif ($omid == 'error') {
      // editing values for 404 pages
      $omid = LASERFIST_OMNITURE_ERROR_OMID;
    }
    elseif ($omid == 'homepage') {
      // editing values for 404 pages
      $omid = LASERFIST_OMNITURE_HOMEPAGE_OMID;
    }
  }
  
  $data = laserfist_omniture_prop_load($omid);
  if ($data) {
    $form = drupal_retrieve_form('omniture_admin_custom_add_form', $form_state);
    
    // Load form id into form
    $form['condition']['omid'] = array(
      '#type' => 'hidden',
      '#value' => $data['condition']->omid
    ); 
    // Load values from {omniture} table into form
    $form['condition']['name']['#default_value'] = $data['condition']->name;
    $form['condition']['type']['#default_value'] = $data['condition']->type;
    $form['condition']['path']['#default_value'] = $data['condition']->path;
    $form['condition']['weight']['#default_value'] = $data['condition']->weight;
    $form['submit'][0]['#value'] = 'Update';

    
    // Load values from {omniture_prop} table into form
    foreach ($form['system'] as $key => $value) {
      if (!isset($form['system'][$key]['#default_value'])){
        // display values in form property.  Without this if statement, it will
        // try to get the #type and #title which will cause the form not to render correctly.
        $form['system'][$key]['#default_value'] = $data['system']->$key;
      }
    }
    
    if ($omid == LASERFIST_OMNITURE_DEFAULT_OMID || $omid == LASERFIST_OMNITURE_ERROR_OMID || $omid == LASERFIST_OMNITURE_HOMEPAGE_OMID) {
      // Hide values if error type or default type omniture
      $form['condition']['type']['#options']['error'] = 'Error page';
      $form['condition']['type']['#options']['default'] = 'Default page';
      $form['condition']['type']['#options']['homepage'] = 'Homepage';
      // Hide these form from error 404 omniture admin page
      $form['condition']['type']['#type'] = 'hidden';
      $form['condition']['path']['#type'] = 'hidden';
      $form['condition']['weight']['#type'] = 'hidden';
    } 
    else {
      $form['#redirect'] = 'admin/content/omniture/custom';
    }
    $form['#validate'] = array('omniture_admin_custom_add_form_validate');
    return $form;
  }
  
  drupal_set_message('The Omniture ID does not exists.',  'error');
}
/**
 * Implementation of hook_submit(). 
 */
function omniture_admin_custom_edit_submit($form, &$form_state) {
  $omid = $form_state['values']['condition']['omid'];
  
  if (is_numeric($omid) && $omid > 0) {
    // Update the omniture condition tie to {omniture} table
    $sql = "UPDATE {omniture} SET  name='%s', path='%s', type='%s',date_modified=%d ,weight=%d WHERE omid = %d";
    db_query($sql,
      trim(strip_tags($form_state['values']['condition']['name'])),
      $form_state['values']['condition']['path'],
      $form_state['values']['condition']['type'],
      time(),
      $form_state['values']['condition']['weight'],
      $omid
    );
    
    // Remove all property for that omniture id
    $sql = "DELETE FROM {omniture_prop} WHERE omid = %d";
    db_query($sql, $omid);
    
    // Re-adds the property for that omniture id
    _omniture_add_property($omid, $form_state);
    
    drupal_set_message('Form has been successfully updated', 'status');
  }
  else {
    drupal_set_message('Form has failed to update.  Omniture ID does not exist', 'error');
  }
}
/**
 * Callback form for sorting and listing omniture weight
 */
function omniture_admin_custom_list($form_state, $type = '') {
  $form['#tree'] = TRUE;
  
  $exclude = "'error', 'default','homepage'";
  switch($type) {
    case 'absolute':
    case 'nodeid':
    case 'nodetype':
    case 'aliases':
      $sql = "SELECT omid, path, name, type, weight, date_modified FROM {omniture} WHERE type NOT IN ($exclude) AND type = '%s' ORDER BY weight";
      $results = db_query($sql, $type);
      break;
    default:
      $type = '';
      $sql = "SELECT omid, path, name, type, weight, date_modified FROM {omniture} WHERE type NOT IN ($exclude) ORDER BY type, name";
      $results = db_query($sql);
      break;
  }
  
  $form['filter'] = array(
    '#type' => 'select',
    '#options' => array(
      '' => t('Filter By'),
      'absolute' => t('Absolute'),
      'nodeid' => t('Node ID'),
      'nodetype' => t('Node Type'),
      'aliases' => t('Aliases'), 
    ),
    '#attributes' => array(
      'onchange' => "window.location.href='/admin/content/omniture/custom/list/'+this.value;" 
    ),
  );
  
  $count = 0;
  while ($data = db_fetch_object($results)) {
    $omid = $data->omid;
    $form['omid'][$omid] = array(
      '#type' => 'value',
      '#value' => $data->omid
    );
    $form['name'][$omid] = array('#value' => $data->name);
    $form['type'][$omid] = array('#value' => $data->type);
    $form['path'][$omid] = array('#value' => $data->path);
    $form['date_modified'][$omid] = array('#value' => ($data->date_modified) ? date('M d, Y h:i a ' ,$data->date_modified) : '');
    if ($type == 'aliases') {
      $form['weight'][$omid] = array(
        '#type' => 'weight',
        '#delta' => 200,
        '#default_value' => $data->weight,
      );
    }
    $count++;
  }
  // pass the type into the list to determine if we allow users to change the weight for aliases
  if ($type == 'aliases' && $count > 1 ) {
    $form['condition']['type'] = array('#type' => 'hidden','#hidden' => $type);
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save')
    );
  }
  return $form;
}
/**
 * Form submit for omniture_admin_custom_list
 */
function omniture_admin_custom_list_submit($form, &$form_state) {
  // save the weight by draggable items
  $row = new stdClass();
  foreach ($form_state['values']['omid'] as $omid => $value) {
    $row->omid = $omid;
    $row->weight = $form_state['values']['weight'][$omid];
    
    drupal_write_record('omniture', $row, 'omid');
    $done = TRUE;
  }

  if (isset($done)) {
    drupal_set_message(t('The configuration options have been saved.'));
  }
  
}
/**
 * Confirmation page for deleting an omniture page and value of that omniture page.
 * @param $form_state
 * @param $omid omniture id
 */
function omniture_admin_custom_list_confirm_delete(&$form_state, $omid) {
  $data = _laserfist_omniture_load($omid);
  if ($data) {
    $form['omid'] = array(
      '#type' => 'value',
      '#value' => $data->omid,
    );
  
    return confirm_form($form,
      t('Are you sure you want to delete the omniture %name?', array('%name' => $data->name)),
      'admin/content/omniture/custom/list',
      NULL,
      t('Delete')
    );
  }
  drupal_set_message('The omniture ID does not exist.', 'error');
}
/**
 * Delete omniture value and all page tie to values tie to that omniture id 
 */
function omniture_admin_custom_list_confirm_delete_submit($form, &$form_state) {
  $omid = $form_state['values']['omid'];
  // Delete all values tie to that omid in {omniture_prop}
  // NOTE: This will reset the page it tie to as the default omniture values set from the original code
  db_query("DELETE FROM {omniture_prop} WHERE omid = %d", $omid);
  
  // Delete the info {omniture}
  db_query("DELETE FROM {omniture} WHERE omid = %d LIMIT 1", $omid);
  
  if (db_affected_rows()) {
    // All gone now!!!
    drupal_set_message(t('The configuration options have been saved.'), 'status');
  }

  $form_state['redirect'] = 'admin/content/omniture/custom/list';
  
}
/**
 * Theme omniture form for listing all available omniture
 */
function theme_omniture_admin_custom_list($form) {
  $has_pages = isset($form['name']) && is_array($form['name']);
  $rows = array();

  if ($has_pages) {
    foreach (element_children($form['name']) as $key) {
      $row = array();
      $row[] = drupal_render($form['name'][$key]);
      $form['weight'][$key]['#attributes']['class'] = 'page-weight';
      $row[] = drupal_render($form['weight'][$key]);
      
      $row[] = nl2br(drupal_render($form['path'][$key]));
      $row[] = drupal_render($form['type'][$key]);
      $row[] = drupal_render($form['date_modified'][$key]);
      $row[] = l(t('edit'), "admin/content/omniture/custom/$key/edit");
      $row[] = l(t('delete'), "admin/content/omniture/custom/$key/delete");

      $rows[] = array(
        'data' => $row,
        'class' => 'draggable',
      );
    }
  }
  if (empty($rows)) {
    $rows[] = array(
      array(
        'data' => t('There are currently no omniture defined.'),
        'colspan' => '6'
      )
    );
  }

  $header[] = t('Name');
  $header[] = t('Weight');

  $header[] = t('Values');
  $header[] = t('Type');
  $header[] = t('Date Modified');
  $header[] = array(
    'data' => t('Operations'),
    'colspan' => '2',
  );
  if (isset($form['condition']['type'])) {
    drupal_add_tabledrag('omniture-table', 'order', 'sibling', 'page-weight');
  }
  return theme('table', $header, $rows, array('id' => 'omniture-table')) . drupal_render($form);

}
/**
 * Callback for Manage Fields
 * Allows the ability to add new fields and enable and disabled fields.
 */
function omniture_admin_field_edit($form_state) {
  $form['#tree'] = TRUE;
  $results = _laserfist_omniture_prop_type_load_all();

  // Building out the form list
  if ($results) {
    foreach ($results as $key => $value) {
      $spid = $results[$key]['spid'];
      $form['spid'][$spid] = array(
        '#type' => 'value',
        '#value' => $spid
      );
      $form['name'][$spid] = array('#value' => $results[$key]['name']);
      $form['description'][$spid] = array('#value' => $results[$key]['description']);
      $form['enabled'][$spid] = array(
        '#type' => 'checkbox',
        '#default_value' => ($results[$key]['enabled'] == 1) ? 1 : 0,
      );
    }
  }
  
  $form['add'] = array(
    '#title' => 'Add fields',
    '#description' => 'Adding new property fields.  All letters will be converted to lowercase',
    '#type' => 'textfield',
    '#maxlength' => 25,
  );
  $form['add']['#validate'] = array('omniture_admin_field_edit_validate');
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  
  return $form;
  
}
/**
 * Theme omniture fields for enabling, disabling and adding new fields
 */
function theme_omniture_admin_field_edit($form) {
  
  $has_pages = isset($form['name']) && is_array($form['name']);
  $rows = array();
  
  if ($has_pages) {
    foreach (element_children($form['name']) as $key) {
      $row = array();
      $row[] = drupal_render($form['enabled'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['description'][$key]);
      $rows[] = array(
        'data' => $row,
        'class' => 'list-omniture-fields',
      );
    }
  }
  if (empty($rows)) {
    $rows[] = array(
      array(
        'data' => t('There are currently no omniture fields available.'),
        'colspan' => '3'
      )
    );
  }
  $header[] = t('Enabled');
  $header[] = t('Name');
  $header[] = t('Description');
  return theme('table', $header, $rows, array('id' => 'omniture-fields')) . drupal_render($form);
}
/**
 * Implementation of hook_validate()
 * If string values is added it must follow suit with a valid javascript variable.
 */
function omniture_admin_field_edit_validate($form, &$form_state) {
  $values = trim($form_state['values']['add']);
  if ($values != '') {
    // check if all the characters are valid
    $accepted_pattern = '/^([A-Za-z]*)?[0-9]*$/';
    if (!preg_match($accepted_pattern, $values)) {
      form_set_error('add', t('Only alphanumeric characters are accepted. Values must not start with any numbers'));
    }
    else{
      // check to see if variables exists in the db
      $sql = "SELECT COUNT(*) AS hits FROM {omniture_type} WHERE name = '%s'";
      $svar = 's.'.$values;
      $result = db_fetch_array(db_query($sql, $realvar));
      $count = (int)$result['hits'];
      if ($count) {
        form_set_error('add', t('This value already exists.  Please use another value when adding a new field name.'));
      }
    }
  }
}
/**
 * Submission of omniture fields.
 */ 
function omniture_admin_field_edit_submit($form, &$form_state) {
  
  $row = new stdClass();
  foreach ($form_state['values']['spid'] as $spid => $value) {
    $row->spid = $spid;
    $row->enabled = $form_state['values']['enabled'][$spid];
    // Update only if the values are changed when disabled or enabled
    if ($form['enabled'][$row->spid]['#default_value'] != $row->enabled) {
      drupal_write_record('omniture_type', $row, 'spid');
      $done = TRUE;
    }
  }
  
  $added = trim($form_state['values']['add']);
  if ($added != '') {
    // add the extra values
    $description = 'Custom added fields';
    // append the s.$var into the value
    $added = strtolower('s.'.$added);
    $sql = "INSERT INTO {omniture_type} (enabled, name, description) VALUES (1, '%s', '%s')";
    db_query($sql, $added, $description);
    $done = TRUE;
  }
  if (isset($done)) {
    drupal_set_message(t('The configuration options have been saved.'), 'status');
  }
  else {
    drupal_set_message(t('No changes were being made.'), 'error');
  }
   
}
