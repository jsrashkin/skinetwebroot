
Welcome to Laserfist Omniture!

1) What this module does:
@todo

2) Configuring this module:
@todo

3) Converting to first-party cookies
Visit the global Omniture settings page at /admin/settings/omniture. Set the
following fields with what the Analytics/Omniture team gives you:
-Tracking Server
-Visitor Migration Key
-Visitor Migration Server
When these values are set, the following Javascript variables should be printed
on the page, below where the s.prop* variables are declared. Example:
  s.trackingServer = 'floridatravellife.com.122.2o7.net';
  s.visitorMigrationKey = '50B8FB26';
  s.visitorMigrationServer = 'analytics.floridatravellife.com';
NOTE: For these first-party cookies to work, the domain "analytics.[domain].com"
must be configured by Web Hosting first.

4) Available hooks
  a) hook_omniture_prop_pattern_alter()
    The variable $patterns should be passed by reference into your
    implementation of this hook. The array inside each item in this
    associative array can be altered. Generally, you'll want to edit the
    "value" key of that inner array. You can set "value" to an array, with the
    first item being the callback function and any other items will be
    parameters passed to that function. Whatever that function returns will
    be used as the token value by Omniture module. Here's an example:

    /**
     * Implements hook_omniture_prop_pattern_alter().
     */
    function dwh_wedding_omniture_prop_pattern_alter(&$pattern) {
      if ('real-weddings' != arg(0) || !is_numeric(arg(1))) {
        return;
      }

      $pattern['[title]']['value'] = array('_dwh_wedding_set_omniture_patterns', 'title');
    }

    /**
     * Set custom Omniture variables for DWH wedding pages.
     */
    function _dwh_wedding_set_omniture_patterns($type) {
      if ('title' == $type) {
        $node = node_load(arg(1));
        return check_plain($node->title);
      }
    }

