<?php
/**
 * $omniture - stdClass object
 * eg $omniture->prop2 to get s.prop2 values.
 */

// Only show intro info and div opener if no custom_path provided (e.g.
// calling it on normal page, not via ajax 
?>
<!-- SiteCatalyst code version: H.22.1.
Copyright 1996-2011 Adobe, Inc. All Rights Reserved
More info available at http://www.omniture.com -->

<?php // Wrap entire block in a div so we can target it with Javascript ?>
<div id="omniture-settings-container">
  <?php if ( !$using_custom_scode ): ?>
    <script type="text/javascript">
      var s_account = '<?php print $s_account ?>';
      var s_dynamicAccountList = '<?php print $s_dynamic_account_list ?>';
      var s_linkTrackVars = '<?php print $s_link_track_vars ?>';
      var s_linkTrackEvents = '<?php print $s_link_track_events ?>';
      var s_formList = '<?php print $s_form_list ?>';
      var s_varUsed = '<?php print $s_var_used ?>';
      var s_eventList = '<?php print $s_event_list ?>';
    </script>
  <?php endif; ?>
  <script type="text/javascript" src="<?php print $script_path; ?>"></script>
  <script type="text/javascript"><!--
	  // function to reload the tag, creating another page hit
	  function omniture_refresh() {
		  if (s !== undefined) {
			  void(s.t());
		  };
	  }

	  /* You may give each page an identifying name, server, and channel on the next lines. */
	  <?php ob_start() ;?>
    <?php print "\n";?>
    <?php if ($omniture['key']):?>
      <?php foreach ($omniture['key'] as $value): ?>
        <?php if ($value != 'prop17'): ?>
          <?php $js_ignore = 'js|'; ?>
          <?php if (substr($$value, 0, strlen($js_ignore))== $js_ignore):?>
            s.<?php print $value; ?> = <?php print strtolower(substr($$value, strlen($js_ignore))); ?>;
          <?php else : ?>
            s.<?php print $value; ?> = "<?php print strtolower($$value); ?>";
          <?php endif; ?>
        <?php endif; ?>
      <?php endforeach; ?>  
    <?php endif; ?>
    s.prop17 = window.location.href;

    <?php // Do we have a custom tracking server?
    if (!empty($s_tracking_server)): ?>
    s.trackingServer = '<?php print $s_tracking_server; ?>';
    <?php endif; ?>

    <?php // Check for migration key.
    if (!empty($visitor_migration_key)): ?>
    s.visitorMigrationKey = '<?php print $visitor_migration_key; ?>';
    <?php endif; ?>

    <?php // And check for migration server.
    if (!empty($visitor_migration_server)): ?>
    s.visitorMigrationServer = '<?php print $visitor_migration_server; ?>';
    <?php endif; ?>

    <?php // Any link filters to add onto the defaults from s_code file?
    if (!empty($s_link_internal_filters)): ?>
    s.linkInternalFilters += ',<?php print $s_link_internal_filters; ?>';
    <?php endif; ?>

    <?php
	    $om = ob_get_contents();
	    ob_end_clean();
	    print $om;
	    global $user;
	    if($user->uid != 1 && user_access('laserfist_omniture devel')) {
		    drupal_set_message('<pre>'.$om.'</pre>');
	    }
    ?>

  /************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
  var s_code=s.t();if(s_code)document.write(s_code)//--></script>
  <script type="text/javascript"><!--
  if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
  //--></script><noscript><img src="http://bonniercorp.122.2o7.net/b/ss/<?php print $s_account ?>/1/H.22.1--NS/0"
  height="1" width="1" border="0" alt="" /></noscript><!--/DO NOT REMOVE/-->
  <!-- End SiteCatalyst code version: H.22.1. -->

</div><!--omniture-settings-container-->
