<?php

/*
	Default Preprocessors
*/
function laserfist_omniture_default_values(&$vars) {
  $path = $vars['custom_path'];
  if (arg(0, $path) == 'node' && is_numeric(arg(1, $path))) {
    $node = node_load(arg(1, $path));
  }
	$vars['node'] = $node; 

	// Front Page
	laserfist_omniture_front_page($vars);

	// Set defaults for regular content types (Articles/galleries/webforms/etc)
	laserfist_omniture_content($vars);

	// If attached to channel add that in
	laserfist_omniture_channel_content($vars);

	// If attached to channel add that in
	laserfist_omniture_photo_video($vars);

	// Browse / taxonomy term
	laserfist_omniture_browse($vars);

	// site pages. one off pages like faq, terms, etc
	laserfist_omniture_site($vars);

	// Sponsored part.
	laserfist_omniture_sponsored($vars);

	// Error Pages
	laserfist_omniture_error_page($vars);

	// Search
	laserfist_omniture_search($vars);

	// Newsletters
	laserfist_omniture_enewsletter($vars);

  $vars['pageName'] = 'laserfist:'.$vars['prop11'];
	if($vars['prop9']) {
		$vars['pageName'] .= ':'.$vars['prop9'];
	}

	// Defaults
	if(!$vars['prop16']) {
		$vars['prop16'] = $vars['channel'];
	}
	if(!$vars['prop11']) {
		$vars['prop11'] = $vars['prop16'];
	}

  $vars['s_tracking_server'] = check_plain(variable_get('omniture_tracking_server', ''));
  $vars['visitor_migration_key'] = check_plain(variable_get('omniture_visitor_migration_key', ''));
  $vars['visitor_migration_server'] = check_plain(variable_get('omniture_visitor_migration_server', ''));
  $vars['s_link_internal_filters'] = check_plain(variable_get('omniture_link_internal_filters',''));
}

/*
	The Front Page
*/
function laserfist_omniture_front_page(&$vars) {
	// Home Page
  if (laserfist_omniture_match_page('/^'. preg_quote(variable_get('site_frontpage', 'node'), '/') .'$/', $vars['custom_path'])) {
    $vars['channel'] = 'home';
    $vars['prop4'] = 'homepage';
    $vars['prop16'] = $vars['channel'];
    $vars['prop9'] = '';
  }
}

/*
	Error pages (404)
*/
function laserfist_omniture_error_page(&$vars) {
	// 404
  $drupal_path = !empty($vars['custom_path']) ? url($vars['custom_path']) : url($_GET['q']);
	if($drupal_path == '/'.variable_get('site_404','404')) {
		$vars['channel'] = 'error page';
		$vars['prop16'] = 'error page';
		$vars['prop9'] = '';
		$vars['prop10'] = '';
		$vars['pageName'] = '';
		$vars['pageType'] = 'errorPage';
	}
}

/*
*/
function laserfist_omniture_content(&$vars) {
	$node = $vars['node'];	

	// do not process channel nodes
	if(!$node || $node->type == 'channel') {
		return;
	}

	// channel nodes dont bring in title or cms id
	if($node->type == 'homepage') {
	  $vars['prop9'] = '';
	} else {
	  $vars['prop9'] = $node->title;
	}
	$vars['prop10'] = $node->nid;

	if(in_array($node->type,array('article','webform'))) {
		$vars['prop4'] = 'article';
		$tags = array();
		// Attach Tags
		$terms = laserfist_gather_related_terms($node);
		foreach($terms as $term) {
			$tags[] = $term->name;
		}
		$vars['prop3'] = implode(',',$tags);
		$vars['channel'] = 'article';
	}

	if(in_array($node->type,array('gallery'))) {
		$vars['prop4'] = 'gallery';
		$vars['channel'] = 'gallery';
	}
	if(in_array($node->type,array('video'))) {
		$vars['prop4'] = 'video';
		$vars['channel'] = 'video';
	}
}

function laserfist_omniture_channel_content(&$vars) {
	$node = $vars['node'];
	if(!$node) { return; }
	$channel_term = laserfist_omniture_get_channel_term($node);

	if(!$channel_term) {
		return;
	}

	$vars['channel'] = strtolower($channel_term->name);

	if(in_array($node->type,array('channel'))) {
		$vars['prop4'] = 'articles list';
	}
}

function laserfist_omniture_get_channel_term(&$node) {
	if($node->type == 'channel' && count($node->nat)) {  
		foreach($node->nat as $term) {
			if($term->vid == 2) {
				$channel_term = $term;
			}
		}
	}
	if($node->type != 'channel' && count($node->taxonomy)) {
		foreach($node->taxonomy as $term) {
			if($term->vid == 2) {
				$channel_term = $term;
			}
		}
	}

	return $channel_term;
}

function laserfist_omniture_photo_video(&$vars) {
  if(arg(0, $vars['custom_path']) == 'photos') {
		$vars['prop4'] = 'gallery list'; 			
		$vars['channel'] = 'gallery'; 			
	}
  if(arg(0, $vars['custom_path']) == 'videos') {
		$vars['prop4'] = 'video'; 			
		$vars['channel'] = 'video'; 			
	}
}

/*
	So far browse by is a taxonomy term page
*/
function laserfist_omniture_browse(&$vars) {
  if(arg(0, $vars['custom_path']) == 'taxonomy' && arg(1, $vars['custom_path']) == 'term' && is_numeric(arg(2, $vars['custom_path']))) {
    $term = taxonomy_get_term(arg(2, $vars['custom_path']));
	}
	if(!$term) {
		return;
	}
	$vars['channel'] = 'browse';
	$vars['prop4'] = 'browse';
	$vars['prop16'] = $vars['channel'].':'.strtolower($term->name);
}

// Site pages
function laserfist_omniture_site(&$vars) {
	if(laserfist_omniture_match_page('/^page\/terms-service/')
			|| laserfist_omniture_match_page('/^page\/privacy-policy/')	
			|| laserfist_omniture_match_page('/^sitemap/')
			|| laserfist_omniture_match_page('/^page\/media-kit/')
			|| laserfist_omniture_match_page('/^page\/contact-us/')	
		) {
		$vars['channel'] = 'site';
	}

	if(laserfist_omniture_match_page('/^sitemap/')) {
		$vars['prop16'] = $vars['channel'] . ':' . 'sitemap';
	}
}

/*
	E News
*/
function laserfist_omniture_enewsletter(&$vars) {
	if(laserfist_omniture_match_page('/^enewsletter/')
			|| laserfist_omniture_match_page('/^enewsletter-thank-you/')	
		) {
		$vars['channel'] = 'enews';
	}
	if(laserfist_omniture_match_page('/^enewsletter/')) {
		$vars['prop16'] = $vars['channel'] . ':' . 'signup';
	}
	if(laserfist_omniture_match_page('/^enewsletter-thank-you/')) {
		$vars['prop16'] = $vars['channel'] . ':' . 'thank you';
	}

  if(arg(0, $vars['custom_path']) == 'newsletter') {
		$vars['channel'] = 'enews';
	}

}

function laserfist_omniture_sponsored(&$vars) {
}

function laserfist_omniture_search(&$vars) {
  if(arg(0, $vars['custom_path']) != 'find') {
		return;
	}
  if (function_exists('apachesolr_static_response_cache')) {
   $response = apachesolr_static_response_cache();
  }

	if(!empty($response)) {
		$vars['results'] = $response->response->numFound;	
	}
	$vars['channel'] = 'search results';
  $vars['prop18'] = check_plain(arg(1, $vars['custom_path']));
	$vars['prop16'] = $vars['channel'];
	$vars['prop11'] = $vars['prop16'];
	$vars['prop12'] = $vars['prop11'];
	$vars['pageName'] = $pagename .':'. $vars['prop12'] .':'. $vars['prop18'];
}
