$('.feature-slideshow .view-content ul').ready(function() {
  $('.feature-slideshow .view-content ul').jcarousel({
    animation: 0,
    auto: 10,
    initCallback: init_carousel,
    itemVisibleInCallback: {onBeforeAnimation: laserfist_item_visible},
    scroll: 1,
    visible: 1,
    wrap: 'last'
  });
});

var laserfist_js = {};
var container;
var init_carousel = function(carousel,state) {
  if (state == 'init') {
    //check to make sure we don't have js var already set with the total
   if('bonnier_carousel' in Drupal.settings) {
     var total_rows = Drupal.settings.bonnier_carousel.total_rows;
   }

    var carousel_data_size = 0;

    if(total_rows && total_rows > 0) {
      carousel_data_size = total_rows;
    } else {
      carousel_data_size = $(carousel.list.get(0)).find('li').size();
    }
    carousel.data_size = carousel_data_size;

    container = $(carousel.container.get(0));
    container.append('<div class="carousel-content"></div>');
    container.append('<div class="carousel-control"></div>');
    control = $(container.find('.carousel-control').get(0));
    carousel.control = control;

    content_block = $(container.find('.carousel-content').get(0));
    carousel.content_block = content_block;

    content_block.append('<div class="title"></div>');
    content_block.append('<div class="dek"></div>');
    
    content_block.title = $(content_block).find('.title');
    content_block.dek = $(content_block).find('.dek');

    control.append('<div class="numbers"></div>');
    control.numbers = $(control).find('.numbers');

    control.append('<div class="counter"><span class="current">1</span> of '+carousel.data_size+'</div>');
    control.counter = $(control).find('.counter');

		var classes;
    for(var i=0; i < carousel.data_size; i++) {
			classes = 'number-'+(i+1);
			if(i == 0) {
				classes += ' first';
			}
			if(i == carousel.data_size-1) {
				classes += ' last';
			}
      control.numbers.append('<a class="number '+classes+'" href="#">'+(i+1)+'</a>');
    }

    control.find('a').bind('click', function() {
      carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
      return false;
    });
    
    carousel.startAutoOrig = carousel.startAuto;
    carousel.startAuto = function() {
      if (!carousel.paused) {
	carousel.startAutoOrig();
      }
    }
    carousel.pause = function() {
      carousel.paused = true;
      carousel.stopAuto();
    };
    carousel.play = function() {
      carousel.paused = false;
      carousel.startAuto();
    };
    $('.feature-slideshow .view-content ul').mouseover(function() {
	carousel.pause();
    });
    $('.feature-slideshow .view-content ul').mouseout(function() {
	carousel.play();
    }); 
    
  }
  carousel.play(); 
};

var laserfist_item_visible = function(carousel, item, i, state, evt) {
  control = carousel.control;
  // This should probably be rewritten
  var title = $(item).find('.views-field-common-title').html() || $(item).find('.views-field-title').html();
  $(content_block.title).html(title);
  var dek = $(item).find('.views-field-common-dek').html() || $(item).find('.views-field-dek').html();
  $(content_block.dek).html(dek);
  
  control.find('a.number').each( function(index,el) {
    el = $(el);
    if(index+1 == i) {
      el.addClass('active');
    } else {
      el.removeClass('active');
    }
  });
	control.find('.counter .current').html(i);
};
// Freaking A. Make this not stupid

jQuery(document).ready(function() {
  jQuery('.gallery-carousel').each( function(i,oDiv) {
    var list = $(oDiv).find('ul'); 
    var size = $(list).children('li').size(); 
    if(size) {
      $(list).jcarousel({
      scroll: 1,
      visible: 4,
      wrap: 'both'
      });
    }


		if(size<=4) {
			$(oDiv).find('.jcarousel-next').remove(); 
			$(oDiv).find('.jcarousel-prev').remove(); 
		}

		if (!size) {
			$(oDiv).remove();
		}
  });
});


jQuery(document).ready(function() {
  jQuery('.feature-slideshow-1').each( function(i,oDiv) {
    var list = $(oDiv).find('ul'); 
    var size = $(list).children('li').size(); 
    if(size) {
      $(list).jcarousel({
      scroll: 1,
      visible: 1,
			initCallback: init_carousel,
			itemVisibleInCallback: {onBeforeAnimation: laserfist_item_visible},
      wrap: 'both'
      });
    }

		if(size<=1) {
			$(oDiv).find('.jcarousel-next').remove(); 
			$(oDiv).find('.jcarousel-prev').remove(); 
		}

		if (!size) {
			$(oDiv).remove();
		}
  });
});
