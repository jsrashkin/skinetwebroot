<?php
/**
 * Implementation of hook_default_panel_minis()
 */
function parenting_pregnancy_default_panels_mini() {
$mini = new stdClass;
$mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
$mini->api_version = 1;
$mini->name = 'header';
$mini->category = 'header';
$mini->title = 'header';
$mini->requiredcontexts = FALSE;
$mini->contexts = FALSE;
$mini->relationships = FALSE;
$display = new panels_display;
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = '';
$display->hide_title = FALSE;
$display->content = array();
$display->panels = array();
  $pane = new stdClass;
  $pane->pid = 'new-1';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'title' => '<none>',
    'body' => '<?php
      global $user;
      if ($user->uid > 0) {
        $items[] .= \'<span class="welcome">Welcome,</span> \'.l($user->name,\'user/\'.$user->uid,array(\'attributes\'=>array(\'class\'=>\'username\')));
        $items[] = l(\'LOGOUT\',\'logout\');
      } else {
        $items[] = l(\'LOG IN\',\'user\');
        $items[] = l(\'REGISTER\',\'user/register\');
      }
      $attr = array();
      $attr[\'class\'] = \'menu\';
      print theme(\'item_list\',$items,null,\'ul\',$attr);
    ?>',
    'format' => '3',
    'substitute' => NULL,
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-1'] = $pane;
  $display->panels['middle'][0] = 'new-1';
  $pane = new stdClass;
  $pane->pid = 'new-2';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'mega_menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'nodes_per_page' => '10',
    'pager_id' => '1',
    'use_pager' => 0,
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $display->content['new-2'] = $pane;
  $display->panels['middle'][1] = 'new-2';
$mini->display = $display;

  $minis['header'] = $mini;


  return $minis;
}
