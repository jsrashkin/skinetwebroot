<?php
  
$contexts[] = array(
  'namespace' => 'admin',
  'attribute' => 'admin',
  'value' => 'admin',
  'path' => array(
    'admin' => 'admin',
    'admin/*' => 'admin/*',
  ),
  'theme_regiontoggle' => array(
    '0' => 'header',
    '1' => 'weekly_menu',
    '2' => 'first_body',
  ),
);
