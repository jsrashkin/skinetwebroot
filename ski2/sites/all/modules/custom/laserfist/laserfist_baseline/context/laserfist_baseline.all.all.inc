<?php
$contexts[] = array(
  'namespace' => 'laserfist_baseline',
  'attribute' => 'all',
  'value' => 'all',
  'sitewide' => '1',
  'block' => array(
    'laserfist_right' => array(
      'module' => 'laserfist',
      'delta' => 'right',
      'weight' => 20,
      'region' => 'right',
      'status' => '0',
      'label' => 'Right Column',
      'type' => 'context_ui',
    ),
    'panels_mini_header' => array(
      'module' => 'panels_mini',
      'delta' => 'header',
      'weight' => 20,
      'region' => 'header',
      'status' => '0',
      'label' => 'Mini panel: "header"',
      'type' => 'context_ui',
    ),
    'inline_login_inline_login_form' => array(
      'module' => 'inline_login',
      'delta' => 'inline_login_form',
      'weight' => 20,
      'region' => 'first_body',
      'status' => '0',
      'label' => 'Inline Login Form',
      'type' => 'context_ui',
    ),
  ),
);
