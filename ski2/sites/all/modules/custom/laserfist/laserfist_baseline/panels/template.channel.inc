<?php

$template = new stdClass;
$template->disabled = FALSE; /* Edit this to true to make a default template disabled initially */
$template->api_version = 1;
$template->name = 'channel';
$template->template_type = 'channel';
$template->category = '';
$template->title = 'Channel';
$template->requiredcontexts = FALSE;
$template->contexts = array(
  '0' => array(
    'name' => 'panels_template_node',
    'id' => 1,
    'identifier' => 'Node being viewed',
    'keyword' => 'node',
  ),
);
$template->relationships = array(
  '0' => array(
    'context' => 'context_panels_template_node_1',
    'name' => 'term_from_taxonomy_node',
    'id' => 1,
    'identifier' => 'Term from taxonomy node',
    'keyword' => 'term',
  ),
);
$display = new panels_display;
$display->layout = 'onecol';
$display->layout_settings = array();
$display->panel_settings = array();
$display->cache = array();
$display->title = '';
$display->hide_title = FALSE;
$display->title_pane = 0;
$display->content = array();
$display->panels = array();
  $pane = new stdClass;
  $pane->pid = 'new-1';
  $pane->panel = 'middle';
  $pane->type = 'node_body';
  $pane->subtype = 'node_body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'context_panels_template_node_1',
    'override_title' => 1,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $display->content['new-1'] = $pane;
  $display->panels['middle'][0] = 'new-1';
  $pane = new stdClass;
  $pane->pid = 'new-2';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'bonnier_fresh_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '10',
    'pager_id' => '',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'override_title' => 0,
    'override_title_text' => '',
    'context' => array(
      '0' => 'relationship_term_from_taxonomy_node_1.tid',
    ),
  );
  $pane->cache = array();
  $pane->style = array();
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $display->content['new-2'] = $pane;
  $display->panels['middle'][1] = 'new-2';
$template->display = $display;
