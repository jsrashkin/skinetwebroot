<?php if ($tag_title): ?>
  <div class="tag-title">
    <?php if ($channel_title): ?>
      <span class="channel-title">
        <?php print $channel_title; ?>:
      </span>
    <?php endif; ?>
    <?php print $tag_title; ?>
  </div>
<?php endif; ?>

<?php if ($tag_description): ?>
  <div class="tag-description">
    <?php print $tag_description; ?>
  </div>
<?php endif; ?>
