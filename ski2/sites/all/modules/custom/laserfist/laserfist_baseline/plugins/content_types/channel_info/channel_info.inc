<?php
/*
	This plugs into the Channel Node and has a tabbed (Articles/Galleries/Answers) 
	listing for the channel
*/
function laserfist_baseline_channel_info_ctools_content_types() {
  return array(
    'single' => TRUE,
    'title' => t('Channel Info'),
    'description' => t('Title and Desc for channel'),
    'required context' => new ctools_context_required(t('Node being Viewed'),'node'),
    'category' => 'Laserfist Baseline',
		'hook theme' => 'laserfist_baseline_channel_info_theme',
  );
}

function laserfist_baseline_channel_info_content_type_render($subtype, $conf, $panel_args, $context) {
	
	$node = $context->data;

	$content = theme('channel_info',$node);

  $block->content = $content;
  $block->title = '';
  $block->delta = 'channel_info';
	$block->css_class = 'channel-info';
  return $block;
}

function template_preprocess_channel_info(&$vars) {
  $node = $vars['node'];
  $vars['tag_title'] = l($node->title,'node/'.$node->nid);
  $vars['tag_description'] = $node->body;

  if($node->type != 'channel') { 
    $channel_node = laserfist_get_channel_node($node);
    if($channel_node) {
      $vars['channel_title'] = l($channel_node->title,'node/'.$channel_node->nid);
    }
  }
}

function laserfist_baseline_channel_info_theme(&$theme) {
	$theme['channel_info'] = array(
		'template' => 'channel-info',
		'arguments' => array('node' => null),
		'path' => drupal_get_path('module','laserfist_baseline').'/plugins/content_types/channel_info',
	);
}

function laserfist_baseline_channel_info_content_type_edit_form(&$form, &$form_state) {
}
