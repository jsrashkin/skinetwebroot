<?php
/**
 * @file
 * Theming for the Copyright CTools content type.
 */

/**
 * Variables:
 * $year - the four-digit current year.
 * $owner - a string containing the name of the organization responsible for
 *   this site.
 */

?>
<p>Copyright &copy; <?php print $year; ?> <?php print $owner; ?>.  All rights reserved.  Reproduction in whole or in part without permission is prohibited.</p>
