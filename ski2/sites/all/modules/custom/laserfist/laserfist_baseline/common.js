// fsol namespace
var fsol = {};

fsol.enable_menus = function() {
	$('#primary-nav').superfish({
		delay:       200,
		animation:   {height:'show'},
		speed:       100,
		autoArrows:  false,
		dropShadows: false 
	});
};
element_load.on_element_load(fsol.enable_menus, 'primary-nav');

// Inline Login Handler
$(document).click( function(evt) {
  var node_add = /node\/add/;
  var source = evt.target.href;
  //Special handling for Fishing Contest
  var fishing_add = /node\/add\/fishing-contest/;
  if (source && source.search(node_add) != -1 && inline_login.loaded && source.search(fishing_add) == -1) {
    evt.preventDefault();
    inline_login.activate_form(evt,evt.target.href);   
  }
  var user_login = /user\/login/;
  if (source && source.search(user_login) != -1 && inline_login.loaded) {
    evt.preventDefault();
    inline_login.activate_form(evt);   
  }
  if (evt.target.id == 'main-login-link') {
    evt.preventDefault();
    inline_login.activate_form(evt);
  }
});

// Make Tabanchors carry the get vars
// This adds the #tabanchor to each tab link so that when the user switches tabs, they 
//   don't have to rescroll down to the tab area. There is an anchor tag named "tabanchor"
//   hardcoded in page.tpl.php that goes along with this.
fsol.enable_tabanchor = function() {
  $('#tabanchor ul.primary li a').each(function (i) {
    this.href = this.href + '?' + window.location.search.substring(1) + '#tabanchor';
  });
};
element_load.on_element_load(fsol.enable_tabanchor, 'tabanchor',true);

$(document).ready(function(){
	if (typeof(bonnier_photos_init) != 'undefined') {
		bonnier_photos_init();
	}
});

//element_load.on_element_load(fsol.bonnier_photos_init, 'mini-panel-panel_trophyroom_form',false);
//element_load.on_element_load(fsol.bonnier_photos_init, 'block-bonnier_photos-0',false);

/* Random Circular */
fsol.init_circular_carousel = function(target) {
  $(target).each( function() {
    $(this).hide();
		var start = 1;
		var children = $(this).children('li');

		var photo_offset = $.jqURL.get('photo');
		if (!photo_offset) {
			photo_offset = 0;
		}

		children.each( function(i,elem) {
			if (i == photo_offset) {
				$(elem).children('div.item-wrapper').addClass('active');
				start = i;
			}
		});

		if(start == 0) {
			start = 1;
		}
    var view_ul = document.createElement('ul');
    view_ul.className = 'view_ul';
    view_ul.data = $(this).find('li');

    for(var i=0; i < 3; i++) {
      var li = document.createElement('li');
      view_ul.appendChild(li);
    }

    this.parentNode.insertBefore(view_ul,this.nextSibling);

    $(view_ul).jcarousel({
      visible: 3,
      wrap: 'circular',
      scroll:1,
			start:start, 
      itemVisibleInCallback: {onBeforeAnimation: mycarousel_itemVisibleInCallback},
      itemVisibleOutCallback: {onAfterAnimation: mycarousel_itemVisibleOutCallback},
      initCallback:   fsol.carousel_init,
      itemFirstInCallback: fsol.set_position    
      });
		});
}

fsol.set_position = function(carousel) {
  carousel.position_box.html(fsol.get_position(carousel));
}
fsol.get_position = function(carousel) {
	var size = carousel.data_size;
	var first = carousel.first;
	var last = carousel.last
	first = first % size;
	last = last % size;
	if (first < 1) {
		first = size-(first*-1);
	}
	if (last < 1) {
		last = size-(last*-1);
	}
  return ""+first+" - "+last+' of '+size+' Photos';
}

fsol.carousel_init = function(carousel,state) {
  if(state=='init') {
    carousel.data_size = carousel.list.get(0).data.length;
    $('#gallery_carousel').after('<div class="carousel-position"></div>');
    carousel.position_box = $($('#gallery_carousel').get(0).nextSibling);
	}
}

fsol.enable_gallery_page_carousel = function() {
  fsol.init_circular_carousel($('#gallery_carousel'));
  $('#gallery-type-carousel').css('visibility','visible');
}  

element_load.on_element_load(fsol.enable_gallery_page_carousel, 'gallery-type-carousel',true);

/* header search - script to add hover to submit button */

$(document).ready(function() {

	// set file vars
	
	var default_src = "/sites/all/themes/fs/images/header/go_default.gif";
	var hover_src = "/sites/all/themes/fs/images/header/go_hover.gif";

	// preload rollover image
	
	$('<img src="' + hover_src + '" alt="" />');
		
	// bind rollover event to submit button
	
	$('.header-submit-button').hover(function() {
		this.src = hover_src;	
	}, function() {	
		this.src = default_src;			
	});

});

// preload header nav images

$(document).ready(function() {

	// set preload paths
	
	var preload_images = new Array();
	
	preload_images.push("/sites/all/themes/fs/images/header/primary_nav_hover_states.gif");
	preload_images.push("/sites/all/themes/fs/images/header/primary_nav_active_states.gif");
	preload_images.push("/sites/all/themes/fs/images/header/secondary_nav_hover_states.gif");
	
	// loop through them and preload
	
	jQuery.each(preload_images, function(i) {	
		$('<img src="' + preload_images[i] + '" alt="" />');
	});
	
});

/* footer jCarousel initialization */
$(document).ready(function() {
  $('.jcarousel_target').each( function() {
    $(this).hide();
    var view_ul = document.createElement('ul');
    view_ul.className = 'view_ul';
    view_ul.data = $(this).find('li');

    for(var i=0; i < 5; i++) {
      var li = document.createElement('li');
      view_ul.appendChild(li);
    }

    this.parentNode.insertBefore(view_ul,this.nextSibling);

    $(view_ul).jcarousel({
      visible: 4,
      wrap: 'circular',
      scroll:1,
      initCallback:   footer_carousel,
      itemVisibleInCallback: {onBeforeAnimation: mycarousel_itemVisibleInCallback},
      itemVisibleOutCallback: {onAfterAnimation: mycarousel_itemVisibleOutCallback},
      itemFirstInCallback: footer_carousel_set_position    
      });

  });
});

function footer_carousel(carousel,state) {
  if(state=='init') {
    carousel.data_size = carousel.list.get(0).data.length;
    carousel.buttonPrev.after('<div class="carousel-position"></div>');
    carousel.position_box = $(carousel.buttonPrev.get(0).nextSibling);
    carousel.buttonPrev.after('<h2 class=>Today on Outdoor Life</h2>');
    footer_carousel_set_position(carousel);
  }
}
function footer_carousel_set_position(carousel) {
  carousel.position_box.html(footer_carousel_get_position(carousel));
}
function footer_carousel_get_position(carousel) {
  return "Displaying "+carousel.first+" - "+carousel.last+' of '+carousel.data_size+' stories';
}
function mycarousel_itemVisibleInCallback(carousel, item, i, state, evt)
{
    // The index() method calculates the index from a
    // given index who is out of the actual item range.
  var data = carousel.list.get(0).data;
  var idx = carousel.index(i, data.length);
  carousel.add(i, data.get(idx-1).innerHTML);
};

function mycarousel_itemVisibleOutCallback(carousel, item, i, state, evt)
{
  carousel.remove(i);
};

/* Remove .panel-separator between sidebar ads */

$(document).ready(function() {
	$('.panel-pane.ad-160-80 + .panel-separator').hide()
});


