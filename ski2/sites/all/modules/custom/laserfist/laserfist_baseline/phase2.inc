<?php
/*
	Mostly taken from parenting_pregnancy
*/


/*
  If tag is tagged with site channel make it highlight
*/
function laserfist_baseline_mega_menus_is_active($menu_node) {
		return;
  $node = menu_get_object();
  if(!$node) {
    return;
  }
  // On the particular channel
  if($menu_node->nid == $node->nid) {
    return TRUE;
  }

  $menu_term = laserfist_get_channel_term($menu_node);
  $menu_term_nid = laserfist_get_channel_node($menu_node);

  $term = laserfist_get_channel_term($node);
  if(!$menu_term) {
    return;
  }
  // Content tagged with site channel
  if($menu_term->tid == $term->tid) {
    return TRUE;
  }
}

/*
	hook_panels_template_template_types
*/
function laserfist_baseline_panels_template_template_types() {
  $options = array(
    'channel' => array(
      'label' => t('Channel/Tag Templates'),
      'parent' => 'node',    ),
  );
  return $options;
}

/*
	hook_panels_template_applicable_types	
*/
function laserfist_baseline_panels_template_applicable_types() {
  $enabled = array();
  $enabled['channel'] = array();
  $enabled['channel'][] = 'channel';
  $enabled['tag'] = array();
  $enabled['tag'][] = 'channel';
  return $enabled;
}


// enable meta tags to work for front page homepage node
function laserfist_baseline_nodewords_tags_alter(&$tags,$parameters) {
  if(drupal_is_front_page()) {
    $node = menu_get_object();
    foreach((array)$node->nodewords as $key => $nodeword) {
      if($val = $nodeword['value']) {
        $tags[$key] = $val;
      }
    }
  }
}

/**
 * Implements hook_bonnier_common_dek_alter().
 *
 * Originally, this function removing the first opening P tag and the last
 * closing P tag from a string (dek or body). However, this caused mal-formed
 * HTML when that string contained more than one set of P tags.
 *
 * Thus, string '<p>Hello</p><p>world</p>' became 'Hello</p><p>world'. I believe
 * a browser would render this gracefully as 'Hello<p>world</p>'. Therefore, to
 * keep some backwards-compatibility, we can safely strip the first set of P
 * tags. This ensures that the browser still renders 'Hello<p>world</p>'.
 *
 * This is not an ideal solution. The ideal solution is to let editors use
 * the WYSIWYG editor to produce the markup they like and run through
 * Drupal's input filter system. However, as we already have content on many
 * sites which expect their first set of P tags to be removed, we will continue
 * that behavior, just using correct HTML.
 */
function laserfist_baseline_bonnier_common_dek_alter(&$node,&$content) {
  if (0 !== strpos($content, '<p>')) {
    return;
  }

  $content = preg_replace('/<p>/i', '', $content, 1);
  $content = preg_replace('/<\/p>/i', '', $content, 1);
}


/*
  Only show published nodes for credits
*/
function laserfist_baseline_laserfist_credit(&$node) {
  $credits = array(bonnier_common_credit($node));
  $items = array();
  foreach($credits as $credit) {
    if($credit->status) {
      $items[] = $credit->title;
    }
  }
  $byline = implode(', ',$items);
  return $byline;
}
