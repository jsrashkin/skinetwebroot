<?php
/**
 * @file
 * Default views hooks.
 */

/**
 * Implementation of hook_views_default_views().
 */
function gallery_view_views_default_views() {
  $views = array();
  $path = dirname(__FILE__) . '/views/';
  
  // Main gallery view
  include($path . 'view.gallery_view.inc');
  $views[$view->name] = $view;

  return $views;
}
