<?php
/**
 * Available variables:
 *  $total - the total number of items.
 *  $current - the current item.
 *  $prev - link to the previous item.
 *  $next - link to the next item.
 *  $slideshow - output of the link that controls starting/stopping.
 */
?>
<?php if ($total > 1): ?>
  <div id="slide-button"></div>
  <script type="text/javascript">
    var slide_link='<?php print $slide_link; ?>';
    var slide_time=<?php print $slide_timer; ?>;
    var slide_start=<?php print $slide_start; ?>;
    var slide_stop=false;
  </script>
  <noscript><!-- Javascript is required to do the slideshow --></noscript>
<?php endif; ?>
