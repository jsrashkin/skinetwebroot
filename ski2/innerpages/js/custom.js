jQuery(function( $ ){

	/*$("header .genesis-nav-menu, .nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu").addClass("responsive-menu").before('<div class="responsive-menu-icon"></div>');

	$(".responsive-menu-icon").click(function(){
		$(this).next("header .genesis-nav-menu, .nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu").slideToggle();
	});*/
	jQuery('.ski-bxslider').bxSlider({
		pagerCustom: '#ski-bx-pager',                  
		controls: false
	});
	jQuery('.gear-bxslider').bxSlider({
		pagerCustom: '.container-gear',                  
		controls: false
    });

    $(window).resize(function(){
		if(window.innerWidth < 767) {
			jQuery('.ski-bxslider').bxSlider({
				pagerCustom: false,                  
				controls: true
			});
			jQuery('.gear-bxslider').bxSlider({
				pagerCustom: false,                  
				controls: true
		    });
		}
	});

	if(window.innerWidth < 767) {
		jQuery('.ski-bxslider').bxSlider({
			pagerCustom: false,                  
			controls: true
		});
		jQuery('.gear-bxslider').bxSlider({
			pagerCustom: false,                  
			controls: true
	    });
	}

});